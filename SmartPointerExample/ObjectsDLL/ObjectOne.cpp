#include "stdafx.h"
#include "ObjectOne.h"
#include <iostream>

using namespace std; 

ObjectOne::ObjectOne()
{
	cout << "ObjectOne Created!!!" << endl;
}


ObjectOne::~ObjectOne()
{
	cout << "ObjectOne DESTROYED!!!!!" << endl;
}

int ObjectOne::Function2(int x, string name)
{
	cout << "\tCalling ObjectOne::Function2 name: --" << name << "--" << x << endl;
	return x+10;
}


int ObjectOne::Function1(int x, string name)
{
	cout << "\tCalling ObjectOne::Function1 name: --" << name << "--" << x << endl;
	return x+5;
}
