// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the OBJECTSDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// OBJECTSDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

// **** MUST HAVE the following macro.
//otherwise you will get class redefinition errors  
#pragma once 

#ifdef OBJECTSDLL_EXPORTS
#define OBJECTSDLL_API __declspec(dllexport)
#else
#define OBJECTSDLL_API __declspec(dllimport)
#endif

#include <string>
using namespace std; 

class IMyInterface
{
public:
	IMyInterface() {}
	virtual ~IMyInterface() {}

	virtual int Function1(int x, string name) = 0;
	virtual int Function2(int x, string name) = 0;
	/*
	virtual string function2(string x) = 0;
	virtual void function3(int x) = 0;
	*/

};

extern "C"
{
	OBJECTSDLL_API IMyInterface* MyCreate(int x);
	OBJECTSDLL_API void MyDelete(IMyInterface* del);
}