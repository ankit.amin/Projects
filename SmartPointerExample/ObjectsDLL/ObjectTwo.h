#pragma once
#include "ObjectsDLL.h"

class ObjectTwo: public IMyInterface
{
public:
	ObjectTwo();
	virtual ~ObjectTwo();

	// Inherited via IMyInterface
	virtual int Function1(int x, string name) override;

	// Inherited via IMyInterface
	virtual int Function2(int x, string name) override;
};

