#pragma once
#include "ObjectsDLL.h"

class ObjectOne : public IMyInterface
{
public:
	ObjectOne();
	virtual ~ObjectOne();

	// Inherited via IMyInterface
	virtual int Function1(int x, string name) override;

	// Inherited via IMyInterface
	virtual int Function2(int x, string name) override;
};

