// ObjectsDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ObjectsDLL.h"
#include "ObjectOne.h"
#include "ObjectTwo.h"
#include <memory>

/*

// This is an example of an exported variable
OBJECTSDLL_API int nObjectsDLL=0;

// This is an example of an exported function.
OBJECTSDLL_API int fnObjectsDLL(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
// see ObjectsDLL.h for the class definition
CObjectsDLL::CObjectsDLL()
{
    return;
}
*/


OBJECTSDLL_API IMyInterface* MyCreate(int x)
{
	IMyInterface *p = nullptr;

	if (x % 2 == 0)
	{
		p = new ObjectOne();
	}
	else
	{
		p = new ObjectTwo();
	}

	return p;
}

OBJECTSDLL_API void MyDelete(IMyInterface * del)
{
	delete del;
	del = nullptr;
}
