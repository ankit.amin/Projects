#include "stdafx.h"
#include "ObjectTwo.h"
#include <iostream>

using namespace std;


ObjectTwo::ObjectTwo()
{
	cout << "ObjectTwo Created!!!" << endl;
}


ObjectTwo::~ObjectTwo()
{
	cout << "ObjectTwo DESTROYED!!!!!" << endl;
}

int ObjectTwo::Function2(int x, string name)
{
	cout << "\tCalling ObjectTwo::Function2 name: ++" << name << "++" << x << endl;
	return x - 10;
}

int ObjectTwo::Function1(int x, string name)
{
	cout << "\tCalling ObjectTwo::Function1 name: ++" << name << "++" << x << endl;
	return x - 5;
}
