#include "stdafx.h"
#include "SmartPointerUsage.h"
#include <iostream>

using namespace std; 

SmartPointerUsage::SmartPointerUsage(IMyInterface* obj, string id)
{
	if (obj == NULL)
	{
		this->m_ptr = NULL;
	}
	else
	{
		this->m_ptr = obj;
		this->myID = id;

		cout << "SmartPointerUsage " << this->myID << " created!!!\n";
		Initialize(obj);
	}
}

SmartPointerUsage::~SmartPointerUsage()
{
	//Demo for memory leak. Please see MEMORY_LEAK_NOTE in Main.cpp
	//delete m_ptr; // only valid if caller is deferring delete to callee.
	cout << "SmartPointerUsage "<< this->myID << " destroyed!!!!!!!!!\n";

}

void SmartPointerUsage::Initialize(IMyInterface * obj)
{
	//Calling function 1
	FunctionOne(obj);

	//Calling function 2
	FunctionTwo(*obj);
}

void SmartPointerUsage::FunctionOne(IMyInterface * obj)
{
	cout << "\tOutput from Function1: " << obj->Function1(1, "SmartPointerUsage::FunctionOne") << endl;
}

void SmartPointerUsage::FunctionTwo(IMyInterface & obj)
{
	cout << "\tOutput from Function2: " << obj.Function2(1, "SmartPointerUsage::FunctionTwo") << endl;
}

void SmartPointerUsage::CircularDependency(shared_ptr<SmartPointerUsage> & p1, shared_ptr<SmartPointerUsage> & p2)
{
	p1->m_p1 = p2; 
	p2->m_p1 = p1; 
}

void SmartPointerUsage::CircularDependency(shared_ptr<SmartPointerUsage> &p1)
{
	m_weakPtr = p1; 
	
	// How to properly access weak_ptr
	//if (!m_weakPtr.expired()) //checks to see if the object it is pointing to exists.
	//{
	//	auto a = m_weakPtr.lock(); //returns shared_ptr of out of m_weakPtr.lock()
	//}
	
	
}