#pragma once
#include "ISmartPointerExample.h"
#include <memory>
#include <string>

using namespace std; 

class SmartPointerUsage : public ISmartPointerExample
{
public:
	SmartPointerUsage(IMyInterface* obj, string id);
	virtual ~SmartPointerUsage();

	// Inherited via ISmartPointerExample
	virtual void Initialize(IMyInterface * obj) override;
	virtual void FunctionOne(IMyInterface * obj) override;
	virtual void FunctionTwo(IMyInterface & obj) override;

	// Demo Circular dep.
	shared_ptr<SmartPointerUsage> m_p1;
	weak_ptr<SmartPointerUsage> m_weakPtr; 
	void CircularDependency(shared_ptr<SmartPointerUsage> & p1, shared_ptr<SmartPointerUsage> & p2);
	void CircularDependency(shared_ptr<SmartPointerUsage> & p1);
private: 
	IMyInterface* m_ptr;
	string myID;

	/* TODO:
	Need to implement Rule of Five.

	SmartPointerUsage(const SmartPointerUsage& o);
	SmartPointerUsage& operator=(const SmartPointerUsage& o);
	SmartPointerUsage(SmartPointerUsage && o);
	SmartPointerUsage& operator=(const SmartPointerUsage && o);
	*/
};

