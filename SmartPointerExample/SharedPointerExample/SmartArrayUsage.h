#pragma once

#include <array>
#include <vector>

const size_t MyIntArraySize = 24;
typedef std::array<int, MyIntArraySize> MyIntArray;

class SmartArrayUsage
{
public:
    SmartArrayUsage(MyIntArray& arr, std::vector<int>& vec);
    ~SmartArrayUsage();

    void PopulateArray();
    void PopulateVector();
    void ResizeVector();

private:
    MyIntArray& m_arr;
    std::vector<int>& m_vec;
};

