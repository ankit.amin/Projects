#pragma once
class CountingObject
{
public:
    CountingObject();
    ~CountingObject();

    static unsigned int numTimesConstructorCalled;
    static unsigned int numTimesDestructorCalled;
};

