#include "stdafx.h"
#include "CountingObject.h"


unsigned int CountingObject::numTimesConstructorCalled = 0;
unsigned int CountingObject::numTimesDestructorCalled = 0;

CountingObject::CountingObject()
{
    ++numTimesConstructorCalled;
}


CountingObject::~CountingObject()
{
    ++numTimesDestructorCalled;
}
