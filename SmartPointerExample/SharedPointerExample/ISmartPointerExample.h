#pragma once
#include "..\ObjectsDLL\ObjectsDLL.h" //for IMyInterface

class ISmartPointerExample
{
public: 
	ISmartPointerExample() {}
	virtual ~ISmartPointerExample() {}

	virtual void Initialize(IMyInterface* obj) = 0;
	virtual void FunctionOne(IMyInterface* obj) = 0;
	virtual void FunctionTwo(IMyInterface& obj) = 0;
};