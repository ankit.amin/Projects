#include "stdafx.h"
#include <memory> //for smart pointers 
#include <iostream> //for cin/cout
#include "SmartPointerUsage.h"
#include "SmartArrayUsage.h"
#include "..\ObjectsDLL\ObjectsDLL.h"
#include <vector>
#include "CountingObject.h"

using namespace std; 


/*
 * SMART POINTERS DO NOT REPLACE THE RAW POINTERS IN VALUE. 
 * THERE WILL ALWAYS BE THE CASE WHERE RAW POINTERS ARE PREFERRED
 * BASED ON ARCHITECTURE OF THE APPLICATION (future topic).
 * 
 * 
 * 3 Types of Smart Pointers in C++ 
 * we can still use pointer semantics (-> and *)
 *	1. unique_ptr (auto_ptr is deprecated and replaced with unique_ptr).
 *	2. shared_ptr
 *	3. weak_ptr 
 *
 * Unique_ptr
 * 	single owner
 * 	deletes when goes out of scope
 * 	most common
 *	can use Move semantics to move the ownership of the pointer. Meaning you can create it and 
 *  hand it off to somebody by using std::move() (C++11).
 *		** useful to understand "RValue Reference" in C++11
 *
 * Shared_ptr
 * 	can have multiple users of one pointer/object
 * 	increment a reference count when given a pointer (ref = count of users)
 * 	Deletes when the refcount goes to 0
 * 	Least common but most often unnecessarily used
 *	pitfalls:
 *		circular dependencies 
 *		ref counter is an atomic operation, keeps it external, 
 *		therefore it is little slower compared unique pointer  
 *
 * Weak_ptr  (solution to circular dependency problem from Shared pointer)
 *  only can access the object and does NOT own the object
 *	is not responsible for how the object is created and destroyed
 *	similar to raw pointer however provides protection against delete operator. (you cannot use delete. it provides safer access to pointer) 
 *  to use it, you have to first create shared_ptr out of it by calling .lock() api
 *
 * -------------------------------------------------------------------------------------------------------
 *
 * How to create Smart Pointers?
 * Smart Pointers must be created explicitly because they do not allow implicit conversions (copy-initialization)
 * 
 * example 1: 
 *	unique_ptr<Object> m_ptr(new Object(param_list)); //direct initialization
 *						OR
 *	unique_ptr<Object> m_ptr = make_unique<Object>(param_list);  //C++14
 *	Above code is Exception safe. 
 *  Meaning if the exception is raised then clean up happens on in between allocations  
 *
 *
 * example 2:
 *	shared_ptr<Object> m_ptr(new Object(param_list)); 		//reference count = 1
 *		Two things are happening here:
 *			1. Object is created (accessible by get() method)
 *			2. m_ptr is created (control block. holds the pointer to the Object, and ref counter)
 *		Not exception safe. Could get memory leak if (1) is created and (2) failed to get created.
 *						OR
 *	shared_ptr<Object> m_ptr = make_shared<Object>(); //ref counter = 1 (C++11)
 *	Above is done in single block allocation. Object, m_ptr and ref counter are allocated with single allocation					
 *
 *	shared_ptr<Object> m_ptr2 = m_ptr; //reference count = 2
 *
 *
 *
 * -------------------------------------------------------------------------------------------------------
 * FUTURE TOPIC: When and Why do we need to implement Rule of Five
 * Summary: 
 *	With C++11, we have Rule of Five:
 *		1. Copy Constructor -->					ex. Object(const Object& o)  
 *		2. An overloaded assignment operator ->	ex. Object operator= (const Object& o)
 *		3. A destructor -->						ex. virtual ~Object()  (memory overhead outweighs advantages of having virutal: prevents 
 *																		undefined behavior with deleteing child object through pointer to 
 *																		parent object) 
 *		4. A move copy constructor -->			ex. Object (Object && o) //&& means rvalue ref. 	
 *		5. A move assignment operator -->		ex. Object& operator= (Object && o)
 *
 *	When and Why do you need to implement it?
 *		When class that have pointer data member that are responsible for createing and deleting pointers
 *		When memory for that pointer is allocated in default or explicit constructors
 *		Why - to promote deep copying 
 *		Why - efficiency of moving resources
 * 
 * -------------------------------------------------------------------------------------------------------
 
 
 
 */

void UniquePtrCaller()
{
	int	x = 0;
	while (x < 10000)
	{
		//Unique Pointers 
		{
			cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
			// Step 1. Get the Object from the DLL. 
			IMyInterface* p = MyCreate(x); //expected behavior: Object created

			// Step 2. Create Smart Pointer
			//Converting raw pointer to Smart Pointer: Unique Pointer
			//Create a UniquePointerExample object and pass the IMyInterface object around
			auto unOne = std::unique_ptr<IMyInterface>(p);
								
			// Step 3. Now we can pass it into our application. 
			//We are passing the pointee of the smart pointer.  (raw pointer)
			//we cannot clean up the pointer in callee class. (because we are passing raw pointer) 
			//this class (caller) is going to delete the pointer when this pointer
			//goes out of scope
			unique_ptr<SmartPointerUsage> upe(new SmartPointerUsage(unOne.get(), std::to_string(x)));
			
			//Demo Memory leak...
			// If we comment out Step 2 and Step 3. and Uncomment out below, then we will have MEMORY LEAK
			//unique_ptr<SmartPointerUsage> upee = make_unique<SmartPointerUsage>(p, std::to_string(x));
			//unique_ptr<ISmartPointerExample> base_ptr = make_unique<SmartPointerUsage>(p, std::to_string(x)); 
						
			
			//MEMORY_LEAK_NOTE
			//Only the unique pointer will be destroyed and not the Object from ObjectsDLL. 
			//Why you ask? 
			//	Because smart pointer is pointing to SmartPointerUsage (object in our application) and 
			//	not the p (object from the ObjectsDLL) 
			//	MEMORY_LEAK_NOTE: TO FIX the memory leak in this case you have to "delete m_ptr" member in ~SmartPointerUsage

		}//Unique pointers scope
		// expected behavior: 
		//		upe is destroyed 
		//		p is destroyed when unOne is destroyed
		x++;
	}//while loop
}//UniquePtrCaller()

void SharedPtrCaller()
{
	int x = 0;
	while (x < 1000)
	{
		//Shared Pointers
		{
			cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
			// Step 1. Get the Object from the DLL. 
			//gets the raw pointer of ObjectDLL's internal object
			//expected behavior: Object is created 
			IMyInterface* p = MyCreate(x);

			// Step 2. Creating Smart Pointer with raw pointer
			auto spOne = std::shared_ptr<IMyInterface>(p);
			
			// Step 3. Pass it into our application
			//spOne->Function1(1, "spOne");
			auto my = make_shared<SmartPointerUsage>(spOne.get(), std::to_string(x) );
			// -------------------------------------------------------------------------------------------
			//gets another raw pointer of ObjectDLL's internal object
			//expected behavior: Object is created when SharedPointer goes out of scope. 
			IMyInterface* pp = MyCreate(x);
			pp->Function1(10, "raw pointer");

			/* two methods of freeing the object
			* Method 1: call following code
			* Method 2: create a smart pointer with raw pointer
			*/

			//Method 1: You will have Memory Leak if we do not call following function 
			//MyDelete(pp); //use what ObjectsDLL provides 
			//
			//OR 
			//
			//delete pp; 
			//pp = nullptr;


			//Method 2: 
			//do not have to call delete on pointer because 
			//when spTwo goes out of scope and ref count == 0 then object will be destroyed
			auto spTwo = std::shared_ptr<IMyInterface>(pp);
			spTwo->Function1(2, "spTwo");
// -------------------------------------------------------------------------------------------
			
			auto ptr = MyCreate(1); //IMyInterface *
			auto sptr1 = shared_ptr<IMyInterface>(ptr); // OK 
			//auto sptr2 = shared_ptr<IMyInterface>(ptr); // BAD!!! //runtime crash
			auto sptr3 = sptr1; //this is properway 
			// Why you ask? 
			// When the scope of sptr1 goes out. It will delete ptr
			// When the scope of sptr2 goes out. It will delete ptr
			// the second delete of ptr will get you Memory Corruption..Undefined behavior. 
			

		}//scope for shared pointer
		// expected behavior: 2 Objects are destroyed

		x++;
	}//while 
}//SharedPtrCaller

void CircularDep()
{
	{
		int x = 0;
		while (x < 1000)
		{
			cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
			IMyInterface *p = MyCreate(1);  //Step 1. get raw pointer
			IMyInterface *pp = MyCreate(2); //Step 1. get raw pointer
			IMyInterface *ppp = MyCreate(3);

			auto a = shared_ptr<IMyInterface>(p); // Step 2. create smart pointer
			auto aa = shared_ptr<IMyInterface>(pp); //Step 2. create smart pointer
			auto aaa = shared_ptr<IMyInterface>(ppp);
				
			auto a1 = make_shared<SmartPointerUsage>(a.get(), std::to_string(x));
			auto a2 = make_shared<SmartPointerUsage>(aa.get(), std::to_string(x));
			auto a3 = make_shared<SmartPointerUsage>(aaa.get(), std::to_string(x));

			a3->CircularDependency(a1, a2);
			//a1->CircularDependency(a2); //Resolution to Circular Dependencies

			x++;
			
		}//while

	}//Circular Dependencies
}//CircularDep()
// Memory leak: Objects will never be destroyed because a1 and a2 are pointing to each other;

struct largeStruct
{
	largeStruct() { cout << "largeStruct constr call!!\n"; }
	double a, b, c, d;
};

void foo(std::unique_ptr<largeStruct> uptr, int x)
{

}

int func_that_throws()
{
	throw new exception();
	return 0;
}
void foo2(int x, std::unique_ptr<largeStruct> uptr)
{
	
}

void LeakExample()
{
	for (int i = 0; i < 1000; ++i)
	{
		try
		{
			int x = func_that_throws();
			foo2(func_that_throws(), unique_ptr<largeStruct>(new largeStruct));
			//Steps before foo2 function is called. 
			//Note: That object creation takes precendence and function stack is created Right to Left 
			// Step 1: Create NEW largeStruct object. (precedence) 
			// Step 2: Create unique_ptr object
			// Step 3: Call func_that_throws(); 
			// Step 4: Call foo2() function

			foo(std::unique_ptr<largeStruct>(new largeStruct), x);
			// Step 1: Create NEW largeStruct object 
			// Step 2: Create unique_ptr object 
			// Step 3: Call foo() function.
			
			foo( std::unique_ptr<largeStruct>(new largeStruct) , func_that_throws());
			// Create NEW largeStruct object 
			// Call func_that_throws()
			// call foo() function
			// This could cause potential leak
		}
		catch (...)
		{
			// Oops, it's possible we leaked if the compiler ran these in this order:
			// new largeStruct, func_that_throws(), std::unique_ptr<largeStruct>
		}
	}

}

void SmartArrayExample()
{
    /*
    
    If you need a static array and you know the size at compile time, use
    std::array.  If you need a dynamic array, use std::vector.

    std::array uses contiguous memory and is safe to use at API boundaries.
    You can access the raw pointer via the std::array::data method.
    It also has STL style iterators and is compatible with STL algorithms.
    For compatibility with range based for loops and the STL use
    begin(), cbegin(), rbegin(), crbegin(), end(), cend(), rend(), crend()

    std::vector also uses contiguous memory and is also safe to use at API
    boundaries.  Every different platform's implementation of vector
    uses contiguous memory.  You can access the raw pointer via
    the std::vector::data method.  And vector is also compatible with the STL.
    
    */

    /*
    
    What's wrong with the following code?

    auto arrayExamplePtr = std::make_unique<SmartArrayUsage>(
        *(std::make_unique<MyIntArray>().get()),
        *(std::make_unique<std::vector<int>>.get())
    );

        arrayExamplePtr->PopulateArray();
        arrayExamplePtr->PopulateVector();

    */
	auto vec = std::make_unique<std::vector<int>>();
    auto arr = std::make_unique<MyIntArray>();
    
    // Don't need to call new; can place constructor arguments in make_unique.
    auto arrayExamplePtr = std::make_unique<SmartArrayUsage>(*(arr.get()), *(vec.get()));

    arrayExamplePtr->PopulateArray();
    arrayExamplePtr->PopulateVector();
    arrayExamplePtr->ResizeVector();
}

void overload1(int* x)
{
    std::cout << "overload1(int* x) called!\n";
}

void overload1(int x)
{
    std::cout << "overload1(int x) called!\n";
}


int main()
{
	//cout << "\t\t\t\t\t\t\t\t---Unique Pointer---\n";
	//UniquePtrCaller();
	
	//cout << "\t\t\t\t\t\t\t\t---Shared Pointer---\n";
	//SharedPtrCaller();

	cout << "\t\t\t\t\t\t\t\t---Circular Dependency---\n";
	CircularDep();
	
    //LeakExample();

    //SmartArrayExample();
	
	/*
    
    C++11 introduces nullptr (which is a nullptr_t).  This gets rid of the ambiguity of NULL.
    If you have an overloaded set of functions where one argument can either be a pointer
    or an integer, using NULL as one of the arguments is ambiguous.  NULL is replaced by 0,
    which the compiler will interpret as an integer, but NULL is generally used for pointers,
    hence the ambiguity.
    
    */
    overload1(NULL);
    overload1(nullptr);

	int x = 0;
	cin >> x;
	return 0;
}

