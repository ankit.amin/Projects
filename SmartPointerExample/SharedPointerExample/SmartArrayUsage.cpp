#include "stdafx.h"
#include "SmartArrayUsage.h"
#include <random>
#include <time.h>
#include <iostream>


SmartArrayUsage::SmartArrayUsage(MyIntArray& arr, std::vector<int>& vec) :
    m_arr(arr),
    m_vec(vec)
{
}


SmartArrayUsage::~SmartArrayUsage()
{
}

void SmartArrayUsage::PopulateArray()
{
    srand(static_cast<unsigned int>(time(0)));

    for (int i = 0; i < MyIntArraySize; ++i)
    {
        m_arr[i] = rand() % 100;
    }

    std::cout << "Array Contents:\n---------------\n";
    std::copy(
        m_arr.begin(), 
        m_arr.end(), 
        std::ostream_iterator<int>(std::cout, "\n"));
    std::cout << "--------------- \n\n";
}

void SmartArrayUsage::PopulateVector()
{
    srand(time(0));

    int maxSize = rand() % 200 + 1;
    for (int i = 0; i < maxSize; ++i)
    {
        m_vec.push_back(rand() % 100);
    }

    std::cout << "Vector Contents:\n----------------\n";
    std::copy(
        m_vec.begin(),
        m_vec.end(),
        std::ostream_iterator<int>(std::cout, "\n"));
    std::cout << "----------------\n\n";
}

void SmartArrayUsage::ResizeVector()
{
    srand(time(0));

    m_vec.clear();

    // Use reserve to allocate space.  Affects capacity() but not size().
    // If we use reserve, then the vector has no idea if you add elements,
    // it simply helps prevent future re-allocations.
    // m_vec.reserve(MyIntArraySize);
    
    // Use resize to allocate space and populate with default value.
    m_vec.resize(MyIntArraySize);

    memcpy(m_vec.data(), m_arr.data(), sizeof(int) * MyIntArraySize);

    // The above memcpy is purely for example.  We could (and should) do the following:
    //std::copy(m_arr.begin(), m_arr.end(), std::back_inserter(m_vec));

    std::cout << "Vector Contents:\n----------------\n";
    std::copy(
        m_vec.begin(),
        m_vec.end(),
        std::ostream_iterator<int>(std::cout, "\n"));
}