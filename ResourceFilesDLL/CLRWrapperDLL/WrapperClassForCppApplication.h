#pragma once

#include <atlbase.h>

#ifdef API_EXPORT_IMPORT
#define APIExpose __declspec(dllexport)
#else
#define APIExpose __declspec(dllimport)
#endif

extern "C"
{
	APIExpose LPTSTR APIFunction1(LPCTSTR s);
	APIExpose LPTSTR APIFunction2(LPCTSTR s);
}


