// This is the main DLL file.
#include "ResourceDLLWrapper.h"

//for LPTSTR
#include <string> 

using namespace System; // Object
using namespace System::Runtime::InteropServices; // Marshal

// For Debugging purpose
//#include <iostream> //For COUT and ENDL

/*
class ResourceDLLWrapperHelper
{
public:
	msclr::auto_gcroot<ResourceClass ^> resourceClass;
};
*/


/*
	Constructor

	Input is Unmanaged C++ string 

	******At this layer we convert the C++ string into C# string.******
*/
ResourceDLLWrapperClass::ResourceDLLWrapperClass(LPCTSTR s)
{
	Console::WriteLine("+ResourceDLLWrapperClass");
	
	//Convert the LPCTSTR (unmanaged) to System::String (managed)
	System::String^ convertedString = gcnew System::String(s);

	//Initialize 
	//m_private = new ResourceDLLWrapperHelper();
	//m_private->resourceClass = gcnew ResourceClass(convertedString->ToString());

	//Initialize the C# object 
	m_private = gcnew ResourceClass(convertedString->ToString());

	Console::WriteLine("-ResourceDLLWrapperClass");
}

/*
	Deconstructor
*/
ResourceDLLWrapperClass::~ResourceDLLWrapperClass()
{
	delete m_private;
}


/*
	This function calls function from C# DLL library.

*/
LPTSTR ResourceDLLWrapperClass::ResourceDLLWrapperFunction1()
{
	Console::WriteLine("+ResourceDLLWrapperFunction1");

	//Call the function from C# library
	//System::String^ managedString = m_private->resourceClass->ResourceClassFunction();
	System::String^ managedString = m_private->ResourceClassFunction();

	//Marshal the data conversion from C# to C++
	LPTSTR returnValue = NULL;

	returnValue = (LPTSTR)Marshal::StringToHGlobalUni(managedString).ToPointer();

	Console::WriteLine("-ResourceDLLWrapperFunction1");

	return returnValue;
}

LPTSTR ResourceDLLWrapperClass::ResourceDLLWrapperFunction2()
{
	Console::WriteLine("+ResourceDLLWrapperFunction2");

	//Call the function from C# library
	//System::String^ managedString = m_private->resourceClass->ResourceClassFunction();
	System::String^ managedString = m_private->GetAllStrings();

	//Marshal the data conversion from C# to C++
	LPTSTR returnValue = NULL;

	returnValue = (LPTSTR)Marshal::StringToHGlobalUni(managedString).ToPointer();

	Console::WriteLine("-ResourceDLLWrapperFunction2");

	return returnValue;
}


