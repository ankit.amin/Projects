// CLRWrapperDLL.h
#pragma once

#include <atlbase.h> //For LPCTSTR
#include <msclr\auto_gcroot.h>
#include <new>

//	*****************************************Need to Add Reference to ResourceFilesDLL for Debugging purpose ONLY *****************************************
//	
//	Otherwise when using #using... you need to do the following:
//
//	Add the path/location of the "ResourceFilesDLL.dll" in  
//									Project Properties->C/C++-->Additional #using Libraries
//
//
#using "ResourceFilesDLL.dll" 
using namespace ResourceFilesDLL;

//class ResourceDLLWrapperHelper;

class ResourceDLLWrapperClass
{
public:
	ResourceDLLWrapperClass(LPCTSTR s);
	virtual ~ResourceDLLWrapperClass();

	LPTSTR ResourceDLLWrapperFunction1();
	LPTSTR ResourceDLLWrapperFunction2();
private:
	
	//ResourceDLLWrapperHelper *m_private;
	msclr::auto_gcroot<ResourceClass ^> m_private;
};