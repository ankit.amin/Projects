
#include "WrapperClassForCppApplication.h" 
#include "ResourceDLLWrapper.h" 

/* For DEBUG purpose only
#include <iostream> //For COUT and ENDL
#include <string>
using namespace std;
*/

using namespace System;


/*
	API function 
	This calls this assembly's internal object (ResourceDLLWrapperClass) function 
*/
APIExpose LPTSTR APIFunction1(LPCTSTR s)
{
	Console::WriteLine("+APIFunction1 in CPP side of the Wrapper DLL");

	//Print out LPCTSTR 
	//std::wstring ss(s);
	//std::wcout << ss << std::endl;

	//Create C++/CLI Object
	ResourceDLLWrapperClass* rdllWrappObject = new ResourceDLLWrapperClass(s);
	
	//Call the Wrapper Function and store the output.
	LPTSTR output = rdllWrappObject->ResourceDLLWrapperFunction1();

	Console::WriteLine("-APIFunction1 ");

	return output;

}

APIExpose LPTSTR APIFunction2(LPCTSTR s)
{
	Console::WriteLine("+APIFunction2 in CPP side of the Wrapper DLL");

	//Print out LPCTSTR 
	//std::wstring ss(s);
	//std::wcout << ss << std::endl;

	//Create C++/CLI Object
	ResourceDLLWrapperClass* rdllWrappObject = new ResourceDLLWrapperClass(s);

	//Call the Wrapper Function and store the output.
	LPTSTR output = rdllWrappObject->ResourceDLLWrapperFunction2();

	Console::WriteLine("-APIFunction2 ");

	return output;

}