// CppApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <atlstr.h> //For HMODULE and LoadLibrary function
#include <atlbase.h>
#include <iostream>
#include <string>

using namespace std;

//
//	Purpose of this Example was to....
//	1. Test the Data flow from C++ to C#  and back (from C# to C++).  Learn how to convert from C++ string (unmanaged) into C# System::String (managed)
//	2. Learn the C++/CLI application. 
//
int _tmain(int argc, _TCHAR* argv[])
{
	
	//Load the CLRWrapper DLL
	HMODULE hDll = LoadLibrary(_T("CLRWrapperDLL.dll"));

	//function pointer to API function from CLRWrapperDLL
	typedef LPTSTR (WINAPIV *pFAPIFunction1) (LPCTSTR s);
	typedef LPTSTR(WINAPIV *pFAPIFunction2) (LPCTSTR s);


	pFAPIFunction1 pFunction1;
	pFAPIFunction2 pFunction2;

	
	//Map function pointer variable to APIFunction1
	pFunction1 = reinterpret_cast<pFAPIFunction1>(GetProcAddress(hDll, "APIFunction1"));
	if (!pFunction1)
	{
		return 0;
	}

	//Get the string from the Resource File
	LPTSTR temp = pFunction1(_T("ResourceFilesDLL.Properties.ResourceFile2"));

	//Cast the Temp LPTSTR to WSTRING
	std::wstring ss(temp);
	std::wcout << "\n\nOUTPUT FROM Resource file \"ResourceFilesDLL.Properties.ResourceFile2\":\n\n" << ss << endl << endl;

	//Map function pointer variable to APIFunction1
	pFunction2 = reinterpret_cast<pFAPIFunction1>(GetProcAddress(hDll, "APIFunction2"));
	if (!pFunction2)
	{
		return 0;
	}

	//Get the string from the Resource File
	LPTSTR tempp = pFunction2(_T("ResourceFilesDLL.Properties.ResourceFile1"));

	//Cast the Temp LPTSTR to WSTRING
	std::wstring sss(tempp);
	std::wcout << "\n\nOUTPUT FROM Resource file \"ResourceFilesDLL.Properties.ResourceFile1\" :\n\n" << sss;




	std::cin.get();

	return 0;
}
