﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Globalization;

using System.Threading;
using System.Collections;
//using System.Collections.Generic;
using System.IO;
//using System.Text;
using System.Runtime.InteropServices;

namespace ResourceFilesDLL
{
    public class ResourceClass
    {

        public ResourceManager rm;

        public ResourceClass(string resxFileName)
        {
            //rm = new ResourceManager("ResourceFilesDLL.ResourceExample", Assembly.GetExecutingAssembly());
            rm = new ResourceManager(resxFileName, Assembly.GetExecutingAssembly());
        }

        public string ResourceClassFunction()
        {
            Console.WriteLine("+ResourceClassFunction");

            String resxName = rm.GetString("ThisIsMyResourceFile", CultureInfo.CurrentCulture);
            CultureInfo ci = new CultureInfo(0x0C0C);
            resxName = resxName 
                + " \nDisplayName:" + ci.DisplayName 
                + " \nEnglishName: " + ci.EnglishName 
                + " \ntypeof Assembly: "+ typeof(ResourceFilesDLL.ResourceClass).ToString();

            Console.WriteLine("-ResourceClassFunction");

            return resxName;
        }  
      
        public string GetAllStrings()
        {
            StringBuilder value = new StringBuilder();

            try
            {
                CultureInfo cultureInfo = new CultureInfo(0x0C0C);
                ResourceSet rs = rm.GetResourceSet(cultureInfo, true, true);

                foreach (DictionaryEntry entry in rs)
                {
                    object resourceKey = entry.Key;
                    object resourceValue = entry.Value;

                    value.Append(resourceKey);
                    value.Append(" = ");
                    value.Append(resourceValue);
                    value.Append("\n");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
            return value.ToString();
        }
       
        public void AddStringsToResourceFiles(string key, string value)
        {
         
        }
    }
}
