﻿using Grpc.Net.Client;
using System;
//using System.Net.Http;
//using System.Threading.Tasks;
using Grpc.Core;
using MyGrpcService;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using static MyGrpcService.Greeter;
using System.Runtime.InteropServices;

namespace GrpcClientConsoleApp
{
    class Program
    {
        class HelloClient
        {
            private string _token = null;
            
            public Channel CreateChannel (string address, int port, bool isSecure)
            {
                Grpc.Core.Channel channel = null; 

                //if (!isSecure)
                //{
                    channel = new Channel(address, port, ChannelCredentials.Insecure);
                //}
                //else
                //{
                //    channel = CreateAuthenticatedChannel(address);
                //}
                return channel;
            }
            
            private GrpcChannel CreateAuthenticatedChannel(string address)
            {
                var credentials = CallCredentials.FromInterceptor((context, metadata) =>
                {
                    if (!string.IsNullOrEmpty(_token))
                    {
                        metadata.Add("Authorization", $"Bearer {_token}");
                    }
                    return Task.CompletedTask;
                });

                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
                });

                return channel;
            }

            private async Task<string> Authenticate(string address)
            {
                Console.WriteLine($"Authenticating as {Environment.UserName}...");
                var httpClient = new HttpClient();
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri($"https://{address}/generateJwtToken?name={HttpUtility.UrlEncode(Environment.UserName)}"),
                    Method = HttpMethod.Get,
                    Version = new Version(2, 0)
                };
                var tokenResponse = await httpClient.SendAsync(request);
                tokenResponse.EnsureSuccessStatusCode();

                var token = await tokenResponse.Content.ReadAsStringAsync();
                Console.WriteLine("Successfully authenticated.");

                return token;
            }

            public Greeter.GreeterClient CreateClient(ChannelBase chan)
            {
                return new Greeter.GreeterClient(chan);
            }

            public async Task<HelloReply> SendMessage(GreeterClient client, HelloRequest request)
            {
                HelloReply reply = null;
                try
                {
                    return reply = await client.SayHelloAsync(request);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error!!!!!! {0}", e.ToString());
                    return null; 
                }
            }

            public async Task MyMethod(string address, int port)
            {

                //Grpc.Core.Channel channel = new Channel("192.168.50.186:50051", ChannelCredentials.Insecure);
                Channel channel = CreateChannel(address, port, false);
                //var client = new Greeter.GreeterClient(channel);
                var client = this.CreateClient(channel);

                //HelloReply reply = await client.SayHelloAsync(new HelloRequest { Name = "Raju!" });
                HelloRequest request = new HelloRequest { Name = "Raju!" };

                HelloReply reply = await this.SendMessage(client, request);

                Console.WriteLine("Greeting: " + reply.Message);
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();

                //return reply;

                /*
                var httpClientHandler = new HttpClientHandler();
                // Return `true` to allow certificates that are untrusted/invalid
                httpClientHandler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;

                var httpClient = new HttpClient(httpClientHandler);

                var channelOptions = new GrpcChannelOptions { HttpClient = httpClient };
                var channel = GrpcChannel.ForAddress("http://192.168.50.32:50051", channelOptions);
                var client = new Greeter.GreeterClient(channel);

                //var channel = CreateAuthenticatedChannel($"https://{Address}");
                //var client = new Greeter.GreeterClient(channel);

                //HelloReply reply = await client.SayHelloAsync(new HelloRequest { Name = "GreeterClient" });

                HelloRequest request = new HelloRequest { Name = "My name is Groot!" };

                var reply = await this.SendMessage(client, request);

                Console.WriteLine("Greeting: " + reply.Message);
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();

                */

            }
        }


        public static async Task Main(string[] args)
        {
            HelloClient myClient = new HelloClient();
            
            await myClient.MyMethod("192.168.50.32", 50051);

            /*
            Grpc.Core.Channel channel = new Channel("192.168.50.186:50051", ChannelCredentials.Insecure);

            var client = new Greeter.GreeterClient(channel);

            HelloReply reply = await client.SayHelloAsync(new HelloRequest { Name = "Raju!" });
            Console.WriteLine("Greeting: " + reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            */

            //String user = "you";

            //var reply = client.SayHello(new HelloRequest { Name = user });
            //Console.WriteLine("Greeting: " + reply.Message);

            //channel.ShutdownAsync().Wait();
            //Console.WriteLine("Press any key to exit...");
            //Console.ReadKey();
        }
    }
}
