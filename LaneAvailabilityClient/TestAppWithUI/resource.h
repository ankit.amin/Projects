//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestAppWithUI.rc
//
#define IDC_MYICON                      2
#define IDD_TESTAPPWITHUI_DIALOG        102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TESTAPPWITHUI               107
#define IDI_SMALL                       108
#define IDC_TESTAPPWITHUI               109
#define IDR_MAINFRAME                   128
#define IDC_TESTFUNC1                   1000
#define IDC_TESTFUNC2                   1001
#define IDC_BUTTON1                     1002
#define IDC_SENDDATA                    1002
#define IDC_SENDDATA2                   1003
#define IDC_DEPOSITED                   1003
#define IDC_HOSTIP                      1004
#define IDC_APPLYIP                     1005
#define IDC_TESTFUNC3                   1006
#define IDC_TESTFUNC4                   1007
#define IDC_TESTFUNC5                   1008
#define IDC_HOSTIP2                     1009
#define IDC_NEWNAME                     1009
#define IDC_LANEID                      1010
#define IDC_TEST                        1011
#define IDC_TESTTIMER                   1011
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
