// This file is a test application with UI that specifically targets
// the different methods in OCDataProcess.cpp, to make sure they function
// correctly as the SSCO would use them. Functionality includes all
// transactions (deposits, withdrawals, loans, pickups, balance updates),
// as well as End of Day Reports, CASHP_ID renaming, and IP Address renaming.

#include "stdafx.h"
#include <atlbase.h>
#include <atlconv.h>
#include "TestAppWithUI.h"
#include "resource.h"
#include "..\LaneAvailabilityClient\json\json.h"
#include "..\LaneAvailabilityClient\ILaneAvailabilityClient.h"
#include <iostream>
#include <cstring>
#include <Windows.h>
using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
BOOL				CreateDefaultJSON();
UINT				iTestTimer = NULL;


//Define function pointers to Get Wrapper Object
typedef ILaneAvailabilityClient* (*pFGetWrapperObject)();

//Define function pointers to Release Wrapper Object
typedef void(*pFReleaseWrapperObject)();

//Instance of GetWrapperObject function pointer
pFGetWrapperObject m_fpWrapperObject;

//Instance of Release Wrapper Object function pointer
pFReleaseWrapperObject m_fpReleaseWrapperObject;

//Get Wrapper Object
ILaneAvailabilityClient* myWrapperObj;

HMODULE handle = NULL;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_TESTAPPWITHUI, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

	handle = LoadLibrary(_T("LaneAvailabilityWrapper.dll"));

	if (handle == NULL)
	{
		cout << "Cannot load LaneAvailabilityWrapper DLL";
		return -1;
	}

	m_fpWrapperObject = reinterpret_cast<pFGetWrapperObject>(GetProcAddress(handle, "GetLaneAvailabilityWrapperObject"));
	if (!m_fpWrapperObject)
	{
		return -1;
	}

	m_fpReleaseWrapperObject = reinterpret_cast<pFReleaseWrapperObject>(GetProcAddress(handle, "ReleaseWrapperObject"));
	if (!m_fpReleaseWrapperObject)
	{
		return -1;
	}

	myWrapperObj = m_fpWrapperObject();
	myWrapperObj->SetNoteHardwareType(true);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TESTAPPWITHUI));

    MSG msg;


    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TESTAPPWITHUI));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_TESTAPPWITHUI);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

 //ShowWindow(hWnd, nCmdShow);
	
 ShowWindow(hWnd, SW_HIDE);
 DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
   UpdateWindow(hWnd);
  // 

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
	TCHAR cdateBuf[128];
   /*
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
			PostQuitMessage(0);
            return (INT_PTR)TRUE;
        }
		// Corresponds to "Change Dispense" button, represents dispensing money
		else if (LOWORD(wParam) == IDC_TESTFUNC1)
		{
			char* myString = "1 : 12, 5 : 6, 25 : 7; 100 : 5, 500 : 2, 1000 : 3, 2000 : 4 ";
			
			USES_CONVERSION;
			myWrapperObj->OCUpdateMessage(A2CT(myString), OC_DISPENSED);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Insert CasH" button, represents inserting money (one bill of each)
		else if (LOWORD(wParam) == IDC_TESTFUNC2)
		{
			myWrapperObj->OCUpdateMessage(_T("USD500"), OC_NOTE_DEPOSITED);
			myWrapperObj->OCUpdateMessage(_T("USD100"), OC_NOTE_DEPOSITED);
			myWrapperObj->OCUpdateMessage(_T("USD25"), OC_COIN_DEPOSITED);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Call Loan" button, represents delivering money
		else if (LOWORD(wParam) == IDC_TESTFUNC3)
		{
			myWrapperObj->OCUpdateMessage(_T("; 5000: 34, 10000: 3"), OC_LOAN);
			myWrapperObj->OCUpdateMessage(_T("1 : 11, 5 : 5, 25 : 7; 100 : 5, 500 : 3, 1000 : 1, 2000 : 5 "), OC_LOAN);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Call Pickup" button, represents returning money
		else if (LOWORD(wParam) == IDC_TESTFUNC4)
		{
			myWrapperObj->OCUpdateMessage(_T("1 : 12, 5 : 6, 25 : 10; 100 : 8, 500 : 2, 1000 : 6, 2000 : 4 "), OC_PICKUP);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "End of Day" button, represents End of Day event
		else if (LOWORD(wParam) == IDC_TESTFUNC5)
		{
			myWrapperObj->OCUpdateMessage(NULL, OC_END_OF_DAY);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Apply IP" button, changes the recipient of the JSON string
		// to the IP Address specified.
		else if (LOWORD(wParam) == IDC_APPLYIP)
		{
			UINT nCountOfCharacters = GetDlgItemText(hDlg, IDC_HOSTIP, cdateBuf, 16);
			myWrapperObj->OCUpdateMessage((LPCTSTR)cdateBuf, (OC_UPDATE_MSG) 100);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "New LANE_ID" button, changes the CASHP_ID of the current
		// mock SSCO to the string specified (use "Lane1", "Lane2", etc. for testing)
		else if (LOWORD(wParam) == IDC_LANEID)
		{
			UINT nCountOfCharacters = GetDlgItemText(hDlg, IDC_NEWNAME, cdateBuf, 16);
			myWrapperObj->OCUpdateMessage((LPCTSTR)cdateBuf, (OC_UPDATE_MSG) 101);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Send UDP Data" button, represents the end of the transaction,
		// at which point all withdrawal, deposit, loan and pickup data for this transaction
		// is sent in a message to the IP Address specified.
		//
		// NOTE: Uncommenting the first line allows for the closing balances to update themselves,
		// since this usually is the SSCO's job, but because we are testing, we have to calculate
		// the values ourselves. Thus, uncommenting the first line allows closing balances to
		// correctly reflect what the SSCO would print out after transactions have occurred.
		else if (LOWORD(wParam) == IDC_SENDDATA)
		{
			// myWrapperObj->OCUpdateMessage(NULL, (OC_UPDATE_MSG) 102);
			myWrapperObj->OCUpdateMessage(NULL, OC_END_OF_TRANSACTION);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Update Balance" button, represents the SSCO updating counts of 
		// dispensable and nondispensable cash at the end of a transaction.
		else if (LOWORD(wParam) == IDC_DEPOSITED)
		{
			/*
				CurrencyAccepted � with note as Bool
				�USD100�, �USD200�, �USD500�, �USD1000�, �USD2000�, �USD5000�, �USD10000�

				CoinAccepted � with note as Bool
				�USD1�, �USD5�, �USD10�, �USD25�, �USD50", "USD100�
			*/
    /*
			myWrapperObj->OCUpdateMessage(_T("1:1000,5:1000,10:1000,25:1000;100:2990,500:1524,1000:2828"), OC_DISPENSABLE);
			myWrapperObj->OCUpdateMessage(_T("1:6,5:3,10:16,25:37,100:1;100:21,200:3,500:60,1000:3,2000:25,5000:3,10000:8"), OC_NONDISPENSABLE);
			return (INT_PTR)TRUE;
		}
		// Corresponds to "Start Test" button, represents the starting and/or stopping of a timer
		else if (LOWORD(wParam) == IDC_TESTTIMER)
		{
			if (iTestTimer)
			{
				KillTimer(hDlg, 0x01);
				iTestTimer = 0;
				SetDlgItemText(hDlg, IDC_TESTTIMER, _T("Start Timer"));
			}
			else
			{
				iTestTimer = SetTimer(hDlg, 0x01, 10000, NULL);
				SetDlgItemText(hDlg, IDC_TESTTIMER, _T("Stop Timer"));
			}

			return (INT_PTR)TRUE;
		}
	case WM_TIMER:
	{
		myWrapperObj->OCUpdateMessage(NULL, OC_END_OF_TRANSACTION);
		return (INT_PTR)FALSE;
	}
        break;
    }
    */
    return (INT_PTR)FALSE;
}