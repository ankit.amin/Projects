// LaneAvailabilityCLRWrapper.h

#pragma once
#include "..\LaneAvailabilityWrapper\LaneAvailabilityWrapper.h"

using namespace System;

namespace LaneAvailabilityCLRWrapper {

    public ref class LaneAvailabilityCLR
    {
    public:
        LaneAvailabilityCLR();
        virtual ~LaneAvailabilityCLR();
        !LaneAvailabilityCLR();

        void Destroy(); 

        void CLRLAUpdateStaticMessage(String ^ msgData);
        void CLRLAUpdateInterventionMessage(String ^ msgData);
        void CLRLAUpdateWeightMessage(String ^ msgData);
        void CLRLAUpdateFinishTrx();

    private:
        CLaneAvailabilityWrapper* m_LAWrapper;
    };
}
