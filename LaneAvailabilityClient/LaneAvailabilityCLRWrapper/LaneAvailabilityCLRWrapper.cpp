// This is the main DLL file.
#include "stdafx.h"
#include "LaneAvailabilityCLRWrapper.h"
#include <iostream>
#include <msclr\marshal.h>

using namespace System;
using namespace msclr::interop;

LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::LaneAvailabilityCLR()
{
    m_LAWrapper = new CLaneAvailabilityWrapper();
}

LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::~LaneAvailabilityCLR()
{
    Destroy();
}

LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::!LaneAvailabilityCLR()
{
    Destroy();
}

void LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::Destroy()
{
    if (m_LAWrapper) 
    {
        delete m_LAWrapper;
        m_LAWrapper = nullptr;
    }
}

void LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::CLRLAUpdateStaticMessage(String ^ msgData)
{
    marshal_context context;
    LPCTSTR cstr = context.marshal_as<const TCHAR*>(msgData);
    Console::WriteLine("msgData: {0}", msgData);
    std::wcout << "cstr: " << cstr << std::endl;
    m_LAWrapper->LAUpdateStaticMessage(cstr);

}

void LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::CLRLAUpdateInterventionMessage(String ^ msgData)
{
    marshal_context context;
    LPCTSTR cstr = context.marshal_as<const TCHAR*>(msgData);
    m_LAWrapper->LAUpdateInterventionMessage(cstr);
}

void LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::CLRLAUpdateWeightMessage(String ^ msgData)
{
    marshal_context context;
    LPCTSTR cstr = context.marshal_as<const TCHAR*>(msgData);
    m_LAWrapper->LAUpdateWeightMessage(cstr);
}

void LaneAvailabilityCLRWrapper::LaneAvailabilityCLR::CLRLAUpdateFinishTrx()
{
    m_LAWrapper->LAUpdateFinishTrx();
}
