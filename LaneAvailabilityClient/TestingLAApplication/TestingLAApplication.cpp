// TestingLAApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestingLAApplication.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
    hDll = LoadLibrary(_T("LaneAvailabilityWrapper.dll"));

    if (hDll != NULL)
    {
        m_fpWrapperObject = reinterpret_cast<pfGetWrapperObject>(GetProcAddress(hDll, "GetLaneAvailabilityWrapperObject"));
        if (!m_fpWrapperObject)
        {
            return 0;
        }

        m_fpReleaseWrapperObject = reinterpret_cast<pfReleaseWrapperObject> (GetProcAddress(hDll, "ReleaseWrapperObject"));
        if (!m_fpReleaseWrapperObject)
        {
            return 0;
        }
    }


    m_myWrapperObject = m_fpWrapperObject();

    LPCTSTR staticData = _T("{ \
                            \"LaneSummary\": \
                            { \
                                \"version\": \"0\", \
                                 \"Terminals\" : \
                                    [{    \
                                    \"store\": \"6\", \
                                    \"lane\" : \"lane4\",  \
                                    \"datetimeStart\" : \"\",  \
                                    \"datetimeEnd\" : \"\",  \
                                    \"terminalType\" : \"\",  \
                                     \"transactions\" : \"\" \
                        }\
                        ]}}");

    m_myWrapperObject->LAUpdateStaticMessage(staticData);
    
    
    int i = 0;

    int transactionNum = 0; 
    cout << "Enter Number of Items to simulate: ";
    cin >> transactionNum;

    while (i < transactionNum)
    {


        /*LPCTSTR weightData = _T("{ \
                              \"weightMismatches\" : \
                              [{   \
                                   \"laneNumber\": \"5\", \
                                   \"dateTime\" : \"\",  \
                                   \"item\" : \"\",  \
                                   \"description\" : \"\",  \
                                   \"expectedWeight\" : \"\",  \
                                   \"observedWeight\" : \"\"  \
                                   }\
                               ]} ");
        m_myWrapperObject->LAUpdateWeightMessage(weightData);

       weightData = _T("{ \
                              \"weightMismatches\" : \
                              [{   \
                                   \"laneNumber\": \"6\", \
                                   \"dateTime\" : \"\",  \
                                   \"item\" : \"\",  \
                                   \"description\" : \"\",  \
                                   \"expectedWeight\" : \"\",  \
                                   \"observedWeight\" : \"\"  \
                                   }\
                               ]} ");
        m_myWrapperObject->LAUpdateWeightMessage(weightData);*/

        std::string data = "{ \
                                \"interventions\" :  \
                                [{  \
                                    \"EAN\":\""+ std::to_string(i) +"\", \
                                    \"assisted\" : \"No\", \
                                    \"interventionEndTime\" : \"2019-10-14T09:35:17.90\", \
                                    \"interventionStartTime\" : \"2019-10-14T09:35:14.39\", \
                                    \"interventionUniqueID\" : \"82-2019-10-14T09:35:14.39\", \
                                    \"lane\" : \"82\", \
                                    \"security\" : \"Yes\", \
                                    \"transactionEndTime\" : \"2019-10-14T09:36:20.239\", \
                                    \"transactionID\" : \"NSPQO5CFK3\", \
                                    \"transactionStartTime\" : \"2019-10-14T09:35:02.273\", \
                                    \"type\" : \"UNEXPECTEDINCREASE\"\
                                ]} "; 
        
        //LPCTSTR interventionData = _T("{ \
        //                        \"interventions\" :  \
        //                        [{  \
        //                            \"EAN\":\""+i+"\", \
        //                            \"FirstName\" : \"\", \
        //                            \"InterventionType\" : \"\", \
        //                            \"Lastname\" : \"\", \
        //                            \"articleID\" : \"\", \
        //                            \"assisted\" : \"No\", \
        //                            \"attendant\" : \"\", \
        //                            \"description\" : \"\", \
        //                            \"expectedWeight\" : \"\", \
        //                            \"interventionEndTime\" : \"2019-10-14T09:35:17.90\", \
        //                            \"interventionStartTime\" : \"2019-10-14T09:35:14.39\", \
        //                            \"interventionUniqueID\" : \"82-2019-10-14T09:35:14.39\", \
        //                            \"lane\" : \"82\", \
        //                            \"observedweight\" : \"\", \
        //                            \"security\" : \"Yes\", \
        //                            \"transactionEndTime\" : \"2019-10-14T09:36:20.239\", \
        //                            \"transactionID\" : \"NSPQO5CFK3\", \
        //                            \"transactionStartTime\" : \"2019-10-14T09:35:02.273\", \
        //                            \"type\" : \"UNEXPECTEDINCREASE\"\
        //                        ]} ");

        std::wstring wdata(data.begin(), data.end());
        LPCTSTR interventionData = wdata.c_str();

        m_myWrapperObject->LAUpdateInterventionMessage(interventionData);



        std::cout << "Transaction: " << i << std::endl;
        i++;

    }
    m_myWrapperObject->LAUpdateFinishTrx();
    //m_myWrapperObject->LAResetFiles();
    m_fpReleaseWrapperObject();
    
    FreeLibrary(hDll);

    std::cin.get();
    return 0;
}

