#pragma once
#include "..\LaneAvailabilityClient\ILaneAvailabilityClient.h"

//Handle to DLL 
HMODULE hDll; 

//Function pointers 
typedef ILaneAvailabilityClient* (*pfGetWrapperObject)(void);
typedef void (*pfReleaseWrapperObject)(void);

//variables of function pointers 
pfGetWrapperObject m_fpWrapperObject; 
pfReleaseWrapperObject m_fpReleaseWrapperObject;

//member variable of type ILaneAvailClient
ILaneAvailabilityClient* m_myWrapperObject;
