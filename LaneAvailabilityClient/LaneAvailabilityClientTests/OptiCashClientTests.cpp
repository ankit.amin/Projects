// LaneAvailabilityClientTests.cpp : Defines the entry point for the console application.
//

#include "gtest/gtest.h"
#include "../LaneAvailabilityClient/OCDataProcess.h"
#include "../LaneAvailabilityClient/OCDataProcess.cpp"
#include "../LaneAvailabilityClient/json/json.h"

// number of denominations (pennies, dimes, etc.) in the US currency
// that SSCO supports and will accept/dispense/keep track of
#define US_DENOMCOUNT 13

// list of US currency denominations coupled with
// monetary value of each (ex. USD1 = penny = 0.01)
US_Denom US_DenomIDs[] =
{
	{ "USD1c", 0.01, 1 },
	{ "USD5c", 0.05, 5 },
	{ "USD10c", 0.1, 10 },
	{ "USD25c", 0.25, 25 },
	{ "USD50c", 0.5, 50 },
	{ "USD100c", 1, 100 },
	{ "USD100", 1, 100 },
	{ "USD200", 2, 200 },
	{ "USD500", 5, 500 },
	{ "USD1000", 10, 1000 },
	{ "USD2000", 20, 2000 },
	{ "USD5000", 50, 5000 },
	{ "USD10000", 100, 10000 }
};

TEST(Construction, CreateDefaultJSON)
{
	OCDataProcess* m_DP = new OCDataProcess();
	m_DP->UpdateCashBalance("1:1000,5:1000,10:1000,25:1000;100:2990,500:1524,1000:2828", true);
	m_DP->UpdateCashBalance("1:6,5:3,10:16,25:37,50:1,100:1;100:21,200:3,500:60,1000:3,2000:25,5000:3,10000:8", false);

	for (int i = 0; i < US_DENOMCOUNT; i++)
	{
		EXPECT_STREQ("Lane ID", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CASHP_ID").asCString());
		EXPECT_EQ(0, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NNORM_DEL").asInt());
		EXPECT_DOUBLE_EQ(US_DenomIDs[i].value, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "VALUE").asDouble());
	}
}

TEST(Construction, CreateDenomID)
{
	OCDataProcess* m_DP = new OCDataProcess();
	EXPECT_STREQ("USD100", m_DP->CreateDenomID("100", "USD"));
	EXPECT_STREQ("YEN50", m_DP->CreateDenomID("50", "YEN"));
	EXPECT_STREQ("MXN2000c", m_DP->CreateDenomID("2000c", "MXN"));
}

TEST(Construction, SetNewLaneID)
{
	OCDataProcess* m_DP = new OCDataProcess();
	m_DP->UpdateCashBalance("1:6,5:3,10:16,25:37,50:1,100:1;100:21,200:3,500:60,1000:3,2000:25,5000:3,10000:8", false);
	m_DP->SetNewLaneID("Lane 1");
	for (int i = 0; i < US_DENOMCOUNT; i++)
	{
		EXPECT_STREQ("Lane 1", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CASHP_ID").asCString());
	}
}

TEST(ProcessDeposit, DepositNote)
{
	OCDataProcess* m_DP = new OCDataProcess();
	
	m_DP->ProcessDeposit("USD100", true);
	EXPECT_EQ(1, m_DP->GetJsonValue("USD100", "NDEPOSITS").asInt());
	EXPECT_STREQ("1.00", m_DP->GetJsonValue("USD100", "DEPOSITS").asCString());
}

TEST(ProcessDeposit, DepositCoin)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessDeposit("USD100", false);
	EXPECT_EQ(1, m_DP->GetJsonValue("USD100c", "NDEPOSITS").asInt());
	EXPECT_STREQ("1.00", m_DP->GetJsonValue("USD100c", "DEPOSITS").asCString());
}

TEST(ProcessDispense, CoinsOnly)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessDispense("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35");
	for (int i = 0; i < 6; i++)
	{
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NWTHDRWLS").asInt());

		char cVal[15];
		sprintf_s(cVal, "%.2f", (10 + (5 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "WTHDRWLS").asCString());
	}
}

TEST(ProcessDispense, NotesOnly)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessDispense("; 100: 10, 200: 15, 500: 20, 1000: 25, 2000: 30, 5000: 35, 10000: 40");
	for (int i = 0; i < 7; i++)
	{
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i + 6].name, "NWTHDRWLS").asInt());
		char cVal[15];
		sprintf_s(cVal, "%.2f", (10 + (5 * i)) * US_DenomIDs[i + 6].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i + 6].name, "WTHDRWLS").asCString());
	}
}

TEST(ProcessLoan, CoinsOnly)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessLoan("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35");
	for (int i = 0; i < 6; i++)
	{
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NNORM_DEL").asInt());

		char cVal[15];
		sprintf_s(cVal, "%.2f", (10 + (5 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NORM_DEL").asCString());
	}
}

TEST(ProcessLoan, NotesOnly)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessLoan("; 100: 10, 200: 15, 500: 20, 1000: 25, 2000: 30, 5000: 35, 10000: 40");
	for (int i = 0; i < 7; i++)
	{
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i + 6].name, "NNORM_DEL").asInt());
		char cVal[15];
		sprintf_s(cVal, "%.2f", (10 + (5 * i)) * US_DenomIDs[i + 6].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i + 6].name, "NORM_DEL").asCString());
	}
}

TEST(ProcessPickup, CoinsOnly)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessPickup("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35");
	for (int i = 0; i < 6; i++)
	{
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NNORM_RTR").asInt());

		char cVal[15];
		sprintf_s(cVal, "%.2f", (10 + (5 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NORM_RTR").asCString());
	}
}

TEST(ProcessPickup, NotesOnly)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->ProcessPickup("; 100: 10, 200: 15, 500: 20, 1000: 25, 2000: 30, 5000: 35, 10000: 40");
	for (int i = 0; i < 7; i++)
	{
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i + 6].name, "NNORM_RTR").asInt());
		char cVal[15];
		sprintf_s(cVal, "%.2f", (10 + (5 * i)) * US_DenomIDs[i + 6].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i + 6].name, "NORM_RTR").asCString());
	}
}

TEST(UpdateCashBalance, CoinsAndBills)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->UpdateCashBalance("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 35, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65", true);
	m_DP->UpdateCashBalance("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35; 100: 40, 200: 45, 500: 50, 1000: 55, 2000: 60, 5000: 65, 10000: 70", false);
	for (int i = 0; i < US_DENOMCOUNT; i++)
	{
		EXPECT_EQ(5 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "DISPENSABLE").asInt());
		EXPECT_EQ(10 + (5 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NONDISPENSABLE").asInt());
		EXPECT_EQ(15 + (10 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NCLOS_BAL").asInt());
		char cVal[15];
		sprintf_s(cVal, "%.2f", (5 + (5 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "BAL_DISP").asCString());
		char cVal2[15];
		sprintf_s(cVal2, "%.2f", (10 + (5 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal2, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "BAL_UNAV").asCString());
		char cVal3[15];
		sprintf_s(cVal3, "%.2f", (15 + (10 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal3, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CLOS_BAL").asCString());
	}
}

TEST(EndOfDay, ReportEndOfDay)
{
	OCDataProcess* m_DP = new OCDataProcess();

	m_DP->UpdateCashBalance("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 35, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65", true);
	m_DP->UpdateCashBalance("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35; 100: 40, 200: 45, 500: 50, 1000: 55, 2000: 60, 5000: 65, 10000: 70", false);
	m_DP->ReportEndOfDay();
	for (int i = 0; i < US_DENOMCOUNT; i++)
	{
		EXPECT_STREQ("F", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CYCLE_TYPE").asCString());
		time_t now = time(0);
		struct tm tstruct;
		char currentTime[80];
		tstruct = *localtime(&now);
		strftime(currentTime, sizeof(currentTime), "%d%m%Y:%R", &tstruct);
		EXPECT_STREQ(currentTime, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "DATE").asCString());
	}
}

TEST(EndOfDay, ResetCycle)
{
	OCDataProcess* m_DP = new OCDataProcess();
	m_DP->ProcessPickup("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35");
	m_DP->ProcessLoan("; 100: 10, 200: 15, 500: 20, 1000: 25, 2000: 30, 5000: 35, 10000: 40");
	m_DP->UpdateCashBalance("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 35, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65", true);
	m_DP->UpdateCashBalance("1: 10, 5: 15, 10: 20, 25: 25, 50: 30, 100: 35; 100: 40, 200: 45, 500: 50, 1000: 55, 2000: 60, 5000: 65, 10000: 70", false);
	m_DP->ReportEndOfDay();
	m_DP->ResetCycle();
	for (int i = 0; i < US_DENOMCOUNT; i++)
	{
		EXPECT_STREQ("P", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CYCLE_TYPE").asCString());
		EXPECT_EQ(15 + (10 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NOPEN_BAL").asInt());
		EXPECT_EQ(15 + (10 * i), m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NCLOS_BAL").asInt());
		char cVal3[15];
		sprintf_s(cVal3, "%.2f", (15 + (10 * i)) * US_DenomIDs[i].value);
		EXPECT_STREQ(cVal3, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CLOS_BAL").asCString());
		EXPECT_STREQ(cVal3, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "OPEN_BAL").asCString());
		EXPECT_EQ(0, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NNORM_DEL").asInt());
		EXPECT_EQ(0, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NNORM_RTR").asInt());
		EXPECT_STREQ("0.00", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NORM_DEL").asCString());
		EXPECT_STREQ("0.00", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NORM_RTR").asCString());
		time_t now = time(0);
		struct tm tstruct;
		char currentTime[80];
		tstruct = *localtime(&now);
		strftime(currentTime, sizeof(currentTime), "%d%m%Y:%R", &tstruct);
		EXPECT_STREQ(currentTime, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CYCLE_START").asCString());
	}

}

TEST(Logistics, CalculateClosingBal)
{
	OCDataProcess* m_DP = new OCDataProcess();
	m_DP->ProcessDispense("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 35, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65");
	m_DP->ProcessLoan("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 35, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65");
	m_DP->ProcessPickup("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 35, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65");
	m_DP->ProcessLoan("1: 5, 5: 10, 10: 15, 25: 20, 50: 25, 100: 30; 100: 34, 200: 40, 500: 45, 1000: 50, 2000: 55, 5000: 60, 10000: 65");
	m_DP->ProcessDeposit("USD100", true);
	for (int i = 0; i < US_DENOMCOUNT; i++)
	{
		EXPECT_EQ(0, m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "NCLOS_BAL").asInt());
		EXPECT_STREQ("0.00", m_DP->GetJsonValue((char*)US_DenomIDs[i].name, "CLOS_BAL").asCString());
	}
}

TEST(Formatting, TrimString)
{
	char* s_Test = "  Trim String ";
	OCDataProcess* m_DP = new OCDataProcess();
	EXPECT_STREQ("TrimString", m_DP->TrimString(s_Test));
}
