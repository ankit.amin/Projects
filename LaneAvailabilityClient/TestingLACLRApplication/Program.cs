﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LaneAvailabilityCLRWrapper;

namespace TestingLACLRApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            CallCPPObject c = new CallCPPObject();
            c.Initialize();
        }
    }

    public class CallCPPObject
    {
        LaneAvailabilityCLR x = null;
        public void Initialize()
        {
            x = new LaneAvailabilityCLR();
            StartTesting();
        }

        public void StartTesting()
        {
            string staticData = "{\"data\": \"value\" }";
            x.CLRLAUpdateStaticMessage(staticData);

            int i = 0;
            int transactionNum = 0;
            Console.WriteLine("Enter Number of Items to simulate: ");
            transactionNum = Int32.Parse(Console.ReadLine());

            while (i<transactionNum)
            {
                string data = "Intervention data" + i.ToString();
                x.CLRLAUpdateInterventionMessage(data);
                Console.WriteLine("Transaction: {0}", i);
                i++;
            }

            x.CLRLAUpdateFinishTrx();
        }

    }
}
