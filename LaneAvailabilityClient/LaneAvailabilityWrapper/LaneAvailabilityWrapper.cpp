// LaneAvailabilityWrapper.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "LaneAvailabilityWrapper.h"
#include "LaneAvailabilityJNIWrapper_LAJNIWrapperClass.h"
#include <tchar.h> //for _T("")
#include <iostream>
using namespace std; 


/*************************************************************/
CLaneAvailabilityWrapper* g_dllWrapperObject = nullptr;

extern "C" LAWRAPPER_API ILaneAvailabilityClient * GetLaneAvailabilityWrapperObject()
{
    if (g_dllWrapperObject == nullptr)
    {
        g_dllWrapperObject = new CLaneAvailabilityWrapper();
    }
    return g_dllWrapperObject;
}

extern "C" LAWRAPPER_API void ReleaseWrapperObject()
{
    delete g_dllWrapperObject;
    g_dllWrapperObject = nullptr;

}

/************************************************************/

// This is the constructor of a class that has been exported.
// see LaneAvailabilityWrapper.h for the class definition
CLaneAvailabilityWrapper::CLaneAvailabilityWrapper()
    : m_dllObject(nullptr),
      m_hDll(nullptr)
{
    m_hDll = LoadLibrary(_T("LaneAvailabilityClient.dll"));
    //m_hDll = LoadLibrary(L"LaneAvailabilityClient.dll");
    if (!m_hDll)
    {
        return; 
    }

    m_pfDllFunction = reinterpret_cast<GetDLLObject>(GetProcAddress(m_hDll, "fnGetLAClient"));
    if (!m_pfDllFunction)
    {
        return;
    }
    
    m_pfReleaseDLLObject = reinterpret_cast<ReleaseDLLObject>(GetProcAddress(m_hDll, "fnReleaseLAClient"));
    m_dllObject = m_pfDllFunction();
    if (!m_dllObject)
    {
        return;
    }
}

CLaneAvailabilityWrapper::~CLaneAvailabilityWrapper()
{
    if (m_dllObject)
    {
        m_pfReleaseDLLObject();
    }

    if (m_hDll)
    {
        FreeLibrary(m_hDll);
    }

}

ILaneAvailabilityClient* CLaneAvailabilityWrapper::GetLaneAvailabilityObject()
{
    return m_dllObject;
}

void CLaneAvailabilityWrapper::LAUpdateStaticMessage(LPCTSTR msgData)
{
    m_dllObject->LAUpdateStaticMessage(msgData);
}
void CLaneAvailabilityWrapper::LAUpdateInterventionMessage(LPCTSTR msgData)
{
    m_dllObject->LAUpdateInterventionMessage(msgData);
}
void CLaneAvailabilityWrapper::LAUpdateWeightMessage(LPCTSTR msgData)
{
    m_dllObject->LAUpdateWeightMessage(msgData);
}
void CLaneAvailabilityWrapper::LAUpdateFinishTrx()
{
    m_dllObject->LAUpdateFinishTrx();
}

bool CLaneAvailabilityWrapper::LAResetFiles()
{
    //std::cout << "Reseting files"; 
    return m_dllObject->LAResetFiles();
}

/************************************************************/