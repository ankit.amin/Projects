// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the LaneAvailabilityWRAPPER_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// LaneAvailabilityWRAPPER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#pragma once

#include "..\LaneAvailabilityClient\ILaneAvailabilityClient.h"

#ifdef LAWRAPPER_EXPORTS
#define LAWRAPPER_API __declspec(dllexport)
#else
#define LAWRAPPER_API __declspec(dllimport)
#endif



class LAWRAPPER_API CLaneAvailabilityWrapper : public ILaneAvailabilityClient
{
private: 
	//File handle for LaneAvailabilityClient DLL 
	HMODULE m_hDll; 

	//Function pointer to get the LaneAvailabilityClient object
	typedef ILaneAvailabilityClient* (*GetDLLObject)();
	
	typedef void (*ReleaseDLLObject)();

	GetDLLObject m_pfDllFunction;
	ReleaseDLLObject m_pfReleaseDLLObject;
    
	//pointer to LaneAvailabilityClient object
	ILaneAvailabilityClient* m_dllObject; 
public: 

	CLaneAvailabilityWrapper();
	virtual ~CLaneAvailabilityWrapper();

	//Function to get Instance of LaneAvailabilityClient object
	ILaneAvailabilityClient* GetLaneAvailabilityObject();

	virtual void LAUpdateStaticMessage(LPCTSTR msgData) override;
    virtual void LAUpdateInterventionMessage(LPCTSTR msgData) override;
    virtual void LAUpdateWeightMessage(LPCTSTR msgData) override;
    virtual void LAUpdateFinishTrx() override;



    // Inherited via ILaneAvailabilityClient
    virtual bool LAResetFiles() override;

};