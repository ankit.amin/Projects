// TestApplication.cpp : Defines the entry point for the console application.
//
#pragma once 

#include "stdafx.h"
#include "..\OptiCashClient\IOptiCashClient.h"
#include <iostream>
#include <cstring>
#include <Windows.h>
using namespace std; 

int main()
{

	//Define function pointers to Get Wrapper Object
	typedef IOptiCashClient* (*pFGetWrapperObject)();

	//Define function pointers to Release Wrapper Object
	typedef void(*pFReleaseWrapperObject)();

	//Instance of GetWrapperObject function pointer
	pFGetWrapperObject m_fpWrapperObject;

	//Instance of Release Wrapper Object function pointer
	pFReleaseWrapperObject m_fpReleaseWrapperObject;

	//Get Wrapper Object
	IOptiCashClient* myWrapperObj;

	HMODULE handle = NULL;
	handle = LoadLibrary(_T("OptiCashWrapper.dll"));

	if (handle == NULL)
	{
		cout << "Cannot load OptiCashWrapper DLL";
		return -1;
	}

	//Get the Wrapper Object. 
	m_fpWrapperObject = reinterpret_cast<pFGetWrapperObject>(GetProcAddress(handle, "GetOptiCashWrapperObject"));
	if (!m_fpWrapperObject)
	{
		return -1;
	}

	m_fpReleaseWrapperObject = reinterpret_cast<pFReleaseWrapperObject>(GetProcAddress(handle, "ReleaseWrapperObject"));
	if (!m_fpReleaseWrapperObject)
	{
		return -1;
	}

	myWrapperObj = m_fpWrapperObject();

	//myWrapperObj->SetCurrentBalanceAPI("example", "p2");

	////Another way to call OptiCash 
	//myWrapperObj->DoTheWork(FINISH_CM_SESSION, "x", "y");

	m_fpReleaseWrapperObject();
	FreeLibrary(handle);
	return 0;
}