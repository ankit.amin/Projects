// Copyright (c) NCR Corp. 2008

#include "stdafx.h"
#include "io.h"   //SSCOP-3544
#include "XMode.h"
#include "ApplicationMonitor.h"
#include "PSXWrapperCAPI.h"
#include "XMLog.h"
#include "XMState.h"
#include "DMWrapper.h"

#include "XMCashDevice.h"
#include "XMCashDeviceFactory.h"
#include "XMDepositTimer.h"

#include "TBWrapperBase.h"

#include "XMRegWrapper.h"           // TAR 378792.

#include "XMMediaStatusBase.h"      // TAR 408521 - For default init
#include "XMCashManagementBase.h"   // TAR 408521   if there is no override DLL.
#include "XMCashierPasswordBase.h"  // TAR 408521

//+RFC 2774 - Ability to lock CM
#include "XMLockPasswordBase.h"        
#include "XMLockScreenBase.h"        
#include "XMLockTimer.h"            
//-RFC 2774 - Ability to lock CM

// +RFC 410499
#include "XMCashStatusBase.h"
#include "XMCashRemoveBase.h"
// -RFC 410499

#include "XMButtonMap.h" //button management
#include "XMLoginImpl.h"  //TAR 448426
#include "BaseStateFactory.h"	//SR752
#include "XMScreenUpdate.h"     //SR969

// +SSCOP-3544
#include "DMCashCounts.h"
#include "DMFileCashCounts.h"
// -SSCOP-3544

#define SCOTINPUTTITLE _T("SCOTInputWindow")

// +SSCOP-3544
#define ACCEPTORCOUNT_FILE _T("%APP_DRIVE%\\scot\\Data\\AcceptorCount.dat")  
#define DISPENSERCOUNT_FILE _T("%APP_DRIVE%\\scot\\Data\\DispenserCount.dat")  

LPCTSTR XM_IDMCASHCOUNT_BASE = _T("SOFTWARE\\NCR\\XM\\CashCount");
LPCTSTR XM_IDMACCEPTOR_CNT   = _T("AcceptorCounts");
LPCTSTR XM_IDMDISPENSER_CNT  = _T("DispenserCounts");
// -SSCOP-3544

// Function pointers for initializing override dll.
typedef XMSTATE_API XMState * (* XMINITIALIZE)(PSXWrapper *psx,
                                               XMCashDevice *xmd,XMConfig *cfg,
                                               IXMLoginDevice *pDevLogin);  //TAR 448426
											   
typedef XMSTATE_API void (* XMUNINITIALIZE)(void);

typedef XMSTATE_API TBWrapper * (* XMCREATETBWRAPPER)(void);

typedef XMSTATE_API long (* XMGETVERSIONNUMBER)(void);  // RFC 434653

XMINITIALIZE   g_xminit;    // Function pointer to initialize XMState dll
XMUNINITIALIZE g_xmdel;     // Function pointer to uninitialize XMState dll
DWORD g_gitcookie = 0;      // 0 is an invalid cookie.
HWND g_DMWrapper = NULL;

/**
 * Callback function to handle interception of TB messages.
 */
LRESULT CALLBACK GetMsgProc(int nCode, WPARAM wParam, LPARAM lParam);

XMode *XMode::m_xm = NULL;
CMutex stateLock;         // Serialize state changes.
CLog *g_logger = NULL;
HHOOK hhook = NULL;       // Handle from SetWindowHookEx()

/**
 * \struct eventmap_t
 * \brief Maps PSX control name to a handler identifier.
 * \see XMode::PSXTrigger
 */
typedef struct
{
    TCHAR  skey[64];    ///< PSX Control name.
    XM_EVENT nevent;    ///< Handler identifier.
} eventmap_t;

// FIXME>  This needs to be configurable in XML.
// FIXME> OK for small table size, may need to use a hash table in the future.
//lint -e651
const eventmap_t g_eventmap[] =
{
    {_T("XMButton1"), XMBUTTON1},
    {_T("XMButton2"), XMBUTTON2},
    {_T("XMButton3"), XMBUTTON3},
    {_T("XMButton4"), XMBUTTON4},
    {_T("XMButton5"), XMBUTTON5},
    {_T("XMButton6"), XMBUTTON6},
    {_T("XMButton7"), XMBUTTON7},
    {_T("XMButton8"), XMBUTTON8},
    {_T("XMButton9"), XMBUTTON9},//RFQ 432305
    {_T("XMButton10"), XMBUTTON10},//RFQ 432305
    {_T("XMLockScreen"), XMLOCKSCREEN},     //RFC 2774 - Ability to lock CM
    {_T("XMNonDispenseCylinder401EmptyButton"), XMBUTTON},
    {_T("XMNonDispenseCylinder501EmptyButton"), XMBUTTON},
    {_T("XMCoinDispenserManualButton"), XMBUTTON},
    {_T("XMNoteDispenserManualButton"), XMBUTTON},
    {_T("XMDMAcceptor"), XMDMACCEPTOR},
    {_T("XMReceiptScrollUp"), XMRECEIPTSCROLLUP},
    {_T("XMReceiptScrollDown"), XMRECEIPTSCROLLDOWN},
    {_T("XMKeyBoardP1"), XMKEYSTROKE},
    {_T("XMKeyBoardP2"), XMKEYSTROKE},
    {_T("XMKeyBoardP4"), XMKEYENTER},
    {_T("XMNumericKeyBoard"), XMKEYCLEAR},
    {_T("XMCashierBackspace"), XMKEYSTROKE}, // RFC 405861
    {_T("XMCoinAddButtonList"), XMBUTTON},
    {_T("XMNoteAddButtonList"), XMBUTTON},
    {_T("XMCoinRemoveButtonList"), XMBUTTON},
    {_T("XMNoteRemoveButtonList"), XMBUTTON},
    {_T("XMNumericKeyPad1"), XMKEYSTROKE},
    {_T("XMNumericKeyPad2"), XMKEYSTROKE},
    {_T("XMNumericKeyPad3"), XMKEYCLEAR},
    {_T("XMNumericKeyPad4"), XMKEYENTER},
    {_T("XMNumericKeyPad5"), XMKEYSTROKE},
    {_T("XMNumericKeyPad6"), XMKEYSTROKE},
    {_T("XMCancelChanges"), XMBUTTON},
    {_T("XMAlphaNumP1"), XMKEYSTROKE},
    {_T("XMAlphaNumP2"), XMKEYSTROKE},
    {_T("XMAlphaNumP3"), XMKEYSTROKE},
    {_T("XMAlphaNumP4"), XMKEYSTROKE},
    {_T("XMAlphaNumP5"), XMKEYSTROKE},
    {_T("XMAlphaNumP6"), XMKEYSTROKE},
    {_T("XMAlphaNumP7"), XMKEYSTROKE},
    {_T("XMAlphaNumP8"), XMKEYSTROKE},
    {_T("XMAlphaNumP1_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP2_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP3_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP4_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP5_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP6_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP7_LCase"), XMKEYSTROKE},
    {_T("XMAlphaNumP8_LCase"), XMKEYSTROKE},
    {_T("XMShiftKeyBoard"), XMKEYSHIFT},
    {_T("XMLine1AlphaNumericKeys"), XMKEYCHAR},
    {_T("XMLine2AlphaNumericKeys"), XMKEYCHAR},
    {_T("XMLine3AlphaNumericKeys"), XMKEYCHAR},
    {_T("XMLine4AlphaNumericKeys"), XMKEYCHAR},
    {_T("XMUNKNOWN"), XMUNKNOWN},
    {NULL, NULL}
};
 
XMODE_API bool InitializeXM(DWORD gitcookie, HWND hwnd /* == NULL */)
{
    assert(g_logger == NULL);

    if(g_logger != NULL)    // Check for redundant calls.
    {
        return false;       // Already initialized.
    }

    g_logger = CLog::GetLog();
    g_DMWrapper = hwnd;

    TRACEAPI(_T("InitializeXM"));

    g_gitcookie = gitcookie;

    XMode *xm = XMode::Instance();

    hhook = NULL;
    if(xm)
    {
        // Try to install windows hook.to monitor TB messages.
        hhook = SetWindowsHookEx(WH_GETMESSAGE, GetMsgProc, NULL, 
                                 GetCurrentThreadId());
        // FIXME>  Handle hhook==NULL.
    }

    g_logger->LogXMFileVersions();

    return (xm != NULL);
}

XMODE_API void UnInitializeXM(void)
{    
    TraceAPI *t = new TraceAPI(_T("UnInitializeXM"));

    if(hhook != NULL)   // TAR 376515 - Don't unhook unconditionally.
    {
        (void)UnhookWindowsHookEx(hhook);
        hhook = NULL;
    }

    CSingleLock singleLock( &stateLock, TRUE );    

    XMode::Destroy();

    delete t;

    CLog::DestroyLog();
    g_logger = NULL;
}

XMODE_API void PreShutdownXM(void)
{ 
    if (g_logger)
        g_logger->Log(TRACE_INFO, _T("PreShutdownXM"));

    XMode *xm = XMode::Instance();
    IDMWrapper *dm=NULL;
    if(xm)
        dm = xm->GetDMWrapper();
    if(dm)
        dm->TerminateDMWrapperThread();
    return;
}

XMode::XMode() : 
    m_sm(NULL), m_psx(NULL), m_cstate(NULL), m_hxm(NULL), m_cdev(NULL), 
    m_dm(NULL), m_dmcc(NULL), m_cntacc(NULL), m_cntdsp(NULL),
    m_cfg(NULL), m_dtimer(NULL), m_reporting(NULL), m_bInTarget(false),
    m_logger(NULL), m_dwThreadID(GetCurrentThreadId()), 
    m_hWndMain(CString(SCOTINPUTTITLE)), m_bMainCall(true),
    m_bBalanceChanged(true), m_bSendBalanceUpdates(true), // SR777
    m_bUseLowSensorAutoCount(false), //Report F53
    m_lLockTimeout(0), m_ltimer(NULL), m_pLockState(NULL),
	m_xmDevLogin(NULL),  //TAR 448426
    m_pCachedNonDispCounts(NULL), m_pCachedDispCounts(NULL), m_pScratch(NULL)
{ 
    TRACEAPI(_T("XMode::XMode"));
    m_logger = CLog::GetLog();

    g_xminit = NULL;
    g_xmdel = NULL;

    // +TAR 455878
    m_pCachedNonDispCounts = CreateCurrencyWrapper();
    m_pCachedDispCounts    = CreateCurrencyWrapper();
    m_pScratch             = CreateCurrencyWrapper();

    assert(m_pCachedNonDispCounts != NULL);
    assert(m_pCachedDispCounts    != NULL);
    assert(m_pScratch             != NULL);
    // -TAR 455878
}

XMODE_API void XMCurrencyDeposited(LPCSTR val, bool note)
{
    TRACEAPI(_T("XMode::XMCurrencyDeposited"));

    XMode *xm = XMode::Instance();
    if(xm)
    {
        xm->CurrencyDeposited(val, note);
    }
}

XMODE_API void XMCurrencyDispensed(LPCSTR dstr)
{
    TRACEAPI(_T("XMode::XMCurrencyDispensed"));

    XMode *xm = XMode::Instance();
    if(xm)
    {
        xm->CurrencyDispensed(dstr);
    }
}

XMODE_API void XMDeviceEvent(UINT evntType, long lParam, bool &handled, 
                             long devClass, long devID)
{
    TRACEAPI_EXT(_T("XMode::XMDeviceEvent"));

    handled = false;
    XMode *xm = XMode::Instance();
    if(xm)
    {
        xm->DeviceEvent(evntType, lParam, handled, devClass, devID);
    }
}

XMODE_API void XMDeviceEventEx(MessageElement &rMsg, bool &bProcessed)
{
    TRACEAPI_EXT(_T("XMode::XMDeviceEvent(MessageElement &)"));

    bProcessed = false;
    XMode *xm = XMode::Instance();
    if(xm)
    {
        xm->DeviceEvent(rMsg, bProcessed);
    }
}


XMode::~XMode()
{ 
    TRACEAPI(_T("XMode::~XMode"));

    try
    {
        // All of the following are freed in UnInitialize().
        //lint -esym(1740,XMode::m_sm,XMode::m_psx,XMode::m_hxm,XMode::m_cdev)
        //lint -esym(1740,XMode::m_dm,XMode::m_dmcc,XMode::m_cfg)
        //lint -esym(1740,XMode::m_cntacc,XMode::m_cntdsp,XMode::m_dtimer)
        //lint -esym(1740,XMode::m_reporting)
        XMode::UnInitialize();
        m_logger = NULL;                        // Address lint warning.
        m_cstate = NULL;                        // Address lint warning.

        DestroyCurrencyWrapper(m_pCachedNonDispCounts);
        DestroyCurrencyWrapper(m_pCachedDispCounts);
        DestroyCurrencyWrapper(m_pScratch);
    }
    catch(...)
    {
    }
}

XMode *XMode::Instance(void)
{
    TRACEAPI_EXT(_T("XMode::Instance"));

    // If singleton is not NULL and the application is not shutting down.
    if(m_xm == NULL)
    {
        m_xm = new XMode();
        if(! m_xm->Initialize() )
        {
            Destroy();
        }
    }
    return m_xm;
}

IDMWrapper *XMode::GetDMWrapper()
{
    return m_dm;
}

void XMode::Destroy(void)
{
    TRACEAPI(_T("XMode::Destroy"));
    assert(m_xm != NULL);

    delete m_xm;
    m_xm = NULL;
}

XMState * const XMode::GetCurrentState(void) const 
{
    TRACEAPI_EXT(_T("XMode::GetCurrentState"));

    //lint -esym(1763,XMode::GetCurrentState)
    return m_cstate;
}

void XMode::SetCurrentState(XMState *xm)
{
    TRACEAPI(_T("XMode::SetCurrentState"));
    
    CSingleLock singleLock( &stateLock, TRUE );    //RFC 2774 - Ability to lock CM
    assert(xm != NULL && m_cstate != NULL);

    if(xm)
    {
        //RFC 2774 - Ability to lock CM
        if( m_cstate  == m_pLockState && m_cstate->IsSystemLocked())
        {
            // If Yes button has been pressed on the lock confirmation screen.
            // This will happen on the PSX thread. 
            // Handle state change after main thread finishes its current task.
            if(m_dwThreadID != GetCurrentThreadId()) 
            {
                PostEvent(XMDELAYEDINIT, 0); //SR700
                return;
            }

            // We are in XMLockScreen and the main thread has finished.
            // Set the previous state to whatever state the handler on the
            // main thread has returned. 
            m_pLockState->SetPrevState(xm);  
            return;
        }

        // If changing to XMLockScreen, then don't uninitialize the previous state yet.
        if(xm != m_pLockState)      
        {
            m_cstate->UnInitialize();
        }
        
        m_cstate = xm;

        // Only initialize the state if the call is on the main thread.
        if(m_dwThreadID == GetCurrentThreadId())
        {
            assert(m_dwThreadID == GetCurrentThreadId()); 
            xm->Initialize();   
        }
        
        return;
    }

    m_logger->Log(TRACE_ERROR, 
                _T("NULL STATE POINTER !!! Staying in the same state."));
    m_logger->LogXMEvent(evlClassBasic, evlTypeWarning, XM_CAT_INTERNAL_ERROR, 
        XM_NULL_POINTER_ERROR, 1, _T("XMode::GetCurrentState"));
}

bool XMode::Initialize(void)
{
    TRACEAPI(_T("XMode::Initialize"));    

    if(m_sm || m_psx)
    {
        m_logger->Log(TRACE_ERROR, _T("Called >1 time!!!"));
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_STARTUP_INIT, 
            XM_XMODEINIT_ERROR, 1, _T("XMode::Initialize"));
        return false;
    }

    // Initialize override DLL, if it exists.
    // 408521 - Move name resolution to external function.
    CString csModuleName;
    (void)GetOverrideDLLName(csModuleName); 

    if(! LoadOverrideDLL(csModuleName))         // RFC 434653
    {
        // This means that we found an override DLL, but we could not load it.
        return false;
    }

    if(m_hxm)                                   // RFC 434653
    {
        // TAR 408521:  Initialize TB wrapper override class.
        // Initialize TBWrapper.
        XMCREATETBWRAPPER tbinit = 
            (XMCREATETBWRAPPER)GetProcAddress(m_hxm, "XMCreateTBWrapper"); 

        if(! tbinit)
        {
            m_logger->Log(TRACE_ERROR, 
                          _T("ERROR:  Could not initialize TBWrapper."));
            m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                                 XM_CAT_STARTUP_INIT, XM_INITIALIZE_ERROR, 2, 
                                 _T("XMode::Initialize"), _T("TBWrapper"));
                
            return false;
        }

        TBWrapper *ttb = (tbinit)();
        if(!ttb || !ttb->Initialize())
        {
            m_logger->Log(TRACE_ERROR, 
                          _T("FATAL ERROR:  Could not initialize TBWrapper!"));
            m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                                 XM_CAT_STARTUP_INIT, XM_INITIALIZE_ERROR, 2, 
                                 _T("XMode::Initialize"), _T("TBWrapper"));
                
            return false;
        }
        TBWrapperBase::SetTBWrapper(ttb);
    }

	//SSCOP-10053 Integrate OptiCash Client into Cash Management
	InitOptiCashClient();


    m_cfg = XMConfig::Instance();
    if(! m_cfg)        // Failed to load configuration files.
    {
        return false;
    }

    CString csLockTimeout;
    if(m_cfg->GetOption(_T("lock-timeout"), csLockTimeout))
    {
        m_lLockTimeout = _ttol(csLockTimeout);
    }

    if(m_lLockTimeout < 0 )        //sanity check
    {
        m_lLockTimeout = 200;    //default value
    }
    
    //Report F53+
    //Saved option UseLowSensorAutoCount to m_bUseLowSensorAutoCount
    CString csAutoCount;
    if(!m_cfg->GetOption(_T("UseLowSensorAutoCount"), csAutoCount))
    {
           m_logger->Log(TRACE_WARNING, 
                    _T("WARNING: Config UseLowSensorAutoCount not found. Using default N."));
           m_bUseLowSensorAutoCount = false;
     }
     else
     {
        if(csAutoCount.CompareNoCase(_T("Y"))==0)
        {
            m_bUseLowSensorAutoCount = true;
        }
     }
     //Report F53-

    if(! PreInitCheck(true))
    {
        m_logger->Log(TRACE_WARNING, 
                      _T("*** Cash Management will be disabled. ***"));
        return false;
    }

    // RFC 396811:  Check if calls are to be made on the main thread.  If the
    // option is not found, or if the option is set, make calls on main thread.
    CString callMain;
    if(m_cfg->GetOption(_T("calls-on-main-thread"), callMain) &&
        callMain == _T('0'))
    {
        const_cast<bool &>(m_bMainCall) = false;
    }

    if(! InitPSX())
    {
        return false;
    }

    if(! InitStateMonitor())
    {
        return false;
    }

    if(m_hxm)   
    {
        // TAR 408521:  If override DLL was loaded, then get function pointers
        //              to initialize and uninitialize.
        g_xminit = (XMINITIALIZE)GetProcAddress(m_hxm, "XMInitialize");
        g_xmdel  = (XMUNINITIALIZE)GetProcAddress(m_hxm, "XMUnInitialize");

        if(! (g_xminit && g_xmdel))
        {
            m_logger->Log(TRACE_ERROR, 
            _T("Could not get function pointers to XMState init functions!"));
            m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                                 XM_CAT_INTERNAL_INTERFACE, 
                                 XM_FUNCTION_POINTER_ERROR, 2, 
                                 _T("XMode::Initialize"), 
                                 _T("XMState init functions"));
            return false;
        }
    }
    else    
    {
        // TAR 408521:  Initialize default implementation.
        g_xminit = XMBaseInitialize;
        g_xmdel  = XMBaseUnInitialize;
    }

    if(! InitDMWrapper())
    {
        return false;
    }

    m_cntacc = CreateCurrencyWrapper();
    m_cntdsp = CreateCurrencyWrapper();
    if(! (m_cntacc && m_cntdsp))
    {
        m_logger->Log(TRACE_ERROR, 
                      _T("FAILED to setup CurrencyWrappers!"));
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                             XM_CAT_INTERNAL_ERROR, XM_SETUP_FAIL, 2, 
                             _T("XMode::Initialize"), 
                             _T("CurrencyWrappers"));
            
        return false;
    }

    if(! InitReporting())
    {
        return false;
    }

    if(! InitCashCounts())
    {
        return false;
    }

    //lint --e{613}
    m_cdev = XMCashDeviceFactory::GetCashDevice(*m_dm, *m_dmcc, *m_psx);  

    // Initialize currency deposit handler.
    m_dtimer = new XMDepositTimer();
    if(! m_dtimer->Initialize())
    {
        return false;
    }

	//+RFC 2774 - Ability to lock CM
	m_pLockState = new XMLockScreenBase();
	if (!m_pLockState)
	{
		return false;
	}

	m_ltimer = XMLockTimer::Instance();
	if ( !m_ltimer )
	{
		m_logger->Log(TRACE_EXTENSIVE, 
				_T("Lock timer thread initialization FAILED ..."));
	}
	//-RFC 2774 - Ability to lock CM
	
	//TAR 448426
    m_xmDevLogin = new XMLoginImpl(*m_dm, *m_cfg);  //lint -e539
	
    // Initialize XMState and pass in helper objects.
    m_cstate = (g_xminit)(m_psx, m_cdev, m_cfg, m_xmDevLogin);	//TAR 448426
    if(m_cstate && m_hxm)
    {
        m_logger->Log(TRACE_INFO, _T("Initialized %s"), (LPCTSTR)csModuleName);
    }        

    // Configure initial state if necessary.
    if(! SetBaseState())
    {
        return false;
    }
    
    //(+)SR969
    CString csOption = _T("N");
    //if((m_cfg->GetOption(_T("DisplayCashManagementAtRAP"), csOption) &&
    //    csOption.CompareNoCase(_T("Y")) == 0 ))
    //{       
        XMScreenUpdate *pXmScr = XMScreenUpdate::Instance();        
        pXmScr->Initialize();      
        m_cstate->InitModule();     
    //}
    //(-)SR969

    //+SR777
    CString csOpt;  
    //If SendBalanceUpdates or DisplayCashManagementAtRAP(SR969) call BalanceUpdates
    if( (m_cfg->GetOption(_T("SendBalanceUpdates"), csOpt) && 
        csOpt.CompareNoCase(_T("Y")) == 0 ) ||
        (csOption.CompareNoCase(_T("Y")) == 0 ))
    {       
        BalanceUpdate(); // TAR 411628: Send balance to report server at startup
    
    }
    if( m_cfg->GetOption(_T("SendBalanceUpdates"), csOpt) && 
        csOpt.CompareNoCase(_T("N")) == 0 )
    {
        m_bSendBalanceUpdates = false;
    }
    // -SR777

    return (m_cstate ? true : false);
}

bool XMode::InitPSX(void)
{
    TRACEAPI(_T("XMode::InitPSX"));    

    m_psx = new PSXWrapperCAPI();
    if(! (m_psx->Initialize() && 
          m_psx->SetMonitor(*this) && m_psx->CreateDisplay()) )
    {
        return false;
    }

    if(!m_psx->Visible(false))
    {
        assert(0);  //lint -e527
        m_logger->Log(TRACE_ERROR, _T("ERROR:  Failed to set visibility."));
    }

    return true;
}

bool XMode::InitStateMonitor(void)
{
    TRACEAPI(_T("XMode::InitStateMonitor"));    

    //lint --e{613}
    assert(m_psx != NULL);
    m_sm = new CApplicationMonitor(*m_psx);
    if(! m_sm)
    {
        m_logger->Log(TRACE_ERROR, 
                      _T("FATAL ERROR:  Could not create StateMonitor"));
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_STARTUP_INIT, 
            XM_CREATE_STATEMONITOR_ERROR, 1, _T("XMode::Initialize"));
        return false;
    }

    // Read configured target state.
    CString csTarget;
    assert(m_cfg != NULL);
    if(!m_cfg->GetOption(_T("target-state"), csTarget)) //lint -e613
    {
        m_logger->Log(TRACE_WARNING, 
              _T("WARNING: Could not read target-state. Using SmMediaStatus"));
        csTarget = _T("SmMediaStatus");
    }

    if(! m_sm->SetMonitor(*this, csTarget))
    {
        return false;
    }

    // Load any additional targets.
    CString csOpt;
    int i=1;
    do
    {
        csOpt.Format(_T("target-state%d"), i);
        if(m_cfg->GetOption(csOpt, csTarget))       //lint -e613
        {
            (void)m_sm->AddTarget(csTarget);
        }
        else
        {
            break;
        }

        i++;

        //lint -e506
    } while(true);

    return true;
}

bool XMode::InitDMWrapper(void)
{
    TRACEAPI(_T("XMode::InitDMWrapper"));    

    m_dm = new DMWrapper(g_gitcookie, g_DMWrapper);

    return m_dm->Initialize();
}

bool XMode::InitReporting(void)
{
    TRACEAPI(_T("XMode::InitReporting"));    
    m_reporting = XMReporting::GetReporting() ;

    return(m_reporting != NULL);
}

bool XMode::InitCashCounts(void)
{
    TRACEAPI(_T("XMode::InitCashCounts"));    
    // +SSCOP-3544
    CString csOption;
    if(m_cfg->GetOption(_T("UseFileCashCounts"), csOption) && (csOption == _T("Y") || csOption == _T("y")) )
    {
        m_dmcc = new DMFileCashCounts(*m_dm, *m_cntacc, *m_cntdsp);  // Use new way of storing counts!
        //if reg exists, then read it into the file, then delete the reg
        HKEY     hKey;
        LONG     lRC;
        lRC=RegOpenKeyEx(HKEY_LOCAL_MACHINE,XM_IDMCASHCOUNT_BASE,0,KEY_ALL_ACCESS , &hKey);
        if(lRC==ERROR_SUCCESS)
        {
            //read counts from reg
            CString csCounts;
            _TCHAR  szBuffer[_MAX_PATH+1];
            DWORD dwDataSize=_MAX_PATH;
            memset(szBuffer,0,dwDataSize*sizeof(_TCHAR));

            lRC = RegQueryValueEx(hKey, XM_IDMACCEPTOR_CNT, NULL, NULL,(LPBYTE)szBuffer, &dwDataSize);
            if(lRC==ERROR_SUCCESS)
            {
                csCounts=szBuffer;
                m_dmcc->UpdateCounts(csCounts, true);
                lRC=RegDeleteValue(hKey,XM_IDMACCEPTOR_CNT);
                if(lRC!=ERROR_SUCCESS)
                {
                    g_logger->Log(TRACE_INFO, _T("Failed to delete Reg Key %s, %d"),XM_IDMACCEPTOR_CNT,lRC);
                }
            }
            else
            { 
                g_logger->Log(TRACE_INFO, _T("No AcceptorCount Reg found"));
            }

            dwDataSize=_MAX_PATH;
            memset(szBuffer,0,dwDataSize*sizeof(_TCHAR));
            lRC = RegQueryValueEx(hKey, XM_IDMDISPENSER_CNT, NULL, NULL,(LPBYTE)szBuffer, &dwDataSize);
            if(lRC==ERROR_SUCCESS)
            {
                csCounts=szBuffer;
                m_dmcc->UpdateCounts(csCounts, false);
                lRC=RegDeleteValue(hKey,XM_IDMDISPENSER_CNT);
                if(lRC!=ERROR_SUCCESS)
                {
                    g_logger->Log(TRACE_INFO, _T("Failed to delete Reg Key %s, %d"),XM_IDMDISPENSER_CNT,lRC);
                }
            }
            else
            { 
                g_logger->Log(TRACE_INFO, _T("No DispenserCount Reg found"));
            }

            ::RegCloseKey(hKey); // Close key
        }
    }
    else                                                           
    {
        m_dmcc = new DMCashCounts(*m_dm, *m_cntacc, *m_cntdsp);     // Use the old way.

        TCHAR chFileName[_MAX_PATH]; 
        if (!GetPartitionPath(ACCEPTORCOUNT_FILE, chFileName))
        {
            m_logger->Log(TRACE_ERROR, 
                          _T("Failed to retrieve environment variables.") );
            m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_INTERNAL_ERROR, 
                        XM_ENV_VAR_ERROR, 1, _T("XMode::InitCashCounts"));
        }
        if(_taccess(chFileName,0)!=-1)
        {
            g_logger->Log(TRACE_INFO, _T("AcceptorCount File found"));
            CString csCounts;
            //read it into the registry
            if(DMFileCashCounts::GetAcceptorCountsFromFile(csCounts))
            {
                //update counts and registy 
                m_dmcc->UpdateCounts(csCounts, true);
                try {
                    CFile::Remove(chFileName);
                } catch(...){}
            }
        }
        else
        {
           g_logger->Log(TRACE_INFO, _T("No AcceptorCount File found"));
        }

        
        if (!GetPartitionPath(DISPENSERCOUNT_FILE, chFileName))
        {
            m_logger->Log(TRACE_ERROR, 
                          _T("Failed to retrieve environment variables.") );
            m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_INTERNAL_ERROR, 
                        XM_ENV_VAR_ERROR, 1, _T("XMode::InitCashCounts"));
        }
        if(_taccess(chFileName,0)!=-1)
        {
            g_logger->Log(TRACE_INFO, _T("DispenserCount File found"));
            CString csCounts;
            //read it into the registry
            if(DMFileCashCounts::GetDispenserCountsFromFile(csCounts))
            {
                //update counts and registy 
                m_dmcc->UpdateCounts(csCounts, false);
                try {
                    CFile::Remove(chFileName);
                } catch(...){}
            }
        }
        else
        {
           g_logger->Log(TRACE_INFO, _T("No DispenserCount File found"));
        }


    }
    // -SSCOP-3544
    if(! m_dmcc->Initialize())
    {
        m_logger->Log(TRACE_ERROR, _T("FAILED to initialize cash counts!"));
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_STARTUP_INIT, 
            XM_INITIALIZE_ERROR, 2, _T("XMode::Initialize"), _T("cash counts"));
        return false;
    }

    return true;
}

bool XMode::SetBaseState(void)
{
    return true;
}

void XMode::UnInitialize()
{
    // IMPORTANT NOTE:  stateLock crit section is already held here!
    TRACEAPI(_T("XMode::UnInitialize"));    

    // Move statemonitor deletion here because this uses TBWrapper.
    delete m_sm;
    m_sm = NULL;

    if(g_xmdel)
    {
        (g_xmdel)();
    }
    else
    {
        TBWrapperBase::Destroy();   // TAR 376515:  Clean-up.
    }

    g_xmdel = NULL;

    FreeOverrideDLL();

    delete m_psx;
    m_psx = NULL;

    delete m_cdev;
    m_cdev = NULL;

    delete m_dmcc;
    m_dmcc = NULL;

    DestroyCurrencyWrapper(m_cntacc);
    DestroyCurrencyWrapper(m_cntdsp);
    m_cntacc = m_cntdsp = NULL;

    delete m_dm;
    m_dm = NULL;

    XMConfig::Destroy();
    m_cfg = NULL;

    delete m_dtimer;
    m_dtimer = NULL;
    
    //+RFC 2774 - Ability to lock CM 
    XMLockTimer::Destroy();
    m_ltimer = NULL;

    delete m_pLockState;
    m_pLockState = NULL;
    //-RFC 2774 - Ability to lock CM 

    XMReporting::DestroyReporting() ; // TAR 376515
    m_reporting = NULL ;
	
	//TAR 448426
    delete m_xmDevLogin;    
    m_xmDevLogin = NULL;

    XMScreenUpdate::Destroy(); //SR969

}

/**
 * Internal event dispositioning mechanism.
 * If the call is on the main thread, then execute the call as normal.
 * If the call is not on the main thread, then post a message to the
 * main window.
 * The GetMsgProc() hook procedure will then call this method again from the 
 * main thread.
 * \note - Added for RFC 396811.
 */
void XMode::HandleEvent(XM_EVENT evt, WPARAM wParam, LPARAM lParam)
{
#ifdef _DEBUG
    TRACEAPI(_T("XMode::HandleEvent"));    
#else
    TRACEAPI_EXT(_T("XMode::HandleEvent"));    
#endif

    // TAR 442920:  Obtain the main window handle if necessary.
    if(m_bMainCall && m_hWndMain == (HWND)NULL) 
    {
        if(! m_hWndMain.Initialize())
        {
            return;
        }
    }

    // If the call isn't on the main thread and not on the lock state, 
    // then repost it.
    if(m_hWndMain != (HWND)NULL && m_dwThreadID != GetCurrentThreadId()
        && m_cstate != m_pLockState  )        //RFC 2774 - Ability to lock CM
    {
		PostEvent(evt, wParam);
        return;
    }

    // If here, then the call must be on the main thread OR the screen is being locked.
    assert(m_hWndMain == (HWND)NULL || m_dwThreadID == GetCurrentThreadId()
        || m_cstate == m_pLockState  );        //RFC 2774 - Ability to lock CM

    assert(m_cstate != NULL && m_psx != NULL);

    XMState *nstate = m_cstate;       // Possible new state. 
    
    CSingleLock singleLock(&stateLock, FALSE);  

    if(! m_cstate->IsSystemBusy() )    
    {
        if(! singleLock.Lock(m_lLockTimeout))
        {
            assert(false);
            m_logger->Log(TRACE_ERROR, 
                      _T("ERROR: Could not obtain state lock."));
        }
    }
    
    CString* pcsTemp;
    CString csChar;
    switch(evt)
    {
        case XMBUTTON1:
            nstate = m_cstate->XPSButton1();
            break;
        case XMBUTTON2:
            nstate = m_cstate->XPSButton2();
            break;
        case XMBUTTON3:
            nstate = m_cstate->XPSButton3();
            break;
        case XMBUTTON4:
            nstate = m_cstate->XPSButton4();
            break;
        case XMBUTTON5:
            nstate = m_cstate->XPSButton5();
            break;
        case XMBUTTON6:
            nstate = m_cstate->XPSButton6();
            break;
        case XMBUTTON7:
            nstate = m_cstate->XPSButton7();
            break;
        case XMBUTTON8:
            nstate = m_cstate->XPSButton8();
            break;
        case XMBUTTON9://RFQ 432305:Pick Up Auto Amount
            nstate = m_cstate->XPSButton9();
            break;
        case XMBUTTON10://RFQ 432305:Pick Up (x) Amount
            nstate = m_cstate->XPSButton10();
            break;
        case XMBUTTON:
            nstate = m_cstate->XPSButton(wParam);
            break;
        case XMRECEIPTSCROLLUP:
            nstate = m_cstate->XPSReceiptUp();
            break;
        case XMRECEIPTSCROLLDOWN:
            nstate = m_cstate->XPSReceiptDown();
            break;
        case XMKEYSTROKE:
            nstate = m_cstate->XPSKeyStroke(wParam);
            break;
        case XMKEYCHAR:
            pcsTemp = (CString*)wParam;
            csChar = *pcsTemp;
            nstate = m_cstate->XPSCharKey(csChar.GetAt(0));
            break;
        case XMKEYSHIFT:
            nstate = m_cstate->XPSKeyShift();
            break;
        case XMKEYENTER:
            nstate = m_cstate->XPSEnterKey();
            break;
        case XMKEYCLEAR:
            nstate = m_cstate->XPSClearKey();
            break;
        case XMCURRENCYEVENT:
            nstate = m_cstate->XDMAcceptor();
            break;
        case XMSHOW:
            m_cstate->Initialize();
            (void)m_psx->Visible();
            m_bInTarget = true;
            break;
        case XMHIDE:
            (void)m_psx->Visible(false);
            m_cstate->UnInitialize();
            m_bInTarget = false;
            break;
        // +SR777
        case XMBALANCEREQUEST:
            BalanceUpdate();    // TAR 411628:  Moved impl to method.
            break;
        // -SR777
        case XMDELAYEDINIT:        //RFC 2774 - Ability to lock CM
            HandleDelayedInit();
            return;
            break;
		case XMFPEVENT:     //TAR 448426
            nstate = m_cstate->HandleFPData();
            break;
        case XMDMACCEPTOR:
        case XMUNKNOWN:
        default:
            m_logger->Log(TRACE_EXTENSIVE,
                          _T("No mapping found for control id [%d]"), evt);
    }

    // TAR 442920:  We know we have a valid message handle now.
    //              Mark our handle as valid.
    m_hWndMain.SetValid();

    (void)singleLock.Unlock();
        
    // Check if state has changed.  Initialize new state if necessary.
    if(m_cstate != nstate)
        SetCurrentState(nstate);
}

//+SR700
void XMode::PostEvent(UINT Msg, LPARAM lParam)
{
    assert(m_hWndMain != NULL);
    if(! m_hWndMain)
    {
        m_logger->Log(TRACE_ERROR, 
                      _T("ERROR:  No window handle to main thread!"));
    }

    if(::PostMessage(m_hWndMain, Msg, 0, lParam) == FALSE)
    {
        m_logger->Log(TRACE_ERROR, _T("::PostMessage FAILED with error %#X"),
                      GetLastError());
    }
}
//-SR700

// +SR777
void XMode::BalanceUpdate(void)
{
    assert(m_dwThreadID == GetCurrentThreadId());

    if(m_dwThreadID != GetCurrentThreadId())
    {
        HandleEvent(XMBALANCEREQUEST, NULL, NULL);
    }

    if(!m_bBalanceChanged && !m_bSendBalanceUpdates) //SSCOADK-6784
    {
        m_logger->Log(TRACE_INFO, _T("INFO: No balance update needed")); //SSCOADK-6784
        return; // No update needed.
    }

    assert(m_cdev != NULL);
    if (m_cdev == NULL)
    {
        m_logger->Log(TRACE_ERROR, _T("ERROR: m_cdev is NOT INITIALIZED"));
        return;
    }

    // +TAR 411628:  Moved implementation from HandleEvent() to here. 
    CString csCounts;
    assert(m_cstate != NULL);
    XMState *nstate = m_cstate;       // Possible new state. 

    CurrencyWrapper *disp  = NULL;
    CurrencyWrapper *ndisp = NULL;
    bool bDiscrepancy = false;  // Add Glory CM Support
    if(m_cdev->ReadDispensableCounts(csCounts, bDiscrepancy))
    {
        disp = CreateCurrencyWrapper(csCounts);
    }

    if(m_cdev->ReadNonDispensableCounts(csCounts))
    {
        ndisp = CreateCurrencyWrapper(csCounts);
    }

    assert(disp != NULL && ndisp != NULL);

    if(disp && ndisp)
    {
		

        nstate = m_cstate->XBalanceUpdate(*disp, *ndisp);
        m_bBalanceChanged = false;      // Reset dirty flag.
    }

    DestroyCurrencyWrapper(disp);
    DestroyCurrencyWrapper(ndisp);
    disp = ndisp = NULL;

    // Check if state has changed.  Initialize new state if necessary.
    if(m_cstate != nstate)
        SetCurrentState(nstate);

    // -TAR 411628:  Moved implementation from HandleEvent() to here. 
}
// -SR777

DWORD XMode::GetMainThreadID(void) const
{
    return m_dwThreadID;
}

/**
 * Implementation of PSXMonitorCallback interface..
 */
void XMode::PSXTrigger(LPCTSTR szControlName, LPCTSTR szContextName, 
                                  LPCTSTR szEventName, long buttonID)
{
    TRACEAPI(_T("XMode::PSXTrigger"));  
 
    int i=0;
    int nEvnt = 0;
 
    m_logger->Log(TRACE_EXTENSIVE, _T("Received event [%s|%s|%s]"), 
                szControlName, szContextName, szEventName);
 
    if(m_cstate == NULL || _tcslen(szControlName) == 0)
    {
        m_logger->Log(TRACE_WARNING, _T("Invalid PSX event"));
        return;
    }
    
    //+RFC 2774 - Ability to lock CM
    assert(m_ltimer != NULL);        //Failed to load lock timer
    if( m_ltimer  && !m_cstate->IsSystemLocked() ) 
    {
        m_logger->Log(TRACE_EXTENSIVE, 
                    _T("Lock screen timer thread resets ..."));
        m_ltimer->StartTimer();
    }
    
    // Compare the context of this event to the current context.  
    // If they are not the same then do not process the event.
    //+TAR 435467
    CString csCtx;
    if(szContextName != NULL && m_psx && m_psx->GetContext(csCtx) && 
       csCtx != szContextName)
    {
          m_logger->Log(TRACE_WARNING, 
                        _T("Received %s from %s, but current context=%s.  IGNORING."),
                    szEventName, szContextName, (LPCTSTR)csCtx);
          return;
    }
    //-TAR 435467
 
    for(i=0; g_eventmap[i].nevent; i++)
    { 
        if(_tcscmp(szControlName, g_eventmap[i].skey) == 0)
        {
            i = nEvnt = g_eventmap[i].nevent;   // Get index of handler mapping.
            break;
        }
    }
 
    
   // BUG.  variable i was not ending up with a value of XMUNKNOWN and this caused 
   // spurious messages to be sent to SCOTApp whenever there is a context change !!!.
    if(i != nEvnt) //event was not found
    {
      i = XMUNKNOWN;
    }
 
    assert(i==XMUNKNOWN || m_cstate != NULL);
    
    //+RFC 2774 - Ability to lock CM
    if ( (XM_EVENT)i == XMLOCKSCREEN )
    {
        m_logger->Log(TRACE_EXTENSIVE,
                    _T(">> Aquiring state lock ...<<"));

         CSingleLock singleLock(&stateLock, FALSE);  
                    
         // Try to obtain the lock.  
         if(! singleLock.Lock(m_lLockTimeout))    
         {
             assert(false);
             m_logger->Log(TRACE_ERROR,
                _T("ERROR: Could not obtain state lock."));
             return;
         }

         // We have the lock.  
         m_logger->Log(TRACE_EXTENSIVE,
                    _T(">> Aquired state lock ...<<"));
                    
        assert(m_pLockState != NULL);
        m_pLockState->SetPrevState(m_cstate);

        m_pLockState->SetSystemLocked(true);

        m_pLockState->SetConfirmation(true);

        SetCurrentState(m_pLockState);

        (void)singleLock.Unlock();
        
        return;
    }
    //-RFC 2774 - Ability to lock CM
    
 
    // If no mapping found, but buttonID is non-zero, then pass to
    //  generic button handler.
    if(i == XMUNKNOWN && m_cstate != NULL && buttonID > 0)
    {
       i = XMBUTTON;
    }
 
    // I think that this is an existing bug.  We shouldn't
    // process events that are not mapped.
    if(i==XMUNKNOWN)
    {
        return;
    }
     
 
    HandleEvent((XM_EVENT)i, buttonID, 0);  
}

void XMode::CurrencyDeposited(LPCSTR val, bool note)
{
    TRACEAPI(_T("XMode::CurrencyDeposited"));
    //+SSCOADK-7123
    CString s(val), csContext;
    COleVariant vstate = (long)UI::Disabled;

    m_psx->GetContext(csContext);
    if(csContext == _T("XMCashStatus"))
    {
        m_psx->SetControlProperty(_T("SMButton8"), UI::PROPERTYSTATE, vstate);

    }
    else if (csContext == _T("XMCashReplenish"))
    {
        m_psx->SetControlProperty(_T("XMButton8"), UI::PROPERTYSTATE, vstate);
    }
    //-SSCOADK-7123

    m_bBalanceChanged = true; //SR777

    m_logger->Log(TRACE_INFO, _T("[%s] deposited."), (LPCTSTR)s);

    assert(m_dmcc != NULL);
    (void)m_dmcc->CurrencyAccepted(val, note);  // Update internal counts.

    // Report Currency Accepted only when in target.
    // Only call XDMAcceptor if in target.
    if(m_bInTarget)
    {
        // PM decided to not record CM currency acceptance at report server.  
        // Doing so may cause problems w/ report server sales calculations.
        // m_reporting->CurrencyAccepted( val, note ) ;

        assert(m_dtimer != NULL);
        m_dtimer->SetTimer(1000);   // FIXME>  Configurable timer value.
    }


	if(!m_bInTarget)
    {
		//SSCOP-10053 Integrate OptiCash Client into Cash Management
		if(m_OCWrapper != NULL)
		{
			USES_CONVERSION;
			if(note)
			{
				m_OCWrapper->OCUpdateMessage(A2CT(val), OC_NOTE_DEPOSITED);
			}
			else
			{
				m_OCWrapper->OCUpdateMessage(A2CT(val), OC_COIN_DEPOSITED);
			}
		}
	}
    
    

    // +TAR 455878:  Need to update caches, so wipe out current data.
    m_pCachedNonDispCounts->Empty();

    // Note:  If we have recyclers, we don't know where the deposited money
    //        goes.  It could go to the cash box/coin overflow or it could go
    //        to the recyclers.  So we need to re-read this cache too.
    m_pCachedDispCounts->Empty();
    // -TAR 455878
}   

void XMode::CurrencyDispensed(LPCSTR dstr)
{
    TRACEAPI(_T("XMode::CurrencyDispensed"));

    m_bBalanceChanged = true; //SR777

    assert(m_dmcc != NULL);
    (void)m_dmcc->CurrencyDispensed(dstr);


    // +TAR 455878
    // Subtract the amount that was dispensed from our cached counts to 
    // potentially avoid an expensive dev query later in XMode::GetCashCounts.
    // Unlike in CurrencyDeposited, we know that the money will be dispensed
    // from the recyclers or dispenser. 
    if(m_pCachedDispCounts->Size() > 0)
    {
        USES_CONVERSION;

        m_pScratch->Empty();
        *m_pScratch = (A2CT(dstr)); 
        *m_pCachedDispCounts -= *m_pScratch;
    }

	if(m_bInTarget)
	{
		int fff =0;
	}


	//SSCOP-10053 Integrate OptiCash Client into Cash Management
		if(!m_bInTarget)
		{
			USES_CONVERSION;
			m_OCWrapper->OCUpdateMessage(A2CT(dstr), OC_DISPENSED);
		}
	

	
    
    // -TAR 455878

    // Report Currency Dispensed only when in cash management.
    /* PM decided to not record CM currency dispenses at report server.  
     * Doing so may cause problems w/ report server sales calculations.
    if(m_bInTarget)
    {
        m_reporting->CurrencyDispensed( dstr ) ;
    }
    */
}

void XMode::DeviceEvent(UINT evntType, long lParam, bool &handled,
                        long devClass, long devID)
{
    TRACEAPI_EXT(_T("XMode::DeviceEvent"));
    CSingleLock singleLock( &stateLock, FALSE );    

    // This call should happen on the main thread.
    assert(GetCurrentThreadId() == m_dwThreadID);

    handled = false;

    // +TAR 386003:  CM now pays attention to device events.
    // Don't forward the event unless we are actually in cash management.
    // We may want to change this later.
    if(! m_bInTarget)
    {
        return;
    }

    if(GetCurrentThreadId() != m_dwThreadID)
    {
        return; // Don't process device events from other threads.
    }

    // Try to get the state lock and bail if we fail to get it.
    // There is a potential for deadlock here.  This approach is not ideal,
    // because it is possible that cash management may miss a device event that
    // it is expecting.
    // For the current usage, this should not be a problem.
    if(! singleLock.Lock(300))
    {
        assert(false);
        m_logger->Log(TRACE_ERROR, 
                  _T("ERROR: Could not obtain lock. Device event [%d] for devClass [%d] not processed."), evntType, devClass);

        return;
    }
    // -TAR 386003

    XMState *cstate = NULL; // Current state.
    
    cstate = GetCurrentState();
    
    if ( cstate != NULL )
    {
        // Note:  Although CM may return a new state, no state change currently
        //        takes place.   
        //        This needs to be fixed in the future.
        (void)cstate->XDMParse( evntType, lParam, handled, devClass, devID );
    }
}

void XMode::DeviceEvent(MessageElement &rMsg, bool &bProcessed)
{
    assert(GetCurrentThreadId() == m_dwThreadID); // Should always be the case.

    XMState *pCurrent = GetCurrentState();
    assert(pCurrent != NULL);

    if(pCurrent)
    {
        (void)pCurrent->XDMParse(rMsg, bProcessed);
    }
}

void XMode::FireCurrencyEvent(void)
{
    TRACEAPI(_T("XMode::FireCurrencyEvent"));
    CSingleLock singleLock( &stateLock, TRUE );    

    HandleEvent(XMCURRENCYEVENT, NULL, NULL);
}

//RFC 409914
bool XMode::GetCurrentBalance(CurrencyWrapper &ndisp, CurrencyWrapper &disp)
{
    bool bRe = false;
    CString csBalance;

    assert(m_cdev !=NULL);

    if (m_cdev == NULL)
    {
        return false;
    }

    if(m_cdev->ReadNonDispensableCounts(csBalance))
    {
        ndisp = csBalance;
        bRe = true;
        m_logger->Log(TRACE_INFO, _T("GetCurrentBalance::NonDispensableCounts: [%s]"),
            (LPCTSTR)csBalance);
    }
    else
    {
        m_logger->Log(TRACE_ERROR, 
                      _T("GetCurrentBalance::ReadNonDispensableCounts() FAILED"));
        bRe = false;
    }

    bool bDiscrepancy = false;  // Add Glory CM Support
    if(m_cdev->ReadDispensableCounts(csBalance, bDiscrepancy))
    {
        disp = csBalance;
        m_logger->Log(TRACE_INFO, _T("GetCurrentBalance::DispensableCounts: [%s]"),
            (LPCTSTR)csBalance);
    }
    else
    {
        m_logger->Log(TRACE_ERROR, 
                      _T("GetCurrentBalance::ReadDispensableCounts() FAILED"));
        bRe = false;
    }
    return bRe;
}

//Report F53+
XMODE_API bool XMProcessCashCounts(BSTR FAR *pResult)
{
    TRACEAPI_EXT(_T("XMODE_API XMProcessCashCounts"));
    bool bSuccess = false;

    XMode *xm = XMode::Instance();
    if(xm)
    {
        bSuccess = xm->ProcessCashCounts(pResult);
    }
    return bSuccess;
}

bool XMode::ProcessCashCounts(BSTR FAR *pResult) const
{
    TRACEAPI_EXT(_T("XMode::ProcessCashCounts"));
    bool bSuccess = false;

    // This call should happen on the main thread.
    assert(GetCurrentThreadId() == m_dwThreadID);
    if(GetCurrentThreadId() != m_dwThreadID)
    {
        return bSuccess; // Don�t process device events from other threads.
    }
    assert(m_cdev != NULL);
    if(m_cdev == NULL)
    {
         m_logger->Log(TRACE_ERROR, _T("ERROR: m_cdev is NOT INITIALIZED"));
        return bSuccess;
    }

    if(m_bUseLowSensorAutoCount == false)
    {
        bSuccess = m_cdev->ProcessCashCounts(pResult);
    }
    return bSuccess;
}
//Report F53-

//+TAR 448434
XMODE_API bool XMGetCashCounts(CString &csDispenserCounts, CString &csAcceptorCounts)
{
	TRACEAPI(_T("XMode::XMGetCashCounts"));
	XMode *xm = XMode::Instance();
    if(xm)
    {
        return xm->GetCashCounts(csDispenserCounts, csAcceptorCounts);
    }
	return false;
}

bool XMode::GetCashCounts(CString &csDispenserCounts, CString &csAcceptorCounts)
{
    assert(m_cdev != NULL);
    if(m_cdev == NULL)
    {
        return false;
    }

    // +TAR 455878

    // Return cached counts for faster performance.
    // If caches are not empty and if we are not in CM.  If we return
    // cached counts while in CM, then the cash/paper status screen will not
    // update correctly.
    if(m_pCachedDispCounts->Size() > 0 && 
       m_pCachedNonDispCounts->Size() > 0 && !m_bInTarget)
    {
        csDispenserCounts = m_pCachedDispCounts->ToString();
        csAcceptorCounts = m_pCachedNonDispCounts->ToString();
        return true;    // Done.
    }

	bool bDiscrepancy = false;  // Add Glory CM Support

    // If here, then we need to fetch new data and update the caches.
    bool bRet = 
           (m_cdev->ReadDispensableCounts(csDispenserCounts, bDiscrepancy) &&
            m_cdev->ReadNonDispensableCounts(csAcceptorCounts));

    assert(bRet);
    if(bRet)
    {
        *m_pCachedDispCounts = (LPCTSTR)csDispenserCounts;
        *m_pCachedNonDispCounts = (LPCTSTR)csAcceptorCounts;
    }

    return bRet;
    // -TAR 455878
}
//-TAR 448434

//+TAR 450643
XMODE_API long XMGetDispenserCapacity(long denom)
{
	TRACEAPI_EXT(_T("XMode::XMGetDispenserCapacity"));
	XMode *xm = XMode::Instance();
	assert (xm != NULL);
	if (xm == NULL)
	{
		return 0;
	}

    return xm->GetDispenserCapacity(denom);
}

long XMode::GetDispenserCapacity(long denom)
{
    assert(m_cdev != NULL);
    if(m_cdev == NULL)
    {
        return 0;
    }

	return m_cdev->GetDispenserCapacity(denom);
            
}
//-TAR 450643

/**
 * Implementation of StateMonitorCallback interface.
 * This method will be called by our StateMonitor (ApplicationMonitor).
 */
void XMode::StateTrigger(LPCTSTR sname, bool toggle)
{
    TRACEAPI(_T("XMode::StateTrigger"));
    CSingleLock singleLock( &stateLock, TRUE );    

    assert(sname != NULL);

    // target state was entered or exited.
    // Main trigger to show or hide cash management display.
    if(toggle)
    {
        m_logger->Log(TRACE_INFO, _T(">> ENTER TARGET [%s] <<"), sname);
        HandleEvent(XMSHOW, 0, 0);
    }
    else
    {
        m_logger->Log(TRACE_INFO, _T(">> EXIT TARGET  [%s] <<"), sname);
        HandleEvent(XMHIDE, 0, 0);
    }
}

/**
 * Perform a pre-initialization check.
 * \return true if OK to load.  false if cash management should not be loaded.
 * \note Added for TAR 378792.
 * \note Changed parameter from void to bool for TAR 450634
 */
// TAR 451959 - renamed bCheckHookService to bCheck to make it more generic
bool XMode::PreInitCheck(bool bCheck)     
{
    TRACEAPI(_T("XMode::PreInitCheck"));

    // See if cash management is configured in ScotOpts.  If not, then bail.
    CString csOpt;
    bool bCMEnabled = true;

    assert(m_cfg != NULL);
    if(m_cfg->GetOption(_T("CashManagementScreen"), csOpt))
    {
        if(csOpt.CompareNoCase(_T("N")) == 0)
        {
            m_logger->Log(TRACE_WARNING, 
                _T("CashManagementScreen is set to 'N' in ScotOpts."));
            bCMEnabled = false;
        }
    }
    else    // Option not found.  Treat the same as CashManagementScreen=N.
    {
        m_logger->Log(TRACE_WARNING, 
                _T("No entry found for CashManagementScreen in ScotOpts."));
        bCMEnabled = false;
    }

    if (bCheck) // TAR 451959
    {
        if(m_cfg->GetOption(_T("CashManagementAPPTrackCashCount"), csOpt))
        {
            if(csOpt.CompareNoCase(_T("Y")) == 0)
            {
                m_logger->Log(TRACE_ERROR, 
                _T("ERROR: CashManagementAPPTrackCashCount is set to 'Y' in ScotOpts."));
                m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                                     XM_CAT_CONFIGURATION, 
                                     XM_SCOTOPTS_TRACKCC_CONFIG_ERROR, 
                                     1, _T("XMode::PreInitCheck"));
                bCMEnabled = false;
            }
        }
        else    // FL defaults to CashManagementAPPTrackCashCount=Y
        {
            m_logger->Log(TRACE_ERROR, 
                    _T("No entry found for CashManagementAPPTrackCashCount in ScotOpts.  Please add CashManagementAPPTrackCashCount=N to ScotOpts."));
            m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                                 XM_CAT_CONFIGURATION, 
                                 XM_SCOTOPTS_TRACKCCNOTFOUND_CONFIG_ERROR, 
                                 1, _T("XMode::PreInitCheck"));
                             
            bCMEnabled = false;
        }
    }

    bool bAdjustCounts = false;     // This should be the SO default.

    if (m_cdev != NULL)
    {
        bAdjustCounts = m_cdev->CheckAutoAdjustBinCashCounts();
    }	

    // If cash management is enabled, then the BCR must not auto adjust its 
    // counts.  If CM is disabled, then the BCR needs to auto adjust its counts.
    if(bCMEnabled && bAdjustCounts)
    {
        m_logger->Log(TRACE_ERROR, 
            _T("FATAL ERROR: AutoAdjustBinCashCounts MUST be set to FALSE in the applicable registry location (DirectIO for OPOS) for cash management to function correctly"));
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_CONFIGURATION, 
            XM_AUTOADJ_REGISTRY_SETTING_ERROR, 3, _T("XMode::PreInitCheck"), _T("AutoAdjustBinCashCounts"), _T("DirectIO for OPOS"));
        bCMEnabled = false;
    }
    else if(! bCMEnabled && ! bAdjustCounts)
    {
        m_logger->Log(TRACE_WARNING, 
                _T("WARNING:  Cash management is disabled, but AutoAdjustBinCashCounts is set to FALSE in applicable registry location (DirectIO for OPOS).  Coin dispense will not work correctly."));
        m_logger->LogXMEvent(evlClassBasic, evlTypeWarning, XM_CAT_CONFIGURATION, 
            XM_AUTOADJ_REGISTRY_SETTING_WARNING, 3, _T("XMode::PreInitCheck"), _T("AutoAdjustBinCashCounts"), _T("DirectIO for OPOS"));
    }

    // +RFC 411652
    // Check if the XMHookService is running or not.  If it is not running,
    // this will not be a fatal condition, but a warning will be written to the
    // event log to indicate that some features may not work correctly.
    
    // If the hook service is running, then the XMAPIHooks DLL should be
    // loaded in the memory of this process.  
    if( bCheck && ! (::GetModuleHandle(_T("XMAPIHooks.dll")) || 
          ::GetModuleHandle(_T("XMAPIHooksD.dll")) ) )
    {
        DWORD rc = GetLastError();
        m_logger->Log(TRACE_WARNING, 
                      _T("WARNING:  Did not detect hook service [%#X]"), rc);

        m_logger->LogXMEvent(evlClassBasic, evlTypeWarning, 
                             XM_CAT_INTERNAL_INTERFACE,
                             XM_HOOKSERVICE_DOWN);
    }
    // -RFC 411652
    
    return bCMEnabled;
}

/**
 * Attempt to load a custom override DLL if it exists. 
 * Attempt to query the override DLL version and insure it matches the 
 * expected version of the current retrofit release.  
 *
 * We do not want to load an override DLL that is not binary compatible.
 * \param[in] csModuleName : Name of override DLL to load.
 * \return true if no override DLL is found -OR- if and override DLL is found
 *         and it is loaded successfully.  Return false otherwise.
 *
 * \note Added for RFC 434653
 */
bool XMode::LoadOverrideDLL(const CString &csModuleName)
{
    TRACEAPI(_T("XMode::LoadOverrideDLL"));

    m_hxm = LoadLibrary(csModuleName);

    if(m_hxm == NULL)   // No override present.  Nothing to do.
    {
        // TAR 408521:  It's no longer a fatal error if the override DLL
        //              cannot be loaded.  
        m_logger->Log(TRACE_INFO, 
            _T("Override DLL [%s] not found.  Using default implementation."),
            (LPCTSTR)csModuleName);
        return true;    // Not a problem.
    }

    // Try to query the DLL for its version.
    XMGETVERSIONNUMBER fpGetVersion =
        (XMGETVERSIONNUMBER)GetProcAddress(m_hxm, "XMGetVersionNumber");

    CString csCurrentVersion;       // Current version as a string.
    csCurrentVersion.Format(_T("%d"), XMODE_CURRENT_VERSION);
    if(! fpGetVersion)              // No function available ??
    {
        m_logger->Log(TRACE_ERROR, 
                  _T("ERROR:  Could not query version of %s.  Cash Management will be disabled."), 
                  (LPCTSTR)csModuleName);

        m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                             XM_CAT_INTERNAL_INTERFACE,
                             XM_OVERRIDE_VERSION_MISMATCH_1,
                             2,
                             (LPCTSTR)csModuleName,
                             (LPCTSTR)csCurrentVersion);

        FreeOverrideDLL();
        return false;
    }

    // Found function, now see if the version is compatible.
    long lVersion = (fpGetVersion)();

    CString csReadVersion;          // Version obtained from override DLL.
    csReadVersion.Format(_T("%d"), lVersion);
    if(lVersion != XMODE_CURRENT_VERSION)
    {
        m_logger->Log(TRACE_ERROR, 
            _T("ERROR: VERSION MISMATCH for override DLL %s.  Expected: %d; Returned: %d.  Cash Management will be disabled."),
            (LPCTSTR)csModuleName, XMODE_CURRENT_VERSION, lVersion);

        m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                             XM_CAT_INTERNAL_INTERFACE,
                             XM_OVERRIDE_VERSION_MISMATCH_2,
                             3,
                             (LPCTSTR)csModuleName,
                             (LPCTSTR)csCurrentVersion,
                             (LPCTSTR)csReadVersion);

        FreeOverrideDLL();
        return false;
    }

    m_logger->Log(TRACE_INFO, _T("Successfully loaded override DLL [%s]"),
                  (LPCTSTR)csModuleName);
    return true;
}

/**
 * Helper method to free override library handle.
 * \note Added for RFC 434653
 */
void XMode::FreeOverrideDLL(void)
{
    if(! m_hxm)
    {
        return;
    }

    m_logger->Log(TRACE_INFO, _T("Freeing library with handle 0x%0X"), m_hxm);
    (void)FreeLibrary(m_hxm);
    m_hxm = NULL;
}

/**
 * IMPORTANT NOTE:  This thread must not sleep, or the application
 *                    will hang waiting on the message queue (GetMessage).
 */
LRESULT CALLBACK GetMsgProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    PMSG pmessage = NULL;    // Message being sniffed.
    MessageElement *me = NULL;

    XMState *cstate = NULL; // Current state.

    //lint -e550 nstate not used now, but it may be used in the future.
    XMState *nstate = NULL; // New state.
    bool processed = false;

    if (nCode < 0)          // do not process message 
    {
        return CallNextHookEx(hhook, nCode, wParam, lParam); 
    }

    // Our stuff here.
    if(lParam)
    {
        pmessage = (PMSG)lParam;
        UINT msgID = pmessage->message;

        // TAR 442920:  Pass it on if message is not being removed from 
        //              the message queue.
        if(wParam != PM_REMOVE)
        {
            return CallNextHookEx(hhook, nCode, wParam, lParam); 
        }

        XMode *pXM = XMode::Instance();
        assert(pXM != NULL);
        if(! pXM)
        {
            g_logger->Log(TRACE_ERROR, 
                          _T("ERROR:  Failed to get XMode instance !!!"));
            return CallNextHookEx(hhook, nCode, wParam, lParam); 
        }

        // TAR 442920 / 438820
        // Make sure that the target window is the SCOT window.
        if(! pXM->IsMessageForUs(pmessage->hwnd))
        {
            return CallNextHookEx(hhook, nCode, wParam, lParam); 
        }

        TRACEAPI_EXT(_T("GetMsgProc"));

        // +RFC 396811:  Call event handler from main thread.
        if(msgID >= XMBUTTON1 && msgID < XMUNKNOWN)
        {
            assert(pXM->m_dwThreadID == GetCurrentThreadId() && 
                   pXM->m_hWndMain != (HWND)NULL);

            // This is a CM-only message, so don't pass it up the chain.
            pmessage->message = WM_NULL;  // Block the message.
            
            pXM->HandleEvent((XM_EVENT)msgID,pmessage->wParam,pmessage->lParam);
                             
            return 0;
        }
        // -RFC 396811

        // +SR777
        const long ID_SECMGR = 0x00040000;            // Security Manager Ocx
        const int SM_ONTRANSACTIONCOMPLETE = 1;
        // -SR777

        const long ID_GP = 0x00002000;                // General Purpose
        
        if(msgID == SCOT_MESSAGE && pmessage->lParam)
        {
            me = (MessageElement *)(pmessage->lParam);

            switch(me->id)
            {
            // +SR777
            case ID_SECMGR:             // Security Mgr.
                // Send balance to report server after transaction ends
                if(me->secmgrInfo.nEvt == SM_ONTRANSACTIONCOMPLETE)
                {
                    // TAR 442920:  We know we have a valid message handle now.
                    //              Mark our handle as valid.
                    pXM->m_hWndMain.SetValid();

                    pXM->BalanceUpdate();
                }
                break;
            // -SR777
            case ID_TRANSBROKER:
                g_logger->Log(TRACE_EXTENSIVE, _T("*** TB MESSAGE ***"));

                if(pXM)
                {
                    g_logger->Log(TRACE_EXTENSIVE, 
                                  _T(">> Aquiring state lock ...<<"));

                    CSingleLock singleLock(&stateLock, FALSE);  
                    
                    // Try to obtain the lock.  This thread cannot block
                    // indefinitely waiting on this lock.  If the lock 
                    // cannot be obtained immediately, then do not process
                    // the message.
                    // This is a design issue that will be addressed in a
                    // later release.
                    if(! singleLock.Lock(200))
                    {
                        assert(false);
                        g_logger->Log(TRACE_ERROR, 
                            _T("ERROR> FAILED TO PROCESS TB MESSAGE. Could not obtain state lock."));
                        g_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_INTERNAL_INTERFACE, 
                            XM_TB_MESSAGE_FAIL, 1, _T("*XMode Class* GetMsgProc"));
                        break;
                    }


                    // We have the lock.  Allow message to be processed.
                    g_logger->Log(TRACE_EXTENSIVE, 
                                   _T(">> Aquired state lock ...<<"));

                    // Pass the event to the CM state machine.  
                    cstate = pXM->GetCurrentState();
                    nstate = cstate->XTBParse(me, processed);

                    (void)singleLock.Unlock();

                    // +TAR 411914 - Change state if calls are on the main 
                    //               thread.  If calls are not on the main 
                    //               thread, then COM calls can cause a 
                    //               deadlock. 
                    if(pXM->m_bMainCall && cstate != nstate)
                    {
                        pXM->SetCurrentState(nstate);
                    }
                    // -TAR 411914
                }
                break;

            case ID_GP:     // TAR 414908:  Check for SCOTApp shutdown.
                if (me->gpInfo.nEvt==GP_STOP || 
                    me->gpInfo.nEvt==GP_STOPSCOT)
                {
                    g_logger->Log(TRACE_INFO, 
                          _T("Detected SCOTApp shutdown [%s]"),
                          me->gpInfo.nEvt == GP_STOP ? _T("GP_STOP") :
                                                       _T("GP_STOPSCOT"));

                    pXM->HandleEvent(XMHIDE, 0, 0);
                }
                break;
            default:
                ;   
            }
        }
    }

    if(! processed)
        return CallNextHookEx(hhook, nCode, wParam, lParam); 
    else
    {
        if(pmessage)
        {
            pmessage->message = WM_NULL;  // Block the message.
        }
        return 0;
    }
}

// TAR 408521:  Initialize the base CM implementation.
//              See header for details on this function.
XMState * XMBaseInitialize(PSXWrapper *psx, XMCashDevice *xmd,XMConfig *cfg,
                            IXMLoginDevice *pDevLogin)  //TAR 448426
{
    assert(psx != NULL && xmd != NULL && cfg != NULL);
    assert(XMStateBase::m_psx == NULL && XMStateBase::m_xdev == NULL);

    XMStateBase::m_psx = psx;
    XMStateBase::m_xdev = xmd;
    XMStateBase::m_cfg = cfg;
    XMStateBase::m_logger = CLog::GetLog();
	XMStateBase::m_xDevLogin = pDevLogin;   //TAR 448426

    TRACEAPI(_T("XMBaseInitialize"));

    // Make sure that we have been given all of the tools that we'll need.
    if(! (psx && xmd && cfg))
    {
        CLog::GetLog()->Log(TRACE_ERROR, _T("ERROR: NULL pointers passed in!"));
        CLog::GetLog()->LogXMEvent(evlClassBasic, evlTypeError, 
                                   XM_CAT_INTERNAL_INTERFACE, 
                                   XM_NULL_POINTER_IN_ERROR, 1, 
                                   _T("XMState * XMBaseInitialize"));
        return NULL;
    }

    XMStateBase::m_tb = TBWrapperBase::Instance();
    if(! XMStateBase::m_tb)
        return NULL;

    // SR752+
    XMStateBase::m_stateFactory = new BaseStateFactory();     
    XMStateBase::m_stateFactory->Initialize();
    // SR752-


    RETURN_STATE(STATE_BASE);
}

// TAR 408521:  UnInitialize the base CM implementation.
//              See header for details on this function.
void XMBaseUnInitialize()
{
    TraceAPI *t = new TraceAPI(_T("XMBaseUnInitialize"));

    XMStateBase::m_psx = NULL;
    XMStateBase::m_xdev = NULL;
    XMStateBase::m_cfg = NULL;
    XMStateBase::m_logger = NULL;

    TBWrapperBase::Destroy();
    XMStateBase::m_tb = NULL;

	XMStateBase::m_xDevLogin = NULL;    //TAR 448426
	
    delete t;

    // SR752+
    XMStateBase::m_stateFactory->UnInitialize();

    delete XMStateBase::m_stateFactory;
    XMStateBase::m_stateFactory = NULL;
    // SR752-
}

/**
 * RFC 2774 - Ability to lock CM
 * \brief This will call XAuthEvent() on the current state
 * \return the assigned state.
 */
void XMode::XAuthEvent()
{
    TRACEAPI(_T("XMode::XAuthEvent"));

    assert(m_cstate != NULL);
    if(m_cstate != NULL)
    {
        m_cstate->XAuthEvent();
    }
}

bool XMode::IsMessageForUs(const HWND hTarget) 
{
#ifdef _DEBUG
    TRACEAPI_EXT(_T("XMode::IsMessageForUs"));
#endif

    // If hTarget is NULL, then this message is not one we are interested in.
    if(hTarget == NULL)
    {
        return false;
    }

    // Initialize window handle if necessary.
    if(m_hWndMain == (HWND)NULL)
    {
        (void)m_hWndMain.Initialize();
    }

    // TAR 438820
    // Check that the message is really destined for the window we are 
    // concerned about.
    if(m_hWndMain != hTarget)
    {
        m_logger->Log(TRACE_EXTENSIVE,
             _T("Message is for [0x%0X].  Not Scot [0x%0X]. Ignoring."),
             hTarget, (HWND)m_hWndMain);

        // Just in case we don't have a valid window handle,  Clear it so that 
        // it can be obtained again.
        if(! m_hWndMain.IsValid())
        {
            m_hWndMain.Reset();
        }

        return false;
    }

    // Message appears to be for us.
    m_logger->Log(TRACE_EXTENSIVE, _T("Message for [0x%0X] is a SCOT_MESSAGE"),
                  hTarget);
    return true;
}

/**
 * RFC 2774 - Ability to lock CM
 * \brief This will be called during lock screen timeout.
 *        This method sets the current state to lock state and post XMDELAYEDINIT event.
 *        This event will be processed when the main thread is finished.
 * \return void.
 */
void XMode::HandleLockTimeout(void) 
{
    TRACEAPI_EXT(_T("XMode::HandleLockTimeout"));

    assert(m_cstate != NULL);

    if ( m_cstate && !m_cstate->IsSystemLocked())
    {
        CSingleLock singleLock( &stateLock, TRUE ); 

        assert(m_pLockState != NULL);
        m_pLockState->SetConfirmation(false);    //< Don't ask for confirmation.
        m_pLockState->SetPrevState(m_cstate);

        m_pLockState->SetSystemLocked(true);

        m_cstate = m_pLockState;
        SetCurrentState(m_pLockState);          //< Be sure to set the current state to m_pLockState...
    }
    return;
}


/**
 * RFC 2774 - Ability to lock CM
 * \brief Handle change to login screen after lock screen.
 */
void XMode::HandleDelayedInit(void)
{
    assert(m_cstate == m_pLockState );
    assert(m_dwThreadID == GetCurrentThreadId());

    if(m_cstate != m_pLockState )
    {
        m_logger->Log(TRACE_ERROR, 
            _T("ERROR: Current state is not lock state."));
        return;
    }

    if(m_dwThreadID != GetCurrentThreadId())
    {
        m_logger->Log(TRACE_ERROR, 
            _T("ERROR: Not on main thread. Unable to process delayed initialization."));    
        return;
    }

    // Uninitialize the previous state and change to the lock password state.
    assert(m_pLockState != NULL);
    XMState * const pPrevState = m_pLockState->GetPrevState();
    XMState * const pNextState = m_pLockState->GetNextState();

    bool bIsLocked = m_pLockState->IsSystemLocked();    //< See if still locked or not.

    assert(pPrevState != NULL && pNextState != NULL);
    if(pPrevState == NULL || pNextState == NULL)
    {
        assert(false);
        return;
    }

    pPrevState->UnInitialize();                    //< Performing delayed state change.
    m_pLockState->SetPrevState(NULL);            //< Done with m_pLockState for now, right?

    m_pLockState->SetSystemLocked(false);        //< Temporarily unlock so we can change state.

    SetCurrentState(pNextState);                //< Change to XMLockPassword.  This should uninitialize XMLockScreen

    if(bIsLocked)                                //< If we were locked before, then restore.
    { 
        m_pLockState->SetSystemLocked(bIsLocked);    //< Lock back up.
    }
}

//TAR 448426
void XMode::PostFPEvent(XM_EVENT pEvent, long lParam, long wParam)
{
    TRACEAPI_EXT(_T("XMode::PostFPEvent"));

    assert(0);      // Not implemented.
    return;
}

//TAR 448426 
void XMode::HandleFPEvent(XM_EVENT pEvent, 
                               long lParam,
                               long wParam,
                               bool bFPConnected)
{
    TRACEAPI_EXT(_T("XMode::HandleFPEvent"));

    m_xmDevLogin->SetFPDeviceConnected(bFPConnected);

    //Post this event. If not on the main thread then 
    //the HandleEvent() method repost it back. if we're on the main 
    //thread now then pass event to the current state.
    HandleEvent(XMFPEVENT, 0, 0);

    if(m_dwThreadID != GetCurrentThreadId())
    {
        //Post back PS message to scotapp
        PostFPEvent(pEvent,lParam,wParam);
        return;
    }
}

	//SSCOP-10053 Integrate OptiCash Client into Cash Management
void XMode::InitOptiCashClient(void)
{
	m_OCWrapper = NULL;

	typedef IOptiCashClient* (*pFGetWrapperObject)();

	//Define function pointers to Release Wrapper Object
	typedef void(*pFReleaseWrapperObject)();

	//Instance of GetWrapperObject function pointer
	pFGetWrapperObject m_fpWrapperObject;

	//Instance of Release Wrapper Object function pointer
	pFReleaseWrapperObject m_fpReleaseWrapperObject;

	//Get Wrapper Object
	//IOptiCashClient* m_OCWrapper;

	m_OChandle = NULL;
	m_OChandle = LoadLibrary(_T("c:\\scot\\dll\\OptiCashWrapper.dll"));


	if (m_OChandle == NULL)
	{

	    return;
	}

	//Get the Wrapper Object. 
	m_fpWrapperObject = reinterpret_cast<pFGetWrapperObject>(GetProcAddress(m_OChandle, "GetOptiCashWrapperObject"));
	if (!m_fpWrapperObject)
	{
		return;
	}

	m_fpReleaseWrapperObject = reinterpret_cast<pFReleaseWrapperObject>(GetProcAddress(m_OChandle, "ReleaseWrapperObject"));
	if (!m_fpReleaseWrapperObject)
	{
		return;
	}

	
	m_OCWrapper = m_fpWrapperObject();



	return;

}

