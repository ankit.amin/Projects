#ifndef _XMODE_H
#define _XMODE_H

#include "StateMonitor.h"
#include "PSXMonitor.h"
#include "XMExport.h"
#include "XMReporting.h"
#include "WinHandleWrapper.h"   // TAR 442920
#include "ScotMsg.h"            // Need MessageElement for TB message intercept.
#include "IOptiCashClient.h"  //LFK OC test

class PSXWrapper;
class XMState;
class CLog;
class XMCashDevice;
class IDMCashCounts;   //SSCOP-3544
class XMConfig;
class IDMWrapper;
class XMDepositTimer;
class CurrencyWrapper;
class XMLockScreenBase;         //RFC 2774 - Ability to lock CM 
class IXMLoginDevice;           //TAR 448426

enum XM_EVENT                   // RFC 396811.  Message identifiers. 
{
    XMBUTTON1 = WM_USER + 500,
    XMBUTTON2,
    XMBUTTON3,
    XMBUTTON4,
    XMBUTTON5,
    XMBUTTON6,
    XMBUTTON7,
    XMBUTTON8,
    XMBUTTON9,//RFQ 432305
    XMBUTTON10,//RFQ 432305
    XMBUTTON,
    XMDMACCEPTOR,
    XMRECEIPTSCROLLUP,      
    XMRECEIPTSCROLLDOWN,
    XMKEYSTROKE,
    XMKEYENTER,
    XMKEYCLEAR,
    XMCURRENCYEVENT,
    XMSHOW,
    XMHIDE,
    XMBALANCEREQUEST,    //SR777
    XMLOCKSCREEN,                // RFC 2774 - Ability to lock CM
    XMDELAYEDINIT,                // RFC 2774 - Ability to lock CM
	XMFPEVENT,                  //TAR 448426
    XMKEYSHIFT,
    XMKEYCHAR,
    XMUNKNOWN                   // New message identifiers above XMUNKNOWN. 
};

/**
 * \class XMode
 * \brief   The main driver of the state machine in the XMState dll.
 * \brief   This class manages the state machine of XMState and provides 
 * \brief   common services to the XMState dll (XMCashDevice, PSXWrapper etc.).
 * \brief   A singleton instance of XMode is retrieved by DeviceManager.ocx.
 * \author Jeff Vales
 * \author Copyright (c) NCR Corp. 2008,2009
 *
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of NCR Corporation. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with NCR.
 */
//lint -e1510
class XMode : public StateMonitorCallback, public PSXMonitorCallback 
{
    // +FRIENDS
    // The Instance() method for this singleton class is a protected member.
    // External access to this class is limited to the following entities.
    // This class should not be used directly by any classes that derive from
    // XMState.  New friends should be added only if there is a good reason
    // for doing so.
    friend LRESULT CALLBACK GetMsgProc(int, WPARAM, LPARAM);
    friend XMODE_API bool InitializeXM(DWORD, HWND);
    friend XMODE_API void PreShutdownXM(void);
    friend XMODE_API void XMCurrencyDeposited(LPCSTR, bool);
    friend XMODE_API void XMCurrencyDispensed(LPCSTR);
    friend XMODE_API void XMDeviceEvent(UINT, long, bool &, long, long);

    /**
     * Pass device event down to CM.
     * \param[in] DM Info MessageElement for the event.
     * \param[out] bProcessed.  Set to true if CM handled the event.  false
     *               otherwise.  If set to true, then SCOTApp should ignore
     *               the event.
     * \note Added for TAR 449776.
     */
    friend XMODE_API void XMDeviceEventEx(MessageElement &rMsg, 
                                          bool &bProcessed); 

    friend XMODE_API bool XMProcessCashCounts(BSTR FAR* ); //Report F53
	friend XMODE_API bool XMGetCashCounts(CString &csDispenserCounts, CString &csAcceptorCounts); //TAR 448434
	friend XMODE_API long XMGetDispenserCapacity(long denom); //TAR 450643
    friend XMODE_API BOOL TBValidateOperatorHook(LPCTSTR, LPCTSTR);
    friend XMODE_API BOOL TBValidateHeadCashierHook(LPCTSTR, LPCTSTR);    //RFC 2774 - Ability to lock CM
	friend XMODE_API void SetCredentials(LPCTSTR szOperatorID, LPCTSTR szOperatorPw); //TAR 448426
                             
    friend class CTBInfo;
    friend class XMDepositTimer;
    friend class XMLockTimer;    ///< RFC 2774 - Ability to lock CM
	friend class XMCashReplenishBase;
	friend class XMStateBase;
    // -FRIENDS
protected:
    static XMode *m_xm;         ///< Singleton instance of this class.
    StateMonitor *m_sm;         ///< For monitoring of FL state.
    PSXWrapper *m_psx;          ///< For controlling local display.
    XMState *m_cstate;          ///< Current state.

    HINSTANCE m_hxm;            ///< Handle to XMState dll.
    XMCashDevice *m_cdev;       ///< Currency device wrapper used by XMState dll

    IDMWrapper *m_dm;           ///< Provides access to devmgr methods.
    IDMCashCounts *m_dmcc;       ///< Tracks currency counts.

    CurrencyWrapper *m_cntacc;  ///< Wrapper for acceptor counts.
    CurrencyWrapper *m_cntdsp;  ///< Wrapper for dispenser counts.

    XMConfig *m_cfg;            ///< Singleton configuration object.
    XMDepositTimer *m_dtimer;   ///< Time buffer deposit events.

    XMReporting *m_reporting ;  ///< Reporting
    bool m_bInTarget;           ///< true when in cash mgmnt.
    CLog *m_logger;

    bool m_bBalanceChanged;     ///< true when currency dispensed or accepted.
    bool m_bSendBalanceUpdates; ///< If false, no balance updates
    
    bool m_bUseLowSensorAutoCount; ///<option UseLowSensorAutoCount //Report F53
    
    //+RFC 2774 - Ability to lock CM      
    XMLockTimer *m_ltimer;
    XMLockScreenBase *m_pLockState;    
    //-RFC 2774 - Ability to lock CM
    
    long m_lLockTimeout;        ///<Number of milliseconds to obtain the lock

    // +TAR 455878:  Performance workaround.  Cache the acceptor and dispenser/
    //   recycler counts.  Final fix needs to be done in SCOTApp.

    // Cached cash box, coin overflow, cash acceptor,or coin acceptor counts.
    CurrencyWrapper *m_pCachedNonDispCounts;  

    // Cached recycler or dispenser counts. 
    CurrencyWrapper *m_pCachedDispCounts;     

    CurrencyWrapper *m_pScratch;        // Scratch pad.
    // -TAR 455878

	//SSCOP-10053 Integrate OptiCash Client into Cash Management
	IOptiCashClient* m_OCWrapper;
	HMODULE m_OChandle;


    XMode(void);
    XMode(const XMode &rhs);
    XMode & operator=(const XMode &rhs);

    static XMode *Instance(void);   ///< Access singleton.

    virtual ~XMode();
    virtual bool Initialize(void);
    virtual void UnInitialize();

    //+SR700
    virtual XMState * const GetCurrentState(void) const;
    virtual void SetCurrentState(XMState *xm);

    virtual bool PreInitCheck(bool bCheck);        // TAR 378792.
    virtual bool LoadOverrideDLL(const CString &csModuleName);  // RFC 434653
    virtual void FreeOverrideDLL(void);                         // RFC 434653
	
    // Refactor XMode initialization code to make it less monolithic.
    virtual bool InitPSX(void);
    virtual bool InitStateMonitor(void);
    virtual bool InitDMWrapper(void);
    virtual bool InitReporting(void);
    virtual bool InitCashCounts(void);
    virtual bool SetBaseState(void);
    //-SR700
    
		//SSCOP-10053 Integrate OptiCash Client into Cash Management 
	virtual void InitOptiCashClient(void);

    /**
     * RFC 2774 - Ability to lock CM
     * \brief This method handles the delayed initialization of lock state
     * \return void
     */
    virtual void HandleDelayedInit(void);

    // +RFC 396811
    const DWORD m_dwThreadID;       // Thread module was initialized on.
    CWinHandleWrapper m_hWndMain;   // HWND of the main dialog.
    const bool m_bMainCall;         // If true, all calls on main thread.
    // -RFC 396811
	
	IXMLoginDevice *m_xmDevLogin;       // TAR 448426
	
public:
    IDMWrapper *GetDMWrapper();

    // FIXME Only UnInitializeXM should be allowed.
    static void Destroy(void);        ///< Destroy singleton.

    /**
     * Notification of currency deposit.
     * Called by device manager.
     */
    virtual void CurrencyDeposited(LPCSTR val, bool note=false);       

    /**
     * Notification that currency has been dispensed.
     * \param[in] dstr - Cash count string of dispensed currency.
     */
    virtual void CurrencyDispensed(LPCSTR dstr);       

    /**
     * Notification of event from device manager.
     * \note Future use.  Currently not used.
     * \param[in] evntType - Type of event (status, error etc.)
     * \param[in] wParam - Event argument for this event.
     * \param[in] lParam - Event argument for this event.
     * \param[out] handled - If the event was handled by this method, then 
     *             this parameter is set to true.  In this case, the caller
     *             would not process this event.  If this parameter is false,
     *             then this method did not handle the event and normal 
     *             processing will occur.
     * \param[in]  deviceClass  Device class of the source device.
     * \param[in]  devID - Device identifier.
     */
    virtual void DeviceEvent(UINT evntType, long lParam, bool &handled, 
                             long devClass, long devID=0);

    /**
     * Pass device event down to CM.
     * \param[in] DM Info MessageElement for the event.
     * \param[out] bProcessed.  Set to true if CM handled the event.  false
     *               otherwise.  If set to true, then SCOTApp should ignore
     *               the event.
     * \note Added for TAR 449776.
     */
    virtual void DeviceEvent(MessageElement &rMsg, bool &bProcessed);

    /**
     * Notify XMState that currency has been deposited.
     */
    virtual void FireCurrencyEvent(void);
   
    // StateMonitorCallback IF.
    virtual void StateTrigger(LPCTSTR sname, bool toggle);

    virtual void PSXTrigger(LPCTSTR szControlName, // PSXMonitorCallback IF.
                            LPCTSTR szContextName, 
                            LPCTSTR szEventName, long buttonID=0); 

    virtual void HandleEvent(XM_EVENT evt, WPARAM wParam, LPARAM lParam);

    /**
     * Wrapper for windows PostMessage that allows override to customize how
     * messages are sent.
     */
    virtual void PostEvent(UINT Msg, LPARAM lParam); //SR700

    DWORD GetMainThreadID(void) const;      // RFC 408854
    virtual void BalanceUpdate(void); //SR777
    
    /* RFC 409914
     * Method to get the current balance by passing back the CurrencyWrapper class
     * \param[out] ndisp - a non-dispensable CurrencyWrapper instance
     * \param[out] disp - a dispensable CurrencyWrapper instance
     * \return true on success.  false on failure.
     */
    virtual bool GetCurrentBalance(CurrencyWrapper &ndisp, 
                                   CurrencyWrapper &disp);

    /*
     * bool ProcessCashCounts(BSTR FAR *pCashCounts) const
     * Description: Replace incorrect note counts from cash management
     *
     * \param [out] pCashCounts     - pointer to the device cash count string, 
     *                note counts saved in this variable will be replaced 
     *                by note counts from cash management
     * \Return true if pCashCounts was successfully changed. 
     *             Else, return false if pCashCounts was not changed or an error was encountered.
     */    
    virtual bool ProcessCashCounts(BSTR FAR *pResult) const; //Report F53
    
    /**
     * \Getting the cash counts.
     * \param[out] csDispenserCounts - Gets the cash acceptor count string
     * \param[out] csAcceptorCounts - Gets the coin acceptor count string
     */
    virtual bool GetCashCounts(CString &csDispenserCounts, CString &csAcceptorCounts); // TAR 448434

	/**
     * Getting the dispenser cash capacity from the device
     * \param[in] denom - the denomination of the cash list
     */
    virtual long GetDispenserCapacity(long denom); // TAR 450643

    /**
     * RFC 2774 - Ability to lock CM
     * \brief This method is called to handle lock screen timeout
     * \return void
     */    
    virtual void HandleLockTimeout(void);

    /**
     * RFC 2774 - Ability to lock CM
     * \brief This will call XAuthEvent() on the current state
     * \return void
     */
    virtual void XAuthEvent(void);

    /**
     * Try to determine if a received message is of interest to us or not.
     * \return true if message is for our window handle.  false otherwise.
     * \param[in] hTarget - The target window for the message.
     * \note Added for TAR 442920.
     */
    virtual bool IsMessageForUs(const HWND hTarget);
	
	/**
     * \brief Post PS message back to scotapp
     * \param [in] pEvent - (KEYSTROKE)to be placed in the event
     * \param [in] lParam - to be placed in the event, if any
     * \param [in] wParam - (ENTERKEY)to be placed in the event
     * \return void
     * \note Added TAR 448426
     */
    virtual void PostFPEvent(XM_EVENT pEvent, long lParam, long wParam); 
	
	/**
     * \brief This method handles fingerprint event comes from scotapp
     * \param [in] pEvent - (KEYSTROKE)to be placed in the event
     * \param [in] lParam - to be placed in the event, if any
     * \param [in] wParam - (ENTERKEY)to be placed in the event
	 * \param [in] bFPConnected - holder to determine if fp device is connected
     * \return void
     * \note Added for TAR 448426
     */
    virtual void HandleFPEvent(XM_EVENT pEvent, 
                               long lParam,
                               long wParam, 
							   bool bFPConnected);
};

/**
 * Initialize/get XMode instance.
 * \param[in] gitcookie - Identifier to get device mgr interface pointer.
 * \param[in] hwnd - Optional window handle of caller.
 * \return    true on success.  false on failure.
 * \note      The XMode DLL handles allocation and freeing of the returned
 *            XMode pointer.
 */
XMODE_API bool InitializeXM(DWORD gitcookie, HWND hwnd=NULL);  
XMODE_API void UnInitializeXM(void);        ///< UnInitialize module.
XMODE_API void PreShutdownXM(void);       

/**
 * Method to initialize the base state machine if an
 * override DLL is not used.
 * \param[in] psx - Pointer to PSX display class.
 * \param[in] xmd - Pointer to cash device interface.
 * \param[in] cfg - Pointer to configuration class.
 * \param[in] pDevLogin - Pointer to login device interface.
 * \return The initial state of the state machine on success and NULL on
 *         failure.
 * \note Added for TAR 408521
 */
XMState * XMBaseInitialize(PSXWrapper *psx, XMCashDevice *xmd,XMConfig *cfg,
                           IXMLoginDevice *pDevLogin);   //TAR 448426

/**
 * Method to un-initialize the base state machine if an
 * override DLL is not used.
 *
 * \note Added for TAR 408521
 */
void XMBaseUnInitialize(void);

/**
 * Callback for currency deposit events.
 * \param[in] val - Currency code + denomination (ex. USD5).
 * \param[in] note - If true, a note was deposited.  If false, a coin.
 */
XMODE_API void XMCurrencyDeposited(LPCSTR val, bool note=false);       

/**
 * Callback for currency dispense events.
 * \param[in] dstr - Cash count string of dispensed currency.
 */
XMODE_API void XMCurrencyDispensed(LPCSTR dstr);       

/**
* TAR 448434 
* \Getting the cash counts.
* \param[out] csDispenserCounts - Gets the dispenser count string
* \param[out] csAcceptorCounts - Gets the acceptor count string
*/
XMODE_API bool XMGetCashCounts(CString &csDispenserCounts, CString &csAcceptorCounts);
 
/**
* TAR 450643
* Getting the dispenser cash capacity from the device
* \param[in] denom - the denomination of the cash list
*/
XMODE_API long XMGetDispenserCapacity(long denom); 
    

/**
 * Notification of event from device manager.
 * \note Future use.  Currently not used.
 * \param[in] evntType - Type of event (status, error etc.)
 * \param[in] wParam - Event argument for this event.
 * \param[in] lParam - Event argument for this event.
 * \param[out] handled - If the event was handled by this method, then 
 *             this parameter is set to true.  In this case, the caller
 *             would not process this event.  If this parameter is false,
 *             then this method did not handle the event and normal 
 *             processing will occur.
 * \param[in]  deviceClass  Device class of the source device.
 * \param[in]  devID - Device identifier.
 */
XMODE_API void XMDeviceEvent(UINT evntType, long lParam, bool &handled, long devClass, long devID=0);

#endif // _XMODE_H
