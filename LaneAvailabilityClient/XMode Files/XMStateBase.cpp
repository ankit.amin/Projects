#include "stdafx.h"
#include "XMStateBase.h"
#include "XMLog.h"

#include "PSXWrapper.h"
#include "TBWrapper.h"
#include "XMCashManagementBase.h"

// +SR777
#include "CurrencyWrapper.h"
#include "XMReporting.h"
#include "XMCashDevice.h"
// -SR777
#include "XMScreenUpdate.h" // SR969

#include "IXMPrinterEx.h"         // TAR 443464
#include "ITBInfo.h"              // TAR 443464

// Copyright (c) NCR Corp. 2008,2009,2010

PSXWrapper * XMStateBase::m_psx = NULL;
XMCashDevice * XMStateBase::m_xdev = NULL;

CLog * XMStateBase::m_logger = NULL;
TBWrapper *XMStateBase::m_tb = NULL;
XMConfig *XMStateBase::m_cfg = NULL;
bool XMStateBase::m_bIsSystemLocked = false;    //RFC 2774 - Ability to lock CM
bool XMStateBase::m_bIsSystemBusy = false;
IXMLoginDevice * XMStateBase::m_xDevLogin = NULL;   //TAR 448426
IStateFactory * XMStateBase::m_stateFactory = NULL;	// SR752
bool XMStateBase::m_bWaitForTB = false;	// SR752 
bool XMStateBase::m_bExitToApp = false;


XMState * XMStateBase::XDMAcceptor()
{
    TRACEAPI(_T("XMStateBase::XDMAcceptor"));

    // RFC 410499:  Moved code to XMCashierPasswordBase.
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton1()
{
    TRACEAPI(_T("XMStateBase::XPSButton1"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton2()
{
    TRACEAPI(_T("XMStateBase::XPSButton2"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton3()
{
    TRACEAPI(_T("XMStateBase::XPSButton3"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton4()
{
    TRACEAPI(_T("XMStateBase::XPSButton4"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton5()
{
    TRACEAPI(_T("XMStateBase::XPSButton5"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton6()
{
    TRACEAPI(_T("XMStateBase::XPSButton6"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton7()
{
    TRACEAPI(_T("XMStateBase::XPSButton7"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton8()
{
    TRACEAPI(_T("XMStateBase::XPSButton8"));
    return STATE_NULL;
}

//+ RFQ 432305
XMState * XMStateBase::XPSButton9()
{
    TRACEAPI(_T("XMStateBase::XPSButton9"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton10()
{
    TRACEAPI(_T("XMStateBase::XPSButton10"));
    return STATE_NULL;
}
//- RFQ 432305

XMState * XMStateBase::XPSKeyShift()
{
    TRACEAPI(_T("XMStateBase::XPSKeyShift"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSCharKey(CString csChar)
{
    TRACEAPI(_T("XMStateBase::XPSCharKey"));
    CLog::GetLog()->Log(TRACE_INFO, _T("XPSCharKey %s was pressed."), csChar);
    return STATE_NULL;
}

XMState * XMStateBase::XPSButton(long buttonID)
{
    TRACEAPI(_T("XMStateBase::XPSButton"));
    CLog::GetLog()->Log(TRACE_INFO, _T("Button %d was pressed."), buttonID);
    return STATE_NULL;
}

XMState * XMStateBase::XPSReceiptUp()
{
    TRACEAPI(_T("XMStateBase::XPSReceiptUp"));
    (void)m_psx->GenerateEvent(_T("SMReceiptScrollUp"), UI::EVENTCLICK);
    return STATE_NULL;
}

XMState * XMStateBase::XPSReceiptDown()
{
    TRACEAPI(_T("XMStateBase::XPSReceiptDown"));
    (void)m_psx->GenerateEvent(_T("SMReceiptScrollDown"), UI::EVENTCLICK);
    return STATE_NULL;
}

XMState * XMStateBase::XPSKeyStroke(long buttonID)
{
    TRACEAPI(_T("XMStateBase::XPSKeyStroke"));
    CLog::GetLog()->Log(TRACE_INFO, _T("Button %d was pressed."), buttonID);
    return STATE_NULL;    
}

XMState * XMStateBase::XPSEnterKey(void)
{
    TRACEAPI(_T("XMStateBase::XPSEnterKey"));
    return STATE_NULL;
}

XMState * XMStateBase::XPSClearKey(void)
{
    TRACEAPI(_T("XMStateBase::XPSClearKey"));
    return STATE_NULL;
}

XMState * XMStateBase::XTBParse(MessageElement *me, bool &processed)
{
    TRACEAPI_EXT(_T("XMStateBase::XTBParse"));
    assert(me != NULL); //lint !e1776

    processed = false;

    // TAR 443464 - Moved handling of print messages to parent class.
    IXMPrinter *pPrint = IXMPrinterEx::Instance();
    if (pPrint && pPrint->IsPrintMessage(me))
    {
        // This will make TBProcessMessageHook return TB_COMPLETEPRINTANDCUT.
        ITBInfo *pInf = ITBInfo::Instance();
        if (pInf != NULL)
        {
            pInf->SetTBSTATE(TB_COMPLETEPRINTANDCUT);
        }
    }
    else    // Just log.
    {
        m_logger->Log(TRACE_EXTENSIVE, _T("nDeviceID: %d; pData length: %d"),
                    me->nDeviceID, me->nLength);
    }

	// SR752+: Only check the TB message if we are expecting 
	// a response from TB.
    if(WaitForTBResponse()) 
    {
        int nTBState = m_tb->ProcessMessage(me);

        if(nTBState != TB_CASHMANAGEMENTCOMPLETED) 
        {
            ITBInfo *pInf = ITBInfo::Instance();
            if (pInf != NULL)
            {
                pInf->SetTBSTATE(nTBState);
            }
            processed = false;
        }
        else
        {
            m_logger->Log(TRACE_INFO, 
				_T("Received a loan or pickup response from the TB."));
            processed = true;
            return HandleTBResponse();
        }
    }
    // SR752-

    return STATE_NULL;
}

XMState * XMStateBase::XDMParse(UINT evntType, long lParam, bool &handled,
                                long devClass, long devID)
{
    TRACEAPI_EXT(_T("XMStateBase::XDMParse"));
    handled = false;
    return STATE_NULL;
}   //lint !e715 

XMState * XMStateBase::XDMParse(MessageElement &rMe, bool &bProcessed)
{
    TRACEAPI_EXT(_T("XMStateBase::XDMParse(MessageElement)"));

    bProcessed = false;

    // TAR 449776:  Let the device make the decision.
    bProcessed = m_xdev->HandleDeviceEvent(rMe, *m_psx);

    return STATE_NULL;
}


// +SR777
XMState * XMStateBase::XBalanceUpdate(CurrencyWrapper &disp,
                                      CurrencyWrapper &ndisp)
{
    TRACEAPI(_T("XMStateBase::XBalanceUpdate"));

    m_logger->Log(TRACE_EXTENSIVE, _T("Total Balance for Dispensable: [%s]"), (LPCTSTR)disp);
    m_logger->Log(TRACE_EXTENSIVE, _T("Total Balance for Nondispensable: [%s]"), (LPCTSTR)ndisp);

    XMReporting *reporting = XMReporting::GetReporting();

    // Pass balance data to XMReporting to send to the report server.
    bool bRet = reporting->CashBalance(disp, ndisp, 
                                       m_xdev->IsRecycler(CW_COINS), 
                                       m_xdev->IsRecycler(CW_NOTES));




	SendBalanceToOptiCash(disp, ndisp);
	


    // Transfer Report Files to Server
    if (bRet)
    {
        (void)reporting->TransportFiles() ;
    }




	

    //(+) SR969  
    XMScreenUpdate* pScrup = XMScreenUpdate::Instance(); 
    assert(pScrup != NULL);
    pScrup->OnBalanceUpdate();
    //(-) SR969

    return STATE_NULL;  // Stay in the same state.
}
// -SR777

bool XMStateBase::CheckTrainingMode(void)
{
    COleVariant trainingMsg;
    (void)m_psx->GetTransactionVariable(_T("TrainingMessage"), trainingMsg);
    CString msg;
    msg = trainingMsg.bstrVal;
    m_logger->Log(TRACE_EXTENSIVE, _T("Training Message: %s"), msg);
    if(msg != _T(""))
    {
        (void)m_psx->SetControlProperty(_T("XMATModeDisplay1"), 
                                        UI::PROPERTYVISIBLE, VARIANT_TRUE);
        (void)m_psx->SetControlProperty(_T("XMATModeDisplay2"), 
                                        UI::PROPERTYVISIBLE, VARIANT_TRUE);
        
        int maxcnt = MAX_PATH;
        TCHAR szTxt[MAX_PATH];
        (void)m_psx->GetString(_T("XM_Attendant"), szTxt, maxcnt);

        if(msg.Find(szTxt) == -1)//either in Training Mode or Attd/Train Mode
            return true;
    }
    else
    {
        (void)m_psx->SetConfigProperty(_T("XMATModeDisplay1"), 
                                       UI::PROPERTYVISIBLE, VARIANT_FALSE);
        (void)m_psx->SetConfigProperty(_T("XMATModeDisplay2"), 
                                       UI::PROPERTYVISIBLE, VARIANT_FALSE);
    }
    return false;
}

//+RFC 2774 - Ability to lock CM
void XMStateBase::XAuthEvent(void)
{
    TRACEAPI(_T("XMStateBase::XAuthEvent"));

    return;
}

bool XMStateBase::IsSystemLocked(void)
{ 
    return m_bIsSystemLocked; 
}

void XMStateBase::SetSystemLocked(bool bLock)
{
    CString csTraceSystemLocked;

    //lint -e730
    csTraceSystemLocked.Format(_T("XMStateBase::SetSystemLocked %d"), bLock);
    TRACEAPI(csTraceSystemLocked);

    m_bIsSystemLocked = bLock;

}
//-RFC 2774 - Ability to lock CM

//+SR700
XMState * const XMStateBase::SetBaseState(int index)
{
    CString csTrace;
    csTrace.Format(_T("XMStateBase::SetBaseState(%d)"), index);

    TRACEAPI(csTrace);


    if(index >= STATE_END)
    {
        m_logger->Log(TRACE_ERROR, 
                      _T("ERROR:  index %d > max index of %d"), 
                      index, STATE_END -1);
        return NULL;
    }

    // SR752+
    STATE(index);
    assert(m_stateFactory->GetState(index) != NULL);
    // SR752-

    return m_stateFactory->GetState(index);     // SR752
}

XMState * const XMStateBase::GoToBaseState(void)
{
    TRACEAPI(_T("XMStateBase::GoToBaseState"));

    XPSButton8();       // Exit state.

    assert(m_stateFactory->GetState(STATE_BASE) != NULL);   // SR752
    return m_stateFactory->GetState(STATE_BASE);            // SR752
}
//-SR700

/**
 * Set local context.
 */
bool XMStateBase::SetContext(LPCTSTR szContext)
{
    assert(szContext != NULL);  //lint !e1776

    if(szContext == NULL)       //lint !e774
    {
        return false;
    }
 
    return m_psx->SetContext(szContext);
}

bool XMStateBase::IsSystemBusy(void)
{ 
    return m_bIsSystemBusy; 
}

void XMStateBase::SetSystemBusy(bool bSystemBusy)
{
    CString csTraceSystemBusy;
    csTraceSystemBusy.Format(_T("XMStateBase::SetSystemBusy %d"), bSystemBusy);
    TRACEAPI(csTraceSystemBusy);

    m_bIsSystemBusy    = bSystemBusy;
}

//TAR 448426
XMState * XMStateBase::HandleFPData(void)
{
    TRACEAPI_EXT(_T("XMStateBase::HandleFPData"));

    return STATE_NULL;
}

// SR752+
bool XMStateBase::WaitForTBResponse(void)
{ 
    return m_bWaitForTB; 
}

// Signal that we're waiting for a response.
void XMStateBase::SetWaitForTBResponse(bool bWaitForTB)
{
    CString csWaitForTB;
    csWaitForTB.Format(_T("XMStateBase::SetWaitForTBResponse %d"), bWaitForTB);
    TRACEAPI(csWaitForTB);

    m_bWaitForTB = bWaitForTB;
}

XMState * XMStateBase::HandleTBResponse(void)   
{
   TRACEAPI(_T("XMStateBase::HandleTBResponse"));

   return STATE_NULL;
}
// SR752-

//(+)SR969
bool XMStateBase::InitModule(void)
{ 
    TRACEAPI_EXT(_T("XMStateBase::InitModule"));

    return false; 
}
//(-)SR969

CString XMStateBase::AppendContextWithLCID(CString csContextIn)
{
   CString csContext = csContextIn;
   UINT nLCID;
   m_psx->GetLanguage(nLCID, 1);
   CString csLCID;
   csLCID.Format(_T("0%x"), nLCID);
   
   csLCID.MakeLower();
   csContext+=csLCID;

   return csContext;
}

void XMStateBase::ExitToApp(void)
{
    m_bExitToApp=true;
    m_psx->GenerateEvent(_T("XMButton8"), UI::EVENTCLICK, m_psx->GetPSXID());
}

//+SSCOADK-7123
void XMStateBase::DepositingFinished(void)
{
    CString csContext;
    COleVariant vstate = (long)UI::Normal;

    m_psx->GetContext(csContext);
    if(csContext == _T("XMCashStatus"))
    {
        m_psx->SetControlProperty(_T("SMButton8"), UI::PROPERTYSTATE, vstate);
    }
    else if (csContext == _T("XMCashReplenish"))
    {
        m_psx->SetControlProperty(_T("XMButton8"), UI::PROPERTYSTATE, vstate);
    }
}
//-SSCOADK-7123


void XMStateBase::SendBalanceToOptiCash(CurrencyWrapper &disp, CurrencyWrapper &ndisp)
{
	m_xmode = XMode::Instance();
	if(m_xmode)
	{
		if(m_xmode->m_OCWrapper != NULL)
		{
			m_xmode->m_OCWrapper->OCUpdateMessage(disp.ToString(), OC_DISPENSABLE);
			m_xmode->m_OCWrapper->OCUpdateMessage(ndisp.ToString(), OC_NONDISPENSABLE);
			m_xmode->m_OCWrapper->OCUpdateMessage(_T("EOT"), OC_END_OF_TRANSACTION);
		}
	}
}
void XMStateBase::SendMessageToOptiCash(LPCTSTR updateData, OC_UPDATE_MSG updateMsg)
{
	m_xmode = XMode::Instance();
	if(m_xmode)
	{
		if(m_xmode->m_OCWrapper != NULL)
		{
			m_xmode->m_OCWrapper->OCUpdateMessage(updateData, updateMsg);
		}
	}
}
