#include "StdAfx.h"
#include "XMCashReplenishBase.h"

#include "XMLog.h"
#include "PSXWrapper.h"
#include "PSXRedrawLock.h"
#include "CurrencyWrapper.h"
#include "XMCashDevice.h"
#include "CountTracker.h"
#include "XMConfig.h"
#include "XMUtils.h"
#include "IEventHandler.h"
#include "XMReporting.h"
#include "XMButtonMap.h" //new ui


#include "ITimerManager.h"

extern LPCTSTR OPERATORID;
extern LPCTSTR CASHDISPENSER;
extern LPCTSTR COINDISPENSER;
extern LPCTSTR ND_NOTEID;
extern LPCTSTR ND_COINID;
extern LPCTSTR PURGEOPERATION;

extern CMap<long, long, IEventHandler *, IEventHandler *> g_cyls;

bool XMCashReplenishBase::m_bCompletedManualReplenish = false;

XMCashReplenishBase::XMCashReplenishBase() : 
	m_lBusyRetries(m_cfg->GetLongOption(_T("CashDeviceBusyRetries"), 15))
{
    TRACEAPI(_T("XMCashReplenishBase::XMCashReplenishBase"));
}

XMCashReplenishBase::~XMCashReplenishBase()
{
    TRACEAPI(_T("XMCashReplenishBase::~XMCashReplenishBase"));
}

LPCTSTR XMCashReplenishBase::GetMainContext(void) const
{
    return _T("XMCashReplenish");
}

void XMCashReplenishBase::Initialize(void)
{
    TRACEAPI(_T("XMCashReplenishBase::Initialize"));
    
    if(HandleCashDeviceBusy())  // SSCOP-1250
    {
        return;
    }

    PSXRedrawLock p(*m_psx);

    XMCashManagementBase::Initialize();
    m_xdev->EnterCM();

    if(! m_receiptstr.IsEmpty())    // Retry failed print attempt.
    {
        PrintReceipt();
    }

    // Adjust controls based on the CM mode we're currently in.
    m_logger->Log(TRACE_INFO, _T("Entering REPLENISH mode."));

    m_psx->SetFrame(GetMainContext());

    //+TAR 437253
    COleVariant vState = VARIANT_FALSE;  
    m_psx->SetConfigProperty(_T("XMButton1"), UI::PROPERTYVISIBLE, vState);
    //-TAR 437253	

    vState = (long)UI::Normal;
    m_psx->SetConfigProperty(_T("XMButton8"), UI::PROPERTYSTATE, vState); //SSCOADK-6038

    // If re-entering the screen from an error and acceptance was
    // previously disabled, we don't want to re-enable the devices.
    // If m_pending->TotalCount() is zero, then there are no 
    // pending manual changes, so we can resume auto deposit.
    if(m_pending->TotalCount(CW_COINS) == 0 &&
       ! m_bCompletedManualReplenish)   // TAR 401801
    {
        BeginDeposit(CW_COINS);
    }

    if(m_pending->TotalCount(CW_NOTES) == 0 &&
       ! m_bCompletedManualReplenish)   // TAR 401801
    {
        BeginDeposit(CW_NOTES);
    }

    if(m_xdev->IsRecycler(CW_NOTES))    // Set fill loader cass button.
    {
        m_psx->SetConfigProperty(
               _T("XMButton3"), 
               UI::PROPERTYTEXTFORMAT, _T("$XM_FillLoaderCassette$"));
    }
   
    //tar 432853
    CString csValue;
    //only set to default menu if its the first time entering cash replenish
    if(m_bInitMenu==false //432853
            && m_psx->GetCustomDataVar(_T("DefaultMenuButton"),csValue))
    {
        m_pBtnMenu->OnClick(csValue);
        m_bInitMenu = true;
    }
    // tar 432853

    SetContext(GetMainContext());
    m_keypad->ShowKeypad(false);
    //new ui -

    CheckTrainingMode();

    m_keypad->SetEnabled(false);

    Redraw(true);
    
    m_bcminit = true;
}

void XMCashReplenishBase::UnInitialize()
{
    XMCashManagementBase::UnInitialize();

    // Cancel any timers that may have been started.
    ITimerManager::Instance().UnRegister(*this);
    SetExpiryCount(-1);     // Reset.
}

void XMCashReplenishBase::Redraw(bool force)
{
    TRACEAPI_EXT(_T("XMCashReplenishBase::Redraw"));

    PSXRedrawLock p(*m_psx);
    CountTracker *tracker;
    CString cval;        
    COleVariant vstate = (long)UI::Normal;  

    // TAR381285 - Flag used to determine if device is online.
    // ReadDispensableCounts() will fail if device is offline.
    bool bReadDispCountSuccess = true;
	bool bDiscrepancy = false;		// Add Glory CM Support

    // Update dispensable counts + display.
    if(force || m_disp->Size() == 0)
    {
        // BNR is very slow.  Don't call device method unless necessary.
        if(m_xdev->ReadDispensableCounts(cval, bDiscrepancy))
        {
            *m_disp = cval;
        }
    }

    if(m_disp->Size() > 0)
    {
        TCHAR *curr[4] = {
                            _T("XMButton2"),_T("XMButton4"),
                            _T("XMButton3"),_T("XMButton5")
                         };

        bool coinDisable = false;
        bool noteDisable = false;
        for(int i=0; i<=2; i+=2)    // Set state of control buttons.
        {
            // If coins or notes have been deposited and counted by the 
            // device, then disable manual add for that device.
        
            // RFC 401805:  Go ahead and disable the SetCoin/NoteCount 
            // buttons.  They will be re-enabled below if applicable.
            vstate = (long)UI::Disabled;

            // Disable '+' buttons if coins or notes were deposited or if
            // the device has a loader and the total loader count is 
            // greater than the capacity.
            if(m_disp->ChangeInCount(i>0) > 0 ||
                (m_xdev->GetLoaderCapacity(i>0) > 0 &&
                     m_xdev->GetTotalLoaderCount(i>0) >=
                     m_xdev->GetLoaderCapacity(i>0) )
               ) 
            {                      
                if(i==0)    // Coins.
                {
                    coinDisable = true;
                }
                else        // Notes
                {
                    noteDisable = true;
                }
            }
            if(0 == m_xdev->GetLoaderCapacity(true) && m_xdev->IsRecycler(CW_NOTES))
            {
                noteDisable = true;
            }

            // +TAR 389193:  If all buttons below the cylinders are going to
            //               be disabled and a cylinder is currently selected
            //               in the respective button list, then unselect
            //               the cylinder.
            if(m_activeCyl)
            {
                if( (coinDisable && m_activeCyl->GetDenom() < 0) ||
                    (noteDisable && m_activeCyl->GetDenom() > 0) )
                {
                    m_activeCyl->UnSelect();
                    m_activeCyl = NULL;
                    m_keypad->Clear();
                }
            }
            // -TAR 389193
            
            // Disable/enable control buttons. 
            m_psx->SetControlProperty(curr[i],   UI::PROPERTYSTATE, vstate);
            m_psx->SetControlProperty(curr[i+1], UI::PROPERTYSTATE, vstate);
        }

        if( m_disp->ChangeInCount(CW_COINS) > 0 || m_disp->ChangeInCount(CW_NOTES) > 0)
        {
            coinDisable = true;
            noteDisable = true;
        }

        CashUnit cu;
        m_disp->Begin();
        while(m_disp->Next(cu))        // Repaint the dispensable cylinders.
        {    
            cval.Format(_T("D_%d"), cu.Denom());
            if( m_counts.Lookup(cval, tracker) )
            {
                if(! m_bcminit)
                {
                    tracker->Reset();   // Set current counts = starting counts.
                }

                tracker->Visible(true);
                *tracker = cu.Count();
                tracker->SetLoaderCount(m_xdev->GetLoaderCount(cu.Denom()));

                // +TAR 384998:  Move to set for replenishment and removal.
                // Disable manual add buttons below cylinders if money
                // has been deposited and counted by the device OR if there
                // is a pending transfer.
                if(cu.Denom() < 0)    // Coins.
                {
                    tracker->Enable(! coinDisable);
                }
                else                // Notes.
                {
                    tracker->Enable(! noteDisable);
                }
                // -TAR 384998

                long ldenom = cu.Denom();

                // If this device does not have loader.
                if(m_xdev->GetLoaderCapacity(ldenom>0) <= 0)
                { 
                    // If the current count for this cylinder is at or 
                    // greater than capacity, then disable the '+' button 
                    // below the cylinder.
                    if((tracker->Count() - tracker->GetLoaderCount() - 
                        tracker->GetPendingCountChange()) >= 
                       m_xdev->GetDispenserCapacity(ldenom))
                    {
                        tracker->Enable(false);
                    }                    
                }

                // +RFC 401805:  If the current count of the tracker is 
                //               less than the dispenser fill count or if
                //               this device has a loader and the loader
                //               count is < the fill count, then
                //               enable the SetNote/CoinCounts button.
                if(cu.Denom() < 0 && ! coinDisable ||
                   cu.Denom() > 0 && ! noteDisable)
                {
                    long lfillLvl = m_xdev->GetDispenserFillLevel(ldenom);
                    if( tracker->Count() < lfillLvl || 
                        (m_xdev->GetLoaderCapacity(ldenom>0) > 0 &&
                         (tracker->GetLoaderCount() + 
                          tracker->GetPendingCountChange()) < lfillLvl
                         )
                       )
                    {
                        vstate = (long)UI::Normal;
                        int indx = ldenom < 0 ? 0 : 2;

                        m_psx->SetControlProperty(curr[indx],
                                                  UI::PROPERTYSTATE, vstate);
                    }
                }
                // -RFC 401805
            
                tracker->Redraw(force);
            }
        }        
    }
    // TAR381285 - ReadDispensableCounts() failed, device is offline.
    else
    {
        bReadDispCountSuccess = false;
    }

    // +TAR 389193 - Moved this block further down because the active cylinder
    //               may be unselected higher up in this method.
    if(m_activeCyl == NULL)
    {   
        // Disable keypad if no cylinder selected.
        m_keypad->SetEnabled(false);

        // Configure leadthru.
        CString lt = _T("$XM_AddCashLeadthru3$");

        if(m_xdev->IsRecycler(CW_COINS))
		{
			lt = _T("$XM_AddCashLeadthru1$");
		    if(m_xdev->IsRecycler(CW_NOTES))
			{
                // If cash device recycles both notes and coins.
                lt = _T("$XM_AddCashLeadthru2$");
			}
		}

        m_psx->SetConfigProperty(_T("XMLeadthruText"), GetMainContext(),
                         UI::PROPERTYTEXTFORMAT, lt);
    }
    // -TAR 389193

    // Update non-dispensable counts.
    if(force || m_ndisp->Size() == 0)
    {
        if(m_xdev->ReadNonDispensableCounts(cval))
        {
            *m_ndisp = cval;
        }
    }
    vstate = (long)UI::Disabled;
    // TAR381285 - ReadNonDispensableCounts() may return true even if device 
    // is offline, must check if ReadDispensableCounts() succeeded.
    if(m_ndisp->Size() > 0 && bReadDispCountSuccess)
    {
        const TCHAR *curr[2] = {ND_COINID, ND_NOTEID};

        for(int i=0; i<2; ++i)
        {
            tracker = m_counts[curr[i]];
            *tracker = m_ndisp->TotalCount(i>0);
            tracker->Visible(true);

            assert(! tracker->PendingEmpty());   // No empty in replenish.

            // If there is a pending transfer, then don't overwrite the
            // currently displayed value with the actual value.
            if(tracker->GetPendingCountChange() == 0)   // RFC 386869
            {
                tracker->SetTotalValue(m_ndisp->TotalValue(i>0));
            }
            tracker->Redraw(force);
        }
    }

    // Enable/Disable "Complete" button if an action is pending.
    // TAR381285 - Check if device was found online.
    if(bReadDispCountSuccess)
    {
        if(m_pending->Size() > 0)
        {
            vstate = (long)UI::Normal;
        }

        m_psx->SetConfigProperty(_T("XMButton1"),UI::PROPERTYSTATE, vstate);
        m_psx->SetControlProperty(_T("XMCancelChanges"), 
                                  UI::PROPERTYSTATE, vstate);
    }

    // Redraw the Number of Purge Operations
    if( m_counts.Lookup(PURGEOPERATION, tracker) )
    {
        tracker->Redraw(force);
    }

    // TAR381285 - If device was found offline, show error message,
    // leave only the "Exit" button enabled and hide cylinders.
    if(!bReadDispCountSuccess)
    {
        ErrorMessage(true);
        DisableAllButtonsExceptExit();
    }

    XMCashManagementBase::UpdateKeypadDisplay();
    
    //tar 419747

    (void)HandleNegativeLoaderCount();

    XMCashManagementBase::Redraw(); // RFQ 2765: Update the total coins/notes inserted
}

void XMCashReplenishBase::AlertMessage(bool show, LPCTSTR strMessage)
{
    TRACEAPI(_T("XMCashReplenishBase::AlertMessage"));

    if(strMessage)
    {
        XMCashManagementBase::AlertMessage(show, strMessage);
        return;
    }

    // +RFC 386866
    CString csPromptID;     // PSX string identifier.
    CString csPrompt;       // Returned PSX string.
    // -RFC 386866

    // TAR380213 - If manual refill alert message comes up during the 
    // Replenishment screen,
    // set manual replenishment flag to true and disable acceptors. 
    // If message is not visible,
    // set manual replenishment flag to false and enable acceptors.
    if(show)
    {
        m_bCompletedManualReplenish = true;
    }
    else
    {
        m_bCompletedManualReplenish = false;
    }
    // TAR380423 - Set the appropriate alert message depending on 
    // whether we have a pending manual refill for both notes and 
    // coins, just notes, or just coins. 

    // RFC 386866:  Determine which prompt to display.
    if(m_pending->Size(CW_NOTES))
    {
       if(m_pending->Size(CW_COINS))
       {//tar 430837
           if(m_xdev->IsRecycler(CW_NOTES))
            {
                csPromptID = _T("XM_ManualBothRecyclerFillAlert");
            }
            else
            {
                csPromptID = _T("XM_ManualBothFillAlert");
            }
        }
        else
        {
            if(m_xdev->IsRecycler(CW_NOTES))
            {
                csPromptID = _T("XM_ManualNoteRecyclerFillAlert");
            }
            else
            {
                csPromptID = _T("XM_ManualNoteFillAlert");
            }
        }
    }
    else if(m_pending->Size(CW_COINS))
    {
       csPromptID = _T("XM_ManualCoinFillAlert");
    }
    
    // +RFC 386866:  Display string corresponding to identifier stored in
    //               csPromptID.  
    if( ! csPromptID.IsEmpty() && m_psx->GetString(csPromptID, csPrompt) )
    {
        if(m_psx->GetString(_T("XM_ClearPrompt1"), csPromptID))
        {
            // Append instruction to select another function or Exit.
            csPrompt += _T("\n\n");
            csPrompt += csPromptID;
        }

        // Display alert box.
        m_psx->SetConfigProperty(_T("XMAlertBox"), GetMainContext(),
                                 UI::PROPERTYTEXTFORMAT, csPrompt);
    }
    // -RFC 386866

    COleVariant vstate = show ? VARIANT_TRUE : VARIANT_FALSE;
    m_psx->SetConfigProperty(_T("XMAlertBox"), GetMainContext(), UI::PROPERTYVISIBLE, vstate);
}

XMState * XMCashReplenishBase::XPSKeyStroke(long keyID)
{
    //RFQ 432305: Moved m_activeCyl checking here from XMCashManagementBase.
    if(! m_activeCyl )
    {        
        assert(false);
        return STATE_NULL;   // Bail if no cylinder is selected.
    }
    return XMCashManagementBase::XPSKeyStroke(keyID);
}

XMState * XMCashReplenishBase::XPSEnterKey(void)
{
    if(! m_activeCyl)
    {   // Enter key should be disabled if no cylinder is selected.  
        assert(false);  
        return STATE_NULL;              //lint !e527 caused by above assert.
    }

    int pchange = 0;                    // Pending count change.
    m_keypad->GetEcho(pchange);         // Get keyed-in count.

    CurrencyWrapper &cpending = *m_pending;
    
    if(pchange)
    {
        // +RFC 397451 / SR748 - Handle keyin-by-value.
        if(! m_bCashManagementDisplayCount)
        {
            // If displaying by value and the entered value is not evenly
            // divisible by the denomination, show message prompt.
            if(pchange % m_activeCyl->GetDenom())
            {
                CString err;        // Message displayed to user.
                CString errf;       // Format for message.
                if(m_psx->GetString(_T("XM_InvalidAmount"), errf))
                {
                    CString denom;  // Formatted denomination.
                    if(FormatCurrency(abs(m_activeCyl->GetDenom()), denom,
                                      m_activeCyl->GetDenom()>0))
                    {
                        err.Format(errf, (LPCTSTR)denom);
                    }

                    m_psx->SetConfigProperty(_T("XMLeadthruText"), 
                                             UI::PROPERTYTEXTFORMAT, err);
                }
                m_keypad->Clear();   // Clear out text (same as base).
                return STATE_NULL;
            }

            pchange /= abs(m_activeCyl->GetDenom());    // Convert to a count.
        }
        // -RFC 397451/SR748

        assert(pchange > 0);  // Should only be able to add.
        
        // TAR 384925 - 
        int nOldPending = m_activeCyl->GetPendingCountChange();
        m_activeCyl->SetPendingCountChange(0);
        long denom = m_activeCyl->GetDenom();
        cpending[denom]=0;

        // Cylinder capacity is the maximum of the cylinder or the loader
        // cassette if the device is a note recycler w/ a loader cassette.
        // RFC 386874:  Signature of GetLoaderCapacity() changed.
        long capacity = m_xdev->GetLoaderCapacity(denom>0); 
        
        if(capacity <= 0)     // No loader cassette for this device.
        {
            capacity = 
                m_xdev->GetDispenserCapacity(denom) - m_activeCyl->Count();
        }
        else                  
        {   // RFC 386875 - Subtract current loader count and pending count 
            //              for the other denominations.
            //              We have one loader cassette whose capacity
            //              is shared by all denominations.  
            capacity -= m_xdev->GetTotalLoaderCount(denom>0);
            capacity -= cpending.TotalCount(denom>0);
        }
       
        //m_activeCyl->SetPendingCountChange(0);
        // If entered count is > cylinder capacity minus the current 
        // count, then reduce the entered amount to this level.
        if(pchange > capacity)
        {
            // If entered count is > capacity, then set to 
            // capacity minus current count.
            CString csPrompt = m_bCashManagementDisplayCount ?
                               _T("XM_ExceededCapacity") :
                               _T("XM_ExceededAmount"); // RFQ 397451 

            CString csTmp;

            if(capacity < 0)
            {
                assert(false);      // Button should have been disabled.
                capacity = 0;       //lint !e527 caused by above assert.
            }

            // Display alert to inform user that count entered is too high.
            if(m_psx->GetString(csPrompt, csTmp))
            {
                if(m_bCashManagementDisplayCount)
                {
                    csPrompt.Format(csTmp, capacity, capacity);
                }
                else    // RFQ 397451: Display / enter by amount.
                {
                    long lamnt = capacity * abs(m_activeCyl->GetDenom());
                    CString csCapacity;
                    if(FormatCurrency(lamnt, csCapacity, false))
                    {
                        csPrompt.Format(csTmp, (LPCTSTR)csCapacity,
                                               (LPCTSTR)csCapacity);
                    }
                }
                AlertMessage(true, csPrompt) ;
            }
            //tar 429131+
            //if pending > capacity (invalid)
            //pending count for that cylinder will be changed back to the old valid count.
            m_activeCyl->SetPendingCountChange(nOldPending);
            cpending[denom] = nOldPending;
            m_keypad->Clear();   // Clear out text (same as base).
            // Disable Enter, Clear and backspace keys.
            m_keypad->SetAltEnabled(false);
            return STATE_NULL;
            //tar 429131-
        }
    }
    
    // tar 429131 - Store pending count only if pchange is not zero
    // if pchange is zero then do nothing even if there's already a pending amount.
    long denom = m_activeCyl->GetDenom();	
    if(pchange != 0 )
    {
        cpending[denom] = abs(pchange);

        // +RFC400821:  Temporarily turn off showing full base on high sensor.
        IEventHandler *ie = NULL;
        if(g_cyls.Lookup(denom, ie))
        {
            ie->SetNormal();
        }
        // -RFC400821
                
        m_activeCyl->SetPendingCountChange(pchange);

        EndDeposit();
    }
    //+tar 437541
    else  // pending change is zero.
    {
        // Remove any pending change for this denomination.
        m_activeCyl->SetPendingCountChange(0);
        cpending.Remove(denom);
    }
    //-tar 437541	

    m_keypad->Clear();  // Clear out echo text.
        
    // Disable Enter, Clear and backspace keys.
    m_keypad->SetAltEnabled(false);

    Redraw();
    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton1(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton1"));

    //+429757 -Action completion code should be refactored into separate function.
    return XMCashManagementBase::XPSButton1();
    
}

XMState * XMCashReplenishBase::XPSButton2(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton2"));

    CString csTxt;
    if(m_psx->GetContext(csTxt) && csTxt == _T("XMConfirmAction"))
    {   // User has pressed "No" in confirmation screen.
        UnInitialize();         // Pseudo state change.
        Initialize();     
        m_keypad->ShowKeypad(true);
        return STATE_NULL;
    }

    SetPendingManualChange();           // Manual coin fill.
    m_keypad->ShowKeypad(true);
    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton3(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton3"));

    SetPendingManualChange(CW_NOTES);   // Manual note fill.   
    m_keypad->ShowKeypad(true);
    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton4(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton4"));

    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton5(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton5"));

    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton6(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton6"));

    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton7(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton7"));

    return STATE_NULL;
}

XMState * XMCashReplenishBase::XPSButton8(void)
{
    TRACEAPI(_T("XMCashReplenishBase::XPSButton8"));

    if(m_bIsExitTouched)
	{
	    m_logger->Log(TRACE_INFO, _T("Ignoring succeeding Exit button pressed"));
	    return STATE_NULL;
	}
	
    COleVariant vstate = (long)UI::Disabled;
    m_psx->SetConfigProperty(_T("XMButton8"), UI::PROPERTYSTATE, vstate); //SSCOADK-6037
	
    m_bcminit = false;
    m_bIsExitTouched = true;  //SSCOP-2197
    EndDeposit();   // TAR 378127 Halt further deposits.

    // +TAR 385014
    int rwait = 2000;
    CString csTmp;
    if(m_cfg->GetOption(_T("replenish-exit-wait"), csTmp))
    {
        rwait = _ttoi(csTmp);
        if(rwait <= 0 || rwait > 8000)
        {
            rwait = 2000;
        }
    }

    Sleep(rwait);           // Allow time for device/SO to process.
    Redraw(true);           // Get latest counts.
    // -TAR 385014

    // Handle any deposited currency that was counted by the devices. 
    // If money was deposited, then call Finalize().
    bool dofinalize = false;
    for(int i=0; i<2; i++)
    {
        if(m_disp->ChangeInCount(i>0) || m_ndisp->ChangeInCount(i>0))
        {
            dofinalize = true;  // Something has changed.
            break;
        }
    }

    if(dofinalize)
    {
        Finalize();
    }
    else                    
    {
        Reset(true);        // TAR 378800
    }

    m_pBtnMenu->Reset();    //tar 434289 

    // Transfer Report Files to Server
    m_reporting->TransportFiles() ;

    AlertMessage(false);    // Hide instructional box.
    RETURN_STATE(STATE_CASHSTATUS);  // Return to media status screen.
}

XMState * XMCashReplenishBase::XPSButton(long buttonID)
{
    return XMCashManagementBase::XPSButton(buttonID);
}

void XMCashReplenishBase::Reset(bool final)
{
    TRACEAPI(_T("XMCashReplenishBase::Reset"));

    XMCashManagementBase::Reset(final);

     // Re-enable deposit for notes and coins.
     // TAR380213 - If we reset while the manual refill alert message is up, 
     //             disable acceptors. Else enable them.
    if(m_bCompletedManualReplenish)
    {
        EndDeposit();
    }
    else
    {
        BeginDeposit();
    }
}

void XMCashReplenishBase::SetPendingManualChange(bool notes)      
{
    TRACEAPI(_T("XMCashReplenishBase::SetPendingManualChange"));
    POSITION p = m_dbyID.GetStartPosition();
    int key;

    // PSX coin cylinder control IDs are 401-499.  note cylinder IDs are 
    // 501-599.  This variable is just used to determine if a particular 
    // cylinder is a note or a coin.
    const int keyTarg = notes ? 5 : 4;
    CountTracker *tracker;      // Current cylinder being accessed.

    CurrencyWrapper &cpending = *m_pending;

    AlertMessage(false);        // Hide instructional box.
    EnableDisableCashDevices(); // TAR385004

    m_logger->Log(TRACE_INFO, _T("SetPendingManualChange(%s)"),
                  notes ? _T("NOTES") : _T("COINS"));

    // Cycle through each tracker and do required manual operation.
    while(p != NULL)                    
    {
        m_dbyID.GetNextAssoc(p, key, tracker);

        // Just get the integer portion of the tracker ID.  If key/100 == 4, 
        // then this is a coin tracker.  If key/100 == 5, then this is a note
        // tracker.
        if(keyTarg == (key/100))    
        {
            // Manually fill device or loader cassette.
            long denom = tracker->GetDenom();

            // +RFC 401805: Subtract current count from configured fill cnt.
            long fillcnt = m_xdev->GetDispenserFillLevel(denom) - 
                           m_xdev->GetLoaderCount(denom);

            // +TAR 406616: Clear out any previous entries that have not been committed.
            tracker->SetPendingCountChange(0);
            // -TAR 406616

            // If device doesn't have a loader cassette, then subtract
            // the current count from the fill amount.
            // RFC 386874:  Signature of GetLoaderCapacity() changed.
            if(m_xdev->GetLoaderCapacity(denom>0) <= 0)
            {
                fillcnt -= tracker->Count();
            }                

            if(fillcnt > 0)     // If space available, then fill it.
            {   
                tracker->SetPendingCountChange(fillcnt);
                cpending[denom] = tracker->GetPendingCountChange();
            }
            // -RFC 401805
        }
    }

    EndDeposit();

    if(m_activeCyl)
    {    // Reset to no active cylinders.
        m_activeCyl->UnSelect();
        m_activeCyl = NULL;
        m_keypad->Clear();
    }
    Redraw();
}

bool XMCashReplenishBase::HandleManualChange(void)
{
    TRACEAPI(_T("XMCashReplenishBase::HandleManualChange"));

    CurrencyWrapper &cpending = *m_pending;
    CurrencyWrapper &cdisp = *m_disp;
    CString csPrompt;               // RFC 386873

    // Handle manual dispenser/recycler fill.
    if(m_pending->Size() == 0)   
    {
        return true;            // Nothing to do.
    }


	
	
    cdisp += cpending;          // Add manual fill amounts.
    if(m_xdev->SetDispensableCounts(cdisp)) // Send to device.
    {
        AlertMessage(true);     // Show instructional box.
        EnableDisableCashDevices(); // TAR385004
    }

    cpending.Empty();           // Clear pending.
    
    return true;
}

bool XMCashReplenishBase::BuildTBLoanData(CString &result)
{
    TRACEAPI(_T("XMCashReplenishBase::BuildTBLoanData"));
    result.Empty();

    // m_pending should be empty unless we are in training mode.
    assert(m_pending->Size() == 0 || CheckTrainingMode());      

    CurrencyWrapper &cpending = *m_pending;

    // Since this is a retrofit, we have to send data that existing TB's 
    // will expect.  The old non-recycler devices cannot have a loan for the
    // acceptors, but it is possible that money is deposited in the recycler
    // and the money is transferred to the overflow bin (recycler full, etc.).
    //
    // We are kindof treating the overflow as the acceptor for receipts and 
    // reporting, but the TB is most likely not expecting a loan for an 
    // acceptor.  For this reason, the loan counts for the overflow and the
    // dispenser will be combined and sent to the TB as a dispenser loan.

    // Determine loan (current count > starting count).
    long cchange;
    CashUnit cu;

    cpending.Empty();      
    m_disp->Begin();
    while(m_disp->Next(cu))
    {
        cchange = cu.ChangeInCount();

        // Should not be able to remove cash in replenishment screen.
        assert(cchange >= 0);  
        if(cchange != 0)
        {
            cpending[cu.Denom()] = cchange;
        } 
    }

    // Now add in non-dispensable.
    m_ndisp->Begin();
    while(m_ndisp->Next(cu))
    {
        cchange = cu.ChangeInCount();

        // Should not be able to remove cash in replenishment screen.
        assert(cchange >= 0);  

        if(cchange != 0)
        {
            cpending[cu.Denom()] += cchange;
        }
    }

    // +-SSCOADK-5777
    if(cpending.Size() > 0 && cpending.TotalCount() > 0)
    {

		//SSCOP-10053 Integrate OptiCash Client into Cash Management				
		SendMessageToOptiCash((LPCTSTR)cpending.ToString(), OC_LOAN);
		SendBalanceToOptiCash(*m_disp, *m_ndisp);

		
        m_logger->Log(TRACE_INFO, _T("Combined count change: [%s]"), 
                      (LPCTSTR)cpending);
        
        // TAR 395586 - Removed "csLoan = " from start of result string.
        result.Format(_T("%s%s"), OPERATORID, (LPCTSTR)m_UID);
        if(cpending.Size(CW_NOTES))   // Get note portion of the string.
        {
            result += CASHDISPENSER;
            result += cpending.ToString(CW_NOTES);  
        }

        if(cpending.Size(CW_COINS))
        {
            result += COINDISPENSER;
            result += cpending.ToString(CW_COINS);  
        }

        m_logger->Log(TRACE_INFO, _T("Loan: [%s]"), (LPCTSTR)result);
        return true;
    }

    m_logger->Log(TRACE_INFO, _T("No loan info found."));
    return false;
}

// TAR385004 - Enable or disable cash acceptor devices, based on 
// whether or not they have a manual refill pending.
void XMCashReplenishBase::EnableDisableCashDevices(void)
{
    // TAR 438583
    if(HandleNegativeLoaderCount())
    {
        return;
    }
    // TAR385004 - Only enable or disable acceptors if under
    // Cash Replenishment mode.
    
    // TAR385004 - If coin manual refill count is 0, enable coin acceptor.
    // Else disable it.
    if(m_pending->TotalCount(CW_COINS) == 0)
    {
        BeginDeposit(CW_COINS);
    }
    else
    {
        EndDeposit(CW_COINS);
    }

    // TAR385004 - If note manual refill count is 0, enable note acceptor.
    // Else disable it.
    if(m_pending->TotalCount(CW_NOTES) == 0)
    {
        BeginDeposit(CW_NOTES);
    }
    else
    {
        EndDeposit(CW_NOTES);
    }
}


void XMCashReplenishBase::ShowNegativeLoaderAlert(bool bShow)
{
    //TAR 438583
    if(!bShow)
    {
         if(m_pending->TotalCount() == 0 &&
                !m_bCompletedManualReplenish)   
        {
            m_logger->Log(TRACE_INFO, 
                    _T("XMCashReplenishBase::ShowNegativeLoaderAler BeginDeposit()"));
            BeginDeposit();
        }
    }

    XMCashManagementBase::ShowNegativeLoaderAlert(bShow);
}

bool XMCashReplenishBase::HandleNegativeLoaderCount(void)
{
    TRACEAPI_EXT(_T("XMCashReplenishBase::HandleNegativeLoaderCount"));

    bool bNegativeCountDetected = 
                    XMCashManagementBase::HandleNegativeLoaderCount();
    
    // Disable coin and note acceptance if negative loader count.
    if(! m_cfg->GetBoolOption(_T("AllowNegativeLoaderCount"), false) &&
         bNegativeCountDetected)
    {
        EndDeposit();
    }

    return bNegativeCountDetected;
}

bool XMCashReplenishBase::HandleCashDeviceBusy(void)
{
    TRACEAPI(_T("XMCashReplenishBase::HandleCashDeviceBusy"));

    // The BNR can only report its busy status if the cash acceptor is not
    // enabled.  If the acceptor is enabled, then the device always reports
    // that it is busy.
    // For this reason, we will only query the busy state of the BNR after
    // a manual loan/refill of the loader cassette.
    //
    // We will also restrict the query to this state only in order to 
    // minimize performance impacts and unintended side-effects.
    if(! m_bCompletedManualReplenish)
    {
        m_logger->Log(TRACE_INFO, 
            _T("No manual add performed.  Not checking device busy status.")); 
        return false;
    }

    CString csCurrentContext;
    if(m_psx->GetContext(csCurrentContext) &&
                csCurrentContext != GetMainContext())
    {
        m_logger->Log(TRACE_INFO,
            _T("Not checking busy status.  Context [%s] is not default [%s]"),
            (LPCTSTR)csCurrentContext, GetMainContext());
        return false;
    }

    if(! m_xdev->IsDeviceBusy())    // Not busy.  Nothing to do.
    {
        return false;
    }

    if(GetExpiryCount() != -1)       
    {
        // We already tried polling the device, but it was still busy after
        // m_lBusyRetries attempts.  Go ahead and show the screen with 
        // possibly stale data.  This is done so we won't get in an endless
        // loop and lock up the system.
        m_logger->Log(TRACE_WARNING, 
            _T("Allowing screen to load even though device is still busy!")); 

        SetExpiryCount(-1);     // Reset.
        return false;
    }

    // Register timer so we can periodically recheck if the device is busy.
    SetTimeOut(m_cfg->GetLongOption(_T("CashDeviceBusyTimeOut"), 1000));
    SetExpiryCount(m_lBusyRetries);
    ITimerManager::Instance().Register(*this);

    return true;
}

void XMCashReplenishBase::TimedOut(unsigned int nTimerId)
{
    TRACEAPI(_T("XMCashReplenishBase::TimedOut"));

    if(m_xdev->IsDeviceBusy())
    {
        if(GetExpiryCount() > 1)
        {
            m_logger->Log(TRACE_INFO, _T("Expiry count: %d"), GetExpiryCount());
            return;
        }

        m_logger->Log(TRACE_WARNING, 
                  _T("Device is still busy after %d retries.  Giving up."),
                  m_lBusyRetries);
    }

    ITimerManager::Instance().UnRegister(*this);

    // Important note:  The reason that SetExpiryCount(-1) is not called here
    // is that the following call to Initialize() will call 
    // HandleCashDeviceBusy() again.  The expiry count will be checked 
    // and the busy status will not be polled again if it is not -1.
    Initialize();
}

XMState *  XMCashReplenishBase::HandleConfirmChanges(void)
{
    COleVariant vstate = (long)UI::Disabled;
       
    if(IsCancelPending())
    {
        return HandleCancelChanges();
    }

    XMCashManagementBase::HandleConfirmChanges();

    CurrencyWrapper &cdisp = *m_disp;
    CurrencyWrapper &cndisp = *m_ndisp;

    m_logger->Log(TRACE_INFO, 
                  _T("* COUNTS BEFORE CHANGES: ndisp: [%s]; disp: [%s] *"), 
                  (LPCTSTR)cndisp, (LPCTSTR)cdisp);

    HandleManualChange();

	// SSCOP-822: Removed TAR 449905 routine due to new routine implemented in SSCOP-822
    Finalize();   

    m_logger->Log(TRACE_INFO, 
                  _T("* COUNTS AFTER CHANGES: ndisp: [%s]; disp: [%s] *"), 
                  (LPCTSTR)cndisp, (LPCTSTR)cdisp);

    // +TAR 384998: Moved from HandleDispense().
    // Enable Exit button after actions.
    vstate = (long)UI::Normal;
    m_psx->SetControlProperty( _T("XMButton8"), UI::PROPERTYSTATE, vstate);
    // -TAR 384998

    //tar 419747+
    (void)HandleNegativeLoaderCount();
    //tar 419747-
        
    return STATE_NULL;
}

XMState *  XMCashReplenishBase::HandleApplyChanges(void)
{
    XMCashManagementBase::HandleApplyChanges();

    // Add pending dispense information to the confirmation screen.
    CString disptxt = m_promptstr;

    EndDeposit();   // TAR 378127
    if(m_pending->Size(CW_COINS))
    {
        BuildPrompt(_T("XM_ConfirmManualCoinFill"), disptxt, *m_pending);
    }

    if(m_pending->Size(CW_NOTES))
    {
        // Determine if filling to the loader cassette or regular note
        // cassettes.
        CString promptID = _T("XM_ConfirmManualNoteFill");

        if(m_xdev->GetLoaderCapacity(CW_NOTES) > 0)        
        {
            // Filling loader cassette.
            promptID = _T("XM_ConfirmFillLoaderCassette");
        }

        BuildPrompt(promptID, disptxt, *m_pending, CW_NOTES);
    }

    if(disptxt.GetLength() > 0)
    {
        m_psx->SetFrame(_T("XMConfirmAction"));

        // +RFC 386876:  Now using text format instead of xaction variable.
        m_psx->SetConfigProperty(_T("XMPendingActions"), UI::PROPERTYTEXTFORMAT,
                                 disptxt);
        // -RFC 386876
        SetContext(_T("XMConfirmAction"));
    }
    else    // There must be nothing to do.  Press cancel button.
    {
        Reset();
        Redraw(true);       // Repaint display.
    }

    //comes here on pressing the Execute button on the Cash Management screen
    CheckTrainingMode();
    
    return STATE_NULL;
}

XMState *  XMCashReplenishBase::HandleCancelChanges(void)
{
    return XMCashManagementBase::HandleCancelChanges();
}

XMState * XMCashReplenishBase::XDMParse(UINT evntType, long lParam,
                                         bool &handled, long devClass, 
                                         long devID)
{
    return XMCashManagementBase::XDMParse(evntType, lParam, handled,
                                          devClass, devID);
}

XMState * XMCashReplenishBase::XDMParse(MessageElement &rMe, bool &bProcessed)
{
    return XMCashManagementBase::XDMParse(rMe, bProcessed);
}
