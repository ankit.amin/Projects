#include "StdAfx.h"
#include "XMCashRemoveBase.h"

#include "XMLog.h"
#include "PSXWrapper.h"
#include "PSXRedrawLock.h"
#include "CurrencyWrapper.h"
#include "XMCashDevice.h"
#include "CountTracker.h"

#include "IEventHandler.h"
#include "ncrdevmgr.h"            // TAR 386003: Need device class enumerations.
#include "Opos.h"
#include "XMUtils.h"              // Need FormatCurrency.
#include "XMConfig.h"

//new ui+
#include "XMButtonMap.h"
#include "XMButton.h"
//new ui-

#include "PSXHelper.h"
#include "OposChan.h"

extern LPCTSTR CASHACCEPTOR;
extern LPCTSTR CASHDISPENSER;
extern LPCTSTR COINACCEPTOR;
extern LPCTSTR COINDISPENSER;
extern LPCTSTR OPERATORID;

extern LPCTSTR ND_NOTEID;
extern LPCTSTR ND_COINID;
extern LPCTSTR PURGEOPERATION;
extern CMap<long, long, IEventHandler *, IEventHandler *> g_cyls;

// Pending dispenser empty, either for notes or coins or both.
#define PENDING_DEMPTY (m_bCoinEmpty || m_bNoteEmpty) 
bool m_bTransferXPressed = false;   // RFQ 3067 - BNR Dispense to Cashbox

// Making the booleans static 
bool XMCashRemoveBase::m_bCoinEmpty = false;    // SSCOP-804
bool XMCashRemoveBase::m_bNoteEmpty = false;    // SSCOP-804
int XMCashRemoveBase::m_nPickUpFlag = -2;       // Default to no function 
bool XMCashRemoveBase::m_bDelayedFinalization = false;    //SSCOP-3881

XMCashRemoveBase::XMCashRemoveBase() :
                        m_bEnablePickUpXAmount(false), 
                        m_nStartingBank(-1)                        
{
    TRACEAPI(_T("XMCashRemoveBase::XMCashRemoveBase"));
}

XMCashRemoveBase::~XMCashRemoveBase()
{
    TRACEAPI(_T("XMCashRemoveBase::~XMCashRemoveBase"));
}

LPCTSTR XMCashRemoveBase::GetMainContext(void) const
{
    //tar 432853+
    //Add another context specific to note recycler.
    //XMCashRemove context = BCR + Conventional note accept/dispense (Puloon or F53).
    //XMCashRemoveBNR context = BCR + Note recycler (BNR).

    if (! m_xdev->IsRecycler(CW_NOTES))
    {
        return(_T("XMCashRemove"));
    }
    else
    {
        return(_T("XMCashRemoveBNR"));
    }
    //tar 432853-
}

void XMCashRemoveBase::Initialize(void)
{
    TRACEAPI(_T("XMCashRemoveBase::Initialize"));
	
	// We don't want the user to press the lock button when 
    // the screen is being repainted or other things that 
    // don't take a long time to complete.
    SetSystemBusy(false);

    PSXRedrawLock p(*m_psx);

    XMCashManagementBase::Initialize();
    m_xdev->EnterCM();

    if(! m_receiptstr.IsEmpty())    // Retry failed print attempt.
    {
        PrintReceipt();
    }
    // Adjust controls based on the CM mode we're currently in.
    m_logger->Log(TRACE_INFO, _T("Entering REMOVE mode."));
    EndDeposit();   // TAR 378127

    m_psx->SetFrame(GetMainContext());
	
    COleVariant vstate = (long)UI::Normal;
    m_psx->SetConfigProperty(_T("XMButton8"), UI::PROPERTYSTATE, vstate); //SSCOADK-6106

    //+TAR 437253
    COleVariant vState = VARIANT_FALSE;  
    m_psx->SetConfigProperty(_T("XMButton1"), UI::PROPERTYVISIBLE, vState);
    //-TAR 437253
    ShowErrorAlert(m_bIsError, m_csErrorMsg);  //tar 436337


    if (m_xdev->IsRecycler(CW_NOTES))
    {
        // Hide the "Pending Text" field mask if this is a recycler
        // and if dispensing is enabled.
        if(m_xdev->DispenseAllowed(CW_NOTES))
        {
            m_psx->SetConfigProperty(_T("XMNotePendingDispenseMask"), 
                                  UI::PROPERTYVISIBLE, VARIANT_FALSE);
        }
    }

    if (!m_xdev->IsRecycler(CW_COINS))
    {
        m_psx->SetConfigProperty(
               _T("XMNonDispenseCylinder401EmptyButton"), 
               UI::PROPERTYTEXTFORMAT, _T("$XM_EmptyCoinAcceptor$"));
    }
    else    // Device is a recycler.
    {
        // Hide the "Pending Text" field mask if this is a recycler
        // and if dispensing is enabled.
        if(m_xdev->DispenseAllowed(CW_COINS))
        {
            m_psx->SetConfigProperty(_T("XMCoinPendingDispenseMask"), 
                                  UI::PROPERTYVISIBLE, VARIANT_FALSE);
        }
    }

    if (!m_xdev->IsRecycler(CW_COINS))
    {
        // Hide the "Pending Dispense" text.
        m_psx->SetConfigProperty(_T("XMPendingCoinArea1"), 
                                 UI::PROPERTYVISIBLE, VARIANT_FALSE);
        m_psx->SetConfigProperty(_T("XMPendingCoinArea2"), 
                                 UI::PROPERTYVISIBLE, VARIANT_FALSE);
    }

    CheckTrainingMode();

    m_keypad->SetEnabled(false);
	SetContext(GetMainContext()); //SSCOADK-6902
	
    //RFQ 432305: Only call SetPickUpFunction() once.
    if(m_nPickUpFlag == -2 )
    {
        if( m_xdev->IsRecycler(CW_NOTES))
            SetPickUpFunction();
        m_nPickUpFlag = 0;
    }

	//+TAR 450190
	CString csValue;
	CString csDefaultMenuButton = _T("DefaultMenuButton");

	if(! (m_xdev->DispenseAllowed(CW_COINS) || m_xdev->DispenseAllowed(CW_NOTES)))
	{
		m_psx->GetCustomDataVar(csDefaultMenuButton, csValue);
		IButton *pDefault = (*m_pBtnMenu)[csValue];
        		assert(pDefault != NULL);
        		pDefault->Hide();
		csDefaultMenuButton = _T("DefaultMenuButtonNoDispense");
	}
	//-TAR 450190
	
    //SetContext(GetMainContext());
    if(! HandleNegativeLoaderCount())  // No neg counts detected.
    {
        //tar 432853
        //CString csValue;
        //only set to default menu if its the first time entering cash remove
        if(m_bInitMenu==false //432853
            && m_psx->GetCustomDataVar(csDefaultMenuButton,csValue))
        {
            m_pBtnMenu->OnClick(csValue);
            m_bInitMenu = true;
        }// tar 432853
        else
        {//tar 436559
            IButton *pActive = m_pBtnMenu->GetActiveButton();
            if(pActive) 
            {   
                pActive->OnClick();
            }
        }
        
    }
    //handling of negative loader count-

    // SR877
    if(m_bDelayedFinalization)
    {
        // Need to update count before reconciling because there might be
        // bills that went in the cashbox during reset.
        CString cval;
        bool bDiscrepancy = false;  // Add Glory CM Support
        if(m_xdev->ReadDispensableCounts(cval, bDiscrepancy))        
        { 
            m_logger->Log(TRACE_INFO, 
            _T("Reconcile counts and finalize the previous transaction."));
            m_xdev->ReconcileCounts();            
        }
        AlertMessage(true, _T("$XM_PlaceJammedBill$"));
    }

    Redraw(true);

    // Only call HandleNegativeLoader again if there's indeed
    // negative loader from the Redraw call.
    if(XMCashManagementBase::HasNegativeLoaderCount(CW_NOTES))
    {
        (void)HandleNegativeLoaderCount();
    }

    // SR877 Need to finalize after Redraw to get updated reconciled counts.
    if(m_bDelayedFinalization)
    {       
        m_bDelayedFinalization = false; //Reset flag
        Finalize();             
    }

    m_bcminit = true;
}

void XMCashRemoveBase::UnInitialize()
{
    XMCashManagementBase::UnInitialize();
}

void XMCashRemoveBase::Redraw(bool force)
{
    TRACEAPI_EXT(_T("XMCashRemoveBase::Redraw"));

    PSXRedrawLock p(*m_psx);
    CountTracker *tracker = NULL;
    CString cval;        
    COleVariant vstate = (long)UI::Normal;  
    
    //RFQ 3067 - BNR Dispense to Cashbox
    // Read configured AllowDispenseToCashbox.
    CString csOpt;
    if(!m_cfg->GetOption(_T("AllowDispenseToCashbox"), csOpt))
    {
        m_logger->Log(TRACE_WARNING, 
            _T("WARNING: Could not read AllowDispenseToCashbox. Using the default value."));
      csOpt = _T("Y");
    }

    bool bAllowDispenseToCashBox = csOpt.CompareNoCase(_T("Y")) == 0 ? true : false;
    //RFQ 3067 - BNR Dispense to Cashbox

    
    // TAR381285 - Flag used to determine if device is online.
    // ReadDispensableCounts() will fail if device is offline.
    bool bReadDispCountSuccess = true;
    bool bDiscrepancy = false;  // Add Glory CM Support

    // Update dispensable counts + display.
    if(force || m_disp->Size() == 0)
    {
        // BNR is very slow.  Don't call device method unless necessary.
        if(m_xdev->ReadDispensableCounts(cval, bDiscrepancy))
        {
            *m_disp = cval;
        }
    }

    // Check for cash discrepancy.
    HandleCashDiscrepancy(bDiscrepancy);    // Add Glory CM Support

    if(m_disp->Size() > 0)
    {
        TCHAR *curr[4] = {
                            _T("XMButton2"),_T("XMButton4"),
                            _T("XMButton3"),_T("XMButton5")
                         };

        bool coinDisable = false;
        bool noteDisable = false;
        for(int i=0; i<=2; i+=2)    // Set state of control buttons.
        {
            vstate = (long)UI::Normal;
            bool bempty = i > 0 ? m_bNoteEmpty : m_bCoinEmpty;
            
            if(bempty || m_pending->Size(i>0) > 0 || 
               (m_disp->TotalCount(i>0) == 0 && m_purge->TotalCount(i>0) <=0) )
            {
                // If no currency,or if there is a pending dispense or manual 
                // add disable note or coin control buttons., 
                vstate = (long)UI::Disabled;
            }

            // +TAR 384998 - Disable '-' buttons below cylinders if 
            //                  there is a pending transfer.
            tracker = NULL;

            if(m_counts.Lookup(i>0 ? ND_NOTEID : ND_COINID, tracker) && 
               tracker->GetPendingCountChange() > 0)
            {
                if(i==0)    // Coins.
                {
                    coinDisable = true;
                }
                else        // Notes
                {
                    if ( !m_bTransferXPressed )   //RFQ 3067 - BNR Dispense to Cashbox
                    {
                        noteDisable = true;
                    }

                }
            }

            // +TAR 398945
            coinDisable = m_bCoinEmpty ? m_bCoinEmpty : coinDisable; 
            noteDisable = m_bNoteEmpty ? m_bNoteEmpty : noteDisable; 
            // -TAR 398945
        
            // -TAR 384998            

            // if m_pending count is 0 and the glory device is set not to allow dispense
            // then don't attempt to enable the note/coin cylinder button when enter remove
            // cash screen. If we don't do it, then user may able to manage to touch the 
            // note/coin cylinder button before it gets disabled.
            DisableCylinderButtons(noteDisable, coinDisable);

            // +TAR 389193:  If all buttons below the cylinders are going to
            //               be disabled and a cylinder is currently selected
            //               in the respective button list, then unselect
            //               the cylinder.
            if(m_activeCyl)
            {
                if( (coinDisable && m_activeCyl->GetDenom() < 0) ||
                    (noteDisable && m_activeCyl->GetDenom() > 0) )
                {
                    m_activeCyl->UnSelect();
                    m_activeCyl = NULL;
                    m_keypad->Clear();
                }
            }
            // -TAR 389193
            
            // Disable/enable control buttons. 
            m_psx->SetControlProperty(curr[i],   UI::PROPERTYSTATE, vstate);
            if(i==2)//we are setting state of XMButton3, 
            {       //also change state for XMButton6
                if(m_xdev->IsRecycler(CW_NOTES))
                {
                    m_psx->SetControlProperty(_T("XMButton6"),   
                        UI::PROPERTYSTATE, vstate);
                }
            }

            // +RFC 401805: Go ahead and disable the transfer buttons, they 
            //              will be conditionally enabled below.
            vstate = (long)UI::Disabled;
            // -RFC 401805

            m_psx->SetControlProperty(curr[i+1], UI::PROPERTYSTATE, vstate);
        }

        CashUnit cu;
        m_disp->Begin();
        while(m_disp->Next(cu))        // Repaint the dispensable cylinders.
        {    
            cval.Format(_T("D_%d"), cu.Denom());
            if( m_counts.Lookup(cval, tracker) )
            {
                if(! m_bcminit)
                {
                    tracker->Reset();   // Set current counts = starting counts.
                }

                tracker->Visible(true);
                *tracker = cu.Count();
                tracker->SetLoaderCount(m_xdev->GetLoaderCount(cu.Denom()));

                // +TAR 384998:  Move to set for replenishment and removal.
                // Disable manual add buttons below cylinders if money
                // has been deposited and counted by the device OR if there
                // is a pending transfer.
                if(cu.Denom() < 0)    // Coins.
                {
                    tracker->Enable(! coinDisable && m_xdev->IsRecycler(CW_COINS));
                }
                else                // Notes.
                {
                    tracker->Enable(! noteDisable && m_xdev->IsRecycler(CW_NOTES));
                }
                // -TAR 384998

                // NOTE:  For BNR only.  We should not be able to dispense
                //        the notes in the loader cassette, so those 
                //        counts are not included for deciding whether to
                //        enable the '-' buttons.
                //
                //        The pending count change is included so that 
                //        the '-' button will be enabled if the contents
                //        of a cylinder are to be emptied, but the change
                //        has not yet been committed.  This will allow
                //        the user to go back and change the entry for this
                //        cylinder.
                // +TAR 384998: Change to disable only.  Cylinder may have
                //                 been disabled above due to coinDisable or
                //                 noteDisable flag.
                if((tracker->Count() - tracker->GetLoaderCount() +
                    abs(tracker->GetPendingCountChange())) <= 0 )
                {
                    tracker->Enable(false);
                }    
                // -TAR 384998

                // +RFC 401805:  If the current count of the tracker is 
                //               greater than the base level count, then
                //               enable the base level buttons.
                if( (tracker->Count() - tracker->GetLoaderCount()) > 
                    m_xdev->GetDispenserBaseLevel(cu.Denom()) )
                {
                    vstate = (long)UI::Normal;
                    int indx = cu.Denom() < 0 ? 1 : 3;
                    m_psx->SetControlProperty(curr[indx],
                                             UI::PROPERTYSTATE, vstate);
                }
                // -RFC 401805
                //RFQ 432305
                // Disable all cylinders if Pickup is performed.
                if(m_nPickUpFlag != 0)
                    tracker->Enable(false);
            
                tracker->Redraw(force);
            }
        }        
         
        //RFQ 3067 - BNR Dispense to Cashbox
        //If true were in Transfer(x) count then disable "Transfer Bills to Base level" on redraw();
        if(m_bTransferXPressed)
        {
            vstate = (long)UI::Disabled;
            m_psx->SetControlProperty(_T("XMButton5"), UI::PROPERTYSTATE, vstate);
        }
        //RFQ 3067 - BNR Dispense to Cashbox
    }
    // TAR381285 - ReadDispensableCounts() failed, device is offline.
    else
    {
        bReadDispCountSuccess = false;
    }

    // +TAR 389193 - Moved this block further down because the active cylinder
    //               may be unselected higher up in this method.
    if(m_activeCyl == NULL && m_nPickUpFlag <= 0)
    {   
        // Disable keypad if no cylinder selected.
        // RFQ 432305: Dont disable keypad if PickUpXAmount is selected.
        m_keypad->SetEnabled(false);
    }
    // -TAR 389193

    // Update non-dispensable counts.
    if(force || m_ndisp->Size() == 0)
    {
        if(m_xdev->ReadNonDispensableCounts(cval))
        {
            *m_ndisp = cval;
        }
    }
    vstate = (long)UI::Disabled;
    // TAR381285 - ReadNonDispensableCounts() may return true even if device 
    // is offline, must check if ReadDispensableCounts() succeeded.
    if(m_ndisp->Size() > 0 && bReadDispCountSuccess)
    {
        const TCHAR *curr[2] = {ND_COINID, ND_NOTEID};

        for(int i=0; i<2; ++i)
        {
            tracker = m_counts[curr[i]];
            *tracker = m_ndisp->TotalCount(i>0);
            tracker->Visible(true);

            if(tracker->PendingEmpty())
            {   // Pending empty, enable "Complete" and "Cancel" buttons.
                vstate = (long)UI::Normal;
                if(i>0 && m_xdev->IsRecycler(CW_NOTES))
                {
                    COleVariant vb = (long)UI::Disabled;
                    // If we have a note recycler and the overflow container
                    // is to be emptied, then disable the "Transfer" buttons.
                    m_psx->SetControlProperty(_T("XMButton6"),//
                                              UI::PROPERTYSTATE, vb);
                    m_psx->SetControlProperty(_T("XMButton5"),
                                              UI::PROPERTYSTATE, vb);
                }

            }

            // If there is a pending transfer, then don't overwrite the
            // currently displayed value with the actual value.
            if(tracker->GetPendingCountChange() == 0)   // RFC 386869
            {
                tracker->SetTotalValue(m_ndisp->TotalValue(i>0));
            }
            tracker->Redraw(force);
        }
    }

    // Enable/Disable "Complete" button if an action is pending.
    // TAR381285 - Check if device was found online.
    if(bReadDispCountSuccess)
    {
        if(PENDING_DEMPTY || m_pending->Size() > 0)
        {
            vstate = (long)UI::Normal;
        }

        m_psx->SetConfigProperty(_T("XMButton1"),UI::PROPERTYSTATE, vstate);
        m_psx->SetControlProperty(_T("XMCancelChanges"), 
                                  UI::PROPERTYSTATE, vstate);
        // If in remove mode and dispensing is not enabled, then disable
        // the "Empty Coins to Base Level" button.
        if(! m_xdev->DispenseAllowed(CW_COINS))
        {
            vstate = (long)UI::Disabled;
            m_psx->SetConfigProperty(_T("XMButton4"),UI::PROPERTYSTATE, vstate);
        }
    }

    // Redraw the Number of Purge Operations
    if( m_counts.Lookup(PURGEOPERATION, tracker) )
    {
        tracker->Redraw(force);
        if(tracker->Count() > 0)
        {
            m_bIsPurge = true;
        }
        else
        {   
            m_bIsPurge = false;
        }
        XMCashManagementBase::ShowPurgeStatusBar(m_bIsPurge);
    }
    //new ui-


    // TAR381285 - If device was found offline, show error message,
    // leave only the "Exit" button enabled and hide cylinders.
    if(!bReadDispCountSuccess)
    {
        ErrorMessage(true);
        DisableAllButtonsExceptExit();
    }
        
    // Hide XMButton7 if <AllowDispenseToCashbox=N> or 
    // if <AllowDispenseToCashbox=Y> and <AllowNoteDispense=N>. 
    COleVariant vState;
    if ( ( !bAllowDispenseToCashBox) || 
        ( bAllowDispenseToCashBox && (! m_xdev->DispenseAllowed(CW_NOTES))))
    {   
            vState = VARIANT_FALSE;
            m_psx->SetControlProperty(_T("XMButton7"),UI::PROPERTYVISIBLE, vState); 
    }
    else
    { //if not hidden change state of the button
        vState = (long)UI::Disabled;

        if ( m_pending->TotalCount(CW_NOTES) > 0 )
        {
            vState = (long)UI::Normal;
        }
    
        if ((m_counts.Lookup(ND_NOTEID, tracker) && 
            tracker->GetPendingCountChange() > 0) ||
            m_pending->TotalCount(CW_NOTES) == 0) //TAR 439695: Rollback fix TAR 430291
        {
            vState = (long)UI::Disabled;
        }
    
        m_psx->SetControlProperty( _T("XMButton7"), UI::PROPERTYSTATE, vState );
    }
    // -Added for RFQ 3067 - BNR Dispense to Cashbox

    XMCashManagementBase::UpdateKeypadDisplay();  
    
    if(m_xdev->IsRecycler(CW_NOTES))
        UpdatePickUpButtons(); //RFQ 432305
}

void XMCashRemoveBase::AlertMessage(bool show, LPCTSTR strMessage)
{
    TRACEAPI_EXT(_T("XMCashRemoveBase::AlertMessage"));

    if(strMessage)
    {
        COleVariant vstate = show ? VARIANT_TRUE : VARIANT_FALSE;
        m_psx->SetConfigProperty(_T("XMAlertBox"), GetMainContext(),
                UI::PROPERTYTEXTFORMAT, strMessage);        
        m_psx->SetConfigProperty(_T("XMAlertBox"), GetMainContext(),
                UI::PROPERTYVISIBLE, vstate);
        return;
    }

    // +RFC 386866
    CString csPromptID;     // PSX string identifier.
    CString csPrompt;       // Returned PSX string.
    // -RFC 386866

    // +TAR 380488  Restore default alert text. 
    if(!show)
    {
        csPromptID = _T("XM_ManualEmptyAlert");
    }
    // -TAR 380488

    // +RFC 386866:  Display string corresponding to identifier stored in
    //               csPromptID.  
    if( ! csPromptID.IsEmpty() && m_psx->GetString(csPromptID, csPrompt) )
    {
        if(m_psx->GetString(_T("XM_ClearPrompt1"), csPromptID))
        {
            // Append instruction to select another function or Exit.
            csPrompt += _T("\n\n");
            csPrompt += csPromptID;
        }

        // Display alert box.
        m_psx->SetConfigProperty(_T("XMAlertBox"), 
                                 UI::PROPERTYTEXTFORMAT, csPrompt);
    }
    // -RFC 386866

    COleVariant vstate = show ? VARIANT_TRUE : VARIANT_FALSE;
    m_psx->SetConfigProperty(_T("XMAlertBox"), GetMainContext(), UI::PROPERTYVISIBLE, vstate);
}

XMState * XMCashRemoveBase::XDMAcceptor()
{
    TRACEAPI(_T("XMCashRemoveBase::XDMAcceptor"));

    assert(0);

    //RFQ 405352 SR 747
    //There is a chance that the user could go to REMOVE screen after 
    //deposit money on Cash Status screen. In this case we will bounce back 
    //to Status screen
    //lint -e527
    m_logger->Log(TRACE_ERROR, 
          _T("ERROR:  Should not receive this event while in remove screen."));

    RETURN_STATE(STATE_CASHSTATUS);
}

XMState * XMCashRemoveBase::XPSKeyStroke(long keyID)
{
    if(! m_activeCyl && m_nPickUpFlag <= 0)
    {
        //RFQ 432305: Dont bail if PickUpXAmount is selected.
        assert(false);
        return STATE_NULL;   // Bail if no cylinder is selected.
    }

    XMCashManagementBase::XPSKeyStroke(keyID);

    if( m_nPickUpFlag > 0)
    {
        CString csPrompt;
        if (m_psx->GetString(_T("XM_KeyPadEnterPickUpAmount"), csPrompt))
        {           
            m_psx->SetConfigProperty(_T("XMLeadthruText"), UI::PROPERTYTEXTFORMAT, csPrompt);
        }
    }
    return STATE_NULL;
}

XMState * XMCashRemoveBase::XPSEnterKey(void)
{    
    if(! m_activeCyl && m_nPickUpFlag <= 0)
    {   // Enter key should be disabled if no cylinder is selected.  
        //RFQ 432305: Dont bail if PickUpXAmount is selected.
        assert(false);  
        return STATE_NULL;
    }

    int pchange = 0;                    // Pending count change.
    m_keypad->GetEcho(pchange);          // Get keyed-in count.
    //+ RFQ 432305: Handle XPSEnterKey for PickUpXAmount 
    if(m_xdev->IsRecycler(CW_NOTES) && m_nPickUpFlag > 0)
    {                               
        CalculatePickUp(pchange);           
        return STATE_NULL;  
    }
    //- RFQ 432305

    CurrencyWrapper &cpending = *m_pending;
    
    if(pchange)
    {
        // +RFC 397451 / SR748 - Handle keyin-by-value.
        if(! m_bCashManagementDisplayCount)
        {
            // If displaying by value and the entered value is not evenly
            // divisible by the denomination, show message prompt.
            if(pchange % m_activeCyl->GetDenom())
            {
                CString err;        // Message displayed to user.
                CString errf;       // Format for message.
                if(m_psx->GetString(_T("XM_InvalidAmount"), errf))
                {
                    CString denom;  // Formatted denomination.
                    if(FormatCurrency(abs(m_activeCyl->GetDenom()), denom,
                                      m_activeCyl->GetDenom()>0))
                    {
                        err.Format(errf, (LPCTSTR)denom);
                    }

                    m_psx->SetConfigProperty(_T("XMLeadthruText"), 
                                             UI::PROPERTYTEXTFORMAT, err);
                }
                m_keypad->Clear();   // Clear out text (same as base).
                return STATE_NULL;
            }

            pchange /= abs(m_activeCyl->GetDenom());    // Convert to a count.
        }
        // -RFC 397451/SR748

        // Set to maximum count if attempting to dispense > count.
        m_activeCyl->SetPendingCountChange(0);

        // TAR 380421 - Display message if dispense count
        // exceeds current count
        // Notes in the loader cannot be dispensed.
        int ncnt = m_activeCyl->Count() - m_activeCyl->GetLoaderCount();

        if(pchange > ncnt)
        {
            int maxchar = MAX_PATH;
            TCHAR szTxt[MAX_PATH];
            CString csTmp;

            if(m_psx->GetString(_T("XM_DispenseThreshold"), szTxt, maxchar))
            {
                csTmp.Format(szTxt, ncnt);
                
                // TAR 380813 - Make message visible
                AlertMessage( true, csTmp ) ;
            }

            // Reset to current count
            pchange = ncnt;
        }
        
        pchange *= -1; // Display negative value.
    }
    
    // If the active cylinder already has a pending count change or a pending
    // count change was just entered, then store it.
    if(pchange != 0 || m_activeCyl->GetPendingCountChange() != 0)
    {
        long denom = m_activeCyl->GetDenom();

        cpending[denom] = abs(pchange);

        // +RFC400821:  Temporarily turn off showing full base on high sensor.
        IEventHandler *ie = NULL;
        if(g_cyls.Lookup(denom, ie))
        {
            ie->SetNormal();
        }
        // -RFC400821
                
        m_activeCyl->SetPendingCountChange(pchange);

        // Update the total pending count display.  
        int tval = cpending.TotalValue(denom>0);
        int tcnt = cpending.TotalCount(denom>0);

        SetPendingText(tcnt, tval, denom>0);
        // +TAR 438430
        if ( 0 == pchange ) // pending change is zero
        {
            m_activeCyl->SetPendingCountChange(0);
            // Remove any pending change for this denomination.
            cpending.Remove(denom);
        }
        // -TAR 438430
    }

    //+RFQ 3067 - BNR Dispense to Cashbox
    if ( m_activeCyl->GetDenom() > 0)
    {
        // Read configured AllowDispenseToCashbox.
        CString csOpt;
        if(!m_cfg->GetOption(_T("AllowDispenseToCashbox"), csOpt))
        {
        m_logger->Log(TRACE_WARNING, 
                    _T("WARNING: Could not read AllowDispenseToCashbox. Using the default value."));
              csOpt = _T("Y");
        }

        bool bAllowDispenseToCashBox = csOpt.CompareNoCase(_T("Y")) == 0 ? true : false;

        // If <AllowDispenseToCashbox=Y> and <AllowNoteDispense=N> then any selected bill counts 
        // will be transferred to the cash box rather than being dispensed to the bill exit after we commit the changes.
        CountTracker *tracker = NULL;
        if ( (bAllowDispenseToCashBox)  && ( !m_xdev->DispenseAllowed(CW_NOTES) || 
             (m_counts.Lookup(ND_NOTEID, tracker) && tracker->GetPendingCountChange() > 0 ) ) )
        {
            XPSButton7();   //Transfer(x) Count
        }
    }
    //-RFQ 3067 - BNR Dispense to Cashbox

    m_keypad->Clear();  // Clear out echo text.
        
    // Disable Enter, Clear and backspace keys.
    m_keypad->SetAltEnabled(false);

    Redraw();
    return STATE_NULL;
}

XMState * XMCashRemoveBase::XPSButton1(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton1"));
    //+429757 -Action completion code should be refactored into separate function.
    return XMCashManagementBase::XPSButton1();   
}

XMState * XMCashRemoveBase::XPSButton2(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton2"));

    CString csTxt;
    if(m_psx->GetContext(csTxt) && csTxt == _T("XMConfirmAction"))
    {   // User has pressed "No" in confirmation screen.
        UnInitialize();         // Pseudo state change.
        Initialize();    
        return STATE_NULL;
    }

    SetPendingManualChange();   
    BuildPrompt(_T("XM_ConfirmManualCoinEmpty"), m_promptstr, *m_disp);

    return STATE_NULL;
}

XMState * XMCashRemoveBase::XPSButton3(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton3"));

    if(!m_xdev->IsRecycler(CW_NOTES))
    {                     // Manual note removal.
        SetPendingManualChange(CW_NOTES);   
        BuildPrompt(_T("XM_ConfirmManualNoteEmpty"), 
                    m_promptstr, *m_disp,  CW_NOTES);
    }

    return STATE_NULL;
}

/**
 * Empty Coins to Base Level.
 */
XMState * XMCashRemoveBase::XPSButton4(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton4"));

    assert(m_pending->TotalCount(CW_COINS) == 0);

    if(PrestageBulkChange(false, false, CW_COINS))
    {
        int tval = m_pending->TotalValue(CW_COINS);
        int tcnt = m_pending->TotalCount(CW_COINS);
        SetPendingText(tcnt, tval, CW_COINS);
    }

    //this will hide the errorAlertBox when this button is pressed
    ShowErrorAlert(false);	//tar 436337

    Redraw();
    return STATE_NULL;
}

bool XMCashRemoveBase::PrestageBulkChange(bool empty, bool transfer, 
                                              bool notes)
{
    TRACEAPI(_T("XMCashRemoveBase::PrestageBulkChange"));

    CashUnit cu;
    CurrencyWrapper &cpending = *m_pending;
    long denom = 0;
    long cnt;
    long baselvl;
    CString key;
    CountTracker *tracker = NULL;

    bool bIncludeLoader = false;
    bool bZeroLoader = false;
    CString csOpt;
    if(m_cfg->GetOption(_T("IncludeLoaderInBaseXfr"), csOpt) &&
                        csOpt.CompareNoCase(_T("Y")) == 0)
    {
        bIncludeLoader = true; 
    }

    if(m_cfg->GetOption(_T("ZeroLoaderOnXfer"), csOpt) &&
       csOpt.CompareNoCase(_T("Y")) == 0)
    {
        bZeroLoader  = true;
    }

    AlertMessage(false);    // Hide instructional box.
    EnableDisableCashDevices(); // TAR385004

    // Remove all but the currency we are working with.  
    // TAR 387551 - Save original m_disp counts.
    CString csDisp = m_disp->ToString();
    m_disp->Empty(! notes);

    // There should not be any other pending operations. 
    assert(cpending.TotalCount(notes) == 0);

    // Update display and m_pending to show decrease in dispenser levels.
    m_disp->Begin();
    while(m_disp->Next(cu))
    {
        denom = cu.Denom();
        cnt = cu.Count();

        if(empty)
        {
            baselvl = 0;
        }
        else   // Transfer to base level.  
        {
            // TAR 412800:  Don't include loader count in total unless
            //              configured to include loader in base xfr.
            if(! bIncludeLoader)
            {
                cnt -= m_xdev->GetLoaderCount(denom);
            }

            baselvl = m_xdev->GetDispenserBaseLevel(denom);
        }

        if(cnt > baselvl)   // If current count is > base level.
        {
            key.Format(_T("D_%d"), denom);
            if(m_counts.Lookup(key, tracker))
            {
                tracker->SetPendingCountChange(baselvl - cnt); // Make negative.
                cpending[denom] = cnt - baselvl;               // Setup change.
            }

            // +RFC400821:  Temporarily disable use of high-sensor for full. 
            IEventHandler *ie = NULL;
            if(g_cyls.Lookup(denom, ie))
            {
                ie->SetNormal();
            }
            // -RFC400821
        }
    }

    if(! transfer)                      // Then we are done.
    {
        m_disp->Empty();                // TAR 387551 : Restore m_disp counts.
        m_disp->SetCounts(csDisp);
        return true;
    }

    // TAR 384662: If this is a transfer operation and the amount to be 
    // transferred exceeds the available space of the overflow container, then 
    // cancel the operation and instruct the user to empty the overflow 
    // container before doing the transfer.

    // Get the current cash box available space.
    long ndremain = m_xdev->GetNonDispensableCapacity(notes);
    if( m_counts.Lookup(notes ? ND_NOTEID : ND_COINID, tracker) )
    {
        ndremain -= tracker->Count();
    }
       
    bool rc = true;
    int pendingCnt = cpending.TotalCount(notes);
    
    // +TAR 412800:  If the notes in the loader won't be transferred, then
    //               don't include the count for them when determining the
    //               cashbox space that is required.

    //+SSCOADK-7291
    int nLoaderCount = m_xdev->GetTotalLoaderCount(notes);
    int nLoaderCapacity = m_xdev->GetLoaderCapacity(notes);

    if(bZeroLoader)
    {
        // If this is a transfer to base level and we don't include the loader
        // in the transfer, then add in the loader count we are about to 
        // subtract.
        if(!empty && !bIncludeLoader)
        {
            pendingCnt += nLoaderCount;
        }
        pendingCnt -= nLoaderCount;
    }
    // -TAR 412800

    if(nLoaderCount > nLoaderCapacity)
    {
        // If calculated loader counts > capacity then the number is probably wrong.
        // Subtract the extra loader counts.
        m_logger->Log(TRACE_WARNING, 
          _T("PrestageBulkChange: Loader count is more than capacity. Assuming loader count = capacity."));
        pendingCnt = pendingCnt - nLoaderCount + nLoaderCapacity;
    }
    //-SSCOADK-7291

    if(pendingCnt > ndremain)
    {
        // Need to restore the current cylinder levels and reset m_pending.
        // We must insure that if the aborted transfer is for notes that 
        // any pending coin operations are not disturbed (and vice-versa).
        m_disp->Begin();
        while(m_disp->Next(cu))
        {
            denom = cu.Denom();
            key.Format(_T("D_%d"), denom);
            if(m_counts.Lookup(key, tracker))
            {
                tracker->SetPendingCountChange(0);  // Clear pending change.
            }
        }

        cpending.Empty(notes);

        m_logger->Log(TRACE_WARNING, 
                      _T("PrestageBulkChange:Insufficient space in overflow."));

        // Display problem to the user.
        m_psx->SetConfigProperty(_T("XMAlertBox"), UI::PROPERTYTEXTFORMAT, 
                                  _T("$XM_CannotCompleteTransfer$"));
        m_psx->SetControlProperty(_T("XMAlertBox"), UI::PROPERTYVISIBLE, 
                                 VARIANT_TRUE);

        rc = false;
    }

    m_disp->Empty();                // TAR 387551 : Restore m_disp counts.
    m_disp->SetCounts(csDisp);
    return rc;
}

/**
 * Transfer Notes to Base Level.
 */
XMState * XMCashRemoveBase::XPSButton5(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton5"));

    assert(m_pending->TotalCount(CW_NOTES) == 0);

    CountTracker *tracker;
    if(PrestageBulkChange(false, true, CW_NOTES) &&
       m_counts.Lookup(ND_NOTEID, tracker) )
    {
        bool bZeroLoader    = false;
        bool bIncludeLoader = false;

        // +TAR 412800:  Determine if we will transfer the loader or not.
        CString csOpt;
        if(m_cfg->GetOption(_T("IncludeLoaderInBaseXfr"), csOpt) &&
                            csOpt.CompareNoCase(_T("Y")) == 0)
        {
            bIncludeLoader = true; 
        }

        if(m_cfg->GetOption(_T("ZeroLoaderOnXfer"), csOpt) &&
           csOpt.CompareNoCase(_T("Y")) == 0)
        {
            bZeroLoader  = true;
        }
        // -TAR 412800
        
        // +TAR 412800:  If configured to xfer loader then add it to the counts.
        int tcnt = m_pending->TotalCount(CW_NOTES);
        int tval = m_pending->TotalValue(CW_NOTES) + 
                   tracker->GetTotalValue();

        // If the loader will be zeroed and the loader is included in the 
        // transfer, then we need to backout the loader counts.
        if(bZeroLoader && bIncludeLoader) 
        {
            tcnt -= m_xdev->GetTotalLoaderCount(CW_NOTES);
            tval -= m_xdev->GetTotalLoaderValue(CW_NOTES);

            assert(tcnt >=0 && tval >= 0);
        }

        // Show the overflow level increase.
        tracker->SetPendingCountChange(tcnt);
        tracker->SetTotalValue(tval);
        // -TAR 412800
    }
    Redraw();
    return STATE_NULL;
}

XMState * XMCashRemoveBase::XPSButton6(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton6"));
    //previous XMButton3 have two function
    //Namely Reset Bill Counts and transfer all bills
    //This two buttons are separated
    //transfer all bills is now using XMButton6
    //button handler for XMButton6 should still be the same (XPSButton3)
    
    //transfer functionality of xmbutton6 here (from xpsbutton3) 
     if(m_xdev->IsRecycler(CW_NOTES))    
    {                       // Transfer all notes.
        assert(m_pending->TotalCount(CW_NOTES) == 0);

        CountTracker *tracker;
        if(PrestageBulkChange(true, true, CW_NOTES) &&
           m_counts.Lookup(ND_NOTEID, tracker) )
        {
            assert(tracker->GetPendingCountChange() == 0);
            int tcnt = m_pending->TotalCount(CW_NOTES);
            int tval = m_pending->TotalValue(CW_NOTES) + 
                       tracker->GetTotalValue();

            // Show the overflow level increase.

            // +TAR 412800:  If loader will be zeroed instead of transferred, 
            //               then subtract the loader count from the pending 
            //               transfer.
            CString csOpt;
            if(m_cfg->GetOption(_T("ZeroLoaderOnXfer"), csOpt) &&
                                csOpt.CompareNoCase(_T("Y")) == 0)
            {
                tcnt -= m_xdev->GetTotalLoaderCount(CW_NOTES);
                tval -= m_xdev->GetTotalLoaderValue(CW_NOTES);

                assert(tcnt >=0 && tval >= 0);
            }
            // -TAR 412800

            tracker->SetPendingCountChange(tcnt);
            tracker->SetTotalValue(tval);
        }
        Redraw();
    }
    return STATE_NULL; 
}

/**
 * Transfer (X) Count/Amount
 * Added for RFQ 3067 - BNR Dispense to Cashbox
 */
XMState * XMCashRemoveBase::XPSButton7(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton7"));

    if(m_xdev->IsRecycler(CW_NOTES))    
    {      
        assert(m_pending->TotalCount(CW_NOTES) > 0);

        AlertMessage(false);
        
        // if the total number of bills to be transfer to cashbox is greater than
        // the cashbox available space, then an alert message will be display.
        if ( m_pending->TotalCount(CW_NOTES) > (m_xdev->GetNonDispensableCapacity(CW_NOTES) 
                                    - m_ndisp->TotalCount(CW_NOTES)) ) 
        {
            // TAR 430534 
            // Clear pending change too.
            CountTracker *tracker=NULL;
            CashUnit cu;
            CString csKey;
            long lDenom=0;
            m_disp->Begin();
            while(m_disp->Next(cu))
            {
                lDenom = cu.Denom();
                csKey.Format(_T("D_%d"), lDenom);
                if(m_counts.Lookup(csKey, tracker))
                {
                tracker->SetPendingCountChange(0);  
                }
            }
            AlertMessage( true, _T("$XM_CannotCompleteTransfer$")) ;
            m_pending->Empty(CW_NOTES);     //Clear pending notes

            COleVariant vstate = (long)UI::Disabled;
            m_psx->SetConfigProperty(_T("XMButton1"),UI::PROPERTYSTATE, vstate);
            m_psx->SetControlProperty(_T("XMCancelChanges"), 
                                  UI::PROPERTYSTATE, vstate);
        }
        else
        {
            CString csResult, csPrompt;
            int nValue, nTotalCnt, nTotalVal ;

            assert(m_pending->TotalCount(CW_NOTES) > 0);

            CountTracker *tracker = NULL;   
            if (m_counts.Lookup(ND_NOTEID, tracker) )   
            {
                assert(tracker != NULL );

                //total notes pending count
                nTotalCnt = m_pending->TotalCount(CW_NOTES);
                
                if ( m_pending->TotalCount(CW_NOTES) <= 0)
                {
                    m_logger->Log(TRACE_INFO, _T("PENDING COUNTS: [%d]"), nTotalCnt);
                    return STATE_NULL;
                }

                //total notes pending value + the cashbox total value
                nTotalVal =  m_pending->TotalValue(CW_NOTES) +      
                                 m_ndisp->TotalValue(CW_NOTES); 
                
                tracker->PendingEmpty(false);
                tracker->SetPendingCountChange(nTotalCnt);
                tracker->SetTotalValue(nTotalVal);
                m_bTransferXPressed = true;
                Redraw();
        
                if ( ! m_bCashManagementDisplayCount )
                {
                    nValue =  m_pending->TotalValue(CW_NOTES);
                    //uses "Currency-Trim-Decimal" option
                    FormatCurrency(abs(nValue), csResult, m_bTrimDecimal);      
                }
                else
                {
                    csResult.Format( _T("%d"), m_pending->TotalCount(CW_NOTES) );
                }
                CString csMessage = _T("XM_TransfertoCashBoxLeadThruText");
                // TAR 439805: Pickup method calls XPSButton7(). We will only display 
                // transfer (x) leadthru if pickup method is not the one calling this function.
                if(m_psx->GetString(csMessage, csPrompt) && 0 == m_nPickUpFlag)
                {
                    csMessage.Format(csPrompt, csResult);
                    m_psx->SetConfigProperty(_T("XMLeadthruText"), UI::PROPERTYTEXTFORMAT, csMessage);
                }
                COleVariant vstate = VARIANT_FALSE;
                m_psx->SetConfigProperty(_T("XMPendingNoteValue"), UI::PROPERTYVISIBLE,vstate);
                m_psx->SetConfigProperty(_T("XMPendingNoteCount"), UI::PROPERTYVISIBLE,vstate);
            }
        }
        //disable "Transfer (x) Count" button
        m_psx->SetControlProperty( _T("XMButton7"), UI::PROPERTYSTATE, (long)UI::Disabled );     
        //disable "Transfer Bills to Base Level" button
        m_psx->SetControlProperty( _T("XMButton5"), UI::PROPERTYSTATE, (long)UI::Disabled );
    }
    return STATE_NULL;
}

XMState * XMCashRemoveBase::XPSButton8(void)
{
    TRACEAPI(_T("XMCashRemoveBase::XPSButton8"));

    //this will hide the errorAlertBox when this button is pressed
    ShowErrorAlert(false);	//tar 436337
    AlertMessage(false);
    m_bIsExitTouched = true;
	
	COleVariant vstate = (long)UI::Disabled;
    m_psx->SetConfigProperty(_T("XMButton8"), UI::PROPERTYSTATE, vstate); //SSCOADK-6106

    XMCashManagementBase::XPSButton8();

    RETURN_STATE(STATE_CASHSTATUS);
}

// RFQ 432305: Pick Up Auto Amount button
XMState * XMCashRemoveBase::XPSButton9(void)
{
    AlertMessage(false);    // Hide instructional box.
    
    m_keypad->SetKeyPadEnabled(false);
    m_nPickUpFlag = -1;
    CalculatePickUp(m_nStartingBank);
    return STATE_NULL;
}

// RFQ 432305: Pick Up (x) Amount button
XMState * XMCashRemoveBase::XPSButton10(void)
{
    AlertMessage(false);    // Hide instructional box.
    m_keypad->SetDisplayByCount(false); //Set display by value  
    
    m_nPickUpFlag = 1;  
    Redraw();
        
    CString csPrompt;
    if (m_psx->GetString(_T("XM_KeyPadEnterPickUpAmount"), csPrompt))
    {           
        m_psx->SetConfigProperty(_T("XMLeadthruText"), UI::PROPERTYTEXTFORMAT, csPrompt);
    }       
    return STATE_NULL;
}

XMState * XMCashRemoveBase::XPSButton(long lButtonID)
{
    //this will hide the errorAlertBox when this button is pressed
    ShowErrorAlert(false);	//tar 436337

    XMCashManagementBase::XPSButton(lButtonID); 
    // RFQ 432305:If cylinder is pressed, disable pick up buttons.
    if(m_xdev->IsRecycler(CW_NOTES))
    {
        IButtonMap & rBMap = *m_pBtnMenu;
        if(m_activeCyl)
        {
            //Set display count back to configured option. We are no 
            //longer in pickup function.
            m_keypad->SetDisplayByCount(m_bCashManagementDisplayCount);             
            rBMap[_T("XMButton9")]->Disable();
            rBMap[_T("XMButton10")]->Disable();        
        } 
    }	

    return STATE_NULL;
}

void XMCashRemoveBase::Reset(bool final)
{
    TRACEAPI(_T("XMCashRemoveBase::Reset"));

    m_bCoinEmpty = false;   // Reset manual dispenser empty flags.
    m_bNoteEmpty = false;   // Reset manual dispenser empty flags.
    //+ RFQ 432305
    //Set display count back to configured option.
    m_keypad->SetDisplayByCount(m_bCashManagementDisplayCount); 
    m_nPickUpFlag = 0; //reset the flag.
    //- RFQ 432305
    m_bTransferXPressed = false;    

    XMCashManagementBase::Reset(final);
}

void XMCashRemoveBase::SetPendingManualChange(bool notes)      
{
    TRACEAPI(_T("XMCashRemoveBase::SetPendingManualChange"));
    POSITION p = m_dbyID.GetStartPosition();
    int key;

    // PSX coin cylinder control IDs are 401-499.  note cylinder IDs are 
    // 501-599.  This variable is just used to determine if a particular 
    // cylinder is a note or a coin.
    const int keyTarg = notes ? 5 : 4;
    CountTracker *tracker;      // Current cylinder being accessed.

    AlertMessage(false);        // Hide instructional box.
    EnableDisableCashDevices(); // TAR385004

    m_logger->Log(TRACE_INFO, _T("SetPendingManualChange(%s)"),
                              notes ? _T("NOTES") : _T("COINS"));

    // Cycle through each tracker and do required manual operation.
    while(p != NULL)                    
    {
        m_dbyID.GetNextAssoc(p, key, tracker);

        // Just get the integer portion of the tracker ID.  If key/100 == 4, 
        // then this is a coin tracker.  If key/100 == 5, then this is a note
        // tracker.
        if(keyTarg == (key/100))    
        {
            if(notes)
                m_bNoteEmpty = true;
            else
                m_bCoinEmpty = true;

            // +RFC400821:  Temporarily turn off high sensor full.
            IEventHandler *ie = NULL;
            if(g_cyls.Lookup(tracker->GetDenom(), ie))
            {
                ie->SetNormal();
            }
            // -RFC400821

            tracker->SetPendingCountChange(-(tracker->Count()));
        }
    }

    // Reset the pending purge operation count
    if(notes && m_bNoteEmpty && m_counts.Lookup(PURGEOPERATION, tracker) )
    {
        tracker->SetPendingCountChange(-(tracker->Count()));
        if(tracker->Count() > 0)
        {
            m_bIsPurge = true;
        }
        else
        {   
            m_bIsPurge = false;
        }
    }


    if(m_activeCyl)
    {    // Reset to no active cylinders.
        m_activeCyl->UnSelect();
        m_activeCyl = NULL;
        m_keypad->Clear();
    }
    Redraw();
}

void XMCashRemoveBase::SetPendingOverflowEmpty(bool notes)      
{
    TRACEAPI(_T("XMCashRemoveBase::SetPendingOverflowEmpty"));
    m_logger->Log(TRACE_INFO, _T("SetPendingOverflowEmpty - %s"),
                  notes ? _T("EMPTY NOTES") : _T("EMPTY COINS"));

    CountTracker *tracker = NULL;
    CString trackerID = ND_COINID;
    
    if(notes)
    {
        trackerID = ND_NOTEID;
        if (m_xdev->IsRecycler(true))
            BuildPrompt(_T("XM_ConfirmEmptyNoteOverflow"), 
                    m_promptstr, *m_ndisp, CW_NOTES);
        else
            BuildPrompt(_T("XM_ConfirmEmptyNoteAcceptor"), 
                    m_promptstr, *m_ndisp, CW_NOTES);
    }
    else
    {
        if (m_xdev->IsRecycler(false))
            BuildPrompt(_T("XM_ConfirmEmptyCoinOverflow"),m_promptstr,*m_ndisp);
        else
            BuildPrompt(_T("XM_ConfirmEmptyCoinAcceptor"),m_promptstr,*m_ndisp);
    }

    if(m_counts.Lookup(trackerID, tracker))
    {
        tracker->PendingEmpty(true);// Show the cylinder as empty.
    }
    Redraw();                       // Update control buttons.
}

bool XMCashRemoveBase::HandleTransfer(void)
{
    TRACEAPI(_T("XMCashRemoveBase::HandleTransfer"));
    bool rc = true;

    if(m_pending->Size() == 0)  // Nothing to transfer.
    {
        return true;
    }

    CountTracker *tracker = NULL;

    // Check for note transfer.
    if(m_pending->TotalCount(CW_NOTES))
    { 
        if(m_counts.Lookup(ND_NOTEID, tracker) && 
           tracker->GetPendingCountChange() > 0)// Should only be true for xfer.
        {
            m_logger->Log(TRACE_INFO, _T("Transferring [%s] to note overflow"),
                          m_pending->ToString(CW_NOTES));

            rc = m_xdev->Transfer(*m_pending, CW_NOTES);
            if(! rc) 
            {
                // FIXME>  Show alert message.
                m_logger->Log(TRACE_ERROR, 
                        _T("Transfer of [%s] FAILED"), 
                          m_pending->ToString(CW_NOTES));
            }

            // +TAR 412800:  Prompt the user to empty the loader cassette.
            CString csOpt;
            if(m_cfg->GetOption(_T("ZeroLoaderOnXfer"), csOpt) &&
                                csOpt.CompareNoCase(_T("Y")) == 0)
            {
                // If there were notes in the loader cassette.
                if(m_pending->TotalCount(CW_NOTES) > 
                   tracker->GetPendingCountChange())
                {
                    BuildPrompt(_T("XM_EmptyLoaderCassetteLT"), m_promptstr,
                                *m_pending, CW_NOTES);
                }
            }
            // -TAR 412800

            m_pending->Empty(CW_NOTES);         // Clear pending notes.
        }
    }

    // Check for coin transfer.
    if(m_pending->TotalCount(CW_COINS))
    { 
        if(m_counts.Lookup(ND_COINID, tracker) && 
           tracker->GetPendingCountChange() > 0)// Should only be true for xfer.
        {
            m_logger->Log(TRACE_INFO, _T("Transferring [%s] to coin overflow"),
                          m_pending->ToString(CW_COINS));

            rc = m_xdev->Transfer(*m_pending, CW_COINS);
            if(! rc) 
            {
                // FIXME>  Show alert message.
                m_logger->Log(TRACE_ERROR, 
                        _T("Transfer of [%s] FAILED"), 
                          m_pending->ToString(CW_COINS));
            }
            m_pending->Empty(CW_COINS);         // Clear pending notes.
        }
    }
       
    Redraw(true);                               // Repaint display.

    //RFQ 432305:Moved disable Apply/Cancel Changes to HandleDispense()
    
    return rc;
}

bool XMCashRemoveBase::HandleDispense(void)
{
    TRACEAPI(_T("XMCashRemoveBase::HandleDispense"));
    bool rc = false;

#if 0
    // There should not be any coin dispenses if coins are being emptied.
    assert((m_pending.Size(CW_COINS)>0) == !m_bCoinEmpty);

    // There should not be any note dispenses if notes are being emptied.
    assert((m_pending.Size(CW_NOTES)>0) == !m_bNoteEmpty);
#endif

    if(m_pending->Size() == 0)
    {
        return true;
    }

    if(m_UID.IsEmpty())
    {
        assert(false);
        m_logger->Log(TRACE_ERROR, 
                _T("ERROR:  Dispense without authentication!"));
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, XM_CAT_INTERNAL_ERROR,
                             XM_CM_AUTHENTICATION_ERROR, 2, 
                             _T("XMCashRemoveBase::HandleDispense"), 
                             _T("Dispense"));
        return false;
    }

    // Create a wrapper to determine any failed dispense amounts.
    CurrencyWrapper *cw = CreateCurrencyWrapper();
    if(cw == NULL)
    {
        return false;
    }

    // Clear pending counts.
    POSITION p = m_dbyID.GetStartPosition();
    CountTracker *tracker;
    CString csTxt;
    int tid;                // tracker identifier.

    CurrencyWrapper &cdisp = *m_disp;
    CurrencyWrapper &cpending = *m_pending;

    // Clear pre-emptive message
    AlertMessage( false ) ; 

    CashUnit cu;
    while(p != NULL)        // Reset pending counts (dispensable only).
    {
        m_dbyID.GetNextAssoc(p, tid, tracker);
        tracker->PendingEmpty(false);

        // +TAR 399009:  Make sure that there are no denominations with zero
        //               or negative counts in the request.  The BCR treats
        //               a dispense request with a count of zero as a signal to
        //               purge the entire hopper.
        if(cpending.GetCashUnit(tracker->GetDenom(), cu) && cu.Count() > 0)
        {
            (*cw)[tracker->GetDenom()] = cu.Count();
        }
        // -TAR 399009
    }

    assert(cw->TotalValue() == cpending.TotalValue());
    //RFQ 432305: If there is a pending note xfr, then don't
    // dispense the note portion of the request.
    CString csXfr;  
    if(m_counts.Lookup(ND_NOTEID, tracker) && 
           tracker->GetPendingCountChange() > 0)
    {       
        csXfr = cw->ToString(CW_NOTES); // Save xfr request         
        cw->Empty(CW_NOTES); // Remove the note portion of the dispense.    
    }
    // Dispense the currency.
    csTxt = cw->ToString();

    m_logger->Log(TRACE_INFO, _T("Dispense request: [%s] (%d)"), 
                  (LPCTSTR)csTxt, cpending.TotalValue());

    cpending.Empty();  // Clear PENDING_DISPENSE

    // Hide Pending fields.
    SetPendingText(0, 0, CW_COINS);
    SetPendingText(0, 0, CW_NOTES);
    
    //TAR 439153 
    CString csCurrentContext;
    m_psx->GetContext(csCurrentContext);
    //TAR 439153 

    // +TAR 384998: Moved button disable to XPSButton1.
    //new ui-remove messages
    CString csAlertmsg;
    if ( cw->TotalCount(CW_COINS) > 0)
    {
        // If sensor is blocked, display error message and return
        long lSensorStatus = m_xdev->GetDispenseSensorStatus( CW_COINS ) ;
        if ( lSensorStatus != OPOS_SUCCESS )
        {
            m_logger->Log( TRACE_INFO, _T("Dispense sensor status: [%d] "), 
                           lSensorStatus ) ;

            DestroyCurrencyWrapper(cw);
            AlertMessage( true, _T("$XM_DispenseSensorBlock$") ) ;

            return ( false ) ;
        }
        // Display pre-emptive message
        csAlertmsg = _T("$XM_DispenseCupFullPreemptive$");
        AlertMessage( true, csAlertmsg);
    }
    if ( cw->TotalCount(CW_NOTES) > 0)
    {
        // If sensor is blocked, display error message and return
        long lSensorStatus = m_xdev->GetDispenseSensorStatus( CW_NOTES ) ;
        if ( lSensorStatus != OPOS_SUCCESS )
        {
            m_logger->Log( TRACE_INFO, _T("Dispense sensor status: [%d] "), 
                           lSensorStatus ) ;

            DestroyCurrencyWrapper(cw);
            AlertMessage( true, _T("$XM_DispenseSensorBlock$") ) ;

            return ( false ) ;
        }
         // Display pre-emptive message
        csAlertmsg = _T( "$XM_DispenseNoteFullPreemptive$");
        AlertMessage( true, csAlertmsg);
    }
    if( cw->TotalCount(CW_NOTES) > 0 && cw->TotalCount(CW_COINS) >0)
    {
         // Display pre-emptive message
        csAlertmsg = _T( "$XM_DispenseFullPreemptive$");
        AlertMessage( true, csAlertmsg);
    }
    
    //new ui - remove messages

    long lrc = m_xdev->Dispense(csTxt);
    m_logger->Log(TRACE_EXTENSIVE, _T("Dispense rc=%#X"), lrc);

    EnableDisableCashDevices(); // TAR385004

    csTxt.Empty();

    // Find out what was dispensed.
    if(m_xdev->GetDispensedCashList(csTxt))
    {
        if(csTxt.GetLength() > 0)
        {
            cpending = (LPCTSTR)csTxt;
            cdisp -= cpending;          // Subtract dispensed amount.
            *cw -= cpending;            // See if everything dispensed OK.
        }
        
        m_logger->Log(TRACE_INFO, _T("Dispense result: [%s] (%d)"),
                      (LPCTSTR)csTxt, cpending.TotalValue());
        rc = true ;
    }
    else
    {
        rc = false;
        m_logger->Log(TRACE_ERROR, _T("GetDispensedCashList FAILED."));
        m_logger->LogXMEvent(evlClassBasic,evlTypeError, XM_CAT_INTERNAL_ERROR,
                             XM_CM_GETDISPENSE_FAIL, 3, 
                             _T("XMCashRemoveBase::HandleDispense"), 
                             (LPCTSTR)*m_disp, (LPCTSTR)*m_ndisp);
    }

    cpending.Empty();
    // RFQ 432305: If there is pending transfer, then restore note values.
    if (! csXfr.IsEmpty())
    {   
        // FIX ME>> Need to insert ; to the zero index of cashlist because
        // there is a bug in cw->ToString(CW_NOTES). It doesn't include ;
        csXfr.Insert(0,_T(";"));        
        cpending = csXfr;
    }

    // +TAR 380488:  Display an error box if actual dispense is not
    //               what was requested.
    if(cw->TotalCount() > 0)
    {
        TCHAR szDisp[CFMTBUFFSZ] = _T("");
        int maxchar = MAX_PATH;
        TCHAR szTxt[MAX_PATH] = _T("");
        CString csMsg, csEvLog;
		rc = false;

        FormatCurrency(cw->TotalValue(), szDisp, CFMTBUFFSZ, false, false);

        m_logger->Log(TRACE_ERROR, _T("ERROR:  FAILED TO DISPENSE %d"),
                      cw->TotalValue());
        // TAR389392 - Add failed dispense amount and cash counts to the event log.
        csEvLog.Format(_T("%d"), cw->TotalValue()); 
        m_logger->LogXMEvent(evlClassBasic, evlTypeError, 
                            XM_CAT_INTERNAL_ERROR, 
                            XM_CM_DISPENSE_FAIL, 4, 
                            _T("XMCashRemoveBase::HandleDispense"), 
                            csEvLog, (LPCTSTR)*m_disp, (LPCTSTR)*m_ndisp);

        bool bNotesCount, bCoinsCount;
        bNotesCount = cw->TotalCount(CW_NOTES)>0;
        bCoinsCount = cw->TotalCount(CW_COINS)>0;
							
        if(bNotesCount && bCoinsCount)
        {
            // Both note and coin dispense failed.
            csMsg = _T("XM_BothDispenseFailure");
        }
        else if(bCoinsCount)
        {
            // Coin dispense failed.
            csMsg = _T("XM_CoinDispenseFailure");
        }
        else if(bNotesCount)
        {
            // Note dispense failed.
            csMsg = _T("XM_NoteDispenseFailure");
        }
        else
        {
            assert(false);  // ???
            csMsg = _T("XM_BothDispenseFailure");
        }
            
        if(m_psx->GetString(csMsg, szTxt, maxchar))
        {
            csMsg.Format(szTxt, szDisp); 
            ShowErrorAlert(true, csMsg);    //tar 436337
            //+tar 439153			
            COleVariant vstate = VARIANT_FALSE;
            m_psx->SetConfigProperty( _T( "XMAlertBox" ), csCurrentContext,
                                            UI::PROPERTYVISIBLE, vstate ) ;
            //-tar 439153																
            EnableDisableCashDevices(); // TAR385004
        }
		
		// Don't call PostDMEvent if overdispense.
        if(cw->TotalCount(CW_NOTES)>0 && lrc != OPOS_ECHAN_OVERDISPENSE)
        {
            m_xdev->PostDMEvent(DMCLASS_CASHCHANGER, CHAN_STATUS_JAM, CW_NOTES);
        }

        if(cw->TotalCount(CW_COINS)>0)
        {
            m_xdev->PostDMEvent(DMCLASS_CASHCHANGER, CHAN_STATUS_JAM, CW_COINS);
        }
    }
    // -TAR 380488
    
    DestroyCurrencyWrapper(cw);

    // +RFC 400821:  Check high-level sensors.
    bool bhandled = false;
    XDMParse(EVT_REFRESH_DATA, 0, bhandled, DMCLASS_CASHCHANGER, 0); 
    // -RFC 400821
    // Temporary codes to keep Complete and Cancel buttons disabled
    COleVariant vstate = (long)UI::Disabled ;
    m_psx->SetControlProperty( _T("XMButton1"), UI::PROPERTYSTATE, vstate);
    m_psx->SetControlProperty( _T("XMCancelChanges"), 
                               UI::PROPERTYSTATE, vstate ) ;
    
    if(lrc == OPOS_ECHAN_OVERDISPENSE)
    {
        // Override rc for overdispense to fool caller that dispense was successful.
        rc = true;
    }
    return rc;
}

bool XMCashRemoveBase::HandleManualChange(void)
{
    TRACEAPI(_T("XMCashRemoveBase::HandleManualChange"));

    CString csPrompt;               // RFC 386873

    // Handle manual note or coin empty.
    if(! m_bCoinEmpty && ! m_bNoteEmpty)
    {
        return false;
    }

    if(m_bCoinEmpty)            
    {
        assert(m_pending->Size(CW_COINS) == 0);
        m_disp->ZeroCounts(CW_COINS);
        m_bCoinEmpty = false;

        //TAR 454593 If the instruction to remove the last remaining
		//coins had been given, then do not give additional 
		//prompt to remove coins
		if( ! m_xdev->AllowRemoveLastFewCoins())
		{
			// +RFC 386873:  Add instructional text for alert prompt.
			if(m_psx->GetString(_T("XM_EmptyCoinHopperLT"), csPrompt))
			{
				csPrompt += _T('\n'); 
			}
			// -RFC 386873
		}
    }

    if(m_bNoteEmpty)           
    {
        assert(m_pending->Size(CW_NOTES) == 0);
        m_disp->ZeroCounts(CW_NOTES);
        m_bNoteEmpty = false;
        CString csTmp;

        // +RFC 386873:  Add instructional text for alert prompt.
        if(m_psx->GetString(_T("XM_EmptyNoteCassetteLT"), csTmp))
        {
            csPrompt += csTmp;
            csPrompt += _T('\n'); 
        }
        // -RFC 386873

        // Clear the Number of Purge Operations
        m_purge->ZeroCounts(CW_NOTES);
        CountTracker *tracker;      // Current cylinder being accessed.
        if( m_counts.Lookup(PURGEOPERATION, tracker) )
        {
            *tracker = m_purge->TotalCount();       // Sets the current count
        }

        if(m_xdev->SetPurgedCounts(m_purge->ToString())) // Send to device.
        {
            m_logger->Log(TRACE_INFO, _T("Number of Purged Operations reset to 0."));
        }
    }

    if(m_xdev->SetDispensableCounts(*m_disp)) // Send to device.
    {
        m_promptstr += csPrompt;              // RFC 386873
        EnableDisableCashDevices();           // TAR385004
    }

    return true;
}

bool XMCashRemoveBase::HandleOverflowEmpty(void)
{
    TRACEAPI(_T("XMCashRemoveBase::HandleOverflowEmpty"));

    CString csPromptID;
    CString csPrompt;

    if(m_counts[ND_COINID]->PendingEmpty())  // Coin Overflow.
    {
        m_ndisp->ZeroCounts(CW_COINS);
        m_counts[ND_COINID]->PendingEmpty(false);

        // Clear device counts.
        if(! m_xdev->ClearNonDispensableCounts(CW_COINS))
        {
            Redraw(true);
            return false;
        }

        if(m_xdev->IsRecycler(CW_COINS))
        {
            csPromptID = _T("XM_EmptyCoinOverflowLT");
        }
        else
        {
            csPromptID = _T("XM_EmptyCoinAcceptorLT");
        }

        // +RFC 386873:  Add instructional text for alert prompt.
        if(m_psx->GetString(csPromptID, csPrompt))
        {
            m_promptstr += csPrompt;
            m_promptstr += _T('\n'); 
        }
        // -RFC 386873

        EnableDisableCashDevices(); // TAR385004
    }

    if(m_counts[ND_NOTEID]->PendingEmpty())  // Note Overflow.
    {
        m_ndisp->ZeroCounts(CW_NOTES);
        m_counts[ND_NOTEID]->PendingEmpty(false);

        // Clear device counts.
        if(! m_xdev->ClearNonDispensableCounts(CW_NOTES))
        {
            Redraw(true);
            return false;
        }

        // +RFC 386873:  Add instructional text for alert prompt.
        if(m_xdev->IsRecycler(CW_NOTES))
        {
            csPromptID = _T("XM_EmptyCashBoxLT");
        }
        else
        {
            csPromptID = _T("XM_EmptyCashAcceptorLT");
        }

        if(m_psx->GetString(csPromptID, csPrompt))
        {
            m_promptstr += csPrompt;
            m_promptstr += _T('\n'); 
        }
        // -RFC 386873

        //+ RFQ - Add Purge Operations Indicator for BNR 
        // Pass an EMPTY STRING to clear purge counts as API specifies
        CountTracker *tracker=NULL;
        if(m_xdev->IsRecycler(CW_NOTES) && m_xdev->SetPurgedCounts(_T("")))
        {
            m_logger->Log(TRACE_INFO, _T("Number of Purged Operations reset to 0."));
            m_purge->ZeroCounts();  //Set purge count to zero            
            if( m_counts.Lookup(PURGEOPERATION, tracker) )
            {
                *tracker = m_purge->TotalCount();       // Update the current count 
            }        
            
        }
        if(m_purge->TotalCount() > 0)
        {
           m_bIsPurge = true;
        }
        else
        {   
            m_bIsPurge = false;
        }
        //- RFQ
        EnableDisableCashDevices(); // TAR385004
    }
    return true;
}

// Added for TAR 412364.
bool XMCashRemoveBase::BuildTBLoanData(CString &result)
{
    TRACEAPI(_T("XMCashRemoveBase::BuildTBLoanData"));

    // Currently, the only way that we can have an increase in the remove
    // screen is with a full note transfer with the BNR where the actual 
    // number of notes in the loader cassette is > the calculated number of
    // notes in the loader cassette.  When the notes are transferred, the
    // device counts the notes and this shows up in CM as a gain.
    if( ! m_xdev->IsRecycler(CW_NOTES) )   // Only valid for the note recycler. 
    {
        return false;
    }

    result.Empty();

    // m_pending should be empty unless we are in training mode.
    assert(m_pending->Size() == 0 || CheckTrainingMode());      

    CurrencyWrapper &cpending = *m_pending;
    cpending.Empty();

    long cchange;
    CashUnit cu;

    m_ndisp->Begin();           // Only should see a gain in the cashbox.

    // Step through each non-dispensable denomination and see if the counts
    // have changed.
    while(m_ndisp->Next(cu))
    {
        cchange = cu.ChangeInCount();

        if(cchange > 0 && cu.Denom() > 0)         
        {
            // Count for this denomination increased.  See if there was a 
            // corresponding decrease for the dispensable count (transfer).
            CashUnit cuDisp;
            if(m_disp->GetCashUnit(cu.Denom(), cuDisp))
            {
                // We should not be able to see an increase in dispensable
                // counts in the removal screen -- right?
                assert(cuDisp.ChangeInCount() <= 0);

                // If money was transferred from the recyclers to the cashbox,
                // then we must calculate the net change to see if any money
                // was added.
                if( (cchange + cuDisp.ChangeInCount()) > 0 )
                {
                    // If the net change > 0, then we have a loan.
                    cchange += cuDisp.ChangeInCount();
                    cpending[cu.Denom()] = cchange;
                }
            }
        } 
    }

    if(cpending.Size() > 0)
    {
        m_logger->Log(TRACE_INFO, _T("Combined count change: [%s]"), 
                      (LPCTSTR)cpending);
        
        result.Format(_T("%s%s"), OPERATORID, (LPCTSTR)m_UID);
        if(cpending.Size(CW_NOTES))   // Get note portion of the string.
        {
            result += CASHDISPENSER;
            result += cpending.ToString(CW_NOTES);  
        }

        // From the code above, there should not be any coin data present.
        assert(cpending.Size(CW_COINS) <= 0);

        m_logger->Log(TRACE_INFO, _T("Loan: [%s]"), (LPCTSTR)result);
        cpending.Empty();       // Clean up.
        return true;
    }

    m_logger->Log(TRACE_INFO, _T("No loan info found."));
    return false;

}

bool XMCashRemoveBase::BuildTBPickupData(CString &result)
{
    TRACEAPI(_T("XMCashRemoveBase::BuildTBPickupData"));
    result.Empty();

    // m_pending should be empty unless we are in training mode.
    assert(m_pending->Size() == 0 || CheckTrainingMode());      

    // Determine note pickup info.  For the recyclers, the overflow is 
    // treated the same as the acceptor.  This may be misleading, because the
    // money in the overflow container does not represent net money taken in
    // as does the acceptors.
    
    long cchange;
    CashUnit cu;
    CurrencyWrapper &cpending = *m_pending;
    cpending.Empty();

    // Handle acceptors/overflow first (non-dispensable).
    CString csCoin;             // Hold coin portion.
    CString csNote;             // Hold note portion.

    m_ndisp->Begin();
    while(m_ndisp->Next(cu))
    {
        cchange = cu.ChangeInCount();

        // +TAR 412364:  If non-dispensable counts increase, then notes
        //               were transferred.
        
#ifdef _DEBUG
        if(cchange > 0)
        {
            assert(cu.Denom() > 0);               // Should be a note.
            assert(m_xdev->IsRecycler(CW_NOTES)); // Should be a note recycler.
        }
#endif 

        // Restrict this change to the note recycler for now.  This will
        // decrease the chance of breaking existing behavior.
        if(cchange > 0 && cu.Denom() > 0 && m_xdev->IsRecycler(CW_NOTES))
        {
            // Check and see if there was a net change.
            CashUnit cuDisp;
            if(m_disp->GetCashUnit(cu.Denom(), cuDisp))
            {
                if( (cchange + cuDisp.ChangeInCount()) >= 0 )
                {
                    // Ignore. No net change in this denomination.
                    // Note:  If the net change is > 0, then this is a loan 
                    //        instead of a pickup.  The loan should have 
                    //        been processed by BuildTBLoanData.  Don't 
                    //        duplicate it here.
                    continue;  
                }
            }
        }

        if(cchange < 0)
        {
            cpending[cu.Denom()] = abs(cchange);
        }
        // -TAR 412364
    }

    if(cpending.Size() > 0)
    {
        m_logger->Log(TRACE_INFO, _T("Non-dispensable pickup [%s]"), 
                      (LPCTSTR)cpending);

        if(cpending.Size(CW_NOTES))    
        {
            csNote.Format(_T("%s%s"),CASHACCEPTOR,cpending.ToString(CW_NOTES));
        }

        if(cpending.Size(CW_COINS))
        {
            csCoin.Format(_T("%s%s"),COINACCEPTOR,cpending.ToString(CW_COINS));
        }
    }

    // Determine recycler / dispenser pickup.
    cpending.Empty();
    m_disp->Begin();
    while(m_disp->Next(cu))
    {
        cchange = cu.ChangeInCount();

        assert(cchange <= 0);  
        if(cchange < 0)
        {
            // +TAR 412364:  Check with non-dispensable to see if there is 
            //               a net change.  There could have been a transfer.
            //
            //               If money has not been transferred, then there
            //               should not be an increase of notes in the cashbox.
            CashUnit cuNDisp;
            if(cu.Denom() > 0 &&                    // If this is a note.
               m_xdev->IsRecycler(CW_NOTES) &&      // If this is a note recycl
               m_ndisp->GetCashUnit(cu.Denom(), cuNDisp) &&
               cuNDisp.ChangeInCount() > 0)         // Indicates a transfer.
            {
                // If the cashbox shows an increase for this denomination,
                // then compute the net count change.
                if((cchange + cuNDisp.ChangeInCount()) < 0)
                {
                    // If the net change < 0, then we have a pickup.
                    cchange += cuNDisp.ChangeInCount();
                }
                else            // >= 0, either no net change or a loan.
                {
                    continue;   // To be handled in BuildTBLoanData().
                }
            }
            // -TAR 412364

            cpending[cu.Denom()] = abs(cchange);
        } 
    }

    if(cpending.Size() > 0)
    {
        CString csTmp;      // Scratch.
        m_logger->Log(TRACE_INFO, _T("Dispensable pickup [%s]"), 
                      (LPCTSTR)cpending);

        if(cpending.Size(CW_NOTES))    
        {
            csTmp.Format(_T("%s%s"),CASHDISPENSER,cpending.ToString(CW_NOTES));
            csNote += csTmp;
        }

        if(cpending.Size(CW_COINS))
        {
            csTmp.Format(_T("%s%s"),COINDISPENSER,cpending.ToString(CW_COINS));
            csCoin += csTmp;
        }
    }

	SendMessageToOptiCash((LPCTSTR)cpending.ToString(), OC_PICKUP);
	SendBalanceToOptiCash(*m_disp, *m_ndisp);
    if(csCoin.GetLength() > 0 || csNote.GetLength() > 0)
    {
        // TAR 395586 - Removed "csPickup = " from start of result string.
        result.Format(_T("%s%s"), OPERATORID, (LPCTSTR)m_UID);
        result += csNote;
        result += csCoin;

        m_logger->Log(TRACE_INFO, _T("Pickup: [%s]"), (LPCTSTR)result);
        return true;
    }
                  
    m_logger->Log(TRACE_INFO, _T("No pickup info found."));
    return false;
}

void XMCashRemoveBase::CalculatePickUp(int nPickUpValue)
{
    assert(m_nPickUpFlag != 0);

    int nPickUp = 0, nVal=0, nCnt=0, nMaxCoinsAllowed=-1, 
        nRemainder1=0, nRemainder2=0, nWhole=0;
    CString csKey, csOptCoins;
    CashUnit cu;
    long lDenom=0;  
    bool bPickUpSuccess = true;
    CurrencyWrapper &cpending = *m_pending; 
    CountTracker *tracker = NULL;
    // TAR 439941: Don't include the loader count for EOD Pickup
    long TotalDispValue= m_disp->TotalValue(CW_NOTES) + m_disp->TotalValue(CW_COINS) - m_xdev->GetTotalLoaderValue(CW_NOTES);
    
    // TAR 439694: Check the pickup value first before actually computing pickup value
    // minus the nondisp. This is to prevent comparing incorrect pickup value.
    if(nPickUpValue < m_ndisp->TotalValue() && m_nPickUpFlag > 0)
    {
        AlertMessage(true,_T("$XM_InvalidPickUpAmount2$"));
        bPickUpSuccess = false;
    } 
    // RFQ 432305: If pickup x amount is selected, pickup = entered value - nondisp.
    // If pickup auto amount is selected, pickup = total disp cylinder - startingbank.
    nPickUp = (m_nPickUpFlag > 0) ? nPickUpValue - m_ndisp->TotalValue() : TotalDispValue - m_nStartingBank;    
            
    if(m_cfg->GetOption(_T("MaxCoinsAllowedToDispense"), csOptCoins))
    {
        nMaxCoinsAllowed = _ttoi(csOptCoins);
        m_logger->Log(TRACE_INFO, _T("nMaxCoinsAllowed = %d"),nMaxCoinsAllowed);        
    }
    
    if (TotalDispValue < nPickUp && m_nPickUpFlag > 0)
    {
        AlertMessage(true,_T("$XM_InvalidPickUpAmount$"));
        bPickUpSuccess = false;
    }  
    else if(TotalDispValue < m_nStartingBank && m_nPickUpFlag < 0)
    {
        AlertMessage(true,_T("$XM_InvalidStartingBank$"));  
        bPickUpSuccess = false;
    }
    else
    {
        cpending.Empty();//TAR 439696 & 439697 
        ClearPendingCountChangeDisplay(); //TAR 435062: clear pending count before performing calculation.
        m_disp->End();
        while(m_disp->RNext(cu) && nPickUp > 0)
        {
            lDenom = cu.Denom();
            csKey.Format(_T("D_%d"), lDenom);
            if(m_counts.Lookup(csKey, tracker) && cu.Count() != 0  ) 
            { 
                // TAR 439941: Do not include the loader count
                long nDenomTotalValue = (cu.Count() - m_xdev->GetLoaderCount(lDenom)) * abs(lDenom);

                nRemainder1 = nPickUp % abs(lDenom);//get the remained value to be picked up
                nWhole = nPickUp / abs(lDenom);//get the count to be picked up

                // subtract the value of the cylinder from the pickup value             
                nWhole *= abs(lDenom);              
                nRemainder2 = nWhole - nDenomTotalValue;
                // Set it to zero if all pickup value has been picked up
                // in the current denomination so that it wont be 
                // included anymore in the next denomination.
                if(nRemainder2 < 0) 
                    nRemainder2 = 0;
                
                // Set pickup amount for the next denomination.
                nPickUp = nRemainder1 + nRemainder2;
                //Add dispense.transfer cash to m_pending.
                cpending[lDenom] = ((nWhole - nRemainder2) / abs(lDenom));                  
                tracker->SetPendingCountChange(-((nWhole - nRemainder2) / abs(lDenom)));                              
            }
        }
        if(nMaxCoinsAllowed >= 0 && cpending.TotalCount(CW_COINS) > nMaxCoinsAllowed)
        {
            //Display error message if pickup exceeded coin dispense limit
            CString csPrompt, csFormat = _T("");
            CString csMessage = _T("XM_CoinAllowedDispenseExceeded");
            if (m_psx->GetString(csMessage, csPrompt))
            {
                csMessage.Format(csPrompt, cpending.TotalCount(CW_COINS), nMaxCoinsAllowed);                                
                m_psx->SetConfigProperty(_T("XMAlertBox"), 
                    UI::PROPERTYTEXTFORMAT, csMessage);
                m_psx->SetControlProperty(_T("XMAlertBox"), 
                    UI::PROPERTYVISIBLE, VARIANT_TRUE);
            }                      
            bPickUpSuccess = false;
        }
        // if there are still cash to be picked up, then display error message.
        else if(nPickUp > 0 ) 
        {
            AlertMessage(true,_T("$XM_UnableToPickUp$"));                       
            bPickUpSuccess = false;
        }
    }
    //Only display the bills to transfer if pickup is successful
    if (bPickUpSuccess && cpending.TotalCount(CW_NOTES) > 0)
    {
        XPSButton7();// sets up note transfer portion.        
        if(m_pending->Size(CW_NOTES) == 0)//if XPSButton7() failed
        {
            bPickUpSuccess = false;         
        }
    }
    //Only display the coins to dispense if pickup is successful
    if(bPickUpSuccess)
    {
        // Update the total pending count display for coins.  
        nVal = m_pending->TotalValue(CW_COINS);
        nCnt = m_pending->TotalCount(CW_COINS);
        SetPendingText(nCnt, nVal, CW_COINS);
        // Only display the pickup amount if pickup is successful
        DisplayCalculatedPickUpAmount();
    }
    else
    {
        m_nPickUpFlag = 0;//Reset flag
        if(cpending.Size() > 0)//TAR 434802
        {
            ClearPendingCountChangeDisplay();
            cpending.Empty();
        }
    }
    m_keypad->Clear(); //Clear out text
    m_keypad->SetAltEnabled(false);

    Redraw();   
}

void XMCashRemoveBase::DisplayCalculatedPickUpAmount()
{
    //Display the calculated pickup amount
    long lStorePickupAmount = m_pending->TotalValue(CW_COINS) + m_pending->TotalValue(CW_NOTES);
    CString csPrompt, csFormat = _T("");
    CString csMessage = _T("XM_CalculatedPickUpAmount");        
    
    if (m_psx->GetString(csMessage, csPrompt))
    {
        FormatCurrency(abs(lStorePickupAmount),csFormat, m_bTrimDecimal);       
        csMessage.Format(csPrompt, csFormat);       
        m_psx->SetConfigProperty(_T("XMLeadthruText"), UI::PROPERTYTEXTFORMAT, csMessage);      
    }
}

void XMCashRemoveBase::SetPickUpFunction(void)
{
    CString csOption = _T(""), csControl = _T("");  
    COleVariant vVis = VARIANT_TRUE;

    if(m_cfg->GetOption(_T("StartingBank"), csOption)) 
    {
        m_nStartingBank = _ttoi(csOption);  
    }       
    if(m_cfg->GetOption(_T("EnablePickUpXAmount"), csOption) && 
        csOption.CompareNoCase(_T("Y")) == 0 )
    {           
        m_bEnablePickUpXAmount = true; 
    }
    if (m_nStartingBank < 0 && m_bEnablePickUpXAmount == false || 
        !m_xdev->DispenseAllowed(CW_COINS))
    {
        if(m_psx->GetCustomDataVar(_T("PickUpFunction"), csControl ))
        {
            vVis = VARIANT_FALSE;       
            m_psx->SetConfigProperty(csControl, UI::PROPERTYVISIBLE, vVis);         
        }
    }   
}

void XMCashRemoveBase::UpdatePickUpButtons(void)
{       
    COleVariant vstate = (long)UI::Disabled;

    COleVariant vVis = VARIANT_FALSE;   
    long TotalDispValue= m_disp->TotalValue(CW_NOTES) + m_disp->TotalValue(CW_COINS);
    if(m_nStartingBank >= 0) //TAR 434803
    {            
        //Only enable button if it's not selected; no cylinder is selected;
        //no pending count; w/ dispensable cash in lane; coin/note is not emptied;
        //and m_nStartingBank not exacly equal to dispensable cash.
        vstate = (long)UI::Disabled;
        if(m_nPickUpFlag == 0 && !m_activeCyl && 0 == m_pending->TotalCount() 
            && TotalDispValue > 0 && !(m_bCoinEmpty || m_bNoteEmpty)
            && m_nStartingBank != TotalDispValue)
        {
            vstate = (long)UI::Normal;      
        }
        m_psx->SetConfigProperty(_T("XMButton9"), UI::PROPERTYSTATE, vstate); 
    } 
    else
        m_psx->SetConfigProperty(_T("XMButton9"), UI::PROPERTYVISIBLE, VARIANT_FALSE); 
    
    if(m_bEnablePickUpXAmount)
    {   
        vstate = (long)UI::Disabled;
        //Only enable button if it's not selected; no cylinder is selected;
        //no pending count; w/ dispensable cash in lane;and coin/note is not emptied.
        if(m_nPickUpFlag == 0 && !m_activeCyl && 0 == m_pending->TotalCount() 
            && TotalDispValue > 0 && !(m_bCoinEmpty || m_bNoteEmpty))
        {
            vstate = (long)UI::Normal;  
        }
        if(m_nPickUpFlag >0)
        {
            m_keypad->ShowKeypad(true); 
            m_keypad->SetKeyPadEnabled(true);
        }
        m_psx->SetConfigProperty(_T("XMButton10"), UI::PROPERTYSTATE, vstate);
    }
    else
        m_psx->SetConfigProperty(_T("XMButton10"), UI::PROPERTYVISIBLE, VARIANT_FALSE); 

    if(m_nPickUpFlag != 0)
    {
        // Disable Dispense to Base level, Transfer to Base Level,
        // Transfer All Bills, Reset Cashbox and Reset Coin Counts 
        // buttons when pickup is performed.
        vstate = (long)UI::Disabled;
        m_psx->SetConfigProperty(_T("XMButton2"), UI::PROPERTYSTATE, vstate); 
        m_psx->SetConfigProperty(_T("XMButton3"), UI::PROPERTYSTATE, vstate); 
        m_psx->SetConfigProperty(_T("XMButton4"), UI::PROPERTYSTATE, vstate);         
        m_psx->SetConfigProperty(_T("XMButton5"), UI::PROPERTYSTATE, vstate);
        m_psx->SetConfigProperty(_T("XMNonDispenseCylinder501EmptyButton"), UI::PROPERTYSTATE, vstate);
    }   
      
}
    
void XMCashRemoveBase::ClearPendingCountChangeDisplay()
{   
    CString csKey;
    CashUnit cu;
    long lDenom = 0;
    m_disp->Begin();
    CountTracker *tracker=NULL;
    while(m_disp->Next(cu))
    {
        lDenom = cu.Denom();
        csKey.Format(_T("D_%d"), lDenom);
        if(m_counts.Lookup(csKey, tracker))
        {
            tracker->SetPendingCountChange(0);
        }
    }
    if(m_counts.Lookup(ND_NOTEID, tracker))
    {
        tracker->SetPendingCountChange(0);
    }
    if(m_counts.Lookup(ND_COINID, tracker))
    {
        tracker->SetPendingCountChange(0);
    }
    SetPendingText(0, 0, CW_COINS);
    SetPendingText(0, 0, CW_NOTES);
}

bool XMCashRemoveBase::HandleNegativeLoaderCount(void)
{
    TRACEAPI_EXT(_T("XMCashRemoveBase::HandleNegativeLoaderCount"));

    bool bNegativeDetected = XMCashManagementBase::HandleNegativeLoaderCount();

    if(bNegativeDetected)
    {
        // Disable all menu buttons.
        m_pBtnMenu->DisableAllButtons();
    }

    return bNegativeDetected;
}

//handling of negative loader count+
void XMCashRemoveBase::ShowNegativeLoaderAlert(bool bShow)
{
    COleVariant vState = bShow ? VARIANT_FALSE: VARIANT_TRUE;
    if (bShow)
    {
        assert(m_xdev->IsRecycler(CW_NOTES) == true);
    }

    // +SSCOP-561
    if(! bShow)
    {
        XMCashManagementBase::ShowNegativeLoaderAlert(bShow);
    }
    else
    {
        // Display different instructions for this screen.
        CString csMessage;
        if(! m_psx->GetString(_T("XM_NegativeAlert_Remove"), csMessage) )
        {
            XMCashManagementBase::ShowNegativeLoaderAlert(bShow);
        }
        else  // Read the string successfully.
        {
            CPSXHelper psxHelper(*m_psx, _T("XMAlertNegativeLoader"));

            psxHelper.SetVisible(bShow);
            psxHelper.SetContext(NULL);  // Apply to all contexts.
            psxHelper.SetTextFormat(csMessage);
        }
    }
    // -SSCOP-561
      
    m_psx->SetConfigProperty(_T("XMCoinAddButtonList"), _T("XMCashRemoveBNR"),
                        UI::PROPERTYVISIBLE, vState);
    m_psx->SetConfigProperty(_T("XMNoteAddButtonList"), _T("XMCashRemoveBNR"), 
                        UI::PROPERTYVISIBLE, vState);
    m_psx->SetConfigProperty(_T("XMCoinRemoveButtonList"), _T("XMCashRemoveBNR"), 
                        UI::PROPERTYVISIBLE, vState);
    m_psx->SetConfigProperty(_T("XMNoteRemoveButtonList"), _T("XMCashRemoveBNR"), 
                        UI::PROPERTYVISIBLE, vState);

    EnableDisableMenuButtons(!bShow);//!bShow = bEnable
                                    //not showing alert so we are enabling buttons
}
//handling of negative loader count-
void XMCashRemoveBase::EnableDisableMenuButtons(bool bEnable)
{
    // assumptions:
    // - BUTTON_MENU_START is the starting ID extension for the menu button of Cash Remove as of the case right now
    // - user defined the menu button name in increasing order

    COleVariant vState = bEnable ? (long)UI::Normal: (long)UI::Disabled;
    CString csCtxID = m_xdev->IsRecycler(CW_NOTES) ? _T("XMCashRemoveBNR") : _T("XMCashRemove"); 
    int buttonID = BUTTON_MENU_START;

    while( buttonID < BUTTON_MENU_MAX )
    {
        CString szControl;
        szControl.Format(_T("XMMenuButton%d"),buttonID);

        if(!m_psx->SetConfigProperty(szControl, csCtxID, UI::PROPERTYSTATE, vState))
        {
            return;
        }
        buttonID++;
    }
}

XMState *  XMCashRemoveBase::HandleConfirmChanges(void)
{
    COleVariant vstate = (long)UI::Disabled;

   m_nPickUpFlag = 0;   // TAR 434566:Reset flag to disable keypad in Redraw()
    
   if(IsCancelPending())
   {
       return HandleCancelChanges();
   }

   //call from cash remove
   XMCashManagementBase::HandleConfirmChanges();

    // SSCOP-822 +   
    // If here you see that in Training Mode, then check if dispense is 
    // allowed in training mode in ScotOpts. But still allow Transfers and Reset.
    CountTracker *tracker=NULL;
    if(CheckTrainingMode() && m_pending->Size() != 0 
		&& (m_counts.Lookup(ND_NOTEID, tracker) && !(tracker->GetPendingCountChange() > 0)))
    {
        CString csOpt;
        if(m_cfg->GetOption(_T("AllowDispenseInTrainingMode"), csOpt))
        {
            m_logger->Log(TRACE_WARNING, 
                    _T("AllowDispenseInTrainingMode is set to '%s' in ScotOpts."), csOpt);
        }

        if(csOpt.IsEmpty() || csOpt == _T("0"))
        {
			Initialize();
            Finalize();	
			//+SSCOADK-440
            COleVariant vstate = (long)UI::Normal;
            m_psx->SetControlProperty( _T("XMButton8"), UI::PROPERTYSTATE, vstate);
			//-SSCOADK-440
            
			return STATE_NULL;
        }
    }
	// SSCOP-822 -

   CurrencyWrapper &cdisp = *m_disp;
   CurrencyWrapper &cndisp = *m_ndisp;

   m_logger->Log(TRACE_INFO, 
                 _T("* COUNTS BEFORE CHANGES: ndisp: [%s]; disp: [%s] *"), 
                 (LPCTSTR)cndisp, (LPCTSTR)cdisp);


   //disable specific buttons
   m_psx->SetControlProperty( _T("XMButton4"), UI::PROPERTYSTATE, vstate);
   m_psx->SetControlProperty( _T("XMButton7"), UI::PROPERTYSTATE, vstate); // RFQ 3067 - BNR Dispense to Cashbox

   //disable menu buttons
   EnableDisableMenuButtons(false);

   HandleOverflowEmpty(); //TAR#412799
   //RFQ 432305: Interchange transfer/dispense priority
   if(HandleDispense())
   {
       if(!HandleTransfer())// SR877
       {
           m_bDelayedFinalization = true;
		   m_logger->Log(TRACE_INFO, 
			   _T("Error detected during transfer. Deferring processing of this transaction until error is fixed"));
       }
   }
   

   HandleLastFewCoinsRemoval(); 
   
   HandleManualChange();

   // SSCOP-822: Removed TAR 449905 routine due to new routine implemented in SSCOP-822
   Finalize();

   m_logger->Log(TRACE_INFO, 
                 _T("* COUNTS AFTER CHANGES: ndisp: [%s]; disp: [%s] *"), 
                 (LPCTSTR)cndisp, (LPCTSTR)cdisp);

   // +TAR 384998: Moved from HandleDispense().
   // Enable Exit button after actions.
   vstate = (long)UI::Normal;
   m_psx->SetControlProperty( _T("XMButton8"), UI::PROPERTYSTATE, vstate);
   // -TAR 384998

   //enable menu buttons
   EnableDisableMenuButtons(true);
 
   //tar 436456
   // after enabling menu buttons
   // we need to click back the active button to set it to selected/pushed state
   IButton *pActive = m_pBtnMenu->GetActiveButton();
   if(pActive) 
   {   
         pActive->OnClick();
         Redraw();
   }


   return STATE_NULL;
}
XMState *  XMCashRemoveBase::HandleApplyChanges(void)
{
    if(m_bCoinEmpty && !m_xdev->CheckCoinDispenserLow())
    {
        //This message will continue to be displayed as long as
        //the bins are not below the low level threshold.
        AlertMessage(true,_T("$XM_CoinHopperEmpty$"));  
        return STATE_NULL;
    }

	// We want the user to be able to press the lock button 
    // and lock the screen even when the main thread is busy 
    // with device stuff (transferring bills, etc.).
    SetSystemBusy(true);
 
    //call from cash remove
    XMCashManagementBase::HandleApplyChanges();
   
     // Add pending dispense information to the confirmation screen.
    CString disptxt = m_promptstr;

    if(m_pending->TotalCount(CW_COINS) && !m_bCoinEmpty)
    {
        BuildPrompt(_T("XM_ConfirmCoinDispense"), disptxt, *m_pending);
    }

    if(m_pending->TotalCount(CW_NOTES) && !m_bNoteEmpty)
    {
        CountTracker *tracker = NULL;
        // Determine if this is a transfer or a dispense.
        if(m_counts.Lookup(ND_NOTEID, tracker) && 
           tracker->GetPendingCountChange() > 0)    // Transfer.
        {
            BuildPrompt(_T("XM_ConfirmTransferNotesToOverflow"), 
                        disptxt, *m_pending, CW_NOTES);
        }
        else                                        // Dispense.
        {
            BuildPrompt(_T("XM_ConfirmNoteDispense"), disptxt, *m_pending, 
                        CW_NOTES);
        }
    }

    if(disptxt.GetLength() > 0)
    {
        m_psx->SetFrame(_T("XMConfirmAction"));

		//Check if coins is to be empty and if the device 
        //allows removing the remaining few coins in the dispenser.
        //This is true for telequip hopper
        if(m_bCoinEmpty && m_xdev->AllowRemoveLastFewCoins())
        {
            CString csMessage;
            if(m_psx->GetString(_T("XM_CollectRemainingCoins"), csMessage))
            {
                m_psx->SetTransactionVariable(_T("XMEcho"),csMessage);
            }
        }

		
        // +RFC 386876:  Now using text format instead of xaction variable.
        m_psx->SetConfigProperty(_T("XMPendingActions"), UI::PROPERTYTEXTFORMAT,
                                 disptxt);
        SetContext(_T("XMConfirmAction"));
        // -RFC 386876
    }
    else    // There must be nothing to do.  Press cancel button.
    {
        Reset();
        Redraw(true);       // Repaint display.
    }

    //comes here on pressing the Execute button on the Cash Management screen
    CheckTrainingMode();
    
    return STATE_NULL;
}


XMState *  XMCashRemoveBase::HandleCancelChanges(void)
{
    bool bhandled = false;
    XDMParse(EVT_REFRESH_DATA, 0, bhandled, DMCLASS_CASHCHANGER, 0); 
    return XMCashManagementBase::HandleCancelChanges();
}

XMState * XMCashRemoveBase::XDMParse(UINT evntType, long lParam,
                                         bool &handled, long devClass, 
                                         long devID)
{
    return XMCashManagementBase::XDMParse(evntType, lParam, handled,
                                          devClass, devID);
}

XMState * XMCashRemoveBase::XDMParse(MessageElement &rMe, bool &bProcessed)
{
    return XMCashManagementBase::XDMParse(rMe, bProcessed);
}

//+tar 436337
void XMCashRemoveBase::ShowErrorAlert(bool bShow, LPCTSTR szMsg )
{
    TRACEAPI(_T("XMCashRemoveBase::ShowErrorAlert"));
    XMCashManagementBase::ShowErrorAlert(bShow, szMsg);
}
//-tar 436337

// SR877
bool XMCashRemoveBase::Finalize(void)
{
	TRACEAPI(_T("XMCashRemoveBase::Finalize"));

	if(m_bDelayedFinalization)
	{
        // Dont call finalize yet.
	    return false;
	}

	return XMCashManagementBase::Finalize();
}


bool XMCashRemoveBase::HandleLastFewCoinsRemoval(void)
{
    TRACEAPI(_T("XMCashRemoveBase::HandleLastFewCoinsRemoval"));

    bool rc = false;

    //remove the remaining few coins in the hopper
    if(m_bCoinEmpty && m_xdev->CheckCoinDispenserLow() && 
        m_xdev->AllowRemoveLastFewCoins())
    {
        // clear warning message when back to remove case 
        // screen from confirmation screen.
        m_psx->SetTransactionVariable(_T("XMEcho"),_T(""));      
        
        rc = m_xdev->CashChangerRemoveCoins();  
    }
    
    return rc;
}