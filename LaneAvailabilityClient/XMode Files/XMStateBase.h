#ifndef _XMSTATEBASE_H
#define _XMSTATEBASE_H

#include "XMState.h"
#include "UIDefines.h"
#include "IStateFactory.h"	// SR752
#include "XMode.h"
#include "IOptiCashClient.h"  //LFK OC test


class PSXWrapper;
class XMCashDevice;
class CLog;
class TBWrapper;
class XMConfig;
class IXMLoginDevice;   //TAR 448426

#include "XMExport.h"

/**
 * 
 *                   !! IMPORTANT NOTICE !! 
 *                   THIS INTERFACE MUST NOT BE CHANGED.
 *                   PLEASE DO NOT CHANGE THIS HEADER FILE IN ANY WAY.
 *
 * 					 CHANGING THIS HEADER FILE WILL REQUIRE A REBUILD OF
 * 					 XMState DLL FOR ANY CUSTOMERS THAT HAVE CUSTOMIZED
 * 					 CASH MANAGEMENT.
 *
 * 					 See _README.txt for details.
 */

const enum CM_DEVICE_TYPE 
{
    DEVICE_BILL_ACCEPTOR,
    DEVICE_COIN_ACCEPTOR,
    DEVICE_BILL_DISPENSER,
    DEVICE_COIN_DISPENSER,
	DEVICE_TYPE_MAX				// Add Glory CM Support
};

/**
 * \class XMStateBase
 * \brief Base implementation shared by all XMState implementations in cash
 *        management.  
 * \author Jeff Vales
 * \author Copyright (c) NCR Corp. 2008, 2009, 2010
 *
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of NCR Corporation. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with NCR.
 */
class XMODE_API XMStateBase : public XMState {

public:
    /**
     * Initialize the XMState dll.  This is called by the XMode dll.
     * \param[in]   psx PSXWrapper instance to be used by all states.
     * \param[in]   xmd XMCashDevice instance to be used by all states.
	 * \param[in]   cfg XMConfig instance to be used by all states.
	 * \param[in]   pDevLogin IXMLoginDevice instance to be used by all states.
     * \return      The default (initial) XMState or NULL on failure.
     */
    friend XMSTATE_API XMState * XMInitialize(PSXWrapper *psx, 
                                              XMCashDevice *xmd,
                                              XMConfig *cfg,
                                              IXMLoginDevice *pDevLogin);    //TAR 448426

    friend XMState * XMBaseInitialize(PSXWrapper *psx, XMCashDevice *xmd,
                                      XMConfig *cfg,
                                      IXMLoginDevice *pDevLogin);            //TAR 448426
    /**
     * UnInitialize the XMState dll.  Called by XMode dll.
     */
    friend XMSTATE_API void XMUnInitialize();
    friend void XMBaseUnInitialize();
    
    // SR752
    friend class BaseStateFactory;      // gain access to the members of this class
    friend class CustomerStateFactory;  // gain access to the members of this class

    friend class XMScreenUpdate; // SR969

    virtual ~XMStateBase() {}
    virtual void Initialize(void) = 0;
    virtual void UnInitialize() = 0;

    virtual XMState * XDMAcceptor();

    virtual XMState * XPSButton1();
    virtual XMState * XPSButton2();
    virtual XMState * XPSButton3();
    virtual XMState * XPSButton4();
    virtual XMState * XPSButton5();
    virtual XMState * XPSButton6();
    virtual XMState * XPSButton7();
    virtual XMState * XPSButton8();
    virtual XMState * XPSButton9();//RFQ 432305
    virtual XMState * XPSButton10();//RFQ 432305

    virtual XMState * XPSKeyShift();
    virtual XMState * XPSCharKey(CString csChar);

    virtual XMState * XPSButton(long buttonID);

    virtual XMState * XPSReceiptUp();
    virtual XMState * XPSReceiptDown();

    virtual XMState * XPSKeyStroke(long buttonID);
    virtual XMState * XPSEnterKey(void);   
    virtual XMState * XPSClearKey(void);  
    
    virtual XMState * XTBParse(MessageElement *me, bool &processed);

    virtual XMState * XDMParse(UINT evntType, long lParam, bool &handled,
                               long devClass, long devID=0);

    virtual XMState * XDMParse(MessageElement &rMe, bool &bProcessed);

    virtual XMState * XBalanceUpdate(CurrencyWrapper &disp,
                                     CurrencyWrapper &ndisp); //SR777

	virtual bool CheckTrainingMode(void);
	
	//+RFC 2774 - Ability to lock CM
	virtual void XAuthEvent(void);	
	virtual bool IsSystemLocked(void);
	virtual void SetSystemLocked(bool bSystemLock);
	//-RFC 2774 - Ability to lock CM

    //+SR700
    virtual XMState * const SetBaseState(int index);
    virtual XMState * const GoToBaseState(void);
    //-SR700

	virtual bool SetContext(LPCTSTR szContext);
	
	/**
	 * \brief See XMState.h for method information
     */
	virtual bool IsSystemBusy(void);

    /**
	 * \brief See XMState.h for method information
     */
	virtual void SetSystemBusy(bool bSystemBusy);
	
	/**
     * \brief See XMState.h for method information
     * \note Added for TAR 448426
     */
    virtual XMState * HandleFPData(void); 
    
     /**
     * \brief See XMState.h for method information
     * \note  Added for SR752
     */
    virtual void SetWaitForTBResponse(bool bWaitForTB);  

    /**
     * \brief See XMState.h for method information
     * \note  Added for SR752
     */
    virtual bool WaitForTBResponse(void);

    /**
     * \brief See XMState.h for method information
     * \note Added for SR752
     */
    virtual XMState * HandleTBResponse(void);   

     /**
     * \brief See XMState.h for method information
     * \note Added for SR969
     */
    virtual bool InitModule(void);      
    virtual CString AppendContextWithLCID(CString csContext);

    /**
     * \brief See XMState.h for method information
     * 
     */
    virtual void ExitToApp(void);
    //+-SSCOADK-7123
    virtual void DepositingFinished(void);


	//SSCOP-10053 Integrate OptiCash Client into Cash Management
	
	
	virtual void SendBalanceToOptiCash(CurrencyWrapper &disp, CurrencyWrapper &ndisp);
	virtual void SendMessageToOptiCash(LPCTSTR updateData, OC_UPDATE_MSG updateMsg);
	

protected:
    static PSXWrapper *m_psx;                 
    static XMCashDevice *m_xdev;            ///< Cash devices.
    static IStateFactory *m_stateFactory;   ///< SR752 - State factory
    static CLog *m_logger;
    static TBWrapper *m_tb;
    static XMConfig *m_cfg;
	static IXMLoginDevice *m_xDevLogin;     ///< TAR 448426 Login devices 
    static bool m_bExitToApp;

	static HMODULE m_OChandle;
	
	//SSCOP-10053 Integrate OptiCash Client into Cash Management 
	void InitOptiCashClient(void);
	
private:
	static bool m_bIsSystemLocked;			///< RFC 2774 - Ability to lock CM
	static bool m_bIsSystemBusy;			///< System is busy flag
    static bool m_bWaitForTB;               ///< SR752
	XMode* m_xmode;
};

#endif // _XMSTATEBASE_H
