﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LaneAvailabilityCSharp
{
    public class LaneAvailabilityCSharp : ILaneAvailabilityCSharp
    {
        public LaneAvailabilityCSharp()
        {

        }

        ~LaneAvailabilityCSharp()
        {
            Dispose();
        }

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        
        public void UpdateStaticMessage(string msgData)
        {
            LAUpdateStaticMessage(msgData);
        }

        public void UpdateInterventionMessage(string msgData)
        {
            LAUpdateInterventionMessage(msgData);
        }

        public void UpdateWeightMessage(string msgData)
        {
            LAUpdateWeightMessage(msgData);
        }

        public void UpdateFinishTrx()
        {
            LAUpdateFinishTrx();
        }

        public bool ResetFiles()
        {
            return LAResetFiles();
        }

        private const string LaneAvailabilityClientDLL = "LaneAvailabilityClient.dll";
        //Interface call for consumers of the client library
        [DllImport(LaneAvailabilityClientDLL, CharSet= CharSet.Unicode, CallingConvention= CallingConvention.Cdecl)]
        private static extern void LAUpdateStaticMessage(string msgData);

        [DllImport(LaneAvailabilityClientDLL, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern void LAUpdateInterventionMessage(string msgData);

        [DllImport(LaneAvailabilityClientDLL, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern void LAUpdateWeightMessage(string msgData);

        [DllImport(LaneAvailabilityClientDLL, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern void LAUpdateFinishTrx();

        [DllImport(LaneAvailabilityClientDLL, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool LAResetFiles();
    }
}
