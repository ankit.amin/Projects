﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaneAvailabilityCSharp
{
    interface ILaneAvailabilityCSharp : IDisposable
    {
        //Interface call for consumers of the client library
        void UpdateStaticMessage(string msgData);
        void UpdateInterventionMessage(string msgData);
        void UpdateWeightMessage(string msgData);
        void UpdateFinishTrx();

        bool ResetFiles();
    }
}
