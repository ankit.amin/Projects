#pragma once
#include "atlbase.h"
#include "atlstr.h"
#include "ITransport.h"
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <vector>


#define COMPUTERNAMESIZE 100
#pragma pack(1)

// Message header structure to conform to DataPump standards
struct MessageHeader
{
    unsigned int seq; //2
    short type; //2
    short subtype; 
    short svalue;
    short version;
    char computername[20];
    MessageHeader()
        :	type(0), subtype(0), svalue(0), version(0)
    {
        TCHAR buffer[COMPUTERNAMESIZE];
        DWORD size = COMPUTERNAMESIZE - 1;
        
        //Get computer name
        GetComputerName(buffer, &size); //Computer Name is temporary unique identifier
        
        //zero out memory 
        memset(computername, 0, sizeof(computername));
        
        CT2A temp(buffer);		
        strncpy(computername, temp, sizeof(computername));  // ASCII version
    };
};

struct MessagePayload
{
    int msgId;
    char text[20917520];  //buffer for data storage
    //char text[2048000];
};

struct Message
{
    MessageHeader hdr;
    MessagePayload msg;
};
#pragma pack()

//#define COMPUTERNAMESIZE 100
//#pragma pack(1)
//
//// Message header structure to conform to DataPump standards
//struct MessageHeader;
//
//struct MessagePayload;
//struct Message;
//#pragma pack()


class UDPTransport : public ITransport
{
public:
    UDPTransport();
    virtual ~UDPTransport();

    // Inherited via ITransport
    virtual bool Initialize(int port, const char* ipAddress, int bufferSize) override;
    virtual bool SendData(const char * data) override;

    virtual bool ConnectToRemoteHost(int port, const char* ipAddress);
    virtual void CloseConnection(); 
    virtual int BuildMessage(const char* data);

private: 

    SOCKET m_socket; /* The socket descriptor */
    SOCKADDR_IN m_server;
    addrinfo m_address; 

    Message m_msg;

    bool m_bSocketInitialized;
    int m_sequence;
    int m_bufferSize;

    CString m_IPAddress; 
    int m_port; 
};

