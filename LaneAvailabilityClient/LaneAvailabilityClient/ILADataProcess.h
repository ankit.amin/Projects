#pragma once
#include <atlstr.h>
#include "json\json.h"

// Allow for expansion of data format type.  Only DF_JSON is currently supported
typedef enum
{
    DF_JSON,
    DF_XML,
    DF_CSV
}LA_DATAFORMAT;

// Interface to handle storing and manipulating data based on consumer messages
class ILADataProcess
{
public:
    ILADataProcess(void) {}
    virtual ~ILADataProcess() {}

    virtual bool SetFormatType(LA_DATAFORMAT eDataFormat) = 0;
    virtual LA_DATAFORMAT GetFormatType() = 0;
    virtual char* CompleteTransaction() = 0;
    virtual bool ResetCycle() = 0;
    virtual Json::Value ParseSSCOStaticData(const char* cData) = 0;
    virtual Json::Value ParseSSCOInterventionData(const char* cData) =0;
    virtual Json::Value ParseSSCOWeightData(const char* cData) =0;
   
    /*virtual bool SetCurrencyType(char* currencyType) = 0;*/
    /*virtual void ProcessDeposit(const char* cDepData, const bool isNote) = 0;
    virtual void ProcessDispense(const char* cDispData) = 0;
    virtual void ProcessLoan(const char* cDispData) = 0;
    virtual void ProcessPickup(const char* cDispData) = 0;
    virtual void UpdateCashBalance(const char* cBalanceData, const bool bDispensable) = 0;
    virtual void ReportEndOfDay() = 0;*/
    //virtual void SetCashPID(const char* newName) = 0; //Future use for notification of Lane ID change
    //virtual void CalculateClosingBal() = 0;  //Future use for notifiation of need to verify closing balance
};