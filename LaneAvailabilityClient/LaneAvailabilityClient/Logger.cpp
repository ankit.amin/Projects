#include "Logger.h"

static CLogger* m_pLogger = NULL;

CLogger::CLogger()
{
    BOOL mRes = FALSE;

    //Create new DataCapture
    m_pDataCap = new CmDataCapture();

    //mRes = m_pDataCap->ReadRegistry(LOG_POLICY_REGISTRY_KEY, _T("DataCapture"));
    mRes = m_pDataCap->ReadRegistry(_T("SOFTWARE\\NCR\\LAClient"), _T("DataCapture"));

    mRes = m_pDataCap->Initialize(_T("LaneAvailability Client Library"));
}

CLogger::~CLogger()
{
}

CLogger* CLogger::GetInstance()
{
    return m_pLogger;
}

bool CLogger::InitializeLogger()
{
    if (m_pLogger == NULL)
    {
        m_pLogger = new CLogger();
    }
    return (m_pLogger != NULL);
}

void CLogger::UnInitialize()
{
    CLogger* lCopyLogger = m_pLogger;
    m_pLogger = NULL;
    if (lCopyLogger)
    {
        lCopyLogger->m_pDataCap->DoDCPrintf(0x00000010, _T("CLogging::UnInitialize - Doing data capture cleanuo"));
        lCopyLogger->m_pDataCap->Flush();
        lCopyLogger->m_pDataCap->Cleanup();
        delete lCopyLogger;
    }
}

////bool LaneAvailabilityClient::WriteActivityToLog(char* clogData, char* clogMessage)
//bool CLogger::WriteActivityToLog(char* clogData, char* clogMessage)
//{
//    USES_CONVERSION;
//    m_pThis->DoDCPrintf(TRACE_INFO, _T("%s%s"), A2W(clogData), A2W(clogMessage));
//    m_pThis->Flush();
//    return 0;
//}
