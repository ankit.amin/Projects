#pragma once

#include <atlstr.h>

//Interface for reading config data from LaneAvailabilityClient .ini file
class ILAConfig
{
public:
    ILAConfig(void) {}
    virtual ~ILAConfig() {}

    virtual char* GetHostIP() = 0;
    virtual int GetHostPort() = 0;
    //virtual int GetTimer() = 0;

    virtual int GetBufferSize() = 0; 

};
