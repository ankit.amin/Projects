// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the LACLIENT_API
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// LACLIENT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#pragma once 
#include <Windows.h>
#ifdef LACLIENT_EXPORTS
#define LACLIENT_API __declspec(dllexport)
#else
#define LACLIENT_API __declspec(dllimport)
#endif


class ILaneAvailabilityClient 
{
public:

    ILaneAvailabilityClient(void) {}
	virtual ~ILaneAvailabilityClient() {}
	
	//Interface call for consumers of the client library
    virtual void LAUpdateStaticMessage(LPCTSTR msgData) = 0;
    virtual void LAUpdateInterventionMessage(LPCTSTR msgData) = 0;
    virtual void LAUpdateWeightMessage(LPCTSTR msgData) = 0;
    virtual void LAUpdateFinishTrx() =0;

    virtual bool LAResetFiles() = 0;

};

//Allow consumer to access single instance of LaneAvailabilityClient
extern "C"
{
    LACLIENT_API ILaneAvailabilityClient* fnGetLAClient(void);
    LACLIENT_API  void fnReleaseLAClient(void);
}
