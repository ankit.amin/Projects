#include "stdafx.h"
#include "LAConfig.h"

#define iniFileName "C:\\Scot\\Config\\LAClient.ini"


/***********************************************************************************************
Function:	LAConfig()

Summary:	Construtor for LAConfig class

Params:		None

Return:		Initialized LAConfig class

***********************************************************************************************/
LAConfig::LAConfig()
{
    //Read configuaration parameters from ini file
    ReadHostIP();
    ReadHostPort();
    ReadBufferSize();
    //ReadTimer();
    
}

/************************************************************************************************
Function:	~LAConfig()

Summary:	Destrutor for LAConfig class

Params:		None

Return:		NULL
***************************************************************************************************/
LAConfig::~LAConfig()
{
    delete m_csHostIP;
    delete m_csBufferSize;
    delete m_csHostPort;

}
/***********************************************************************************************
Function:	GetHostIP()

Summary:	Allow access to configured UDP host IP address

Params:		None

Return:		char* to Host IP address
***************************************************************************************************/
char* LAConfig::GetHostIP()
{
    return m_csHostIP;
}
/***********************************************************************************************
Function:	GetHostPort()

Summary:	Allow access to configured UDP host port number

Params:		None

Return:		int value of port
***************************************************************************************************/
int LAConfig::GetHostPort()
{
    return atoi(m_csHostPort);
}


int LAConfig::GetBufferSize()
{
   //return atoi(m_csBufferSize);
   return 0;
}

/*
int LAConfig::GetTimer()
{
    return atoi(m_csTimer);
}
*/
/***********************************************************************************************
Function:	readHostIP()

Summary:	Private LAConfig function that reads host IP from ini file

Params:		None

Return:		void.  Assigns host IP to private char*
***************************************************************************************************/
void LAConfig::ReadHostIP()
{
    m_csHostIP = new char[30];
    GetPrivateProfileStringA(("UDPHost"), ("Host_IPAddress"), NULL, m_csHostIP, 30, iniFileName);

}


/***********************************************************************************************
Function:	readHostPort()

Summary:	Private LAConfig function that reads host port number from ini file

Params:		None

Return:		void.  Assigns host port number to private char*
***************************************************************************************************/
void LAConfig::ReadHostPort()
{
    m_csHostPort = new char[10];
    GetPrivateProfileStringA(("UDPHost"), ("Host_Port"), NULL, m_csHostPort, 10, iniFileName);
}

/*
void LAConfig::ReadTimer()
{
    m_csTimer = new char[10];
    GetPrivateProfileStringA(("SendTimer"), ("SendTimerInSecs"), NULL, m_csTimer, 10, iniFileName);
}
*/

void LAConfig::ReadBufferSize()
{
    //m_csBufferSize = new char[10];
    //GetPrivateProfileStringA(("Buffer"), ("BufferSize"), NULL, m_csBufferSize, 10, iniFileName);
}