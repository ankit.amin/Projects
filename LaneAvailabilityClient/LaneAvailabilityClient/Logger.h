#pragma once
#include "CmDataCapture.h"

const UINT TRACE_INFO =     0x00000001;
const UINT TRACE_DEBUG =    0x00000002;
const UINT TRACE_WARNING =  0x00000004;
const UINT TRACE_ERROR =    0x00000008;

//LPCTSTR LOG_POLICY_REGISTRY_KEY(_T("SOFTWARE\\NCR\\LAClient"));

#define Trace \
    if(CLogger::GetInstance()) \
        if (CLogger::GetInstance()->GetDataCapture() ) \
            CLogger::GetInstance()->GetDataCapture()->DoDCPrintf
class CLogger
{
public:
    CLogger();

    inline CmDataCapture* GetDataCapture() { return m_pDataCap; }

    static CLogger* GetInstance();

    static bool InitializeLogger();
    static void UnInitialize();

private:
    ~CLogger();
    CmDataCapture* m_pDataCap;

};