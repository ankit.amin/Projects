#pragma once
#include "ILaneAvailabilityClient.h"
#include "Logger.h" //for Logging
#include "LADataProcess.h"
#include "UDPTransport.h"
#include "LAConfig.h"

#define SECSPERDAY 86400

class LaneAvailabilityClient : public ILaneAvailabilityClient
{
public:
    LaneAvailabilityClient();
	virtual ~LaneAvailabilityClient();

	//Interface call for consumers of the client library
    virtual void LAUpdateStaticMessage(LPCTSTR msgData) override;
    virtual void LAUpdateInterventionMessage(LPCTSTR msgData) override;
    virtual void LAUpdateWeightMessage(LPCTSTR msgData) override;
    virtual void LAUpdateFinishTrx() override;
    virtual char* TrimString(char* cMessage);
    
    
private:
    LaneAvailabilityClient(const LaneAvailabilityClient & rhs) = delete;
    LaneAvailabilityClient & operator=(const LaneAvailabilityClient& rhs)= delete;

    ILADataProcess* m_pLADataProcess;
    ITransport* m_pTransport;
    ILAConfig* m_pConfig;
    //CmDataCapture* m_pDataCap;

    void CompleteTransaction();
    void SetEODTimer();

    static LRESULT WINAPI TimerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired);

public:
    // Inherited via ILaneAvailabilityClient
    virtual bool LAResetFiles() override;

};



