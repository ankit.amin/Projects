#pragma once
 
// Interface for creating and managing UDP socket connection
class ITransport
{
public: 
	ITransport() {}
	virtual ~ITransport() {}

	virtual bool Initialize(int port, const char* ipAddress, int bufferSize) = 0; 
	virtual bool SendData(const char* data) = 0; 
	virtual void CloseConnection() = 0;

};
