// LaneAvailabilityClient.cpp references the functions found here.

#include "LADataProcess.h"
#include <memory>
#include <time.h>
#include <iostream>
#include <fstream>
#include <ostream>

using namespace std;
using namespace Json;

// Function: LADataProcess()
//
// Summary: Constructor for the class. Creates the JSON
// structure and stores the update string.
//
// Parameters: None
//
// Returns: None
LADataProcess::LADataProcess()
{
    //InitializeMsgData();
}

/// <summary>Destructure</summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
LADataProcess::~LADataProcess()
{
}

/// <summary></summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
bool LADataProcess::InitializeMsgData()
{
    return true;
}

/// <summary></summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
bool LADataProcess::FindComputerName()
{
    return true;
}

/// <summary></summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
bool LADataProcess::SetFormatType(LA_DATAFORMAT eDataFormat)
{
    m_DataFormat = eDataFormat;
    return true;
}

/// <summary></summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
LA_DATAFORMAT LADataProcess::GetFormatType()
{
    return m_DataFormat;
}

/// <summary></summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
char* LADataProcess::CompleteTransaction()
{
    // Get the JSON as a string and return it
    char m_transData[20917520];

    return m_transData;
}


/// <summary>
/// When End of Day for an SSCO is called, 
/// the machine also resets intraday data that it has collected. 
/// Operations that must occur
/// </summary>
/// <param name=""></param>  
/// <returns>Documentation of return type</returns>  
bool LADataProcess::ResetCycle()
{
    if (std::remove("StaticData.json") != 0)
    {
        return false;
    }

    if (std::remove("InterventionDataFile.json") != 0)
    {
        return false;
    }

    if (std::remove("WeightDataFile.json") != 0)
    {
        //return false;
    }

    return true;
}


// Function: ParseSSCOStaticData()
//
// Summary: Parses a message into weightmismatch and intervention data of the terminal 

// Parameters: const char * cData is the message passed to the method, identical to the
// ones from the methods above. 
// Returns: string complete accumulated data intervention and weight mismtch for the terminal
Json::Value LADataProcess::ParseSSCOStaticData(const char* cData)
{
    // access the static Data
    Json::Value staticDataFile;
    //Json::Reader dataReader;
    Json::Reader dataReader;
    std::string retVal = "";
    bool parsingSuccessful2 = dataReader.parse(cData, staticDataFile);
    if (!parsingSuccessful2)
    {
        //WriteActivityToLog("Failed to parse ParseSSCOStaticData: ", dataReader.getFormattedErrorMessages());
        cout << dataReader.getFormattedErrorMessages() << endl;
    }
    std::ifstream infile("StaticData.json");
    ofstream myfile;

    // if the file does not exist
    if (!infile.good()) 
    {
        myfile.open("StaticData.json");
        
        if (myfile.is_open())
        {
            myfile << staticDataFile;
        }
        else 
            cout << "Unable to open file";
        
        myfile.close();

        Json::StreamWriterBuilder firstBuilder;
        const std::string retVal = Json::writeString(firstBuilder, staticDataFile);
        //return retVal;
    }
    Json::StreamWriterBuilder firstBuilder;
    retVal = Json::writeString(firstBuilder, staticDataFile);
    //std::cout << retVal;
    return staticDataFile;
}

Json::Value LADataProcess::ParseSSCOInterventionData(const char* cData)
{
    // access the static Data
    Json::Value interventionDataFile;
    Json::Reader dataReader;
    const std::string retVal = "";
    bool parsingSuccessful2 = dataReader.parse(cData, interventionDataFile);
    if (!parsingSuccessful2)
    {
        //cout << dataReader.getFormattedErrorMessages() << endl;
    }
    std::ifstream infile("InterventionDataFile.json");
    ofstream myfile;

    // if the file does not exist
    if (!infile.good()) 
    {
        myfile.open("InterventionDataFile.json");
        if (myfile.is_open())
        {
            myfile << interventionDataFile;
        }
        else
            cout << "Unable to open file";
            myfile.close();

        Json::StreamWriterBuilder firstBuilder;
        const std::string retVal = Json::writeString(firstBuilder, interventionDataFile);
        return interventionDataFile;
    }

    // if thew file already exists

    // open the old json file and acesss it old mismatch
    std::ifstream t("InterventionDataFile.json");
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    Json::Value root;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(str, root);
    if (!parsingSuccessful)
    {
        cout << reader.getFormattedErrorMessages() << endl;
    }
    //aggregate, clear the old file and repopulate the file with the new data aggregated
    Json::Value newFileInterventions = interventionDataFile["interventions"];
    if (newFileInterventions.size() != 0)
    {
        try
        {
            int lengthInter = root["interventions"].size();
            for (int i = 0; i < newFileInterventions.size(); i++) {
                root["interventions"][lengthInter + i] = newFileInterventions[i];
            }
        }
        catch (Exception e)
        {
            //cout << e;
        }
    }


    //myfile.open("weightMismatch.json", std::ios_base::app);
    std::ofstream ofs;
    ofs.open("InterventionDataFile.json", std::ofstream::out | std::ofstream::trunc);
    if (ofs.is_open())
    {
        ofs << root;
    }
    else cout << "Unable to open file";
    ofs.close();
    Json::StreamWriterBuilder builder;
    const std::string output = Json::writeString(builder, root);
    //std::cout << output;
    return root;
}

Json::Value LADataProcess::ParseSSCOWeightData(const char* cData)
{
    // access the static Data
    Json::Value weightDataFile;
    Json::Reader dataReader;
    const std::string retVal = "";
    bool parsingSuccessful2 = dataReader.parse(cData, weightDataFile);
    if (!parsingSuccessful2)
    {
        cout << dataReader.getFormattedErrorMessages() << endl;
    }
    std::ifstream infile("WeightDataFile.json");
    ofstream myfile;

    // if the file does not exist
    if (!infile.good()) {
        myfile.open("WeightDataFile.json");
        if (myfile.is_open())
        {
            myfile << weightDataFile;
        }
        else cout << "Unable to open file";
        myfile.close();

        Json::StreamWriterBuilder firstBuilder;
        const std::string retVal = Json::writeString(firstBuilder, weightDataFile);
        return weightDataFile;
    }

    // if thew file already exists

    // open the old json file and acesss it old mismatch
    std::ifstream t("WeightDataFile.json");
    std::string str((std::istreambuf_iterator<char>(t)),
        std::istreambuf_iterator<char>());
    Json::Value root;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(str, root);
    if (!parsingSuccessful)
    {
        cout << reader.getFormattedErrorMessages() << endl;
    }
    //aggregate, clear the old file and repopulate the file with the new data aggregated
    //aggregate, clear the old file and repopulate the file with the new data aggregated
    Json::Value newFileMismatch = weightDataFile["weightMismatches"];
    if (newFileMismatch.size() != 0)
    {
        try
        {
            int length = root["weightMismatches"].size();
            for (int i = 0; i < newFileMismatch.size(); i++) {
                root["weightMismatches"][length + i] = newFileMismatch[i];
            }
        }
        catch (Exception e)
        {
            //cout << e;
        }
    }


    //myfile.open("weightMismatch.json", std::ios_base::app);
    std::ofstream ofs;
    ofs.open("WeightDataFile.json", std::ofstream::out | std::ofstream::trunc);
    if (ofs.is_open())
    {
        ofs << root;
    }
    else cout << "Unable to open file";
    ofs.close();
    Json::StreamWriterBuilder builder;
    const std::string output = Json::writeString(builder, root);
    //std::cout << output;
    return root;
}

// Function: CurrentDateTime()
//
// Summary: Returns the current DateTime formatted as "ddMMYYYY:HH:mm" (ex. 21062018:15:12
// corresponds to June 21st, 2018 at 15:12, or 3:12 pm).
//
// Parameters: None
//
// Returns: char* currentTime is the formatted string for current DateTime.
char* LADataProcess::CurrentDateTime()
{
    // Create current datetime as per specifications
    time_t now = time(0);
    struct tm tstruct;
    char currentTime[80];
    tstruct = *localtime(&now);
    strftime(currentTime, sizeof(currentTime), "%d%m%Y:%R", &tstruct);

    return currentTime;
}
