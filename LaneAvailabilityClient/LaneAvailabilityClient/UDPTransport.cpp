#include "stdafx.h"
#include "UDPTransport.h"

#define _WINSOCK_DEPRECATED_NO_WARNINGS


/***********************************************************************************************
Function:	UDPTransport()

Summary:	Constructor for UDP Transport class.  Intitializes socket monitoring parameters.

Params:		None

Return:		
***************************************************************************************************/
UDPTransport::UDPTransport()
    : m_sequence(0),
    m_bSocketInitialized(false)
{
}

/***********************************************************************************************
Function:	~UDPTransport()

Summary:	Destructor for UDP Transport class. 

Params:		None

Return:
***************************************************************************************************/
UDPTransport::~UDPTransport()
{
}
/***********************************************************************************************
Function:	BuildMessage(...)

Summary:	Constructs a message for UDP transport that corresponds to the DataPump message format.

Params:		const char * data -  string containing the data from LADataProcess that provides information
            on the lane activities

Return:		int representings number of bytes in message
***************************************************************************************************/
int UDPTransport::BuildMessage(const char * data)
{
    m_msg.hdr.seq = m_sequence++;
    m_msg.hdr.type = 10; //TYPE_JSON
    
    
    m_msg.msg.msgId = 0; 

    //m_msg.msg.text.resize(m_bufferSize);

    //clear memory for data
    memset(&m_msg.msg.text, 0, sizeof(m_msg.msg.text));
    //memset(&m_msg.msg.text, 0, m_bufferSize+1);

    //convert char* to CString
    CString tempData(data);
    CT2A szAscii(tempData);
    strncpy(m_msg.msg.text, szAscii, sizeof(m_msg.msg.text));

    int x = (int) sizeof(m_msg);
    int y = (int) sizeof(m_msg.hdr);
    int z = (int) sizeof(m_msg.msg.text);
    int zz = strlen(m_msg.msg.text);
    int a = (int)tempData.GetLength();

    int minOfTwo = min(sizeof(m_msg), sizeof(m_msg.hdr) + 4 + tempData.GetLength());

    return (minOfTwo > tempData.GetLength()) ? minOfTwo : -1;
}
/***********************************************************************************************
Function:	SendData(...)

Summary:	Sends message data over UDP connection to DataPump plugin.

Params:		const char * data -  string containing the data from LADataProcess that provides information
on the lane activities

Return:		bool reprenseting status of attempt to send data to host
***************************************************************************************************/
bool UDPTransport::SendData(const char * data)
{
    int msgLength = BuildMessage(data);

    if (msgLength == 0)
    {
        msgLength = (int) sizeof(m_msg);
    }

    if (msgLength == -1)
    {
        //message bigger than buffer. Do not send otherwise DP server will crash
        return false;
    }

    if (sendto(m_socket, (const char*)&m_msg, msgLength, 0, (SOCKADDR *)&m_server, sizeof(m_server)) == SOCKET_ERROR)
    {
        return false; //cannot connect
    }
    else
    {
        //delete[] m_msg.msg.text;
        return true;
    }
}
/***********************************************************************************************
Function:	ConnectToRemoteHost(...)

Summary:	Initializes UDP socket connection to the DataPump host.

Params:		int port - port number for socket connection
            const char* ipAddress - string representing IP address of DataPump host

Return:		bool reprenseting status of attempt to create socket connection to DataPump
***************************************************************************************************/
bool UDPTransport::ConnectToRemoteHost(int port, const char* ipAddress)
{

    //struct addrinfo *result = NULL, *ptr = NULL, hints; 
    struct hostent *remoteHost;



    WSAData wsaData; // Start up Winsock 

    int error = WSAStartup(0x0202, &wsaData);

    if (error)
    {		
        WSACleanup();
        return false;
    }

    if (wsaData.wVersion != 0x0202)
    {		
        WSACleanup();
        return false;
    }

    remoteHost = gethostbyname(ipAddress);

    char* remoteIP = inet_ntoa(*(struct in_addr *) *remoteHost->h_addr_list);

    //Zero out the data structure 
    memset(&m_server, 0, sizeof(m_server));

    //information needed for SCOKET init
    m_server.sin_family = AF_INET; //address family 

    // convert the port from host to network byte order 
    m_server.sin_port = htons(port);

    //CString csIPAddress(ipAddress);
    //CT2A ascii_ipAddress(csIPAddress); //convert to ASCII 
    //m_server.sin_addr.s_addr = inet_addr(ascii_ipAddress); //Server IP
    m_server.sin_addr.s_addr = inet_addr(remoteIP); //Server IP

    m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); //Create socket 

    //freeaddrinfo(addrs);

    if (m_socket == INVALID_SOCKET)
    {	
        //freeaddrinfo(result);
        WSACleanup();
        
        return false; //cannot create the socket 
    }

    return true;

}
/***********************************************************************************************
Function:	CloseConnection()

Summary:	Close UDP socket connection to the DataPump host.

Params:		None

Return:		void
***************************************************************************************************/
void UDPTransport::CloseConnection()
{
    if (m_socket)
    {
        closesocket(m_socket);
    }
    WSACleanup();
}

/***********************************************************************************************
Function:	Initialize()

Summary:	Receives parameters necessary to initialize UDP socket connection to the DataPump host.

Params:		int port - port number for socket connection
            const char* ipAddress - string representing IP address of DataPump host
            bufferSize - bufferSize of the Data to send over.

Return:		void
***************************************************************************************************/
bool UDPTransport::Initialize(int port, const char* ipAddress, int bufferSize)
{
    bool status = ConnectToRemoteHost(port, ipAddress);

    m_bufferSize = bufferSize;

    return status;
}