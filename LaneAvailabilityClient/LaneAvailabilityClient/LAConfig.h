#pragma once
#include "ILAConfig.h"


class LAConfig : public ILAConfig
{
public:
	LAConfig();
	~LAConfig();
	
	virtual char* GetHostIP() override;
	virtual int GetHostPort() override;
    //virtual int GetTimer() override;
    virtual int GetBufferSize() override;

private:
	//read configuration values from the ini file
	virtual void ReadHostIP();
	virtual void ReadHostPort();
    //virtual void ReadTimer();
    virtual void ReadBufferSize();

protected:
    char* m_csHostIP;  //IP Address of UDP Host
    char* m_csHostPort; //Connection Port for UDP Host
    //char* m_csTimer;

    char* m_csBufferSize;
};

