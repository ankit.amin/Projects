/*************************************************************************
 *
 * CmSynch.h
 *
 *   C++ Classes for Win32 Synchronization
 *    - CmSynch            Base Class. Not usually used directly.
 *    - CmEvent            Event Object
 *      CmSemaphore        Semaphore Object
 *      CmMutex            Mutex Object
 *      CmCriticalSection  Critical Section Object
 *    - CmWaitMany         Wait for 2 or more synchronization objects
 *
 *   The synchronization object classes each create the underlying Win32
 *   object during construction and close it during destruction.
 *   Their public members are:
 *
 *     CmSynch
 *       Wait(), GetStatus(), GetHandle(), SetHandle()
 *     CmEvent
 *       Wait(), SetEvent(), PulseEvent(), ResetEvent(),
 *       GetStatus(), GetHandle()
 *     CmSemaphore:
 *       Acquire(), Release(),
 *       GetStatus(), GetHandle()
 *     CmMutex:
 *       Acquire() [and Lock()], Release() [and Unlock()],
 *       GetStatus(), GetHandle()
 *     CmCriticalSection
 *       Acquire() [and Lock()], Release() [and Unlock()],
 *
 *   The wait for multiple object class gets its object handles from the
 *   synchronization objects above (except CmCriticalSection).
 *
 *     CmWaitMany
 *       Wait(), GetStatus(), GetIndex()
 *   
 *   NOTE/CAUTION:  Be aware that...
 *   (1) Mutexes are owned by a *thread*, not a process.  Therefore, if
 *       you acquire a mutex, then you must ensure that the same thread
 *       releases that mutex.
 *   (2) Mutexes and critical sections have counts, so that the same thread
 *       may acquire it several times.  The thread must of course release it
 *       the same number of times before it becomes available for others.
 *       This behavior can be useful when an object is used to protect both a
 *       lower-level and a higher-level subroutine, where the higher calls the
 *       lower.
 *   
 *   Example use:
 *     CmMutex Mutex;                  // Create unnamed mutex.
 *     void SampleSynch( CmEvent* pEvent )
 *     {
 *         Mutex.Acquire();            // Lock the mutex.
 *         // Do some things.
 *         Mutex.Release();            // Unlock the mutex.
 *     
 *         CmSynch* SyncObjects[] = { &Mutex, pEvent };
 *         CmWaitMany WaitMany( SyncObjects, 2 ); // Create to wait for 2 objects.
 *         if ( WaitMany.Wait( 500, FALSE ) )     // Wait up to 1/2 sec for either.
 *         {
 *             if ( WaitMany.GetIndex() == 0 )
 *             {
 *                 // Do some things when mutex is signalled.
 *                 Mutex.Release();    // The Wait() acquired the mutex, so will release.
 *             }
 *             else
 *             {
 *                 // Do some things when event is signalled.
 *             }
 *         }
 *     }
 *
 *   Copyright (c) 1996 NCR.  All rights reserved.
 *
 * Author: Bonnie Buell 
 *
 *
 * $Workfile:: CmSynch.h                                                 $
 *
 * $Revision:: 13                                                        $
 *
 * $Archive:: /SSPSW7/Current/Common/include/CmSynch.h                   $
 *
 * $Date:: 10/18/12 5:48p                                                $
 *
 *************************************************************************
 * 
 * Copyright (c) NCR Corporation 1998.  All rights reserved.
 *
 *************************************************************************
 *
 * $History:: CmSynch.h                                                  $
 //
 //*****************  Version 13  *****************
 //User: Kh100012     Date: 10/18/12   Time: 5:48p
 //Updated in $/SSPSW7/Current/Common/include
 //SSCOB-5102 : Mutually exclusive problem for NCR_BAG & NCR_ESP
 //CADD had an issue with certain cases of mutually exclusive logic that
 //would allow it to fall through when it should have been waiting.  Added
 //code to fix.
 //
 //
 //*****************  Version 12  *****************
 //User: Kh100012     Date: 4/01/11    Time: 7:24a
 //Updated in $/SSPSW/Current/Common/include
 //TAR 441678: Moved some functionality into a common class.  In the
 //interest of compatibility across builds, this is wrapped in a #ifdef as
 //for other parts of this TAR and should only be included in SSPSW
 //builds.
 * 
 * *****************  Version 11  *****************
 * User: Wr185001     Date: 6/20/07    Time: 9:53a
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * Correct compile warnings
 * 
 * *****************  Version 10  *****************
 * User: Dm180015     Date: 12/14/06   Time: 4:04a
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * 
 * *****************  Version 9  *****************
 * User: Bj185005     Date: 05-07-15   Time: 3:36p
 * Updated in $/Fastlane/4.0 Patch D/Development/Platform/include
 * checking in changes from 4.0 patch C
 * 
 * *****************  Version 1  *****************
 * User: Dm180015     Date: 25/01/05   Time: 18.01
 * Created in $/Fastlane/4.0 Patch B/PLATFORM/Include
* 
* *****************  Version 9  *****************
* User: Dm180015     Date: 5/10/04    Time: 16.17
* Updated in $/FastLane/Patches/4.0 RAP/Development/Platform/include
* 
* *****************  Version 7  *****************
* User: Cm150000     Date: 12/13/01   Time: 2:58p
* Updated in $/SCOTx Software/Source/Release 3.0/Security/Include
* 
* *****************  Version 4  *****************
* User: Fa150001     Date: 4/13/00    Time: 2:21p
* Updated in $/SCOTx Software/Source/Release 2.0/Platform/include
* Upgrade CmDataCapture (CmClass) files from v1.3 to v1.8.
 * 
 * *****************  Version 10  *****************
 * User: Farmer       Date: 12/21/99   Time: 4:50p
 * Updated in $/OPOS/Rel.1_8/Include/NCR
 * Updated source code with Windows CE modfications
 * 
 * *****************  Version 8  *****************
 * User: Dw160003     Date: 11/05/99   Time: 2:45p
 * Updated in $/OPOS/Rel.1_7/Include/NCR
 * added VSS header
 * 
 * *****************  Version 1  *****************
 * User: MonroeC       Date: 2/07/96    Time: 10:01a
 * Created in $/OPOS/Rel.1_1/Include/NCR
 * 
 * *****************  Version 2  *****************
 * User: MonroeC       Date: 2/24/96    Time: 10:01a
 * Made CmSynch non-abstract class for special cases
 * 
 * *****************  Version 3  *****************
 * User: MonroeC       Date: 3/07/96    Time: 10:01a
 * Removed alternate constructor for CmWaitMany
 * 
 *
 ************************************************************************/


#ifndef _CMSYNCH_H_
#define _CMSYNCH_H_

#include <afxwin.h>


///////////////////////////////////////////////////////////////////////////
// Classes

//CObject
    class CmSynch;
        class CmEvent;
        class CmSemaphore;
        class CmMutex;
class CmCriticalSection;
class CmWaitMany;


///////////////////////////////////////////////////////////////////////////
// Basic Synchronization Object
//   Not often used directly by an application, but may be used if
//   the handle is set, either by the constructor form that passes
//   a handle as the first parameter, or by the SetHandle function.

class CmSynch : public CObject
{
    DECLARE_DYNAMIC( CmSynch )

public:
    CmSynch( LPCTSTR pszName = NULL );
    CmSynch( HANDLE hSync, LPCTSTR pszName = NULL );
    virtual ~CmSynch();

    BOOL Wait(                  // Wait for the synch object. TRUE if succeeds.
                                //   Status put in m_Status.
        DWORD dwTimeout = INFINITE );
    int GetStatus() const;      // Gets status of last Acquire or Wait.
        // The values are those returned by WaitForSingleObject:
            //   WAIT_OBJECT_0  = Success.
            //   WAIT_ABANDONED = Success, but previous mutex owner thread
            //                      terminated without releasing the mutex.
            //   WAIT_TIMEOUT   = Timeout elapsed.
            //   WAIT_FAILED    = Other unspecified error.
    HANDLE GetHandle() const;   // Gets handle for object.
                                //   If CritSec, then zero.
    void SetHandle(             // Sets handle for object.
        HANDLE h );             //   Usually not necessary. Can be used
                                //   to set handle to thread, for example.

protected:
    HANDLE m_hObject;
    int    m_Status;
    void   CloseHandle();

#ifndef _WIN32_WCE
#ifdef _DEBUG
public:
    CString m_sName;
    virtual void AssertValid() const;
    virtual void Dump( CDumpContext& dc ) const;
#endif
#endif
};


///////////////////////////////////////////////////////////////////////////
// CmEvent

class CmEvent : public CmSynch
{
    DECLARE_DYNAMIC( CmEvent )

public:
    CmEvent(                // Constructor creates the event.
        BOOL    bSignaled     = FALSE,  // Not signaled.
        BOOL    bManualReset  = TRUE,   // Manual reset.
        LPCTSTR pszName       = NULL,   // Unnamed.
        LPSECURITY_ATTRIBUTES pAttributes = NULL );
    virtual ~CmEvent();     // Destructor closes the event.

    BOOL Wait(              // Wait for event to be set. TRUE if succeeds.
                            //   Status put in m_Status.
        DWORD dwTimeout = INFINITE );
    BOOL SetEvent();        // Set the semaphore.
                            //   If AutoReset, will be reset as soon as one
                            //     thread waits for the event.
    BOOL PulseEvent();      // Pulse the semaphore: set/reset.
                            //   If ManualReset, all waiting threads released.
                            //   If AutoReset, one waiting thread released.
    BOOL ResetEvent();      // Reset the semaphore (for use by ManualReset events).
private:
	SECURITY_DESCRIPTOR gSD;			// global security descriptor
	SECURITY_ATTRIBUTES gAttributes;	// global security attribute
};


///////////////////////////////////////////////////////////////////////////
// CmSemaphore

class CmSemaphore : public CmSynch
{
    DECLARE_DYNAMIC( CmSemaphore )

public:
    CmSemaphore(            // Constructor creates the semaphore.
        LONG    lInitialCount = 1,      // Count at 1.
        LONG    lMaximumCount = 1,      // Maximum is 1.
        LPCTSTR pszName       = NULL,   // Unnamed.
        LPSECURITY_ATTRIBUTES pAttributes = NULL );
    virtual ~CmSemaphore(); // Destructor closes the semaphore.

    BOOL Acquire(           // Acquire a semaphore unit. TRUE if succeeds.
                            //   Status put in m_Status.
        DWORD dwTimeout = INFINITE );
    BOOL Release(           // Release 1 or more units.
        LONG   lCount      = 1,
        LPLONG plPrevCount = NULL );
private:
	SECURITY_DESCRIPTOR gSD;			// global security descriptor
	SECURITY_ATTRIBUTES gAttributes;	// global security attribute
};

///////////////////////////////////////////////////////////////////////////
// CmMutex

class CmMutex : public CmSynch
{
    DECLARE_DYNAMIC( CmMutex )

public:
    CmMutex(                // Constructor creates the mutex.
        BOOL    bInitialOwner = FALSE,  // Not owned.
        LPCTSTR pszName       = NULL,   // Unnamed.
        LPSECURITY_ATTRIBUTES pAttributes = NULL );
    virtual ~CmMutex();     // Destructor closes the semaphore.

    BOOL Acquire(           // Acquire the mutex. TRUE if succeeds.
                            //   Status put in m_Status.
        DWORD dwTimeout = INFINITE );
    BOOL Release();

    BOOL Lock(DWORD dwTimeout = INFINITE) // Alias for Acquire().
        { return Acquire(dwTimeout); }
    BOOL Unlock()                         // Alias for Release().
        { return Release(); }
private:
	SECURITY_DESCRIPTOR gSD;			// global security descriptor
	SECURITY_ATTRIBUTES gAttributes;	// global security attribute
};


///////////////////////////////////////////////////////////////////////////
// CmCriticalSection

class CmCriticalSection
{
public:
    CmCriticalSection();    // Constructor initializes critsec.
    ~CmCriticalSection();   // Destructor deletes critsec.

    BOOL Acquire();         // Acquire (enter) the critical section.
    BOOL Acquire(           // Acquire (enter) the critical section.
        DWORD dwTimeout );  //   Timeout is ignored.
    BOOL Release();         // Release (leave) the critical section.


    BOOL Lock(DWORD dwTimeout = INFINITE) // Alias for Acquire().
        { dwTimeout; return Acquire(); }

    BOOL Unlock()                         // Alias for Release().
        { return Release(); }

protected:
    CRITICAL_SECTION m_CritSect;
};


///////////////////////////////////////////////////////////////////////////
// CmWaitMany

const int CM_WAIT_WAKE_MASK = WAIT_OBJECT_0 + 1;

class CmWaitMany
{
public:
    CmWaitMany(                     // Constructor builds object handle array.
        CmSynch* paObjects[],       //   Pointer to array of objects.
        DWORD   dwCount );          //   Number of objects to wait on.
    ~CmWaitMany();                  // Destructor.

    BOOL Wait(                      // Wait for one or all objects to be signalled.
                                    // Returns TRUE if successful; else FALSE.
        DWORD dwTimeout  = INFINITE,//   Timeout in milliseconds.
        BOOL  bWaitAll   = TRUE,    //   TRUE=wait for all; FALSE=wait for one.
        DWORD dwWakeMask = 0 );     //   Mask of additional input events that wait up.

    int GetStatus() const;  // Gets status of last Wait.
    int GetIndex() const;   // Gets index of last Wait. Use only if success.

protected:
    DWORD m_dwRawStatus;    // Raw status from WaitForMultipleObjects:
        //  ** Successful **
        //    WAIT_OBJECT_0  to  (WAIT_OBJECT_0 + dwCount - 1)
        //      If bWaitAll=TRUE:  All objects are signalled.
        //      If bWaitAll=FALSE: The object at (status - WAIT_OBJECT_0)
        //        is signalled.
        //    WAIT_OBJECT_0 + dwCount
        //      Input of dwWakeMask type available in the input queue.
        //    WAIT_ABANDONED_0  to  (WAIT_ABANDONED_0 + dwCount - 1)
        //      A previous mutex owner thread terminated without releasing the mutex.
        //      If bWaitAll=TRUE:  All objects are signalled, with at least
        //        one abandoned mutex.
        //      If bWaitAll=FALSE: The object at (status - WAIT_ABANDONED_0) is an
        //        abandoned mutex now owned by the caller.
        //  ** Failed **
        //    WAIT_TIMEOUT   = Timeout elapsed.
        //    WAIT_FAILED    = Other unspecified error.
    int m_nStatus;          // Status of last Wait.
        //   WAIT_OBJECT_0  = Successfully waited for objects.
        //   CM_WAIT_WAKE_MASK = Success due due input in input queue.
        //   WAIT_ABANDONED = Success, but previous mutex owner thread
        //                      terminated without releasing the mutex.
        //   WAIT_TIMEOUT   = Timeout elapsed.
        //   WAIT_FAILED    = Other unspecified error.
    int m_nIndex;           // Index of last Wait (only if successful).
        //   0 to dwCount-1 = Signalled object number (if bWaitAll=FALSE).
        //   -1             = Input in input queue.

    HANDLE* m_paHandles;
    HANDLE  m_hPrehandles[4];
    DWORD   m_dwCount;
};


#ifdef	SSPS_RPSW 
/// \brief Class to mimic pthread condition variable
///
/// Why do this at all?  A piece being ported already relied on
/// condition variables.  This may go away as the implementation is
/// tweaked, but to get it to work, it was quick to implement a
/// condition variable.
///
/// Why not just an event?  The event part is fine, but the condition
/// variable lets you protect some external data (e.g. a queue, a
/// counter, etc.), so it has a mutex as well as the condition to be
/// sent/broadcast.  The event is great for the latter but I wanted
/// the mutex, too, to be able to lock around the data and then
/// broadcast, then unlock.  This helps encapsulate the thing...
///
/// Note that condition variables are guaranteed to unlock the mutex
/// for you when you get ready to go to wait on them, and you are
/// guaranteed to wake up with the mutex locked for you.
///
/// Currently use a mutex, but could we just use a Critical Section?
class CmConditionVariable
{
public:
   /// \brief Construction with passed in mutex
   CmConditionVariable(CmCriticalSection *ptmLockObject=NULL) { m_ptmConditionLockObject = ptmLockObject; m_ulWaitCount = 0;}

   /// \brief Lock associated mutex
   BOOL Lock();
   /// \brief Unlock associated mutex
   BOOL Unlock();

   /// \brief synonym for lock...
   BOOL Acquire();

   /// \brief sysnonym for unlock...
   BOOL Release();
   
   /// \brief Wait on condition to be signaled.
   BOOL Wait(DWORD dwTimeout = INFINITE);
   /// \brief Broadcast (signal) condition
   void Broadcast();
   /// \brief indicated what the associated mutex is.
   ///
   /// \note If mutex is deleted/goes out of scope, etc., this would
   /// cause problems.
   void SetMutex(CmCriticalSection *ptmLockObject) { m_ptmConditionLockObject = ptmLockObject; }

   /// \brief set up a way to explicitly reset condition (event)
   ///
   /// This allows a way to reset the event a condition variable
   /// relies on.
   BOOL ResetCondition() { return(m_teConditionEvent.ResetEvent()); }
   
private:
   /// \brief Pointer to the mutex associated with the condition
   /// variable.
   ///
   /// We pass this in rather than have one in the class because
   /// sometimes we use the same mutex for 2 different condition
   /// variables...e.g. one counter, but 2 cond. vars.--one on
   /// increment and one on decrement.  Both use the same mutex to
   /// control the same variable (the counter), but have different
   /// events...
   CmCriticalSection *m_ptmConditionLockObject;

   /// \brief Event variable that represents the "condition" variable.
   ///
   /// Setting this event represents the change to the condition
   /// variable.
   CmEvent m_teConditionEvent;

   /// \brief Wait count
   ///
   /// Number places waiting on this condition variable.  We use the
   /// same mutex that protects everything else to protect this...
   unsigned long m_ulWaitCount;
};

#endif

///////////////////////////////////////////////////////////////////////////

#define CMSYNCH_INLINE inline
#include "CmSynch.inl"
#undef  CMSYNCH_INLINE

///////////////////////////////////////////////////////////////////////////

#endif

//..end..
