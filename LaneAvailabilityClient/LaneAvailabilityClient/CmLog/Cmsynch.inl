///////////////////////////////////////////////////////////////////////////
//
// CmSynch.inl
//
//   Inline functions for CmSynch.h
//
//   Copyright (c) 1996 NCR.  All rights reserved.
//
//   Date                   Modification                          Author
//---------|---------------------------------------------------|-------------
// 96/02/07 Initial version.                                    C. Monroe
//
/////////////////////////////////////////////////////////////////////////////

CMSYNCH_INLINE int CmSynch::GetStatus() const
    {   return m_Status;   }
CMSYNCH_INLINE HANDLE CmSynch::GetHandle() const
    {   return m_hObject;   }

CMSYNCH_INLINE BOOL CmMutex::Acquire( DWORD dwTimeout /* = INFINITE */ )
    {   return CmSynch::Wait( dwTimeout );   }
CMSYNCH_INLINE BOOL CmMutex::Release()
    {   ASSERT( m_hObject != NULL );
        return ::ReleaseMutex( m_hObject );   }

// WTS: Windows CE Updates (12-98)
// Windows CE does not support semaphores
#ifndef _WIN32_WCE

CMSYNCH_INLINE BOOL CmSemaphore::Acquire( DWORD dwTimeout /* = INFINITE */ )
    {   return CmSynch::Wait( dwTimeout );   }
CMSYNCH_INLINE BOOL CmSemaphore::Release(
        LONG lCount /* = 1 */, LPLONG plPrevCount /* = NULL */ )
    {   ASSERT( m_hObject != NULL );
        return ::ReleaseSemaphore( m_hObject, lCount, plPrevCount );   }

#endif // not _WIN32_WCE

CMSYNCH_INLINE BOOL CmEvent::Wait( DWORD dwTimeout /* = INFINITE */ )
    {   return CmSynch::Wait( dwTimeout );   }
CMSYNCH_INLINE BOOL CmEvent::SetEvent()
    {   ASSERT( m_hObject != NULL );
        return ::SetEvent( m_hObject );   }
CMSYNCH_INLINE BOOL CmEvent::PulseEvent()
    {   ASSERT( m_hObject != NULL );
        return ::PulseEvent( m_hObject );   }
CMSYNCH_INLINE BOOL CmEvent::ResetEvent()
    {   ASSERT( m_hObject != NULL );
        return ::ResetEvent( m_hObject );   }

CMSYNCH_INLINE CmCriticalSection::CmCriticalSection()
    {   ::InitializeCriticalSection( &m_CritSect );   }
CMSYNCH_INLINE CmCriticalSection::~CmCriticalSection()
    {   ::DeleteCriticalSection( &m_CritSect );   }
CMSYNCH_INLINE BOOL CmCriticalSection::Acquire()
    {   ::EnterCriticalSection( &m_CritSect );
        return TRUE;   }
CMSYNCH_INLINE BOOL CmCriticalSection::Acquire(
         DWORD dwTimeout  /* = INFINITE */ )  
    {   
		dwTimeout;
		::EnterCriticalSection( &m_CritSect );
        return TRUE;   
	}
		
CMSYNCH_INLINE BOOL CmCriticalSection::Release()
    {   ::LeaveCriticalSection( &m_CritSect );
        return TRUE;   }

CMSYNCH_INLINE int CmWaitMany::GetStatus() const
    {   return m_nStatus;   }
CMSYNCH_INLINE int CmWaitMany::GetIndex() const
    {   return m_nIndex;   }

#ifdef SSPS_RPSW 
CMSYNCH_INLINE BOOL CmConditionVariable::Acquire() {
               return(Lock());
}

CMSYNCH_INLINE BOOL CmConditionVariable::Release() {
               return(Unlock());
}

#endif

//..end..