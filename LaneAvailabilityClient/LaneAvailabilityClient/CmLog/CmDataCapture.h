/*************************************************************************
 *
 * CmDataCapture.h
 *
 *   Defines the classes CmDataCapture.
 *   This class defines and implements data capture routines.
 *   For use with VC++ 4.x.
 *
 *   Class CmDataCapture
 *
 **** Documentation ***
 *
 *   Constructor (at global scope or as a data member in your object).
 *      CmDataCapture DCap;
 *
 *   Set up initial options for time, dcap file, mask, and control.
 *      DCap.ReadRegistry(
 *          LPCTSTR pszBaseKeyName,         // Registry key name (within HKEY_LOCAL_MACHINE).
 *          LPCTSTR pszKeyName = NULL );    // Registry subkey to open/create.
 *                                          //   If missing or NULL, use "DataCapture".
 *                                          //   To use the base key, set to "".
 *
 *   Set up options... Usually one-time.
 *
 *      // Characters prepended to each line -> Default = none.
 *      DCap.SetPrefix(
 *          LPCTSTR pszPrefix );            // Prefix string.
 *
 *      // Time options -> Default = hh:mm:ss. (See constants below.)
 *      //   Options are one or more of:
 *      //     (1) mm/dd hh:mm:ss, hh:mm:ss, or mm:ss,
 *      //     (2) Milliseconds since boot.
 *      //     (3) Thread ID. (Not related to time; just an extra option.)
 *      DCap.SetTimeOptions(
 *          DWORD dwTimeOptions );          // Zero or more options.
 *
 *      // Data capture file spec and max size (0 = no limit) -> Default = none.
 *      //   If the max size is reached, data cap will copy it to a file with
 *      //   extension
 *      DCap.SetFile(
 *          LPCSTR pszFSpec,                // File specification.
 *          DWORD dwMaxKSize );             // Max size (0 = no limit).
 *
 *   Control the capture:
 *
 *      // Bitmask of data to capture -> Default = all bits on.
 *      DCap.SetCaptureMask(
 *          DWORD dwCaptureMask );          // The bitmask.
 *
 *      // Set/reset capturing -> Default = debugger + dcap app. (See constants below.)
 *      //   Options are either "off", or one or more of:
 *      //     (1)  Data capture application (via dcap thread).
 *      //     (2)  Debugger output (via dcap thread).
 *      //     (3)  Data capture file (via dcap thread).
 *      //    (4-6) Same as above, but processed before dcap function.
 *      DCap.SetCaptureControl(
 *          DWORD dwControl );              // Data cap control.
 *
 *      // Prepare for capture. Start a low-priority data cap thread that
 *      //   performs the output (except for direct debugger output).
 *      // If not called by app, "Initialize" is automatically called by the
 *      //   first "Capture" function call.
 *      DCap.Initialize(
 *          LPCTSTR pszInitString = NULL ); // Banner line.
 *
 *      // End capture. Stop the data cap thread.
 *      //   If not called by app, "Cleanup" is automatically called by Destructor.
 *      DCap.Cleanup();
 *
 *      // Flush captured data to the output device(s).
 *      // Usually done by the data cap thread. Also, called by "Cleanup".
 *      DCap.Flush();
 *
 *   Perform data capture.
 *
 *      // Capture some data.
 *      //   The capture is performed if
 *      //     (1) Any bits of the "dwMask" parameter match bits in the current
 *      //           capture mask (set by "SetCaptureMask").
 *      //     (2) An output device has been selected (set by "SetCaptureControl").
 *      //   The other capture functions perform formatting if needed,
 *      //   then call this function.
 *      DCap.Capture(
 *          DWORD dwMask,                   // Capture bitmask.
 *          LPCTSTR pszString,              // String to be captured.
 *          LPCVOID pbBytes = NULL,         // Optional array of bytes.
 *          DWORD dwLenBytes = 0 );         // Length of optional byte array.
 *
 *      // Format data and capture it.
 *      DCap.DCPrintf(mask,fmt,...)                 // Printf.
 *      DCap.DCDataPrintf(mask,data,len,fmt,...)    // Data bytes plus Printf.
 *
 *      // Other forms (preserved from older version).
 *      DCap.DCPrintf1,     DCap.DCPrintf2      DCap.DCPrintf3      DCap.DCPrintf4
 *      DCap.DCDataPrintf1  DCap.DCDataPrintf2  DCap.DCDataPrintf3  DCap.DCDataPrintf4
 *
 *   Copyright (c) 1996-1997 NCR.  All rights reserved.
 *
 * Author: C. Monroe 
 *
 *
 * $Workfile:: CmDataCapture.h                                           $
 *
 * $Revision:: 18                                                        $
 *
 * $Archive:: /SSPSW/Current/Common/include/CmDataCapture.h              $
 *
 * $Date:: 11/29/10 3:53p                                                $
 *
 *************************************************************************
 * 
 * Copyright (c) NCR Corporation 1998.  All rights reserved.
 *
 *************************************************************************
 *
 * $History:: CmDataCapture.h                                            $
 * 
 * *****************  Version 18  *****************
 * User: Wr185001     Date: 11/29/10   Time: 3:53p
 * Updated in $/SSPSW/Current/Common/include
 * TAR 441678:  Make CmRegistry and CmDataCapture classes conditional
 * compile new CADD enhancements only if compiling with VStudio 2008 or
 * better.  Otherwise CADD enhancements to these classes are excluded for
 * legacy compatibility.  
 * 
 * *****************  Version 17  *****************
 * User: Wr185001     Date: 11/29/10   Time: 3:47p
 * Updated in $/SSPSW/Current/Common/include
 * TAR 441678:  Make CmRegistry and CmDataCapture classes conditional
 * compile new CADD enhancements only if compiling with VStudio 2008 or
 * better.  Otherwise CADD enhancements to these classes are excluded for
 * legacy compatibility.  
 * 
 * *****************  Version 16  *****************
 * User: St185026     Date: 9/08/10    Time: 4:12p
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * TAR 391288: CADD Development
 * 
 * Visual Studio 2008 support
 * 
 * *****************  Version 3  *****************
 * User: St185026     Date: 5/04/10    Time: 4:31p
 * Updated in $/Fastlane/ADD 3.0/Development/Platform/include
 * TAR 391288: CADD Development
 * 
 * Added support to CmRegistry and CmDataCapture for "registry" backed by
 * sectioned file instead of Windows Registry. Retains old functionality
 * unless use new function SetUseFileName().
 * DCapVersion 2.0.3
 * 
 * *****************  Version 2  *****************
 * User: St185026     Date: 4/23/10    Time: 4:40p
 * Updated in $/Fastlane/ADD 3.0/Development/Platform/include
 * TAR 391288: CADD Development
 * Adds prefix function support
 * 
 * *****************  Version 1  *****************
 * User: St185026     Date: 4/10/09    Time: 4:28p
 * Created in $/Fastlane/ADD 3.0/Development/Platform/INCLUDE
 * TAR 391288: CADD Development
 * 
 * *****************  Version 15  *****************
 * User: Nn185020     Date: 11/04/08   Time: 5:25p
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * Remove m_hSMemOwner since we do not use it anymore
 * 
 * *****************  Version 14  *****************
 * User: Wr185001     Date: 10/31/08   Time: 4:54p
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * TAR 373723: Missing data in Datacap due to rollover.  
 * Just some fixes here for performance.  No data was found missing. 
 * 
 * Also the file max limit was not being observed in multi thead heavy
 * usage.  
 * 
 * *****************  Version 13  *****************
 * User: Nn185020     Date: 10/23/08   Time: 2:45p
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * Migrate changes for REQ SR690 - Sending Log Data to Memory Buffers.
 * This feature gets lost when separating RPSW from FastLane
 * 
 * *****************  Version 11  *****************
 * User: Wr185001     Date: 6/11/08    Time: 4:11p
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * TAR 372723 :  Fix for datacap roll over.  
 * 
 * *****************  Version 10  *****************
 * User: Dm185016     Date: 10/20/06   Time: 7:55a
 * Updated in $/Fastlane/4.2/Development/Platform/include
 * Added method to obtain the file name in use
 * 
 * *****************  Version 1  *****************
 * User: Dm185016     Date: 10/19/05   Time: 2:22p
 * Created in $/Fastlane5.0/platform/include
 * 
 * *****************  Version 2  *****************
 * User: Tb185000     Date: 3/08/05    Time: 12:53p
 * Updated in $/Fastlane/4.0 Patch B/Platform/Include
 * Do File Backup on its own thread.
 * 
 * *****************  Version 1  *****************
 * User: Dm180015     Date: 25/01/05   Time: 18.01
 * Created in $/Fastlane/4.0 Patch B/PLATFORM/Include
* 
* *****************  Version 7  *****************
* User: Tb185000     Date: 4/27/04    Time: 9:46a
* Updated in $/FastLane/Construction/Development/Platform/include
* 
* *****************  Version 3  *****************
* User: Tb185000     Date: 4/07/04    Time: 1:13p
* Updated in $/FastLane/Construction/Development/Platform/Opos/REL.2_4/In
* Updated to eliminate the DCap thread and various optimizations....
* 
* *****************  Version 2  *****************
* User: Tb185000     Date: 4/06/04    Time: 11:22a
* Updated in $/FastLane/Construction/Development/Platform/Opos/REL.2_4/In
 * 
 * *****************  Version 1  *****************
 * User: De200005     Date: 3/12/03    Time: 9:01a
 * Created in $/FastLane/Construction/Development/Platform/Opos/REL.2_4/In
 * 
 * *****************  Version 1  *****************
 * User: De200005     Date: 3/12/03    Time: 9:00a
 * Created in $/FastLane/Construction/Development/Platform/Opos/Rel.2_4/RE
 * 
 * *****************  Version 2  *****************
 * User: Cg151000     Date: 3/01/02    Time: 3:16p
 * Updated in $/OPOS/Rel.2_2/Include/NCR
 * Added new mask for Windows CE RETAILMSG macro capability.
 * 
 * *****************  Version 16  *****************
 * User: Farmer       Date: 12/21/99   Time: 4:50p
 * Updated in $/OPOS/Rel.1_8/Include/NCR
 * Updated source code with Windows CE modfications
 * 
 * *****************  Version 14  *****************
 * User: Dw160003     Date: 11/04/99   Time: 4:25p
 * Updated in $/OPOS/Rel.1_7/Include/NCR
 * added VSS Header
 * 
 * *****************  Version 1  *****************
 * User: C. Monroe       Date: 3/01/96    Time: 10:01a
 * Created in $/OPOS/Rel.1_4/Include/NCR
 *
 * *****************  Version 2  *****************
 * User: C. Monroe       Date: 5/14/96    Time: 10:01a
 * Add Thread ID option to "time"
 * 
 * *****************  Version 3  *****************
 * User: C. Monroe       Date: 5/20/96    Time: 10:01a
 * Clarify maximum Printf formating size
 * 
 * *****************  Version 4  *****************
 * User: C. Monroe       Date: 5/22/96    Time: 10:01a
 * Add subkey to read registry function
 * 
 * *****************  Version 5  *****************
 * User: C. Monroe       Date: 6/06/96    Time: 10:01a
 * Add more immediate output options
 * 
 * *****************  Version 6  *****************
 * User: C. Monroe       Date: 7/26/96    Time: 10:01a
 * Allow Printf forms to have variable parameters
 * 
 *
 ************************************************************************/


#ifndef _CMDATACAPTURE_H_
#define _CMDATACAPTURE_H_

#include <afxtempl.h>
#include <afxmt.h>
#include <afxdisp.h>
#include "CmSynch.h"        // Synchronization classes

// Check to see which compiler we are using and did the 
// project define the SSPS_RPSW for VS 2008 as required or 
// it should not define it for VStudio 6.0 
#if (_MSC_VER >= 1500 )

#ifndef SSPS_RPSW 
// #define SSPS_RPSW  1
#pragma message("warning - SSPS_RPSW should be defined for VStudio 2008 compiles")
#endif 

#else	// _MSC_VER shows VStudio 6.0 

#ifdef	SSPS_RPSW 
#pragma message("SSPS_RPSW should not be defined for VStudio 6.0 compiles")
#endif 

#endif // _MSC_VER - which version of compiler? 

#ifndef _NOCAPTURE

// Internal use class.
class CmDCQueueItem
{
public:
    CmDCQueueItem( LPCTSTR pszString, LPCVOID pbBytes = NULL, DWORD dwLenBytes = 0 );
    CmDCQueueItem();
    CmDCQueueItem( const CmDCQueueItem& qi );
    CmDCQueueItem& operator=( const CmDCQueueItem& qi );
    ~CmDCQueueItem();
public:
    COleDateTime m_DateTime;
    DWORD m_nMilli;
    DWORD m_nTID;
    CString m_sString;
    LPVOID m_psData;
    DWORD m_dwDataLen;
};

#endif

/////////////////////////////////////////////////////////////////////////////
// Constants

//   for SetCaptureControl... select OFF, or one or more of others.
const DWORD NCRCAP_OFF          = 0x0000;   // DCap off.
//      - The following options capture by enqueuing data, which is processed
//         by a lower-priority thread: Choose for best performance.
const DWORD NCRCAP_DCAP_APP     = 0x01; // Display to data capture application.
const DWORD NCRCAP_DEBUG        = 0x02; // Display to debugger.
const DWORD NCRCAP_FILE         = 0x04; // Write to file.
const DWORD NCRCAP_RETAIL_MSG	= 0x08; // Serial debugger output (Windows CE only).

//      - The following options capture by processing inline.
//         Choose to ensure immediate capture, but may cause some delays to
//         the caller (e.g. capture to file will flush to disk before returning).
const DWORD NCRCAP_DCAP_APP_NOW = 0x10; // Display to data capture application immediately.
const DWORD NCRCAP_DEBUG_NOW    = 0x20; // Display to debugger immediately.
const DWORD NCRCAP_FILE_NOW     = 0x40; // Write to file immediately.
const DWORD NCRCAP_RETAIL_MSG_NOW = 0x80; // Write to serial debugger immediately.

//   for m_dwTimeOptions... select zero or more.
const DWORD NCRCAP_TIME_MS   = 0x0100;  // Display mm:ss.
const DWORD NCRCAP_TIME_HMS  = 0x0300;  // Display hh:mm:ss.
const DWORD NCRCAP_TIME_MDHMS= 0x0700;  // Display mm/dd hh:mm:ss.
const DWORD NCRCAP_TIME_MILLI= 0x0800;  // Display milliseconds since boot.
                                        //   May optionally add # of digits to display.
const DWORD NCRCAP_TIME_TID  = 0x1000;  // Display Thread ID after time.

#ifdef	SSPS_RPSW

const DWORD NCRCAP_TIME_PRE  = 0x2000;  // Display Prefix function after time.

class CmDataCaptureBase
{
public:
   CmDataCaptureBase() { ; }
   virtual ~CmDataCaptureBase() { ; }
   virtual void DoDCPrintf( DWORD dwMask, LPCTSTR fmt, ... ) = 0;
   virtual void DoDCWinErrorPrintf( DWORD dwMask, LPCTSTR fmt, ... ) = 0;
   virtual void DoDCDataPrintf( DWORD dwMask, LPCVOID pbBytes, DWORD dwLenBytes, LPCTSTR fmt, ... ) = 0;
   virtual BOOL Cleanup() = 0;
};


#define _UNUSED(x) x

class CmNullDataCapture : public CmDataCaptureBase
{
protected:
   static CmNullDataCapture *pInstance;
public:
   CmNullDataCapture() {;}
   virtual ~CmNullDataCapture() {; }
   virtual void DoDCPrintf( DWORD dwMask, LPCTSTR fmt, ... ) { _UNUSED(dwMask); _UNUSED(fmt);}
   virtual void DoDCWinErrorPrintf( DWORD dwMask, LPCTSTR fmt, ... ) { _UNUSED(dwMask); _UNUSED(fmt);}
   virtual void DoDCDataPrintf( DWORD dwMask, LPCVOID pbBytes, DWORD dwLenBytes, LPCTSTR fmt, ... ) { _UNUSED(dwMask); _UNUSED(pbBytes); _UNUSED(dwLenBytes); _UNUSED (fmt);}
   virtual BOOL Cleanup() {return(TRUE);}
   static CmNullDataCapture * Instance();
};


class CMPrefixFunction
{
public:
   virtual CString CreatePrefix() = 0;
};


/////////////////////////////////////////////////////////////////////////////
// CmDataCapture class

class CmDataCapture : public CmDataCaptureBase
#endif  // SSPS_RPSW

#ifndef SSPS_RPSW 

class CmDataCapture 

#endif // SSPS_RPSW
{
public:
    CmDataCapture();                            // Constructor.
    ~CmDataCapture();                           // Destructor.

#ifdef	SSPS_RPSW
    void SetUseFileName( LPCTSTR pszFileName ); // Sets Data Capture to use sectioned file instead of registry
#endif 

    BOOL ReadRegistry( LPCTSTR pszBaseKeyName,  // Read initial values from registry.
        LPCTSTR pszKeyName = NULL );            //   Registry subkey to open/create.
                                                //     If NULL, use "DataCap".
                                                //     If "", use the base key.
    BOOL SetPrefix( LPCTSTR pszPrefix );        // Set characters prepended to each line.

#ifdef	SSPS_RPSW
    BOOL SetPrefixFunction( CMPrefixFunction *pf ); // Set function to retrieve data to prepended to each line
#endif 

	BOOL SetTimeOptions( DWORD dwTimeOptions ); // Set time options.
    BOOL SetFile(                               // Set capture file and maximum size.
        LPCSTR pszFSpec, DWORD dwMaxKSize );

    BOOL Initialize(                            // Prepare for capture. Start thread.
        LPCTSTR pszInitString = NULL );
    BOOL Cleanup();                             // End capture. Stop thread.
    void Flush();                               // Flush captured data.
    DWORD SetCaptureMask( DWORD dwCaptureMask );// Set mask of data to capture.
    DWORD GetCaptureMask( void ) const { return m_dwCaptureMask; }
    DWORD SetCaptureControl( DWORD dwControl ); // Set or reset capturing.

    const _TCHAR *GetFileName() const { return (const _TCHAR *)m_sFSpec; }

    void Capture(                               // Capture string if mask is in CaptureMask.
        DWORD dwMask, LPCTSTR pszString,
        LPCVOID pbBytes = NULL, DWORD dwLenBytes = 0 );
    void DoDCPrintf( DWORD dwMask, LPCTSTR fmt, ... );
    void DoDCDataPrintf( DWORD dwMask, LPCVOID pbBytes, DWORD dwLenBytes, LPCTSTR fmt, ... );

	void DoDCWinErrorPrintf( DWORD dwMask, LPCTSTR pszFormat, ... );
	static void DoDCDataPrintfConditional( CmDataCapture *pDCap, DWORD dwMask, 
												LPCVOID pbBytes, DWORD dwLenBytes, 
												LPCTSTR pszFormat, ... );

	static void DoDCWinErrorPrintfConditional( CmDataCapture *pDCap, DWORD dwMask, 
													LPCTSTR pszFormat, ...);
	static void DoDCPrintfConditional( CmDataCapture *pDCap, DWORD dwMask, 
											LPCTSTR pszFormat, ...);

    BOOL IsCaptureActive( DWORD dwMask = 0xFFFFFFFF) const
       { return m_dwControl != 0 && (m_dwCaptureMask & dwMask ) != 0; }
// WTS: Windows CE Updates
#ifdef _WIN32_WCE
    void DoDCUnicodeDataPrintf(DWORD dwMask, LPCVOID pbBytes, DWORD dwLenBytes,
        LPCTSTR fmt, ... );
#endif

#ifndef _NOCAPTURE

protected:
    void DebugOutput( const CString& s );
    void ProcessItem( const CmDCQueueItem& QI, DWORD dwControl );
    CString Format( const CmDCQueueItem& QI ) const;
    void ShMemCopyIn( const CString& s );
    CString ReadRegString( HKEY hKey, LPCTSTR pszValue );
    BOOL ReadRegNumber( HKEY hKey, LPCTSTR pszValue, int& nNumber, unsigned Options = 0 );
    void FlushDCQueue( DWORD dwTimeout = 0 );
	void CheckBackup();

    BOOL m_bInitialized;                        // Is DCap initialized?
    CString m_sPrefix;                          // Characters prepended to each line.
    
#ifdef	SSPS_RPSW
	CMPrefixFunction *m_pcpfPrefixFunction;     // Function pointer to call for additional prepended data
#endif

    DWORD m_dwTimeOptions;                      // Select options for time reporting.
    CString m_sFSpec;                           // DCap file specification.
    DWORD m_dwMaxKSize;                         // Maximum KB for dcap file.
    HANDLE m_hDCapFile;                         // Dcap file handle.
    DWORD m_dwControl;                          // Control parameter.
    DWORD m_dwCaptureMask;                      // Capture mask.

#ifdef	SSPS_RPSW
	CString m_sRegFileName;                     // Filename of "registry" file
    BOOL m_bUsingRegFile;                       // True if using file instead of registry
#endif

    CCriticalSection m_csAccess;                // Locked when performing DCap API.
    CCriticalSection m_csQueue;                // Locked when performing DCap API.
    CList< CmDCQueueItem, CmDCQueueItem& > m_DCQueue;
    CmDCQueueItem m_StartQI;                    // Data capture init q info.
    CmMutex m_SMemMutex;                        // Locked when manipulating shared memory.
    CmEvent m_SMemDataEvent;                    // Signalled when data in shared memory.
    DWORD m_DCapSMemStatus;                     // Status of shared memory.
    HANDLE m_hFileMapping;                      // Handle to file mapping object.
    LPDWORD m_pSMemInfo;                        // Pointer to shared memory info.
    CmMutex m_DOMutex;                          // Locked when outputting to debugger.
private:
   BOOL OpenCaptureFile( void );
   void BackupCaptureFile( void );
   BOOL IsUsingFile( void ) const;
private:
   CWinThread* m_pBackupThread;                // Back up file on another thread
   static UINT BackupThreadEntry( PVOID pvArg );
   UINT BackupThread( void );
#endif

private:
	SECURITY_ATTRIBUTES gAttributes;			// global security attributes
	SECURITY_DESCRIPTOR gSD;					// global security descriptor
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// The "DCPrintf..."     functions format a string and capture it.
// The "DCDataPrintf..." functions also capture some byte data.
//  ** Caution **
//    - The ...Printf functions use a fixed formating buffer size of 1024.
//      Overflowing this size will corrupt the stack!
//    - If a larger buffer size is required, then format the data yourself
//      (perhaps using a CString's Format function) and pass the data to
//      the Capture function.

#define DCPrintf        DoDCPrintf
#define DCDataPrintf    DoDCDataPrintf

#ifdef _WIN32_WCE
#define DCUnicodeDataPrintf     DoDCUnicodeDataPrintf
#endif

// (Earlier versions of these limited the number of parameters.
//  Now they merely map to the internal function.)
#define DCPrintf1       DoDCPrintf
#define DCPrintf2       DoDCPrintf
#define DCPrintf3       DoDCPrintf
#define DCPrintf4       DoDCPrintf
#define DCDataPrintf1   DoDCDataPrintf
#define DCDataPrintf2   DoDCDataPrintf
#define DCDataPrintf3   DoDCDataPrintf
#define DCDataPrintf4   DoDCDataPrintf

#ifdef  _NOCAPTURE

inline CmDataCapture::CmDataCapture() {}
inline CmDataCapture::~CmDataCapture() {}

inline BOOL CmDataCapture::ReadRegistry( LPCTSTR pszBaseKeyName, LPCTSTR pszKeyName )
    {   UNUSED( pszBaseKeyName ); UNUSED( pszKeyName ); return TRUE;   }
inline BOOL CmDataCapture::SetPrefix( LPCTSTR pszPrefix )
    {   UNUSED( pszPrefix ); return TRUE;   }

#ifdef	SSPS_RPSW
inline BOOL CmDataCapture::SetPrefixFunction( CMPrefixFunction *cp )
    {   UNUSED( cp ); return TRUE;   }
#endif 

inline BOOL CmDataCapture::SetTimeOptions( DWORD dwTimeOptions )
    {   UNUSED( dwTimeOptions ); return TRUE;   }
inline BOOL CmDataCapture::SetFile( LPCSTR pszFSpec, DWORD dwMaxKSize )
    {   UNUSED( pszFSpec ); UNUSED( dwMaxKSize ); return TRUE;   }

inline BOOL CmDataCapture::Initialize( LPCTSTR pszInitString )
    {   UNUSED( pszInitString ); return TRUE;   }
inline BOOL CmDataCapture::Cleanup()
    {   return TRUE;   }
inline void CmDataCapture::Flush() {}
inline DWORD CmDataCapture::SetCaptureMask( DWORD dwCaptureMask )
    {   UNUSED( dwCaptureMask ); return 0;   }
inline DWORD CmDataCapture::SetCaptureControl( DWORD dwControl )
    {   UNUSED( dwControl ); return 0;   }

inline void CmDataCapture::Capture(
    DWORD dwMask, LPCTSTR pszString, LPCVOID pbBytes, DWORD dwLenBytes )
    {   UNUSED( dwMask ); UNUSED( pszString ); UNUSED( pbBytes ); UNUSED( dwLenBytes );   }
inline void CmDataCapture::DoDCPrintf( DWORD dwMask, LPCTSTR fmt, ... )
    {   UNUSED( dwMask ); UNUSED( fmt );   }
inline void CmDataCapture::DoDCDataPrintf( DWORD dwMask, LPCVOID pbBytes, DWORD dwLenBytes, LPCTSTR fmt, ... )
    {   UNUSED( dwMask ); UNUSED( pbBytes ); UNUSED( dwLenBytes); UNUSED( fmt );   }

#endif

/////////////////////////////////////////////////////////////////////////////

#endif      // #ifndef _CMDATACAPTURE_H_

//..end..
