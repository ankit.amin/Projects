#pragma once

#include "ILADataProcess.h"
#include "json\json.h"

#define TYPE_JSON_LaneAvailability 13
#define COMPUTERNAMESIZE 100
#define MAXSTRUCTSIZE 7500  //Maximum allowed size of json data structure


class LADataProcess : public ILADataProcess
{
public:
    LADataProcess();
    ~LADataProcess();
    virtual bool SetFormatType(LA_DATAFORMAT eDataFormat);
    virtual LA_DATAFORMAT GetFormatType() override;
    virtual char* CompleteTransaction();
    virtual bool ResetCycle() override;
    virtual Json::Value ParseSSCOStaticData (const char* cData) override;
    virtual Json::Value ParseSSCOInterventionData(const char* cData) override;
    virtual Json::Value ParseSSCOWeightData(const char* cData) override;

    char* CurrentDateTime();
    
    Json::Value LaneAvailabilityData;

private:
    bool FindComputerName();
    bool InitializeMsgData();
    //CmDataCapture *m_pDataCap;

protected:
    LA_DATAFORMAT	m_DataFormat = DF_JSON; //Default format to json.  Only supported format type.
};

