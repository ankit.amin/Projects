// LaneAvailabilityClient.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "LaneAvailabilityClient.h"
#include <iostream>
#include <memory>
#include <time.h>
#include <iostream>
#include <fstream>
//#include <ostream>
//#include <sstream>

using namespace std;
using namespace Json;

LaneAvailabilityClient* g_OPC = nullptr; // pointer to this instance of LaneAvailability Client

Json::Value staticDataFile;
Json::Value interventionDataFile;
Json::Value weightDataFile;
/***********************************************************************************************
Function:	fnGetLaneAvailabilityClient()

Summary:	Public function that allows access to the single instance of the LaneAvailabilityClient.  If the 
            client does not exist, the constructor is called to create it.

Params:		None

Return:		ILaneAvailabilityClient* to the OC instance.
***************************************************************************************************/
LACLIENT_API ILaneAvailabilityClient* fnGetLAClient(void)
{
    if (g_OPC == nullptr)
    {
        g_OPC = new LaneAvailabilityClient();
    }
    return g_OPC;
}
/***********************************************************************************************
Function:	fnReleaseLaneAvailabilityClient()

Summary:	Public function that destroys the single instance of the LaneAvailabilityClient.  

Params:		None

Return:		void
***************************************************************************************************/
LACLIENT_API void fnReleaseLAClient()
{
    if (g_OPC)
    {
        delete g_OPC;
        g_OPC = nullptr;
    }
}
/***********************************************************************************************
Function:	LaneAvailabilityClient()

Summary:	Constructor for the LaneAvailabilityClient class.  The constructor will also create instances of 
            LaneAvailabilityClient support classes.

Params:		None

Return:		
***************************************************************************************************/
LaneAvailabilityClient::LaneAvailabilityClient()
{
    
    m_pLADataProcess = new LADataProcess();  
    m_pTransport = new UDPTransport();
    m_pConfig = new LAConfig();
    
    CLogger::InitializeLogger();
    //InitializeLogging();
    
    Trace(TRACE_INFO, _T("Initialization of LaneAvailabilityClient Logging"));
    
    // Initialize UDP transport socket based on configured data
    if (m_pTransport->Initialize(m_pConfig->GetHostPort(), m_pConfig->GetHostIP(), m_pConfig->GetBufferSize() ) )
    {
        Trace(TRACE_INFO, _T("Successful Connection to Host: "), m_pConfig->GetHostIP());
    }
    else
    {
        Trace(TRACE_INFO, _T("Connection Failure to Host: "), m_pConfig->GetHostIP());
    }

}

/***********************************************************************************************
Function:	~LaneAvailabilityClient()

Summary:	Destructor for the LaneAvailabilityClient class. 

Params:		None

Return:
***************************************************************************************************/
LaneAvailabilityClient::~LaneAvailabilityClient()
{
    
}
/***********************************************************************************************
Function:	LAUpdateStaticMessage(...)

Summary:	Primary public interface for consumer of of LaneAvailability Client library.  This function is 
            responsible for identifying the type of message that is provided and directing the data 
            to the proper parsing function. for now it process only Json data

Params:		
            LPCTSTR msgData - string containing the message data from the consumer.
            
Return:		void
***************************************************************************************************/
void LaneAvailabilityClient::LAUpdateStaticMessage(LPCTSTR msgData)
{
    USES_CONVERSION;
    char* msgData_c = T2A(msgData); //convert any wides strings to multi-byte
    staticDataFile = m_pLADataProcess->ParseSSCOStaticData(msgData_c);
}

void LaneAvailabilityClient::LAUpdateInterventionMessage(LPCTSTR msgData)
{
    USES_CONVERSION;
    char* msgData_c = T2A(msgData); //convert any wides strings to multi-byte
    interventionDataFile = m_pLADataProcess->ParseSSCOInterventionData(msgData_c);
}

void LaneAvailabilityClient::LAUpdateWeightMessage(LPCTSTR msgData)
{
    USES_CONVERSION;
    char* msgData_c = T2A(msgData); //convert any wides strings to multi-byte
    weightDataFile = m_pLADataProcess->ParseSSCOWeightData(msgData_c);
}

void LaneAvailabilityClient::LAUpdateFinishTrx()
{    
    Json::Value newFileInterventions = interventionDataFile["interventions"];

    int interventionSize = newFileInterventions.size(); 

    //if (newFileInterventions.size() != 0)
    if (interventionSize!= 0)
    {
        try
        {
            int lengthInter = staticDataFile["LaneSummary"]["Terminals"][0]["interventions"].size();
            //for (int i = 0; i < newFileInterventions.size(); i++) 
            for (int i = 0; i < interventionSize; i++)
            {
                //staticDataFile["LaneSummary"]["Terminals"][0]["interventions"][lengthInter + i] = newFileInterventions[i];
                staticDataFile["LaneSummary"]["Terminals"][0]["interventions"][i] = newFileInterventions[i];
            }
        }
        catch (Exception e)
        {
            //cout << e;
        }
    }

    //Json::Value newFileMismatch = weightDataFile["weightMismatches"];
    //
    //if (newFileMismatch.size() != 0)
    //{
    //    try
    //    {
    //        int length = staticDataFile["LaneSummary"]["Terminals"][0]["weightMismatches"].size();
    //        for (int i = 0; i < newFileMismatch.size(); i++) 
    //        {
    //            staticDataFile["LaneSummary"]["Terminals"][0]["weightMismatches"][length + i] = newFileMismatch[i];
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        //cout << e;
    //    }
    //}

    Json::StreamWriterBuilder builder;
    std::string output = Json::writeString(builder, staticDataFile);

    //cout << "StaticFile: "<< staticDataFile.toStyledString() << endl;

    char* p_aggregated_msg = new char[output.length() + 1];
    //memcpy(p_aggregated_msg, output.c_str(), output.length() + 1);
    strcpy_s(p_aggregated_msg, output.length() + 1, output.c_str());

    //std::cout << "MY DATA: " << output << endl; 
    char * temp = TrimString(p_aggregated_msg);
    
    //std::cout << p_aggregated_msg;
    //WriteActivityToLog("Lane Sending: ", temp);
    m_pTransport->SendData(temp);
    
    delete[] p_aggregated_msg;
    delete[] temp;

}

// Function: TrimString()
//
// Summary: Helper function which trims away whitespace from the message so that it
// takes up less space in the JSON structure.
//
// Parameter: char* cMessage represents the message string to be trimmed.
//
// Returns: char* trimStr represents the lane string once trimmed.
char * LaneAvailabilityClient::TrimString(char * cMessage)
{
    // Trim string
    int mlen = strlen(cMessage);
    char *trimStr = new char[mlen + 1];
    //auto trimStr = std::make_unique<char[]>(mlen + 1);

    memset(trimStr, 0x00, mlen);
    //memset(trimStr.get(), 0x00, mlen);

    int k = 0;
    for (int i = 0; i < mlen; i++)
    {
        if (cMessage[i] != 0x20 && cMessage[i] != '\n'&& cMessage[i] != '\t')
        {
            trimStr[k] = cMessage[i];
            trimStr[k + 1] = 0x00;
            k++;
        }
    }
    return trimStr;
}

/***********************************************************************************************
Function:	CompleteTransaction()

Summary:	Private function that, on transaction completion, forwards current data for transmit to UDP host.

Params:		None

Return:		void
***************************************************************************************************/
void LaneAvailabilityClient::CompleteTransaction()
{
    /*
    char dstData[60000];
    
    strcpy_s(dstData, m_pLADataProcess->CompleteTransaction());
    
    m_pTransport->SendData(dstData);
        
    //WriteDataStructToFile(m_testData);
    */

    m_pTransport->SendData(m_pLADataProcess->CompleteTransaction());

    //char dstData[srcDataLength+1];
}
/***********************************************************************************************
Function:	SetEODTimer()

Summary:	Creates a timer that will fire at 24 hour intervals beginning with the first occurrence of the 
            configured hour and minute of the End-of-Day reporting

Params:		None

Return:		void
***************************************************************************************************/
void LaneAvailabilityClient::SetEODTimer()
{
    
}

//void LaneAvailabilityClient::WriteDataStructToFile(char * cDataStruct)
//{
//    FILE* dataFile;
//
//    if ((dataFile = fopen("c:\\scot\\logs\\LADataStruct.log", "w")) == NULL)
//    {
//        //WriteActivityToLog("Failed to open Data Struct File", NULL);
//        Trace(TRACE_INFO, _T("Failed to open Data Struct file"));
//
//        return;
//    }
//    int mLen = strlen(cDataStruct);
//    size_t dataSize = strlen(cDataStruct);
//    fputs(cDataStruct, dataFile);
//    fflush(dataFile);
//    fclose(dataFile);
//    /*if (fwrite(cDataStruct, sizeof(char), 10000, dataFile) != dataSize)
//    {
//        WriteActivityToLog("Failed to write Data Struct File", NULL);
//    }
//    else
//    {
//        WriteActivityToLog("Succussfully Updated Data Struct File", NULL);	
//    }*/
//    return;
//}



/***********************************************************************************************
Function:	TimerRoutine()

Summary:	24-hour timer that will initiate the End-of-Day reporting

Params:		None

Return:		LRESULT value always 1. Error checking not implemented.
***************************************************************************************************/
LRESULT WINAPI LaneAvailabilityClient::TimerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
    return 1;
}

bool LaneAvailabilityClient::LAResetFiles()
{
    if (m_pLADataProcess != NULL)
    {
        return m_pLADataProcess->ResetCycle();
    }

    return false;
}