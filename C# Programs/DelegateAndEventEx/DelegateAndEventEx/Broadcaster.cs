﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateAndEventEx
{
    //Event Arguments
    public class BroadcastEventArgs : EventArgs
    {
        string strName;
        public string Mystring 
        { 
            get
            {
                return this.strName;
            }
            set
            {
                this.strName = value;
            }
        }
    }

    public class Broadcaster
    {
        /*
         *  1 - Define a delegate (this could be defined anywhere in the assembly) 
         *  2 - Define an event based on the that delegate
         *  3 - Function which Raises the event ("protected virtual void OnEventRaised with name starting with On...)
         *  **** #2 and #3 are defined and impemented in same Class
         */

        // 1 - Define a delegate (this could be defined anywhere in the assembly) 
        public delegate void BroadcastEventHandler(object o, BroadcastEventArgs arg);

        // 2 - Define an event based on the that delegate
        public event BroadcastEventHandler BroadcastEvent;

        // 3 - Function which Raises the event ("protected virtual void OnEventRaised with name starting with On...)
        protected virtual void OnBroadcastEvent(string s)
        // following is good as well 
        // internal void OnBroadcastEvent(string s)
        {
            if (this.BroadcastEvent != null)
            {
                //Send the Event to every subsriber
                BroadcastEvent(this, new BroadcastEventArgs() { Mystring = s });
            }
        }

        public void doWork()
        {
            // doWork;

            //Fire the events
            OnBroadcastEvent("Broadcast Event");
            OnTheEvent("The Event");
        }

        /**************************************************************************************/

        /*
         * Another way to do EventHandler
         */

        /*
         * Using "EventHandler" delegate provided by .NET framework
         * 
         * Syntax: 
         *          <accessor> event EventHandler<CustomEventArgs> event name
         */
        public event EventHandler<BroadcastEventArgs> theEvent;

        protected virtual void OnTheEvent(string s)
        {
            if (theEvent != null)
            {
                //Send the Event to every subsriber
                theEvent(this, new BroadcastEventArgs()
                {
                    Mystring = s
                });
            }
        }
    }
}
