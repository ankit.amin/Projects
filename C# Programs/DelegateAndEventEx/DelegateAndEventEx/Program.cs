﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateAndEventEx
{
    class Program
    {
        static void Main(string[] args)
        {
            var b = new Broadcaster();
            var s = new Subscriber();

            //Add Subscribers for the Broadcaster's event
            b.BroadcastEvent += b_BroadcastEvent;
            b.BroadcastEvent += s.EventFiredByBroadcaster;
            b.theEvent += s.EventFiredByBroadcaster;

            //Another way of subscribing to Events
            b.BroadcastEvent += new Broadcaster.BroadcastEventHandler(b_BroadcastEvent);
            b.BroadcastEvent += new Broadcaster.BroadcastEventHandler(s.EventFiredByBroadcaster);
            b.theEvent += new EventHandler<BroadcastEventArgs>(s.EventFiredByBroadcaster);

            b.doWork();
            Console.ReadLine();
        }
        
        //Example #1: Following funciton signature must match the BroadcastEventHandler signature
        static void b_BroadcastEvent(object o, BroadcastEventArgs arg)
        {
            Console.WriteLine("Static event");
        }

    }
}

