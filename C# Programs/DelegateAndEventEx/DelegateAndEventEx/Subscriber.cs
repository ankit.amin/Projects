﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateAndEventEx
{
    class Subscriber
    {
        //Example #2: Following funciton signature must match the BroadcastEventHandler signature
        public void EventFiredByBroadcaster (object o, BroadcastEventArgs e)
        {
            Console.WriteLine("Subscriber recieved an Event fired by Broadcaster " + e.Mystring );
            Console.WriteLine("o =  " + o.GetType().ToString());   
        }
    }
}
