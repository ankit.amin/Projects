﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Timers;
using System.Threading;

namespace ConsoleApp4
{
    /*
    class Program
    {
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("timer event fired");
        }

        static void Main(string[] args)
        {
            double MInstowait = 0.0;

            DateTime nowtime = DateTime.Now;
            DateTime scheduledTime = new DateTime(nowtime.Year, nowtime.Month, nowtime.Day, 10, 13, 0, 0);

            Console.WriteLine("_Timenow = {0}", nowtime);
            Console.WriteLine("Schduletime = {0}", scheduledTime);

            try
            {
                int tickTime = (int)(DateTime.Now - scheduledTime).TotalMinutes;
                Console.WriteLine("No of_MIns to wait = {0}", tickTime);

                MInstowait = tickTime;
            }
            catch (Exception e)
            {
                Console.WriteLine("error");
            }

            System.Timers.Timer aTimer = new System.Timers.Timer((double)MInstowait * 60 * 1000);
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
        }*/

    /*
     * 
     * 
     * TimeSpan day = new TimeSpan(24, 00, 00);    // 24 hours in a day.
TimeSpan now = TimeSpan.Parse(DateTime.Now.ToString("HH:mm"));     // The current time in 24 hour format
TimeSpan activationTime = new TimeSpan(4,0,0);    // 4 AM

TimeSpan timeLeftUntilFirstRun = ((day - now) + activationTime);
if(timeLeftUntilFirstRun.TotalHours > 24)
timeLeftUntilFirstRun -= new TimeSpan(24,0,0);


Timer execute = new Timer();
execute.Interval = timeLeftUntilFirstRun.TotalMilliseconds;
execute.Elapsed += ElapsedEventHandler(doStuff);    // Event to do your tasks.
execute.Start();

public void doStuff(object sender, ElapsedEventArgs e)
{ 
    // Do your stuff and recalculate the timer interval and reset the Timer.
}
} */


    /*
    class Program
    {
        static void Main(string[] args)
        {
            //Time when method needs to be called
            var DailyTime = "22:29:00";
            var timeParts = DailyTime.Split(new char[1] { ':' });

            //Console.WriteLine(timeParts.ToString());

            var dateNow = DateTime.Now;
            var date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day,
                        int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2]));

            Console.WriteLine("Date Now: {0}", dateNow.ToString());
            Console.WriteLine("Date: {0}", date.ToString());

            TimeSpan ts;
            if (date > dateNow)
            {
                ts = date - dateNow;
                Console.WriteLine("TimeSpan: {0}", ts.ToString());
            }
            else
            {
                date = date.AddDays(1);
                ts = date - dateNow;
                Console.WriteLine("date to fire event has passed");
                Console.WriteLine("TimeSpan: {0}", ts.ToString());
            }

            Task.Delay(ts).ContinueWith((x) => SomeMethod());

            Console.Read();
        }

        static void SomeMethod()
        {
            Console.WriteLine("Method is called.");
        }
    }*/


    class Program
    {
        static void Main(string[] args)
        {
            /*
            var t = new Timer(TimerCallback);

            // Figure how much time until 4:00
            DateTime now = DateTime.Now;
            DateTime fourOClock = DateTime.Today.AddHours(13.42);

            // If it's already past 4:00, wait until 4:00 tomorrow    
            if (now > fourOClock)
            {
                fourOClock = fourOClock.AddDays(1.0);
            }

            int msUntilFour = (int)((fourOClock - now).TotalMilliseconds);

            // Set the timer to elapse only once, at 4:00.
            t.Change(msUntilFour, Timeout.Infinite);
            */

            /*
            TimeSpan myTimeNow = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            TimeSpan myTimeSpan2 = new TimeSpan(20, 00, 00);
            
            TimeSpan TsObj = new TimeSpan();
            TsObj = myTimeSpan2.Subtract(myTimeNow);
            
            Int32 milsec = Convert.ToInt32(TsObj.TotalMilliseconds);
            
            My_Timer = new System.Threading.Timer(timer_click, null, milsec, 0);


            Console.Read();
            */


            // Create an AutoResetEvent to signal the timeout threshold in the
            // timer callback has been reached.
            var autoEvent = new AutoResetEvent(false);

            var statusChecker = new StatusChecker(10);

            // Create a timer that invokes CheckStatus after one second, 
            // and every 1/4 second thereafter.
            Console.WriteLine("{0:h:mm:ss.fff} Creating timer.\n", DateTime.Now);

            var stateTimer = new Timer(statusChecker.CheckStatus, autoEvent, 1000, 250);

            // When autoEvent signals, change the period to every half second.
            autoEvent.WaitOne();
            stateTimer.Change(0, 500);

            Console.WriteLine("\nChanging period to .5 seconds.\n");

            // When autoEvent signals the second time, dispose of the timer.
            autoEvent.WaitOne();
            stateTimer.Dispose();

            Console.WriteLine("\nDestroying timer.");


            testTimer t = new testTimer();
            t.SetUpTimer(new TimeSpan(13, 56, 00));


            Console.Read();
        }

        static void TimerCallback(object state)
        {
            Console.WriteLine("Method is called.");
        }

    }

    class StatusChecker
    {
        private int invokeCount;
        private int maxCount;

        public StatusChecker(int count)
        {
            invokeCount = 0;
            maxCount = count;
        }

        // This method is called by the timer delegate.
        public void CheckStatus(Object stateInfo)
        {
            AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;

            Console.WriteLine("{0} Checking status {1,2}.", DateTime.Now.ToString("h:mm:ss.fff"), (++invokeCount).ToString());

            if (invokeCount == maxCount)
            {
                // Reset the counter and signal the waiting thread.
                invokeCount = 0;
                autoEvent.Set();
            }
        }
    }
}

/* HOW TO LOG TO WINDOWS EVENT LOGS
 * 
 * using (EventLog eventLog = new EventLog("Application"))
   {
        eventLog.Source = "Application";
        eventLog.WriteEntry("Log message example", EventLogEntryType.Information, 101, 1);
        eventLog.WriteEntry("MyApplication crashed: ", EventLogEntryType.Error);
   }
 * */
