﻿using System;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApp4
{
    public class testTimer
    {
        public System.Threading.Timer timer;

        public void SetUpTimer(TimeSpan alertTime)
        {
            DateTime dtCurrentTime = DateTime.Now;

            TimeSpan timeToGo = alertTime - dtCurrentTime.TimeOfDay;

            if (timeToGo < TimeSpan.Zero)
            {
                Console.WriteLine("Time already passed!");
                return;
            }
            this.timer = new System.Threading.Timer(x =>
           {
               this.SomeMethodToRun();
           }, null, timeToGo, Timeout.InfiniteTimeSpan);
        }

        public void SomeMethodToRun()
        {
            Console.WriteLine("Running the task!!");
        }
    }
    /*

     using System;  
using System.Threading;  
public static class Program  
{  
    static void Main(string[] args)  
    {  
        // get today's date at 10:00 AM  
        DateTime tenAM = DateTime.Today.AddHours(10);  
 
        // if 10:00 AM has passed, get tomorrow at 10:00 AM  
        if (DateTime.Now > tenAM)  
            tenAM = tenAM.AddDays(1);  
 
        // calculate milliseconds until the next 10:00 AM.  
        int timeToFirstExecution = (int)tenAM.Subtract(DateTime.Now).TotalMilliseconds;  
 
        // calculate the number of milliseconds in 24 hours.   
        int timeBetweenCalls =  (int)new TimeSpan(24, 0, 0).TotalMilliseconds;  
 
        // set the method to execute when the timer executes.   
        TimerCallback methodToExecute = ProcessFile;  
 
        // start the timer.  The timer will execute "ProcessFile" when the number of seconds between now and   
        // the next 10:00 AM elapse.  After that, it will execute every 24 hours.   
        System.Threading.Timer timer = new System.Threading.Timer(methodToExecute, null, timeToFirstExecution, timeBetweenCalls );  
 
        // Block the main thread forever.  The timer will continue to execute.   
        Thread.Sleep(Timeout.Infinite);  
    }  
 
    public static void ProcessFile(object obj)  
    {  
        // do your processing here.   
    }  
} 
     */

}
