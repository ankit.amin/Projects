﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Training
{
    //Global delegate aka "function pointer in C#"
    public delegate void FunctionPointer(int x);

    public abstract class AbstractClass
    {
        //Derrived classes must impement this. 
        public abstract void AbstractFunction(); 

        //Derived classes can "override" this otherwise use the following implementation
        public virtual void VirtualFunction()
        {            
            Console.WriteLine("Base Virtual Function!");
        }
    }
}

