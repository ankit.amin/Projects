﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Training
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractClass a = new ConcreteClass();
            
            a.AbstractFunction();
            a.VirtualFunction();

            //Create the ConcreteClass Object
            ConcreteClass c = new ConcreteClass();

            //Create delegate instance
            FunctionPointer f = c.ConcreteFunctionDelegate;

            //Invoke the delegate
            f(3);
        }
    }
}
