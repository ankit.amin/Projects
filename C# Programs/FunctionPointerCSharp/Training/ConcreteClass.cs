﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Training
{
    public class ConcreteClass : AbstractClass 
    {
        
        public override void AbstractFunction()
        {
            Console.WriteLine("Implementation of AbstractFunction in ConcreteClass");
        }

        //Function matching the delegate in AbstractClass.cs
        public void ConcreteFunctionDelegate(int x)
        {
            Console.WriteLine("ConcreteFunctionDelegate");
        }
    }
}
