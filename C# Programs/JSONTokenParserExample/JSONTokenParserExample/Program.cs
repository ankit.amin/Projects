﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace JSONTokenParserExample
{
    class JTokenComparer : IEqualityComparer<JToken>
    {
        public bool Equals(JToken x, JToken y)
        {
            return JToken.DeepEquals(x, y);
        }

        public int GetHashCode(JToken obj)
        {
            return 0;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string jsonStr = "{ \"theTeam\":[{\"x\":\"1\",\"y\":\"2\"},{\"x\":\"2\",\"y\":\"member\"}]}";

            JObject obj = JObject.Parse(jsonStr);

            JArray a = (JArray)obj["theTeam"];
            JObject newObjet = new JObject();
            newObjet["x"] = 5;
            newObjet["y"] = 6;

            a.Add(newObjet);
            //Console.WriteLine(a.ToString());

            //Get the current array in the file
            string j = "{ \"6_lane4\": { \"LaneSummary\": { \"Terminals\": [{ \"datetimeEnd\": \"\", \"datetimeStart\": \"\", \"interventions\": [ { \"EAN\": \"0\", \"assisted\": \"No\", \"interventionEndTime\": \"2019-10-14T09:35:17.9\", \"interventionStartTime\": \"2019-10-14T09:35:14.39\", \"interventionUniqueID\": \"82-2019-10-14T09:35:14.39\", \"lane\": \"82\", \"security\": \"Yes\", \"transactionEndTime\": \"2019-10-14T09:36:20.239\", \"transactionID\": \"NSPQO5CFK3\", \"transactionStartTime\": \"2019-10-14T09:35:02.273\", \"type\": \"UNEXPECTEDINCREASE\" } ], \"lane\": \"lane4\", \"store\": \"6\", \"terminalType\": \"\", \"transactions\": \"\" } ], \"version\": \"0\" }  } }";

            //new array from lane
            string newj = "{ \"6_lane4\": { \"LaneSummary\": {\"Terminals\": [ { \"datetimeEnd\": \"\", \"datetimeStart\": \"\", \"interventions\": [ { \"EAN\": \"0\", \"assisted\": \"No\", \"interventionEndTime\": \"2019-10-14T09:35:17.9\", \"interventionStartTime\": \"2019-10-14T09:35:14.39\", \"interventionUniqueID\": \"82-2019-10-14T09:35:14.39\", \"lane\": \"82\", \"security\": \"Yes\", \"transactionEndTime\": \"2019-10-14T09:36:20.239\", \"transactionID\": \"NSPQO5CFK3\", \"transactionStartTime\": \"2019-10-14T09:35:02.273\", \"type\": \"UNEXPECTEDINCREASE\" }, { \"EAN\": \"1\", \"assisted\": \"No\", \"interventionEndTime\": \"2019-10-14T09:35:17.9\", \"interventionStartTime\": \"2019-10-14T09:35:14.39\", \"interventionUniqueID\": \"82-2019-10-14T09:35:14.39\", \"lane\": \"82\", \"security\": \"Yes\", \"transactionEndTime\": \"2019-10-14T09:36:20.239\", \"transactionID\": \"NSPQO5CFK3\", \"transactionStartTime\": \"2019-10-14T09:35:02.273\", \"type\": \"UNEXPECTEDINCREASE\" }, ],\"lane\": \"lane4\", \"store\": \"6\",\"terminalType\": \"\",\"transactions\": \"\" } ], \"version\": \"0\" } }}";


            JObject jObj = JObject.Parse(j);
            JObject jObj2 = JObject.Parse(newj);

            //get the intervention array 
            JArray ja = (JArray)jObj["6_lane4"]["LaneSummary"]["Terminals"][0]["interventions"];
            JArray jaa = (JArray)jObj2["6_lane4"]["LaneSummary"]["Terminals"][0]["interventions"];

            //string n = "{}";
            //foreach (JObject item in ja)
            //{
            //    n = item.ToString();
            //    //Console.WriteLine(item.ToString());
            //    //a.Add(item);
            //    //a.Add(item);
            //}

            //JObject nn = JObject.Parse(n);
            //nn["EAN"] = "1";

            ////Create new array from existing one
            //JArray y = new JArray(ja);

            ////iterate through 
            //foreach (JObject item in y)
            //{
            //    //is the element duplicate
            //    if (!JToken.DeepEquals(item, nn))
            //    {
            //        //Console.WriteLine(item.ToString());
            //        //Console.WriteLine(nn.ToString());

            //        ja.Add(nn); //add the element to original array
            //    }
            //}
            List<JToken> listFromFile = new List<JToken>();
            List<JToken> listFromLane = new List<JToken>();

            foreach (JToken item in ja)
            {
                listFromFile.Add((JToken)item);
            }

            foreach (JToken item in jaa)
            {
                listFromLane.Add((JToken)item);
            }

            listFromFile.AddRange(listFromLane);

            IEnumerable<JToken> u = listFromFile.Distinct(new JTokenComparer());
            foreach (var item in u)
            {
                //Console.WriteLine(item.ToString());
            }


            //Converting List to JArray
            JArray aa = new JArray();

            foreach (var item in u)
            {
                aa.Add(item);
            }
            Console.WriteLine(aa.ToString());
            //Console.WriteLine(jObj.ToString());
        }
    }
}


