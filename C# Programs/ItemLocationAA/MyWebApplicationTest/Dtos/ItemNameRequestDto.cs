﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebApplicationTest.Dtos
{
    public class ItemNameRequestDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}