﻿using MyWebApplicationTest.Dtos;
using MyWebApplicationTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWebApplicationTest.Controllers
{
    public class MyTestController : ApiController
    {

        // GET api/values
        public IEnumerable<ItemNameResponseDto> Get()
        {
            //return new string[] { "value1", "value2" };

            ItemNames _itemNames = new ItemNames();

            List<ItemNameResponseDto> responseList = new List<ItemNameResponseDto>();

            foreach (var item in _itemNames.People)
            {
                ItemNameResponseDto itemNameResponseDto = new ItemNameResponseDto()
                {
                    Id = item.Id,
                    Name = item.Name
                };
                responseList.Add(itemNameResponseDto);
            }
            return responseList;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "Ankit";
        }

        // POST api/values

        //public void Post(string value)
        public void Post([FromBody] ItemNameRequestDto value)
        {
            ItemNames _itemNames = new ItemNames();

            Person _person = new Person()
            {
                Id = value.Id,
                Name = value.Name
            };

            _itemNames.People.Add(_person);
            _itemNames.SaveChanges();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
