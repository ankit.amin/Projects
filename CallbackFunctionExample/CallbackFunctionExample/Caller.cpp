#include "stdafx.h"
#include "Caller.h"


Caller::Caller(void)
{

}


Caller::~Caller(void)
{
}


//Regsiter the callback function for the Callee. 
//Required information: Callee's Static CallBack function
//Callee's Static callback function should match the signature of "callbackFuntionPtr" from above
void Caller::RegisterCallbackFuntion(CallbackFunctionPtr calleeStaticCBFunction, void* calleeObject)
{
	this->m_cbFuntion = calleeStaticCBFunction;
	this->m_calleeObject = calleeObject;
}

void Caller::Test()
{
	//function pointer calling convention #1
	m_cbFuntion(m_calleeObject, 10);

	//function pointer calling convention #2
	//(*m_cbFuntion)(m_calleeObject, 10);

}