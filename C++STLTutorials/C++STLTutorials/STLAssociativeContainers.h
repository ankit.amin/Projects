#pragma once
class STLAssociativeContainers
{
public: 
	STLAssociativeContainers() {};
	virtual ~STLAssociativeContainers() {};

	void CallSTLSet();
	void CallSTLMultiSet();
	
	void CallSTLMap();
	void CallSTLMultiMap();

};

