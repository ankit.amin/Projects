#pragma once
#include <cstring>
#include <iostream>
#include <string>
#include <stdarg.h>
#include <stdio.h>
#include <vector>
using namespace std;

class MyClass
{
public: 
	//Constructor
	MyClass();

	//Destructor
	virtual ~MyClass() noexcept;

	// Copy constructor 
	MyClass(const MyClass& source);

	// Move Constructor
	MyClass(MyClass&& source) noexcept;

	// Copy Assignment Operator
	MyClass& operator=(const MyClass& source);

	// Move assignment Operator
	MyClass& operator=(MyClass&& source) noexcept;

    void LogData(int count, ...);

    vector<int> RunningSum(vector<int>& nums);
    vector<int> SearchRange(vector<int>& nums, int target);

    int GetNthFib(int n);

    vector<int> Shuffle(vector<int>& nums, int n);

    void VectorOfVectors();

    int LengthOfLongestIncreasingSubsequence(int nums[]);
    int MaxSumSubarray(int a[], int k);
    bool CheckSubarraySum(vector<int>& nums, int k);
    int FirstUniqChar(string s);
    string LongestDupSubstring(string S);
    string IsDuplicate(string S, int len);
	char* data;
};

/*
///*!
// * C++ Rule of Five full example
// * Author: Gary Huang <gh.nctu+code [AT] gmail.com>
//
#ifndef FOO_H
#define FOO_H

#include <iostream>
#include <string>

class Foo
{
public:
    Foo() : m_name(), m_size(0), m_data(nullptr)
    {
        std::cout << this << ": construct()" << std::endl;
    }
    Foo(std::string name, size_t size)
        : m_name(std::move(name)),
        m_size(size),
        m_data(m_size ? new int[m_size] : nullptr)
    {
        std::cout << this << ": construct(name, size)" << std::endl;
    }
    Foo(const Foo& other)
        : m_name(other.m_name),
        m_size(other.m_size),
        m_data(m_size ? new int[m_size] : nullptr)
        // or just
        // : Foo(name, size)
    {
        std::cout << this << ": copy constructor" << std::endl;
        std::copy(other.m_data, other.m_data + other.m_size, m_data);
    }
    Foo(Foo&& other) noexcept
        : m_name(std::move(other.m_name)),
        m_size(other.m_size),
        m_data(other.m_data)
    {
        std::cout << this << ": move constructor" << std::endl;
        other.m_size = 0;
        other.m_data = nullptr;
    }
#if 0
    // naive
    Foo& operator=(const Foo& other)
    {
        std::cout << this << ": copy assignment" << std::endl;
        if (this != &other)
        {
            size_t n_size = other.m_size;
            int* n_data = n_size ? new int[n_size] : nullptr;
            std::copy(other.m_data, other.m_data + other.m_size, n_data);

            delete[] m_data;
            m_name = other.m_name;
            m_size = n_size;
            m_data = n_data;
        }
        return *this;
    }
    Foo& operator=(Foo&& other) noexcept
    {
        std::cout << this << ": move assignment" << std::endl;
        if (this != &other)
        {
            delete[] m_data;
            m_name = std::move(other.m_name);
            m_size = other.m_size;
            m_data = other.m_data;
            other.m_size = 0;
            other.m_data = nullptr;
        }
        return *this;
    }
#else
    // copy-and-swap idiom
    // DEPRECATED due to too slow
    // Foo(Foo &&other)
    //     : m_name(), m_size(0), m_data(nullptr)
    //     // or just
    //     // : Foo()
    // {
    //     std::cout << this << ": move constructor" << std::endl;
    //     swap(*this, other);
    // }
    Foo& operator=(Foo other) noexcept
    {
        std::cout << this << ": move/copy assignment" << std::endl;
        swap(*this, other);
        return *this;
    }
#endif
    ~Foo()
    {
        std::cout << this << ": destruct" << std::endl;
        delete[] m_data;
    }

    std::string name() const { return m_name; }
    size_t size() const { return m_size; }

    friend void swap(Foo& first, Foo& second)
    {
        using std::swap;
        swap(first.m_name, second.m_name);
        swap(first.m_size, second.m_size);
        swap(first.m_data, second.m_data);
    }

private:
    std::string m_name;
    size_t m_size;
    int* m_data;
};

#endif // FOO_H
*/