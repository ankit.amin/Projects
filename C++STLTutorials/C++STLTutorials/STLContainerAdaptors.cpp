#include "STLContainerAdaptors.h"

/*
stacks are implemented as container adaptors, which are classes that use an encapsulated
object of a specific container class as its underlying container, providing a specific
set of member functions to access its elements. Elements are pushed/popped from the
"back" of the specific container, which is known as the top of the stack.

The underlying container may be any of the standard container class templates or some
other specifically designed container class. The container shall support the following operations:
	empty
	size
	back
	push_back
	pop_back

The standard container classes VECTOR, DEQUE and LIST fulfill these requirements.
By default, if no container class is specified for a particular stack class instantiation, the standard container DEQUE is used.
*/
#include <stack>
void STLContainerAdaptors::CallSTLStack()
{
	cout << "--------------STL Container Adaptor: Stack--------------\n";
	stack<int> myS({ 234,76,23,897,35,8,25 });

	myS.push(23908);

	while (!myS.empty())
	{
		cout << myS.top() << " ";
		myS.pop();
	}
	cout << "\n";
}

/*
queues are implemented as containers adaptors, which are classes that use an
encapsulated object of a specific container class as its underlying container,
providing a specific set of member functions to access its elements.

Elements are pushed into the "back" of the specific container and popped from its "front".

The underlying container may be one of the standard container class template or some other
specifically designed container class. This underlying container shall support
at least the following operations:
	empty
	size
	front
	back
	push_back
	pop_front

The standard container classes DEQUE and LIST fulfill these requirements.

By default, if no container class is specified for a particular queue class instantiation, the standard container DEQUE is used.
*/
#include <queue>
void STLContainerAdaptors::CallSTLQueue()
{
	cout << "--------------STL Container Adaptor: Queue--------------\n";
	queue<int> myQ ({12,456,675,234,768,23});
	
	myQ.push(78);

	while (!myQ.empty())
	{
		cout << myQ.front() << " ";
		myQ.pop();
	}

	cout << "\n";
}

/*
Priority queues are a type of container adaptors, specifically designed such that its
first element is always the greatest of the elements it contains, according to some
strict weak ordering criterion.

This context is similar to a heap, where elements can be inserted at any moment,
and only the max heap element can be retrieved (the one at the top in the priority queue).

Priority queues are implemented as container adaptors, which are classes that use
an encapsulated object of a specific container class as its underlying container,
providing a specific set of member functions to access its elements. Elements are
popped from the "back" of the specific container, which is known as the top of the priority queue.

The underlying container may be any of the standard container class templates or
some other specifically designed container class. The container shall be accessible
through random access iterators and support the following operations:
	empty()
	size()
	front()
	push_back()
	pop_back()

The standard container classes vector and deque fulfill these requirements.

By default, if no container class is specified for a particular priority_queue class instantiation, the standard container VECTOR is used.

Support of random access iterators is required to keep a heap structure internally at all times.
This is done automatically by the container adaptor by automatically calling the algorithm functions
	make_heap,
	push_heap
	pop_heap
when needed.


A heap is essentially an instance of a priority queue
A min heap is structured with the root node as the smallest and each child subsequently larger than its parent
A max heap is structured with the root node as the largest and each child subsequently smaller than its parent
A min heap could be used for "Smallest Job First" CPU Scheduling
A max heap could be used for "Priority" CPU Scheduling


*/
void STLContainerAdaptors::CallPriorityQueue()
{
	cout << "--------------STL Container Adaptor: Priority_Queue--------------\n";
	std::priority_queue<int> q;

	//By default this is max_heap
	for (int n : {1, 8, 5, 6, 3, 4, 0, 9, 7, 2})
	{
		q.push(n);
	}

	//Prints decending order
	print_queue(q);

	//This will be min_heap
	std::priority_queue<int, std::vector<int>, std::greater<int> > q2;

	for (int n : {1, 8, 5, 6, 3, 4, 0, 9, 7, 2})
	{
		q2.push(n);
	}

	//Prints in asendening order
	print_queue(q2);

	// Using lambda to compare elements.
	auto cmp = [](int left, int right)
	{
		return (left ^ 1) < (right ^ 1);
	};

	std::priority_queue<int, std::vector<int>, decltype(cmp) > q3(cmp);

	for (int n : {1, 8, 5, 6, 3, 4, 0, 9, 7, 2})
	{
		q3.push(n);
	}

	print_queue(q3);

}





