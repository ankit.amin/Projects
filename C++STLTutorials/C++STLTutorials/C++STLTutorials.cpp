// C++STLTutorials.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "STLSequenceContainers.h"
#include "STLAssociativeContainers.h"
#include "STLUnorderedContainers.h"
#include "STLContainerAdaptors.h"
#include "MyClass.h"

int main()
{

    /*
        Sequence Container
            Vector      #include <vector>   vector<int> myVector;
            Deque       #include <deque>    deque<int> deq;
            List        #include <list>     
            Forward_list #include <forward_list>    forward_list<int> myList;
            array       #include <array>    array<int, ARRAYSIZE> myArray; 


        Associative Container (binary tree)
            set         #include <set>      set<int, greater<int>> mySet;
            multiset    #include <set>      multiset<char> myMultiSet; 
            map         #include <map>      map<int, string> myMap; myMap.insert(make_pair(2, "name"));
            multimap    #include <map>      multimap<int, int> myMap;

        Unordered Containers (hash table)
            Unordered set           #include <unordered_set>    unordered_set<string> mySet 
            Unordered multiset
            Unordered map           #include <unordered_map>    unordered_map<char, string> myMap;
            Unordered multimap

        Other #include files
            iterator
            algorithm
            numeric
            functional
    
    
    
List and Forward_list	
Operation       Time Complexity
Insert Head	    O(1)
Insert Index    O(n)
Insert Tail	    O(1)
Remove Head	    O(1)
Remove Index    O(n)
Remove Tail	    O(1)
Find Index	    O(n)
Find Object	    O(n)
	
Map 	
Operation	        Time Complexity
Insert	            O(log(n))
Access by Key	    O(log(n))
Remove by Key	    O(log(n))
Find/Remove Value	O(log(n))
	
std::unordered_map	
Operation	        Time Complexity
Insert	            O(1)
Access by Key	    O(1)
Remove by Key	    O(1)
Find/Remove Value	--
	
set	
Operation	    Time Complexity
Insert	        O(log(n))
Remove	        O(log(n))
Find	        O(log(n))
	
stack	
Operation       Time Complexity
Push	        O(1)
Pop	            O(1)
Top	            O(1)

    */
    STLSequenceContainers* stlSequenceContainers = new STLSequenceContainers();
    stlSequenceContainers->CallSTLVector();

    stlSequenceContainers->CallSTLDeque();

    stlSequenceContainers->CallSTLList();
    /////////////////////////////////////////////////

#include <iostream>
    using namespace std; 
    cout << "\t\t\tAssociative Containers\n";
    STLAssociativeContainers *stlAssociativeContainers = new STLAssociativeContainers();
    stlAssociativeContainers->CallSTLSet();
    stlAssociativeContainers->CallSTLMultiSet();
    stlAssociativeContainers->CallSTLMap();
    stlAssociativeContainers->CallSTLMultiMap();

    
    /////////////////////////////////////////////////
    cout << "\t\t\tUnordered Containers\n";
    STLUnorderedContainers* stlUnorderedContainers = new STLUnorderedContainers();
    stlUnorderedContainers->CallSTLUnorderedSet();
    stlUnorderedContainers->CallSTLUnorderedMap();
    
    /////////////////////////////////////////////////
    STLContainerAdaptors* stlContainerAdaptors = new STLContainerAdaptors();
    stlContainerAdaptors->CallPriorityQueue();
    stlContainerAdaptors->CallSTLQueue();
    stlContainerAdaptors->CallSTLStack();

    /////////////////////////////////////////////////
    cout << "\t\t\tMyClass example\n";
    MyClass* m = new MyClass();
    //std::unique_ptr<MyClass> m(new MyClass());


    std::cout << "Hello World!\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

/*
#include <iostream>
#include <algorithm>
using namespace std;

// Utility function to print contents of an array
void print(int arr[], int n)
{
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
}

// Utility function to reverse elements of an array
void reverse(int arr[], int n)
{
    for (int low = 0, high = n - 1; low < high; low++, high--) {
        swap(arr[low], arr[high]);
    }
}

int main()
{
    int arr[] = { 1, 2, 3, 4, 5 };
    int n = sizeof(arr) / sizeof(arr[0]);

    reverse(arr, n);
    print(arr, n);

    return 0;
}*/