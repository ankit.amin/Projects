#pragma once
class STLSequenceContainers
{
public:
	STLSequenceContainers();
	virtual ~STLSequenceContainers();

	void CallSTLVector();

	void CallSTLDeque();

	void CallSTLList();

	void CallSTLForwardList();
};

