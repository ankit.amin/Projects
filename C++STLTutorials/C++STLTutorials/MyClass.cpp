#include "MyClass.h"
#include <iostream>

/*
class CPPR5Class
{
public:
	// Default constructor
	CPPR5Class() :
		data (new char[14])
	{
		std::strcpy(data, "Hello, World!");
	}

	// Copy constructor
	CPPR5Class (const CPPR5Class& original) :
		data (new char[std::strlen (original.data) + 1])
	{
		std::strcpy(data, original.data);
	}

	// Move constructor
	// noexcept needed to enable optimizations in containers
	CPPR5Class (CPPR5Class&& original) noexcept :
		data(original.data)
	{
		original.data = nullptr;
	}

	// Destructor
	// explicitly specified destructors should be annotated noexcept as best-practice
	~CPPR5Class() noexcept
	{
		delete[] data;
	}

	// Copy assignment operator
	CPPR5Class& operator= (const CPPR5Class& original)
	{
		CPPR5Class tmp(original);         // re-use copy-constructor
		*this = std::move(tmp); // re-use move-assignment
		return *this;
	}

	// Move assignment operator
	CPPR5Class& operator= (CPPR5Class&& original) noexcept
	{
		if (this == &original)
		{
			// take precautions against `CPPR5Class = std::move(CPPR5Class)`
			return *this;
		}
		delete[] data;
		data = original.data;
		original.data = nullptr;
		return *this;
	}

private:
	friend std::ostream& operator<< (std::ostream& os, const CPPR5Class& cPPR5Class)
	{
		os << cPPR5Class.data;
		return os;
	}

	char* data;
};

int main()
{
	const CPPR5Class cPPR5Class;
	std::cout << cPPR5Class << std::endl;

	return 0;
}
*/

// Default constructor
MyClass::MyClass()
{
	cout << "----------------------------------------\n";
	LogData(1, "MyClass Default Constructor");
	data = new char[20];
	strcpy_s(data, strlen(data)+1 , "Hello, World!");

	vector<int> x;
	x.push_back(5);
	x.push_back(2);
	x.push_back(3);
	x.push_back(4);
	x.push_back(5);

	vector<int> out = RunningSum(x);
	for (size_t i = 0; i < out.size(); i++)
	{
		cout << out.at(i) << "-";
	}
	cout << endl; 

	vector<int> y;
	y.push_back(5);
	y.push_back(6);
	y.push_back(7);
	y.push_back(7);
	y.push_back(8);
	SearchRange(y, 7);

	cout << "----------------------------------------\n";
	//cout << "Nth Fib: " << GetNthFib(5) << endl;

	VectorOfVectors();

}

// Destructor
// explicitly specified destructors should be annotated noexcept as best-practice
MyClass::~MyClass() noexcept
{
	LogData(1, "~MyClass() Destructor");
	delete[] data;
}

// Copy constructor 
MyClass::MyClass(const MyClass& source)
{
	LogData(1, "MyClass Copy constructor");
	data = new char[std::strlen(source.data) + 1];
	strcpy_s(data, strlen(data)+1, source.data);

}

// Move Constructor
// noexcept needed to enable optimizations in 
MyClass::MyClass(MyClass&& source) noexcept
{
	LogData(1, "MyClass Move Constructor");
	data = source.data;
	source.data = nullptr;
}

// Copy Assignment Operator
MyClass& MyClass::operator=(const MyClass& source)
{
	LogData(1, "MyClass Copy Assignment Operator");
	MyClass tmp(source);    // re-use copy-constructor
	*this = std::move(tmp); // re-use move-assignment
	return *this;
}

// Move assignment Operator
MyClass& MyClass::operator=(MyClass&& source) noexcept
{
	LogData(1, "MyClass Move Assignment Operator");
	if (this == &source)
	{
		// take precautions against `MyClass = std::move(MyClass)`
		return *this;
	}
	delete[] data;
	data = source.data;
	source.data = nullptr;
	return *this;
}

// LogData
void MyClass::LogData(int count, ...)
{
	//Get the list of variadic arguments
	va_list ap;

	//Temp variable holding output of arguments
	char* arg;

	//Start traversing the va_list 
	va_start(ap, count);

	//Print of the va_arg
	for (int i = 0; i < count; i++)
	{
		arg = va_arg(ap, char*);
		cout << arg << endl;
	}

	// End va_end list
	va_end(ap);
}

#include <algorithm>
using namespace std;

//
vector<int> MyClass::RunningSum(vector<int>& nums)
{
	if (nums.size() == 1)
	{
		return nums;
	}
	if (nums.size() == 0 || nums.size() > 1000)
	{
		vector<int> x;
		return x;
	}
	int sum = 0;
	int x = 0;
	for (int i : nums)
	{
		sum = sum + i;
		//cout << sum << endl;
		nums.at(x) = sum;
		//cout << nums.at(x) << " ";
		x++;
	}
	cout << "\n";
	return nums;
}

/*
* Find the first and last index of repeated #
*/
vector<int> MyClass::SearchRange(vector<int>& nums, int target)
{
	vector<int> output;
	output.push_back(-1);
	output.push_back(-1);

	auto lPOS = find(nums.begin(), nums.end(), target);
	int leftIndex = -1;
	int rightIndex = -1;

	if (lPOS != nums.end())
	{
		leftIndex = lPOS - nums.begin();
		cout << "Left Index: " << leftIndex << endl;
	}
	auto rPOS = find(nums.rbegin(), nums.rend(), target);

	if (rPOS != nums.rend())
	{
		rightIndex = nums.size() - (rPOS - nums.rbegin()) - 1;
		cout << "Right Index: " << rightIndex << endl;
	}
	return output;
}

/*
* Input:  0, 1, 1, 2, 3, 5, 8, 13, 
* Output: 1, 2, 3, 4, 5, 6, 7, 8, 
*/
int MyClass::GetNthFib(int n)
{
	cout << "Input n: --> " << n << "\n";
	if (n == 1)
	{
		return 0;
	}
	if (n == 2)
	{
		return 1;
	}
	cout << " n: " << n << " __ ";
	int x = GetNthFib(n - 1);

	cout << "\nx: " << x <<"\n n: " << n << "--";
	int y = GetNthFib(n - 2);
	cout << "\ny: " << y;

	int sum = x + y;
	cout << "\n Sum: " << sum << endl;
	return sum;

}

vector<int> MyClass::Shuffle(vector<int>& nums, int n)
{
	vector<int> x(nums.size());

	for (size_t i = 0; i < nums.size(); i++)
	{
		if (i % 2 == 0)
		{
			x.at(i) = nums.at(i / 2);
		}
		else
		{
			x.at(i) = nums.at(nums.size() / 2 + i / 2);
		}
	}
	return x; 
}

void MyClass::VectorOfVectors()
{
	vector< vector<int> > vv; 

	int row = 3;
	int col = 4; 

	//call resize vv to row;
	vv.resize(row);
	for (size_t i = 0; i < row; i++)
	{
		//call to resize the internal array to col size
		vv.at(i).resize(col);
	}

	//Another way to initialize 2D array
	vector< vector<int> > mat; 
	mat.resize(row, vector<int>(col));

	//Print the 2D arrays
	for (size_t i = 0; i < row; i++)
	{
		for (size_t j = 0; j < col; j++)
		{
			//cout << vv.at(i).at(j) << " ";
			//cout << mat.at(i).at(j) << "-";
		}
		cout << endl;
	}

}


int MyClass::LengthOfLongestIncreasingSubsequence(int nums[])
{

}


/*
* Sliding Windows Technique
*	Use when things we iterate over sequentially 
*		Contiguous sequence of elements 
*		String, Arrays, Linked list, 
*	Use when asked for 
		Min, Max, Longest, Shortest, Contained 
			We need to calculate like Max sum or Min sum 

	Question Variants
		Fixed Length 
			Max Sum subarray of size K 
		Dynamic Variant 
			Smallest sum >= some value S
		Dynamic Variant w/ Auxillary data structure 
			Longest substring w/ no more than k distinct characters
			String Permutations 

	Commonalities 
		Keywords to look for: Substring, SubArray, Longest / Smallest, Contains, 

* 
*/

int MyClass::MaxSumSubarray(int a[], int windowSize)
{
	int maxValue = -10000;
	int currentRunningSum = 0;
	
	int arraySize = sizeof(a) / sizeof(a[0]);

	for (size_t i = 0; i < arraySize; i++)
	{
		currentRunningSum += a[i];
		if (i >= windowSize - 1) //k=window size
		{
			maxValue = std::max(maxValue, currentRunningSum); //get the max of running Sum and max Value

			//subtract left most element from currentRunningSum
			currentRunningSum -= a[i - (windowSize - 1)];
		}
	}
	return maxValue;

}
/*
Given a list of non-negative numbers and a target integer k, write a function to check if the array has a continuous subarray of size at least 2 that sums up to a multiple of k, that is, sums up to n*k where n is also an integer.



Example 1:

Input: [23, 2, 4, 6, 7],  k=6
Output: True
Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
Example 2:

Input: [23, 2, 6, 4, 7],  k=6
Output: True
Explanation: Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.
*/

#include<unordered_map>
bool MyClass::CheckSubarraySum(vector<int>& nums, int k) 
{
	k = abs(k);

	int n = nums.size();

	if (!k) 
	{
		for (int i = 0; i < n; ++i) 
		{
			if (nums[i] == 0)
			{
				if (i < n - 1 && nums[i + 1] == 0)
				{
					return 1;
				}
			}
		}
		return 0;
	}

	long long pre = 0;

	unordered_map<long long, long long> m;

	m[0] = -1;

	for (int i = 0; i < n; ++i) 
	{
		nums[i] = (nums[i] + k) % k;
	
		pre += nums[i];
		
		pre = (pre + k) % k;
		
		if (m.find(pre) == m.end())
		{
			m[pre] = i;
		}
		else if (m[pre] + 1 < i)
		{
			return 1;
		}
	}

	return 0;

}

/*
Given a string, find the first non-repeating character in it and return its index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode"
return 2.
*/

int MyClass::FirstUniqChar(string s) 
{
	int charCounter[26] = { 0 }; // There are only 26 chars in ABCD...

	for (int i = 0; i < s.length(); i++)
	{
		charCounter[s[i] - 'a']++;
	}

	for ( int  i = 0; i < 26; i++)
	{
		cout << charCounter[i] << "-";
	}
	cout << endl;

	for (int i = 0; i < s.length(); i++)
	{
		if (charCounter[ s[i] - 'a' ] == 1)
		{
			return i;
		}
	}
	return -1;
}
/*******************************************************************************************/
/*1044. Longest Duplicate Substring
Hard

647

227

Add to List

Share
Given a string S, consider all duplicated substrings: (contiguous) substrings of S that occur 2 or more times.  (The occurrences may overlap.)

Return any duplicated substring that has the longest possible length.  (If S does not have a duplicated substring, the answer is "".)

 

Example 1:

Input: "banana"
Output: "ana"
Example 2:

Input: "abcd"
Output: ""
 */
string MyClass::LongestDupSubstring(string S) 
{
	int left = 1, right = S.size();
	string res = "";
	while (left < right) 
	{
		int mid = left + (right - left) / 2;
		
		string dup = isDuplicate(S, mid);
		if (dup != "")
		{
			res = dup;
			left = mid;
		}
		else
		{
			right = mid - 1;
		}

	}
	return res;
}
#include <unordered_set>
string MyClass::IsDuplicate(string S, int len)
{
	unordered_set<string> set;
	int bound = S.size() - len + 1;
	int i = 0;
	while (i < bound)
	{
		if (set.count(S.substr(i, len + i)))
		{
			return S.substr(i, len + i - i);
		}
		else
		{
			set.insert(S.substr(i, len + i - i));
		}
		i++;
	}
	return "";

}
/*******************************************************************************************/


class Solution {
public:
	bool hasCycle(ListNode* head)
	{
		ListNode* slow = head;
		ListNode* fast = head;
		while (slow != NULL && fast != NULL)
		{
			slow = slow->next;
			if (fast->next != NULL)
				fast = fast->next->next;
			else
				return false;
			if (slow == fast)
				return true;
		}
		return false;
	}
};

//Expand From Centre : O(n^2)
string longestPalindrome(string s) {

	int n = s.size();
	if (n <= 1)
		return s;

	int best_len = 0;
	string best_s = "";

	//For Odd length Palindromes
	for (int mid = 0; mid < n; mid++)
	{
		for (int x = 0; mid + x < n and mid - x >= 0; x++)
		{
			if (s[mid - x] != s[mid + x])
				break;

			int len = 2 * x + 1;

			if (len > best_len)
			{
				best_len = len;
				best_s = s.substr(mid - x, len);
			}
		}

	}
	//For Even length Palindromes
	for (int mid = 0; mid < n - 1; mid++)
	{
		for (int x = 1; mid + x < n and mid - x + 1 >= 0; x++)
		{
			if (s[mid - x + 1] != s[mid + x])
				break;
			int len = 2 * x;

			if (len > best_len)
			{
				best_len = len;
				best_s = s.substr(mid - x + 1, len);
			}
		}

	}
	return best_s;
}
