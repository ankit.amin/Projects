#pragma once
#include <unordered_map>
#include <xstring>

class STLUnorderedContainers
{
public:
	STLUnorderedContainers() {}

	virtual ~STLUnorderedContainers() {}

	void CallSTLUnorderedSet();
	void CallSTLUnorderedMultiSet();

	void CallSTLUnorderedMap();
	void HelperFunction(const std::unordered_map<char, std::string> &m );

	void CallSTLUnorderedMultiMap();


};

