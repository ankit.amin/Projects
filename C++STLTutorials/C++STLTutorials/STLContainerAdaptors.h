#pragma once
#include <iostream>
#include <queue>

using namespace std; 

class STLContainerAdaptors
{
public:
	STLContainerAdaptors() {}
	virtual ~STLContainerAdaptors() {}

	void CallSTLStack();
	void CallSTLQueue();
	void CallPriorityQueue();
	
	template<typename T>
	void print_queue(T& q)
	{
		while (!q.empty()) 
		{
			std::cout << q.top() << " ";
			q.pop();
		}
		std::cout << '\n';
	}

};

