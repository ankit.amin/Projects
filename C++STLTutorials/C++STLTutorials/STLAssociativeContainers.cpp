#include "STLAssociativeContainers.h"
#include <iostream>


#include <set>
using namespace std;
/*
	Always SORTED and default criteria is < (less than)
		Because it is sorted, no push_back() or push_front() functions
	NO DUPLICATES 
	Values of the elements CANNOT be modified once the data is inserted into SET. They are READ ONLY
	
	Insert, Search takes: O(log n)
	Traversing is slow compared to Vector and Deque
	No Random access, no [] operator
*/
void STLAssociativeContainers::CallSTLSet()
{
	cout << "--------------STL Set--------------\n";

	// Empty set of int, by default it is sorted in asendening (< less than) order
	set<int> mySetLessThanOrder;
	set<int, greater<int>> mySetGreaterThanOrder;

	mySetLessThanOrder.insert({100, 99, 98, 97}); //stores the data in 97,98,99,100 order
	
	cout << "Printing mySetLessThanOder:\t";
	for (auto i : mySetLessThanOrder)
	{
		cout << i << "  ";
	}
	cout << "\n";

	mySetGreaterThanOrder.insert({ 1,2,3,4,5 }); //stores the data in 5,4,3,2,1 order
	cout << "Printing mySetGreaterThanOder:\t";
	for (auto i : mySetGreaterThanOrder)
	{
		cout << i << "  ";
	}
	cout << "\n";


	set<int>::iterator it; 
	it = mySetGreaterThanOrder.find(3);
	cout << *it << "\n"; //values are READ-ONLY. 

	pair<set<int>::iterator, bool> ret; 
	ret = mySetGreaterThanOrder.insert(6);
	if (ret.second == false)
	{
		it = ret.first;
	}

	for (auto i : mySetGreaterThanOrder)
	{
		cout << i << "  ";
	}
	cout << "\n";
}


/*
	Always SORTED and default criteria is < (less than)
		Because it is sorted, no push_back() or push_front() functions
	
	ALLOWS DUPLICATES values
	
	Values of the elements CANNOT be modified once the data is inserted into Multiset. They are READ ONLY

	Insert, Search takes: O(log n)
	Traversing is slow compared to Vector and Deque
	No Random access, no [] operator
*/
void STLAssociativeContainers::CallSTLMultiSet()
{
	cout << "--------------STL Multiset--------------\n";
	multiset<char> myMultiSet;

	myMultiSet.insert({'z','a','e', 'y','z', 'a', 'b'});

	for (auto i : myMultiSet)
	{
		cout << i << "  ";
	}
	cout << "\n";
}

/*
	Always SORTED and default criteria is < (less than)
		Because it is sorted, no push_back() or push_front() functions
	NO DUPLICATES
	Values of the elements CANNOT be modified once the data is inserted into SET. They are READ ONLY

	Insert, Search takes: O(log n)
	Traversing is slow compared to Vector and Deque
	No Random access, no [] operator
*/
#include <map>
void STLAssociativeContainers::CallSTLMap()
{
	cout << "--------------STL Map--------------\n";
	map<int, string> myMap;

	myMap.insert(pair<int, string>(1, "Ankit"));
	myMap.insert(make_pair(2, "Amin"));


	map<int, string>::iterator it = myMap.begin();
	myMap.insert(it, make_pair(3, "Raju"));

	cout << "Printing myMap:\n";
	//for (map<int, string>::iterator i = myMap.begin(); i !=myMap.end(); i++) //Valid FOR Loop
	for (auto i : myMap)
	{
		//cout << (*i).first << " => " << (*i).second << endl; //Valid when using FOR LOOP above
		cout << (i).first << " => " << (i).second << endl; //Valid when using auto in FOR loop
	}

}

/*
	Always SORTED and default criteria is < (less than)
		Because it is sorted, no push_back() or push_front() functions
	ALLOWS DUPLICATES
	Values of the elements CANNOT be modified once the data is inserted into SET. They are READ ONLY

	Insert, Search takes: O(log n)
	Traversing is slow compared to Vector and Deque
	No Random access, no [] operator
*/
void STLAssociativeContainers::CallSTLMultiMap()
{
	multimap<int, int> myMap; 
	myMap.insert(make_pair(1, 2));
	myMap.insert(make_pair(1, 3));
	for (auto i : myMap)
	{
		cout << i.first << " ==> " << i.second << endl;
	}
	cout << "\n";
}
