#include "STLUnorderedContainers.h"
#include <iostream>
using namespace std; 

/*
	Has Buckets 
	Each Bucket has Entries (Entries are linked list)

	Search is O(1) if and only iff Hash function is good function

	Fastest Search/Insert at any place: O(1)
	Associative Containers (set/multiset, map/multimap) takes: O(log n)
	Sequence Containers: Vector, Deque take: O(n)
	List takes O(1) to Insert, O(n) to Search

	Unordered Set/Multiset: Element VALUE CANNOT BE CHANGED 
	Unordered Map/Multimap: Element KEY CANNOT BE CHANGED
*/
#include <unordered_set>
#include <vector>
void STLUnorderedContainers::CallSTLUnorderedSet()
{
	cout << "**********************STL Unordered Set**********************\n";

	//Create Underorded Set
	unordered_set<string> mySet = { "Ankit", "Amin", "Raju", "Arjun", "Aarav" };

	//find the element 
	unordered_set<string>::const_iterator citr = mySet.find("Amin"); // O(1)

	if (citr != mySet.end()) //must check if the Iterator does not point to End of set. If Iterator == end of set then Item is not found
	{
		cout << *citr << "\n";
	}
	else
	{
		cout << "Element not found\n";
	}

	mySet.insert("Tina");

	vector<string> myVector = { "Madhu", "Jayshri" };

	//Add the vector into Unordered Set
	mySet.insert(myVector.begin(), myVector.end());

	cout << "Load factor:\t" << mySet.load_factor() << endl;
	string x = "Arjun";
	cout << x << " is in bucket: " << mySet.bucket(x) << endl;
	cout << "Total number of buckets: " << mySet.bucket_count() << endl;
}

void STLUnorderedContainers::CallSTLUnorderedMultiSet()
{}

#include <unordered_map>
void STLUnorderedContainers::CallSTLUnorderedMap()
{
	cout << "**********************STL Unordered Map**********************\n";

	unordered_map<char, string> myMap;

	myMap.insert({ {'A',"Ankit"},{'B', "Amin"},{'C',"Raju"} });

	for (auto i : myMap)
	{
		cout << (i).first << " ==> " << (i).second << endl;
	}

	/***************************************************/
	//IMPORTENT: following myMap['A'] statement searches myMap for the KEY 'A'. 
	//				If KEY IS FOUND Then returns the Value for that KEY
	//				If KEY IS NOT FOUND then it CREATEs a KEY 'A' and then initializes it to default value of String in this case
	cout << myMap['A'] << endl;  //No range check 
	cout << myMap.at('A') << endl; // Has range check 

	myMap['D'] = "Arjun"; // this creates the KEY 'D' and initializes its VALUE to "Arjun"
	
	//IMPORTANT PROPERTY
	myMap['C'] = "Aarav"; // this is VALID...this provide WRITE access to container
	myMap.insert(make_pair('C', "ASDAS")); //ERROR: Insert function CANNOT be used to modify existing element in the container.

	for (auto i : myMap)
	{
		cout << (i).first << " ==> " << (i).second << endl;
	}

	//call HelperFunction 
	HelperFunction(myMap);
}

void STLUnorderedContainers::HelperFunction(const unordered_map<char, string>& m)
{
	cout << "-----> HelperFunction <-------\n";
	//m['A'] = "Sunday"; //Error because this is m is CONST 
	//cout << m['A'] << endl; // Error becuase random access like this provide WRITE access to container
	auto itr = m.find('A');
	if (itr != m.end())
	{
		auto& val = *itr;
		cout << val.first << " --> " << val.second << endl;
		//cout << (*itr)->first << "-->" << *itr->second << endl;
	}

}

void STLUnorderedContainers::CallSTLUnorderedMultiMap()
{}
