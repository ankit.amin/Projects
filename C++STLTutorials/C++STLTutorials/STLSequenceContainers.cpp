#include "STLSequenceContainers.h"
#include <iostream>
#include <array>
#include <vector>

#include <deque>

using namespace std; 
#define ARRAYSIZE 5

STLSequenceContainers::STLSequenceContainers()
{

}

STLSequenceContainers::~STLSequenceContainers()
{

}

/*
    Faster insert and remove at the end :O(1)   
    Slow insert/remove in middle or beginning : O(n)
    Slow search: O(n)
*/

void STLSequenceContainers::CallSTLVector()
{
    cout << "-----------------STL Vector Example----------------\n";
    int x = 16;
    x = 16 >> 2;
    std::cout << x;
    // Fixed size array 
    // Lookup time O(1)
    std::array<int, ARRAYSIZE> myArray;
    myArray = { 1,2,3,4,5 };

    //////////////////////////////////////////////////
    // Dynamic array
    std::vector<int> myVector;
    //myVector = { 1 };
    ////myVector.push_back(10);
    //std::cout << "After adding 2nd element--Size of Vector: " << myVector.size() << " Capacity: " << myVector.capacity() << std::endl;
    //myVector.push_back(20);
    //std::cout << "After adding 3rd element--Size of Vector: " << myVector.size() << " Capacity: " << myVector.capacity() << std::endl;

    std::cout << "Printing Vector: ";
    
    for (int i = 0; i < 32; i++)
    {
        //Adding new element in Vector
        myVector.push_back(i);
        
        //Following is proper way to randomly accessing Vector.
        //Throws RangeError if deferecing out of bounds element
        std::cout << myVector.at(i) << "-->";

        //Following is bad way to randomly access Vector
        //Does NOT throw exception on out of bounds element
        //myVector[2]; 
        
        std::cout << "Size of Vector: " << myVector.size() << " Capacity: " << myVector.capacity() << std::endl;
    }
    std::cout << std::endl;

    //BETTER WAY: of Iterating through Vector 
    for (vector<int>::iterator itr = myVector.begin(); itr != myVector.end(); itr++)
    {
        cout << *itr << " ";
    }
    //myVector.emplace_back(5);
    std::cout << std::endl;

    //C++11 Way to iteratoring through Vector 
    for (auto itr : myVector)
    {
        cout << itr << "--";
    }

    cout << std::endl ;
}


/*
    Faster insert and remove at beginning and the end: O(1)
    Slow insert/remove in middle: O(n)
    Slow search: O(n)
*/
void STLSequenceContainers::CallSTLDeque()
{
    cout << "----------------------STL Deque Example----------------------\n";
    std::deque<int> deq;

    deq.push_back(5);
    deq.push_front(2);

    for (auto itr : deq)
    {
        cout << itr << "--";
    }

    cout << "\n";
}


/*
    Faster insert and remove at any place :O(1)
    Slow search: O(n) (really slow compared to Vector because of memory locality rule in cache
    no randome access, no [] operator
    Splice the list is O(1) order
*/
#include<list>
void STLSequenceContainers::CallSTLList()
{
    cout << "----------------------STL List Example----------------------\n";
    //list<int> myList; 
    list<int> myList = { 5, 3, 9 };
    myList.push_back(6);
    myList.push_front(4);


    auto itr = find(myList.begin(), myList.end(), 3);

    cout << *itr << "\n";

    myList.insert(itr, 8);

    for (auto it : myList)
    {
        cout << it << "--";
    }
    cout << "\n";
    
    list<int> myList2;// = new list<int>();

    myList2.push_back(99);
    myList2.push_back(98);
    myList2.push_back(100);
    //99,98, 100

    //Copy myList2 at the end of myList
    // Splice is O(1)
    myList.splice(myList.end(), myList2, myList2.begin(), myList2.end());
    //mylist = 4,5,8,3,9,6,99,98,100

    for (auto i : myList)
    {
        cout << i << "--";
    }
    cout << "\n";


    
}


/*
    Can only be traversed from BEGINNING ONLY
*/
#include <forward_list>
void STLSequenceContainers::CallSTLForwardList()
{
    forward_list<int> myList;

    myList.push_front(1);
    myList.push_front(2);
    
}