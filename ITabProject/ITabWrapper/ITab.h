
#include "ITABARSCNTRL.h"


class CITab : public I_ITABARSCntrl
{
private:
	TReportEventCB m_callback;
	int m_nCurrentSelectedBay;

	typedef I_ITABARSCntrl* (*PFGetITab)();
	typedef I_ITABARSCntrl* (*PFReleaseITab)();

	HMODULE m_hDll;
	PFGetITab m_pfGetITab;
	PFGetITab m_pfReleaseITab;
	I_ITABARSCntrl* m_iTabObj;
public:
	CITab();
	virtual ~CITab();
	virtual int Initialize(TReportEventCB aReportCB);
	virtual int Initialize(const wchar_t * aCOMPort, const wchar_t * aLogFileFullPath, TReportEventCB aReportCB) override;
	virtual int RequestPackingBay(int aTotalAreas, bool aAvailableAreas[]) override;
	virtual int Recover() override;
	virtual int Stop() override;
	virtual int SelfTest() override;
	virtual void DeInitialize() override;
	//virtual bool getKPI(LPCTSTR szKpiName, void *pBuffer, int sizeOfBuf);
	void InvokeCallback(int eventId, int eventDetail);

};
