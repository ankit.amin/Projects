#include "stdafx.h"
#include "ITab.h"
#include <tchar.h>

LPCWSTR RegKey_ITab = L"SOFTWARE\\NCR\\SCOT\\CurrentVersion\\SCOTAPP\\ITab";
LPCWSTR RegName_ITab_COMPORT = L"COMPort";
LPCWSTR RegName_ITab_LOGFILE = L"LogFile";
LPCWSTR RegName_ITab_DLL = L"Dll";

static CITab * m_mainThis= NULL;

int ReadRegValue(HKEY root, LPCWSTR wKey, LPCWSTR name, LPWSTR presult, DWORD cbData)
{
	HKEY hKey;
	LSTATUS ret = RegOpenKeyEx(root, wKey, 0, KEY_READ | KEY_WOW64_64KEY, &hKey);
	if (ret == ERROR_SUCCESS)
	{
		DWORD type = REG_SZ;
		ret = RegQueryValueEx(hKey, name, 0, 0, (LPBYTE)presult, &cbData);
		RegCloseKey(hKey);
	}
	return ret;
}

static void __stdcall eventCallback(int eventId, int eventDetail)
{
	if (m_mainThis != NULL)
		m_mainThis->InvokeCallback(eventId, eventDetail);
}

void CITab::InvokeCallback(int eventId, int eventDetail)
{
	if (m_callback)
		m_callback(eventId, eventDetail);
}

CITab::CITab()
:m_callback(NULL),
m_nCurrentSelectedBay(-1)
{
	WCHAR dll[512] = L"";
	DWORD cbData = 512 * sizeof(WCHAR);

	ReadRegValue(HKEY_LOCAL_MACHINE, RegKey_ITab, RegName_ITab_DLL, dll, cbData);

	m_hDll= LoadLibrary(dll);
	if (!m_hDll)
	{
		return ;
	}

	m_pfGetITab = reinterpret_cast<PFGetITab>(GetProcAddress(m_hDll, "Get_ITABARSCntrl"));
	if (!m_pfGetITab)
	{
		return ;
	}

	m_pfReleaseITab = reinterpret_cast<PFReleaseITab>(GetProcAddress(m_hDll, "Release_ITABARSCntrl"));
	if (!m_pfReleaseITab)
	{
		return ;
	}

	m_iTabObj = m_pfGetITab();
	m_mainThis = this;
}

CITab::~CITab()
{
	if (m_pfReleaseITab)
		m_pfReleaseITab();
	if (m_hDll)
		FreeLibrary(m_hDll);
}

int CITab::Initialize(const wchar_t * aCOMPort, const wchar_t * aLogFileFullPath, TReportEventCB aReportCB)
{
	return m_iTabObj->Initialize(aCOMPort, aLogFileFullPath, aReportCB);
}

int CITab::Initialize(TReportEventCB callback)
{
	m_callback = callback;
	WCHAR comPort[512];
	DWORD cbData = 512 * sizeof(WCHAR);
	int ret= ReadRegValue(HKEY_LOCAL_MACHINE, RegKey_ITab, RegName_ITab_COMPORT, comPort, cbData);

	WCHAR logfile[512];
	if (ret == 0) {
		cbData = 512 * sizeof(WCHAR);
		ReadRegValue(HKEY_LOCAL_MACHINE, RegKey_ITab, RegName_ITab_LOGFILE, logfile, cbData);
	}
	if (ret == 0)
		return m_iTabObj->Initialize(comPort, logfile, callback);
	else
		return ret;
}

int CITab::RequestPackingBay(int totalAreas, bool availableAreas[])
{
	return m_iTabObj->RequestPackingBay(totalAreas, availableAreas);

}

int CITab::Recover()
{
	return m_iTabObj->Recover();
}

int CITab::Stop()
{
	return m_iTabObj->Stop();
}

void CITab::DeInitialize()
{
	m_iTabObj->DeInitialize();
}

int CITab::SelfTest()
{
	return m_iTabObj->SelfTest();
}

CITab *gp_ITab = NULL;

extern "C" __declspec(dllexport) I_ITABARSCntrl*	 Get_ITABARSCntrl()
{
	if (gp_ITab == NULL)
		gp_ITab = new CITab();
	return gp_ITab;
}

extern "C" __declspec(dllexport) void		 Release_ITABARSCntrl()
{
	delete gp_ITab;
	gp_ITab = NULL;
}

