/*
ITAB
Alternative Rapid Scan - Controller box API DLL interface
Author: Lukas.Seget@Consilia-Brno.cz
Date: 2015-08-02
Version: 00.03
Version History:
00.03, 2015-08-02
 - std::string and LPCSTR changed to const wchar_t*  
 - removed #include <string> 
00.02, 2015-07-30
 - Bugfix - missing asterisk in the callback typedef
 - Initialize return value changed to int
 - eventType renamed to aEventID
 - eventID renamed to aEventDetail
 - Added first part of aEventID definitions
00.01, 2015-07-29 - Initial version
*/

#pragma once
#ifndef ITABARSCNTRL_DLL
#define ITABARSCNTRL_DLL


/* EventID - Events/Information */
#define ITABARSCNTRL__EVENT_TRANSACTION_STARTED         1000  // Detail = bay
#define ITABARSCNTRL__EVENT_TRANSACTION_FINISHED        1001  // Detail = bay
#define ITABARSCNTRL__EVENT_BAY_AVAILABLE               1010  // Detail = bay
#define ITABARSCNTRL__EVENT_BAY_NOT_AVAILABLE           1011  // Detail = bay
#define ITABARSCNTRL__EVENT_SELFTEST_STARTED            1020  // Detail = 0
#define ITABARSCNTRL__EVENT_SELFTEST_FINISHED           1021  // Detail = result
#define ITABARSCNTRL__EVENT_BAY_BACKUP_DETECTED         1030  // Detail = bay
#define ITABARSCNTRL__EVENT_BAY_BACKUP_SOLVED           1031  // Detail = bay
#define ITABARSCNTRL__EVENT_BAY_SAFETY_COVER_UP         1040  // Detail = bay
#define ITABARSCNTRL__EVENT_BAY_SAFETY_COVER_OK         1041  // Detail = bay
#define ITABARSCNTRL__EVENT_RECOVERY_FINISHED           1050  // Detail = result
/* EventID - Warnings */
#define ITABARSCNTRL__WARNING_BAY_MAIN_MOTOR_OVERTEMP    -30   // Detail = bay
#define ITABARSCNTRL__WARNING_BAY_BAY_MOTOR_OVERTEMP     -40   // Detail = bay
#define ITABARSCNTRL__WARNING_BAY_ARM_BLOCKED            -50   // Detail = bay
/* EventID - Errors */
#define ITABARSCNTRL__ERROR_HW_ERROR                      -1   // Detail = info
#define ITABARSCNTRL__ERROR_COM_PORT_NOT_AVAILABLE        -2   // Detail = 0
#define ITABARSCNTRL__ERROR_CONTROL_BOX_OFFLINE           -3   // Detail = 0
#define ITABARSCNTRL__ERROR_CONTROL_BOX_NOT_RESPONDING    -4   // Detail = 0


typedef void(__stdcall *TReportEventCB)(int aEventID, int aEventDetail);

class I_ITABARSCntrl
{
public:
	virtual int Initialize(const wchar_t * aCOMPort, const wchar_t * aLogFileFullPath, TReportEventCB aReportCB) = 0;
	virtual int RequestPackingBay(int aTotalAreas, bool aAvailableAreas[]) = 0;
	virtual int Recover() = 0;
	virtual int Stop() = 0;
	virtual int SelfTest() = 0;
	virtual void DeInitialize() = 0;
	/*virtual bool getKPI(const wchar_t* szKpiName, void *pBuffer, int sizeOfBuf) = 0;*/
};

#if defined(ITABARSCNTRL_DLL_COMPILATION)
    extern "C" __declspec(dllexport) I_ITABARSCntrl* Get_ITABARSCntrl();  //DLL
    extern "C" __declspec(dllexport) void Release_ITABARSCntrl();         //DLL
#else
    typedef I_ITABARSCntrl* (*TGet_ITABARSCntrl)();                       //Application
#endif
#endif //ITABARSCNTRL_DLL
