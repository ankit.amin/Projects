
#include "ITABARSCNTRL.h"
#include "DlgMain.h"

#define STOP_DELAY 5000

class CITab : public I_ITABARSCntrl
{
private:
	CDlgMain *m_dlg;

	int m_nCurrentSelectedBay;
public:
	CITab();
	virtual ~CITab();
	virtual int Initialize(const wchar_t *aCOMPort, const wchar_t *aLogFileFullPath, TReportEventCB aReportCB) override;
	virtual int RequestPackingBay(int aTotalAreas, bool aAvailableAreas[]) override;
	virtual int Recover() override;
	virtual int Stop() override;
	virtual int SelfTest() override;
	virtual void DeInitialize() override;
	//virtual bool getKPI(LPCTSTR szKpiName, void *pBuffer, int sizeOfBuf);
};
