#include "stdafx.h"
#include "ITab.h"
#include <vector>

CDlgMain *main_dlg=NULL;

CITab::CITab()
:m_nCurrentSelectedBay(-1),
m_dlg(NULL)
{

}
CITab::~CITab()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (m_dlg) {
		m_dlg->DestroyWindow();
		delete m_dlg;
	}
}

int CITab::Initialize(const wchar_t *wCom, const wchar_t *wLogstr, TReportEventCB callback)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	m_dlg = main_dlg;
	if (m_dlg != NULL)
	{
		m_dlg->SetCallback(callback);
		m_dlg->UpdateData(TRUE);
		CString str;
		str.Format(_T("%ld: Initialize(%s, %s, ...)"), GetTickCount(), wCom, wLogstr);
		m_dlg->m_LBHistory.AddString(str);

		m_dlg->m_csComPort = wCom;
		m_dlg->m_csLogFile = wLogstr;
		m_dlg->UpdateData(FALSE);
	}
	m_dlg->UpdateData(TRUE);
	for (int i = 0; (i < 3); i++) {
		if (m_dlg->m_bBusy[i])
			m_dlg->SendEvent(ITABARSCNTRL__EVENT_BAY_NOT_AVAILABLE, i + 1, 0);
		else
			m_dlg->SendEvent(ITABARSCNTRL__EVENT_BAY_AVAILABLE, i + 1, 0);
	}
	return 0;
}

int CITab::RequestPackingBay(int totalAreas, bool availableAreas[])
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	bool bSuccess = false;
	CString m_acsStation[] = { _T("Station A"), _T("Station B"), _T("Station C") };

	m_dlg->UpdateData(TRUE);

	int nNewSelection = m_nCurrentSelectedBay;
	for (int i = 0; i < totalAreas; i++) {
		nNewSelection = (nNewSelection + 1) % totalAreas;
		if (availableAreas[nNewSelection] && !m_dlg->m_bBusy[nNewSelection]) {
			m_nCurrentSelectedBay = nNewSelection;
			bSuccess = true;
			break;
		}
	}
	if (bSuccess) {
		m_dlg->m_csStatus = _T("Running");
		m_dlg->m_csSelectedStation = m_acsStation[m_nCurrentSelectedBay];
		m_dlg->UpdateData(FALSE);
	}
	else{
		m_dlg->m_csStatus = _T("");
		m_dlg->m_csSelectedStation = _T("");
		m_dlg->UpdateData(FALSE);
	}

	CString str;
	if (totalAreas > 2) {
		str.Format(_T("%ld: RequestPackingBay(%d, [%d, %d, %d])"), GetTickCount(),
			totalAreas, availableAreas[0], availableAreas[1], availableAreas[2]);
	}
	else 
		str.Format(_T("%ld: RequestPackingBay(%d,...)"), GetTickCount(), totalAreas);
	m_dlg->m_LBHistory.AddString(str);
	
	m_dlg->UpdateData(TRUE);
	Sleep(m_dlg->m_nDelayRequestPackingBay);
	if (bSuccess) {
		m_dlg->SendEvent(ITABARSCNTRL__EVENT_TRANSACTION_STARTED, m_nCurrentSelectedBay+1, 0);
		for (int i = 0; (i < totalAreas); i++) {
			if (i != m_nCurrentSelectedBay)
				m_dlg->SendEvent(ITABARSCNTRL__EVENT_BAY_NOT_AVAILABLE, i+1, 0);
		}
		m_dlg->m_nReturnRequestPackingBay = m_nCurrentSelectedBay + 1;
		m_dlg->UpdateData(FALSE);
		return m_nCurrentSelectedBay+1;
	} 
	else
		return -11;
}

int CITab::Recover()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString str;
	str.Format(_T("%ld: Recover()"), GetTickCount());
	m_dlg->m_LBHistory.AddString(str);

	m_dlg->UpdateData(TRUE);
	Sleep(m_dlg->m_nDelayRecover);
	return m_dlg->m_nReturnRecover;
}

int CITab::Stop()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString str;
	str.Format(_T("%ld: Stop()"), GetTickCount());
	m_dlg->m_LBHistory.AddString(str);

	m_dlg->UpdateData(TRUE);

	Sleep(m_dlg->m_nDelayStop);
	if (m_nCurrentSelectedBay >= 0) {
		m_dlg->SendEvent(ITABARSCNTRL__EVENT_TRANSACTION_FINISHED, m_nCurrentSelectedBay+1, STOP_DELAY);
		m_nCurrentSelectedBay = -1;
	}
	return 0;
}

void CITab::DeInitialize()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString str;
	str.Format(_T("%ld: DeInitialize()"), GetTickCount());
	m_dlg->m_LBHistory.AddString(str);
	return;
}

int CITab::SelfTest()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString str;
	str.Format(_T("%ld: SelfTest()"), GetTickCount());
	m_dlg->m_LBHistory.AddString(str);

	m_dlg->UpdateData(TRUE);
	Sleep(m_dlg->m_nDelaySelfTest);
	return m_dlg->m_nReturnSelfTest;
}

//bool CITab::getKPI(LPCTSTR szKpiName, void *pBuffer, int sizeOfBuf)
//{
//	m_dlg.SetDlgItemTextW(ID_COMMAND, _T("getKPI"));
//	return false;
//}

CITab *gp_ITab = nullptr;

extern "C" __declspec(dllexport) I_ITABARSCntrl*	 Get_ITABARSCntrl()
{
	if (gp_ITab == nullptr)
		gp_ITab = new CITab();

	main_dlg = new CDlgMain();
	main_dlg->Create(IDD_DLGMAIN);
	main_dlg->ShowWindow(SW_SHOWDEFAULT);

	return gp_ITab;
}

extern "C" __declspec(dllexport) void		 Release_ITABARSCntrl()
{
	delete gp_ITab;
	gp_ITab = nullptr;
}