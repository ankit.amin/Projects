#pragma once
#include "resource.h"
#include "afxwin.h"
#include "ITABARSCNTRL.h"
#include "afxcmn.h"
#include <list>

// CDlgMain dialog

class CDlgMain : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgMain)

public:
	CDlgMain(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgMain();

	TReportEventCB m_callback;

	void SetCallback(TReportEventCB callback);

// Dialog Data
	enum { IDD = IDD_DLGMAIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	typedef struct { int id; int detail; } ITabEvent;

	std::list<ITabEvent*> m_list;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBusystationc();
	afx_msg void OnBnClickedBtnSendevent();
	int m_nEventId;
	int m_nEventDetail;
	int m_nDelayRecover;
	int m_nDelayRequestPackingBay;
	int m_nDelaySelfTest;
	int m_nDelayStop;
	int m_nReturnRecover;
	int m_nReturnRequestPackingBay;
	int m_nReturnSelfTest;
	int m_nReturnStop;
	CString m_csComPort;
	CString m_csLogFile;
	CString m_csSelectedStation;
	BOOL m_bBusy[3];
	CListBox m_LBHistory;
	CString m_csStatus;
	void SendEvent(int nEventId, int nEventDetail, int nDelay);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
