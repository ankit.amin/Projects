//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ITabEmulator.rc
//
#define IDC_BusySTATIONA                201
#define ID_COMPORT                      202
#define ID_LOGFILE                      203
#define ID_COMMAND                      204
#define IDC_BUSYSTATIONB                205
#define IDC_BUSYSTATIONC                206
#define ID_SELECTED_STATION             207
#define IDC_EDIT_EVENTTYPE              208
#define IDC_EDIT_EVENTID                209
#define IDC_BTN_SENDEVENT               211
#define IDC_STATIC_STATUS               212
#define IDD_DLGMAIN                     1000
#define IDC_EDITRC_RequestPackingBay    1007
#define IDC_EDITRC_Recover              1008
#define IDC_EDITRC_Stop                 1009
#define IDC_EDITRC_SelfTest             1010
#define IDC_LIST2                       1014
#define IDC_EDITDL_RequestPackingBay    1017
#define IDC_EDITDL_Recover              1018
#define IDC_EDITDL_Stop                 1019
#define IDC_EDITDL_SelfTest             1020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
