// ITabEmulatorMFC.h : main header file for the ITabEmulatorMFC DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CITabEmulatorApp
// See ITabEmulatorMFC.cpp for the implementation of this class
//

class CITabEmulatorApp : public CWinApp
{
public:
	CITabEmulatorApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
