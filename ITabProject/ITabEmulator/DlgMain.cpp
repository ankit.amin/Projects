// DlgMain.cpp : implementation file
//

#include "stdafx.h"
#include "ITabEmulator.h"
#include "DlgMain.h"
#include "afxdialogex.h"
#include "resource.h"

// CDlgMain dialog

IMPLEMENT_DYNAMIC(CDlgMain, CDialogEx)

CDlgMain::CDlgMain(CWnd* pParent /*=NULL*/)
: CDialogEx(CDlgMain::IDD, pParent),
m_callback(nullptr)
, m_nEventId(0)
, m_nEventDetail(0)
, m_nDelayRecover(0)
, m_nDelayRequestPackingBay(0)
, m_nDelaySelfTest(0)
, m_nDelayStop(0)
, m_nReturnRecover(0)
, m_nReturnRequestPackingBay(0)
, m_nReturnSelfTest(0)
, m_nReturnStop(0)
, m_csComPort(_T(""))
, m_csLogFile(_T(""))
, m_csSelectedStation(_T(""))
, m_csStatus(_T(""))
{
	m_bBusy[0] = FALSE;
	m_bBusy[1] = FALSE;
	m_bBusy[2] = FALSE;
}

CDlgMain::~CDlgMain()
{
}

void CDlgMain::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_EVENTID, m_nEventDetail);
	DDX_Text(pDX, IDC_EDIT_EVENTTYPE, m_nEventId);
	DDX_Text(pDX, IDC_EDITDL_Recover, m_nDelayRecover);
	DDX_Text(pDX, IDC_EDITDL_RequestPackingBay, m_nDelayRequestPackingBay);
	DDX_Text(pDX, IDC_EDITDL_SelfTest, m_nDelaySelfTest);
	DDX_Text(pDX, IDC_EDITDL_Stop, m_nDelayStop);
	DDX_Text(pDX, IDC_EDITRC_Recover, m_nReturnRecover);
	DDX_Text(pDX, IDC_EDITRC_RequestPackingBay, m_nReturnRequestPackingBay);
	DDX_Text(pDX, IDC_EDITRC_SelfTest, m_nReturnSelfTest);
	DDX_Text(pDX, IDC_EDITRC_Stop, m_nReturnStop);
	DDX_Text(pDX, ID_COMPORT, m_csComPort);
	DDX_Text(pDX, ID_LOGFILE, m_csLogFile);
	DDX_Text(pDX, ID_SELECTED_STATION, m_csSelectedStation);
	DDX_Check(pDX, IDC_BusySTATIONA, m_bBusy[0]);
	DDX_Check(pDX, IDC_BUSYSTATIONB, m_bBusy[1]);
	DDX_Check(pDX, IDC_BUSYSTATIONC, m_bBusy[2]);
	DDX_Control(pDX, IDC_LIST2, m_LBHistory);
	DDX_Text(pDX, IDC_STATIC_STATUS, m_csStatus);
}


BEGIN_MESSAGE_MAP(CDlgMain, CDialogEx)
	ON_BN_CLICKED(IDOK, &CDlgMain::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_SENDEVENT, &CDlgMain::OnBnClickedBtnSendevent)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDlgMain message handlers


void CDlgMain::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}


void CDlgMain::OnBnClickedBtnSendevent()
{
	UpdateData(TRUE);
	SendEvent(m_nEventId, m_nEventDetail, 0);
}

void CDlgMain::SetCallback(TReportEventCB callback)
{
	m_callback = callback;
}

#define TIMER_ID_ITAB_EVENT  WM_TIMER +1

void CDlgMain::SendEvent(int nEventId, int nEventDetail, int nDelay)
{
	ITabEvent *ev= new ITabEvent();
	ev->id = nEventId;
	ev->detail = nEventDetail;
	m_list.push_back(ev);

	SetTimer(TIMER_ID_ITAB_EVENT, nDelay, NULL);
}

void CDlgMain::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent) {
	case TIMER_ID_ITAB_EVENT:
		for (std::list<ITabEvent*>::iterator it = m_list.begin(); it != m_list.end(); it++)
		{
			ITabEvent *pev = *it;
			if (m_callback != nullptr)
			{
				switch (pev->id) 
				{
				case ITABARSCNTRL__EVENT_TRANSACTION_FINISHED:
				{
					for (int i = 0; (i < 3); i++)
					{
						if (m_bBusy[i])
							SendEvent(ITABARSCNTRL__EVENT_BAY_NOT_AVAILABLE, i+1, 0);
						else
							SendEvent(ITABARSCNTRL__EVENT_BAY_AVAILABLE, i+1, 0);
					}
					m_csStatus = _T("Stopped");
					m_csSelectedStation = _T("");
					UpdateData(FALSE);
					break;
				}
				default:
					break;
				}
				m_callback(pev->id, pev->detail);
			}
			delete pev;
		}
		m_list.clear();
		break;
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}
