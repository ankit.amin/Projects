
#include "I_ITab.h"

class CITab : public I_ITab
{
public:
	virtual bool Initialize(PF_ITAB_CALLBACK callback) override;
	virtual int RequestPackingBay(int totalAreas, int *availableAreas) override;
	virtual bool Reset() override;
	virtual bool Stop(double lastScanTimeStamp) override;
	virtual bool UnInitialize() override;
	virtual bool getKPI(LPCTSTR szKpiName, void *pBuffer, int sizeOfBuf);
};
