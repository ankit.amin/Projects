#pragma once

// windows.h defines CALLBACK as: __stdcall
typedef void(CALLBACK *PF_ITAB_CALLBACK)(int eventType, int eventID);

class I_ITab
{
public:
	virtual bool Initialize(PF_ITAB_CALLBACK callback) = 0;
	virtual int RequestPackingBay(int totalAreas, int *availableAreas) = 0;
	virtual bool Reset() = 0;
	virtual bool Stop(double lastScanTimeStamp) = 0;
	virtual bool UnInitialize() = 0;
	virtual bool getKPI(LPCTSTR szKpiName, void *pBuffer, int sizeOfBuf) = 0;
};

#if defined(ITAB_DLL) // DLL project defines this to control export/import
#	define ITABAPI	__declspec(dllexport)	// used by dll
#else
#	define ITABAPI	__declspec(dllimport)	// used by client
#endif

