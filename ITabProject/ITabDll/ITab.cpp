#include "stdafx.h"
#include "ITab.h"


bool CITab::Initialize(PF_ITAB_CALLBACK callback)
{
	return true;
}

int CITab::RequestPackingBay(int totalAreas, int *availableAreas)
{
	return 0;
}

bool CITab::Reset()
{
	return 0;
}

bool CITab::Stop(double lastScanTimeStamp)
{
	return true;
}

bool CITab::UnInitialize()
{
	return true;
}

bool CITab::getKPI(LPCTSTR szKpiName, void *pBuffer, int sizeOfBuf)
{
	return false;
}

CITab *gp_ITab = nullptr;

extern "C" ITABAPI I_ITab*	 Get_ITab()
{
	if (gp_ITab == nullptr)
		gp_ITab = new CITab();
	return gp_ITab;
}

extern "C" ITABAPI void		 Release_ITab()
{
	delete gp_ITab;
	gp_ITab = nullptr;
}