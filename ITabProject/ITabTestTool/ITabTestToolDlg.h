
// CITabTestToolDlg.h : header file
//

#pragma once
#include "ITabarsCntrl.h"
#include "afxwin.h"


// CITabTestToolDlg dialog
class CITabTestToolDlg : public CDialogEx
{
// Construction
public:
	CITabTestToolDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MFCAPPLICATION1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	typedef I_ITABARSCntrl* (*PFGetITab)();
	typedef I_ITABARSCntrl* (*PFReleaseITab)();

	PFGetITab m_pfGetITab;
	PFGetITab m_pfReleaseITab;
	I_ITABARSCntrl* m_iTabObj;


// Implementation
protected:
	HICON m_hIcon;
	HMODULE m_hDll;
	void ShowRc(int rc);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedCheck2();
	BOOL m_bStationA;
	BOOL m_bStationB;
	BOOL m_bStationC;
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	CListBox m_lbEvents;
	afx_msg void OnBnClickedButtonLoad();
	CString m_strDll;
	afx_msg void OnBnClickedButtonSelftest();
	CString m_strCom;
	afx_msg void OnEnChangeEditDll();
};
