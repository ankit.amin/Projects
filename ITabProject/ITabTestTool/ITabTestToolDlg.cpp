
// MFCApplication1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "ITabTestTool.h"
#include "ITabTestToolDlg.h"
#include "afxdialogex.h"

#include <string.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CITabTestToolDlg dialog

static CITabTestToolDlg *pMainDlg = NULL;

static void __stdcall eventCallback(int eventId, int eventDetail)
{
	if (pMainDlg) {
		CString str;
		str.Format(_T("EventId:%d, EventDetail:%d"), eventId, eventDetail);
		pMainDlg->m_lbEvents.AddString(str);
		int nCount = pMainDlg->m_lbEvents.GetCount();
		pMainDlg->m_lbEvents.SetCurSel(nCount - 1);
		pMainDlg->m_lbEvents.SetTopIndex(nCount - 1);
	}
}

CITabTestToolDlg::CITabTestToolDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CITabTestToolDlg::IDD, pParent)
	, m_bStationA(TRUE)
	, m_bStationB(TRUE)
	, m_bStationC(TRUE)
	, m_strDll(_T("ITABARSCNTRL.dll"))
	,m_hDll(nullptr)
	, m_strCom(_T("COM5"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME); 
}

void CITabTestToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK2, m_bStationA);
	DDX_Check(pDX, IDC_CHECK3, m_bStationB);
	DDX_Check(pDX, IDC_CHECK4, m_bStationC);
	DDX_Control(pDX, IDC_LIST1, m_lbEvents);
	DDX_Text(pDX, IDC_EDIT_DLL, m_strDll);
	DDX_Text(pDX, IDC_EDIT_COM, m_strCom);
}

BEGIN_MESSAGE_MAP(CITabTestToolDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CITabTestToolDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CITabTestToolDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CITabTestToolDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CITabTestToolDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CITabTestToolDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CITabTestToolDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CITabTestToolDlg::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_SELFTEST, &CITabTestToolDlg::OnBnClickedButtonSelftest)
	ON_EN_CHANGE(IDC_EDIT_DLL, &CITabTestToolDlg::OnEnChangeEditDll)
END_MESSAGE_MAP()


// CITabTestToolDlg message handlers

BOOL CITabTestToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	pMainDlg = this;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CITabTestToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CITabTestToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CITabTestToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CITabTestToolDlg::OnBnClickedOk()
{
	if (m_pfReleaseITab)
		m_pfReleaseITab();
	FreeLibrary(m_hDll);
	CDialogEx::OnOK();
}

void CITabTestToolDlg::OnBnClickedButton1()
{
	const wchar_t * strLogfile=L"c:\\scot\\logs\\logfile.log";

	UpdateData(TRUE);

	if (m_iTabObj != nullptr)
	{
		ShowRc(m_iTabObj->Initialize(m_strCom, strLogfile, eventCallback));
	}
}

void CITabTestToolDlg::OnBnClickedButton2()
{
	UpdateData(TRUE);
	bool abStations[3];
	abStations[0] = m_bStationA;
	abStations[1] = m_bStationB;
	abStations[2] = m_bStationC;

	if (m_iTabObj != nullptr)
		ShowRc(m_iTabObj->RequestPackingBay(3, abStations));
}

void CITabTestToolDlg::OnBnClickedButton3()
{
	if (m_iTabObj != nullptr) {
		ShowRc(m_iTabObj->Stop());
	}
}

void CITabTestToolDlg::ShowRc(int rc)
{
	WCHAR wstr[32];
	_ltow_s(rc, wstr, 10);
	m_lbEvents.AddString(wstr);
	int nCount = m_lbEvents.GetCount();
	m_lbEvents.SetCurSel(nCount - 1);
	m_lbEvents.SetTopIndex(nCount - 1);
}

void CITabTestToolDlg::OnBnClickedButton4()
{
	if (m_iTabObj != nullptr)
		ShowRc(m_iTabObj->Recover());
	
}


void CITabTestToolDlg::OnBnClickedButton5()
{
	if (m_iTabObj != nullptr)
		m_iTabObj->DeInitialize();
}



void CITabTestToolDlg::OnBnClickedButtonLoad()
{
	UpdateData(TRUE);
	m_hDll = LoadLibrary(m_strDll);
	if (!m_hDll)
	{
		return;
	}

	m_pfGetITab = reinterpret_cast<PFGetITab>(GetProcAddress(m_hDll, "Get_ITABARSCntrl"));
	if (!m_pfGetITab)
	{
		return;
	}

	m_pfReleaseITab = reinterpret_cast<PFReleaseITab>(GetProcAddress(m_hDll, "Release_ITABARSCntrl"));
	if (!m_pfReleaseITab)
	{
		return;
	}

	m_iTabObj = m_pfGetITab();
}

void CITabTestToolDlg::OnBnClickedButtonSelftest()
{
	if (m_iTabObj != nullptr)
		ShowRc(m_iTabObj->SelfTest());
}


void CITabTestToolDlg::OnEnChangeEditDll()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
