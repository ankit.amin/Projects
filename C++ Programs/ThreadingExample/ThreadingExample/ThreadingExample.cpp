// ThreadingExample.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <thread>

using namespace std;

thread::id main_thread_id = std::this_thread::get_id();

void ThreadLaunchingAnotherThread(int x)
{
	std::cout << "\tInternal Thread ID: " << x << "++" << std::this_thread::get_id() << "++" << std::endl;
}

void ThreadFunction(int xx)
{
	for (int x = 0; x < xx; x++)
	{
		std::cout << "Thread Function's thread ID: "<< x << "--" << std::this_thread::get_id() << "--" << std::endl;
		thread *t = new thread(ThreadLaunchingAnotherThread, x);
		t->join();
	}
}




int _tmain(int argc, _TCHAR* argv[])
{
	//This start thee Thread
	thread *t = new thread(ThreadFunction, 4);

	cout << "Main thread: " << main_thread_id << endl;
	
	//Forces the Main thread to wait until secondary thread is finished
	t->join();

	cin.get();
	return 0;
}


