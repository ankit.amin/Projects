#include "RuleOfThree.h"
#include <cstring>
#include <iostream>
#include <string>
using namespace std;

/*
class CPPR5Class
{
public:
	// Default constructor
	CPPR5Class() :
		data (new char[14])
	{
		std::strcpy(data, "Hello, World!");
	}

	// Copy constructor
	CPPR5Class (const CPPR5Class& original) :
		data (new char[std::strlen (original.data) + 1])
	{
		std::strcpy(data, original.data);
	}

	// Move constructor
	// noexcept needed to enable optimizations in containers
	CPPR5Class (CPPR5Class&& original) noexcept :
		data(original.data)
	{
		original.data = nullptr;
	}

	// Destructor
	// explicitly specified destructors should be annotated noexcept as best-practice
	~CPPR5Class() noexcept
	{
		delete[] data;
	}

	// Copy assignment operator
	CPPR5Class& operator= (const CPPR5Class& original)
	{
		CPPR5Class tmp(original);         // re-use copy-constructor
		*this = std::move(tmp); // re-use move-assignment
		return *this;
	}

	// Move assignment operator
	CPPR5Class& operator= (CPPR5Class&& original) noexcept
	{
		if (this == &original)
		{
			// take precautions against `CPPR5Class = std::move(CPPR5Class)`
			return *this;
		}
		delete[] data;
		data = original.data;
		original.data = nullptr;
		return *this;
	}

private:
	friend std::ostream& operator<< (std::ostream& os, const CPPR5Class& cPPR5Class)
	{
		os << cPPR5Class.data;
		return os;
	}

	char* data;
};
*/
int main()
{
	//const MyClass mMyClass;
	//std::cout << mMyClass<< std::endl;

	return 0;
}


// Default constructor
MyClass::MyClass()
{
	data = new char[20];
	strcpy_s(data, strlen(data) + 1, "Hello, World!");
}

// Destructor
// explicitly specified destructors should be annotated noexcept as best-practice
MyClass::~MyClass() noexcept
{
	delete[] data;
}

// Copy constructor 
MyClass::MyClass(const MyClass& source)
{
	data = new char[std::strlen(source.data) + 1];
	strcpy_s(data, strlen(data) + 1, source.data);

}

// Move Constructor
// noexcept needed to enable optimizations in 
MyClass::MyClass(MyClass&& source) noexcept
{
	data = source.data;
	source.data = nullptr;
}

// Copy Assignment Operator
MyClass& MyClass::operator=(const MyClass& source)
{
	MyClass tmp(source);    // re-use copy-constructor
	*this = std::move(tmp); // re-use move-assignment
	return *this;
}

// Move assignment Operator
MyClass& MyClass::operator=(MyClass&& source) noexcept
{
	if (this == &source)
	{
		// take precautions against `MyClass = std::move(MyClass)`
		return *this;
	}
	delete[] data;
	data = source.data;
	source.data = nullptr;
	return *this;
}