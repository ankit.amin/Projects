#include "stdafx.h"
#include "StateOne.h"
#include "StateTwo.h"

//This is include so that StateOne can Call Machine's methods
#include "Machine.h"

/***********************FOR SINGLETON PATTERN******************************
StateOne *StateOne::sm_instance = NULL;
bool StateOne::m_isDestroyed = false;
/**************************************************************************/

/***********************FOR STATE PATTERN**********************************/
StateOne::StateOne(void)
{
}


StateOne::~StateOne(void)
{
}

//Called from Machine MachineState1
void StateOne::Action1(Machine *m)
{
	//std::cout << "StateOne::Action1" << std::endl;

	//Following calles StateOne's Action 2 method indirectly because
	//this allows Machine to change other behavior when calling 
	//StateOne's Action2 method. 
	m->MachineStateAction2();
	
	//This calls ACState's ChangeMachineState. 
	//That inturn calls Machine's (m) ChangeStateMachine() 
	//with new State (StateTwo).
	ChangeState(m, new StateTwo());
}

//Called from Machine MachineState2
void StateOne::Action2(Machine *m)
{
	std::cout << "StateOne::Action2" << std::endl;
}

//Called from Machine MachineState3
void StateOne::Action3(Machine *m)
{
	//std::cout << "StateOne::Action3" << std::endl;
}

void StateOne::PrintState()
{
	std::cout << "Current State is StateOne" << std::endl;
}