#pragma once
#include "ACState.h"
#include <iostream>

class StateTwo : public ACState
{
/***********************FOR SINGLETON PATTERN******************************
public:
	
	//Used to create an instance
	static StateOne *GetInstance()
	{
		if (sm_instance == NULL)
		{
			//If Singleton is destroyed then throw an exception 
			//otherwise create new Instance 
			if(m_isDestroyed)
			{
				DeleteInstance();
			}
			else
			{
				sm_instance = new StateOne();
			}
		}
		return sm_instance;
	}

	//Used to delete an instance
	static void DeleteInstance()
	{
		//delete sm_instance;
		//sm_instance =  NULL;

		throw std::runtime_error("Singleton is Destroyed");
	}

private:
	//This class will be a Singlton Design Pattern
	//Ensure the Singleton Pattern
	StateOne(void);
	
	//For proper way to declare/implement Singleton Design Pattern
	StateOne(const StateOne&);
	StateOne& operator=(const StateOne&);

	//Destructor is called when we "delete" a sm_instance
	//sm_instance is set to null not deleted;
	//m_isDestroyed flag is set to true;
	//The reason the flag is set to true is that when next time an application refers to sm_instance it will  
	//cause error. 
	StateOne::~StateOne()
	{
		sm_instance = NULL;
		m_isDestroyed = true;
	}
	
	static StateOne *sm_instance;
	static bool m_isDestroyed;
/**************************************************************************/

/***********************FOR STATE PATTERN**********************************/

public:
	StateTwo(void);
	virtual ~StateTwo(void);
	//Machine is included in "State.h" therefore we do not have to include them here. 
	//These functions are called be Machine
	virtual void Action1(Machine *m);
	virtual void Action2(Machine *m);
	virtual void Action3(Machine *m);
	virtual void PrintState();
};
