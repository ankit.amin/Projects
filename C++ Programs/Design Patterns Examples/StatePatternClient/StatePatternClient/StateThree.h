#pragma once
#include "ACState.h"
#include <iostream>

class StateThree : public ACState
{
public:
	static StateThree &GetInstance();

	//Create a new instance  and store the pointer to it
	//in m_pInstance
	static void CreateInstance();

	static void DeleteInstance();

	//Machine is included in "State.h" therefore we do not have to include them here. 
	//These functions are called be Machine
	virtual void Action1(Machine *m);
	virtual void Action2(Machine *m);
	virtual void Action3(Machine *m);
	virtual void PrintState();

protected:
	StateThree(void);
	virtual ~StateThree(void);
	
	StateThree(const StateThree &s);
	StateThree &operator=(const StateThree &rhs);
	
private:
	static StateThree *m_pInstance;
	static bool m_isDestroyed;
	
};
