#pragma once
#include "ACState.h"

class Machine
{
public:
	Machine(void);
	virtual ~Machine(void);

	void MachineStateAction1();
	void MachineStateAction2();
	void MachineStateAction3();
	void MachinePrintState();

private: 
	//this is declared such that State class can call ChangeMachineState method. 
	friend class ACState; 
	void ChangeMachineState(ACState *s);

private: 
	ACState *m_currentState;

};

