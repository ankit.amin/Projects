#include "stdafx.h"
#include "ACState.h"
//This must be included so ChangeState (m) can call ChangeMachineState
#include "Machine.h"

ACState::ACState(void)
{
}


ACState::~ACState(void)
{
}

/*
 * This function calls Machine's ChangeMachineState function 
 * and passes an instance of Concrete State class.
 * 
 * The reason ChangeMachineState is accessiable is because of 
 * "friend class ACState" declaration in Machine class
 */
void ACState::ChangeState(Machine *m, ACState *s)
{
	//Change Machine's state to another other state
	m->ChangeMachineState(s);
}

