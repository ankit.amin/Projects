// StatePatternClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Machine.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	Machine *m_machine =  new  Machine();
	
	//Following calls StateOne's print method
	//because m_currentState is initialized with StateOne()
	m_machine->MachinePrintState();

	//Call's StateOne's Action1 method
	m_machine->MachineStateAction1();
	m_machine->MachinePrintState();


	m_machine->MachinePrintState();
	m_machine->MachineStateAction1();
	m_machine->MachinePrintState();

	std::cin.getline(" ",2);
	return 0;
}

