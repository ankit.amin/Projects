#include "stdafx.h"
#include "StateTwo.h"
#include "StateOne.h"
#include "StateThree.h"

StateTwo::StateTwo(void)
{
}


StateTwo::~StateTwo(void)
{
}

//Called from Machine MachineState1
void StateTwo::Action1(Machine *m)
{
	//std::cout << "StateTwo::Action1" << std::endl;
	Action2(m);
}

//Called from Machine MachineState2
void StateTwo::Action2(Machine *m)
{
	//std::cout << "StateTwo::Action2" << std::endl;
//	ChangeState(m, &StateThree::GetInstance());
	ChangeState(m, new StateOne());
}

//Called from Machine MachineState3
void StateTwo::Action3(Machine *m)
{
	//std::cout << "StateTwo::Action3" << std::endl;
	ChangeState(m, this);
}

void StateTwo::PrintState()
{
	std::cout << "Current State is StateTwo" << std::endl;
}