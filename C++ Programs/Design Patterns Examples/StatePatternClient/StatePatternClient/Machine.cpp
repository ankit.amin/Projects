#include "stdafx.h"
#include "Machine.h"
//included only for m_currentState state initialization
#include "StateOne.h"

Machine::Machine(void)
{
	//Start of with StateOne as default
	//m_currentState = StateOne::GetInstance();
	m_currentState = new StateOne();
}

Machine::~Machine(void)
{
	delete m_currentState;
	m_currentState = NULL;
}

//Machine's State Action1
void Machine::MachineStateAction1()
{
	//Call State's Action1 Method
	m_currentState->Action1(this);
}

//Machine's State Action2
void Machine::MachineStateAction2()
{
	//Call State's Action2 Method
	m_currentState->Action2(this);
}

//Machine's State Action3
void Machine::MachineStateAction3()
{
	//Call State's Action3 Method
	m_currentState->Action3(this);
}

//Change Machine' State
void Machine::ChangeMachineState(ACState *s)
{
	//This state could be any of the Concrete States
	//which are extended from State abstract class.
	m_currentState = s;
}

//Call's State's Print Method
void Machine::MachinePrintState()
{
	m_currentState->PrintState();
}
