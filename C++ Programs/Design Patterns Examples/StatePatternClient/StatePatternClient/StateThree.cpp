#include "stdafx.h"
#include "StateThree.h"
#include "Machine.h"
#include "StateOne.h"

/*********************************************************************/
StateThree* StateThree::m_pInstance = 0;
bool StateThree::m_isDestroyed = false;

StateThree::StateThree(void)
{
}

StateThree::~StateThree(void)
{
}

StateThree& StateThree::GetInstance()
{
	if (!m_pInstance)
	{
		if(m_isDestroyed)
		{
			DeleteInstance();
		}
		else
		{
			CreateInstance();
		}
	}
	return (*m_pInstance);
}

//Create a new instance  and store the pointer to it
//in m_pInstance
void StateThree::CreateInstance()
{
	if (m_pInstance == NULL)
	{
		static StateThree tempInstance;
		m_pInstance = &tempInstance;
	}
}

void StateThree::DeleteInstance()
{
	throw std::runtime_error("Dead Reference Detected");

	delete m_pInstance;
	m_pInstance =NULL;
	m_isDestroyed = true;
}
/****************************************************************************/

//Called from Machine MachineState1
void StateThree::Action1(Machine *m)
{
	//std::cout << "StateThree::Action1" << std::endl;

	//Following calles StateThree's Action 2 method indirectly because
	//this allows Machine to change other behavior when calling
	//StatStateThreeeOne's Action2 method.
	m->MachineStateAction2();

	//This calls ACState's ChangeMachineState.
	//That inturn calls Machine's (m) ChangeStateMachine()
	//with new State (StateTwo).
	//ChangeState(m, new StateOne());
}

//Called from Machine MachineState2
void StateThree::Action2(Machine *m)
{
	std::cout << "StateThree::Action2" << std::endl;
}

//Called from Machine MachineState3
void StateThree::Action3(Machine *m)
{
	//std::cout << "StateThree::Action3" << std::endl;
}

void StateThree::PrintState()
{
	std::cout << "Current State is StateThree" << std::endl;
}