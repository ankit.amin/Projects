#pragma once

//Forward Declaration 
class Machine;

class ACState
{
public:
	ACState(void);
	virtual ~ACState(void);

	virtual void Action1(Machine *m) = 0;
	virtual void Action2(Machine *m) = 0;
	virtual void Action3(Machine *m) = 0;
	virtual void PrintState() = 0;

	void ChangeState(Machine *m, ACState *s);
};

