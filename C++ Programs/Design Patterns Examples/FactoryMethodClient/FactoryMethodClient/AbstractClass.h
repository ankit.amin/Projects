#pragma once
//#include <iostream>
#include <string>

class AbstractClass
{
public:
	AbstractClass(void);
	virtual ~AbstractClass(void);

	void SetName( std::string s);
	void SetInt( int x);

	std::string GetName() const;
	int GetInt() const;

	static AbstractClass* FactoryMethod(int choice);
	
private:
	std::string m_name;
	int m_int;
};

