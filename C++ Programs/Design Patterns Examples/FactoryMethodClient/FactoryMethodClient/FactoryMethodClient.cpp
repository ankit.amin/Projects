// FactoryMethodClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "AbstractClass.h"
#include "ConcreteProductA.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int getUserInput = 0;

	
	/*
	AbstractClass *abc = (AbstractClass *) new ConcreteProductA();
	
	cout << abc->GetInt() << "-->" << abc->GetName() << endl;
	*/
	while (getUserInput < 2)
	{
		cout << "Enter 0 or 1" << endl;
		cin >> getUserInput;
		AbstractClass *abc = AbstractClass::FactoryMethod(getUserInput);
		cout << abc->GetInt() << "-->" << abc->GetName() << endl;
	}
	return 0;
}

