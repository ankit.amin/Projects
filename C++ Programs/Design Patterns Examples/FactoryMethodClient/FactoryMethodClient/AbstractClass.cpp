#include "stdafx.h"
#include "AbstractClass.h"
#include "ConcreteProductA.h"
#include "ConcreteProductB.h"

AbstractClass::AbstractClass(void)
{
}


AbstractClass::~AbstractClass(void)
{
}

void AbstractClass::SetName(std::string s)
{
	this->m_name = s;
}

void AbstractClass::SetInt(int x)
{
	this->m_int = x;
}

std::string AbstractClass::GetName() const
{
	return this->m_name;
}

int AbstractClass::GetInt() const
{
	return this->m_int;
}

AbstractClass* AbstractClass::FactoryMethod(int choice)
{
	AbstractClass *createdObject = NULL;
	switch(choice)
	{
	case 0: 
		//need to have PUBLIC in front of "class ConcreteProductA : public AbstractClass"
		//in ConcreateProduct.h otherwise following line is required (casting is required)
		//createdObject = (AbstractClass*) new ConcreteProductA();
		createdObject = new ConcreteProductA();		
		break;
	case 1:
		createdObject = new ConcreteProductB();
		break;
	default:
		createdObject = new ConcreteProductA();
		break;
	}

	return createdObject;
}