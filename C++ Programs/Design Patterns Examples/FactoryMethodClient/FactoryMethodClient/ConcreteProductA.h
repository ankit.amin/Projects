#pragma once
#include "AbstractClass.h"

//have to put public in front of AbstractClass 
//otherwise will casting is required error FactoryMethod class
//to return AbstractClass *
class ConcreteProductA : public AbstractClass
{
public:
	ConcreteProductA(void);
	virtual ~ConcreteProductA(void);
};

