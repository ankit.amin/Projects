#pragma once
#include "AbstractClass.h"

//have to put public in front of AbstractClass 
//otherwise will casting is required error FactoryMethod class
//to return AbstractClass *
class ConcreteProductB : public AbstractClass
{
public:
	ConcreteProductB(void);
	virtual ~ConcreteProductB(void);
};

