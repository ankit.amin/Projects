#pragma once

class IAbstractProduct
{
public:
	IAbstractProduct() {}

	//Need to define destructor. MUST have {}
	virtual ~IAbstractProduct(){}

	virtual void printProductLabel() = 0;
};
