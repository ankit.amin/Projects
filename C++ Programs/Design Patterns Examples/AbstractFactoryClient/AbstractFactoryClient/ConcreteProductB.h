#pragma once
#include "IAbstractProduct.h"

//MUST include "public" infront of IAbstracProduct
//Otherwise we will get link errors. 
//in C++ by default everything is "private"
class ConcreteProductB : public IAbstractProduct
{
public:
	ConcreteProductB(void);
	virtual ~ConcreteProductB(void);
	virtual void printProductLabel();
};

