#pragma once
#include "IAbstractFactory.h"

//MUST include "public" infront of IAbstractFactory
//Otherwise we will get link errors. 
//in C++ by default everything is "private"
class ConcreteFactoryA : public IAbstractFactory
{
public:
	ConcreteFactoryA(void);
	virtual ~ConcreteFactoryA(void);

	virtual IAbstractProduct *createProduct();
};

