#pragma once
#include "IAbstractFactory.h"

//MUST include "public" infront of IAbstractFactory
//Otherwise we will get link errors. 
//in C++ by default everything is "private"
class ConcreateFactoryB : public IAbstractFactory
{
public:
	ConcreateFactoryB(void);
	virtual ~ConcreateFactoryB(void);

	virtual IAbstractProduct *createProduct();
};

