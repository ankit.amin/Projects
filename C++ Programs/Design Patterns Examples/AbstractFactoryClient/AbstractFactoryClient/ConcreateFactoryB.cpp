#include "stdafx.h"
#include "ConcreateFactoryB.h"
#include "ConcreteProductB.h"
#include <iostream>


ConcreateFactoryB::ConcreateFactoryB(void)
{
}


ConcreateFactoryB::~ConcreateFactoryB(void)
{
}

IAbstractProduct *ConcreateFactoryB::createProduct()
{
	using namespace std;

	cout<< "ConcreateFactoryB creates concrete product B" << endl;
	IAbstractProduct *abstractProduct = new ConcreteProductB();

	return abstractProduct;
}