#pragma once

//Do not use the forward declaration 
//otherwise we will get  "deletion of pointer to incomplete type" warning
//Recommendation: use #inlcude "IAbstractProduct.h"
//class IAbstractProduct;
#include "IAbstractProduct.h"

class IAbstractFactory
{
public:
	IAbstractFactory() {}

	//Need to define destructor. MUST have {}
	virtual ~IAbstractFactory() {}

	virtual IAbstractProduct *createProduct() = 0;
};
