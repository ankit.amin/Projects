// AbstractFactoryClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Application.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Application *newApplication = new Application();
	delete newApplication;

	return 0;
}
