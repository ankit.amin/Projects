#include "stdafx.h"
#include "Application.h"
#include "ConcreteFactoryA.h"
#include "ConcreateFactoryB.h"

#include <iostream>

Application::Application(void)
{
	theConcreteFactory = NULL;
	theConcreteProduct = NULL;

	//Create a Concrete Factory
	theConcreteFactory = createAbstractFactory();

	//Uses 1 of the concreate Factories to create a Concrete Product
	theConcreteProduct = theConcreteFactory->createProduct();
}


Application::~Application(void)
{
	delete theConcreteFactory;
	delete theConcreteProduct;
}

//Returns 1 of Concrete Factories
//Following function is also refered to as FACTORY METHOD pattern
IAbstractFactory *Application::createAbstractFactory()
{
	using namespace std;
	int userInput;

	cout << "Enter A-1 or B-2" << endl;
	cin >> userInput;

	if (userInput == 1)
	{
		return new ConcreteFactoryA();
	}
	else
		return new ConcreateFactoryB();

}
