#pragma once
#include "IAbstractFactory.h"

class Application
{
public:
	Application(void);
	virtual ~Application(void);

	IAbstractFactory *createAbstractFactory();

	IAbstractFactory *theConcreteFactory; 
	IAbstractProduct *theConcreteProduct;
};

