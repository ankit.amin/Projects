#include "stdafx.h"
#include "ConcreteFactoryA.h"
#include "ConcreteProductA.h"
#include <iostream>


ConcreteFactoryA::ConcreteFactoryA(void)
{
}


ConcreteFactoryA::~ConcreteFactoryA(void)
{
}

IAbstractProduct* ConcreteFactoryA::createProduct()
{
	using namespace std;

	cout << "ConcreateFactoryA created product A" << endl;
	IAbstractProduct *abstractProduct = new ConcreteProductA();

	return abstractProduct;
}