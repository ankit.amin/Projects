#pragma once
#include "IAbstractProduct.h"

//MUST include "public" infront of IAbstracProduct
//Otherwise we will get link errors. 
//in C++ by default everything is "private"
class ConcreteProductA : public IAbstractProduct
{
public:
	ConcreteProductA(void);
	virtual ~ConcreteProductA(void);
	virtual void printProductLabel();
};

