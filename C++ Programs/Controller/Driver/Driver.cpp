#include "Driver.h"
#include <iostream>

Driver::Driver(void)
{
	//Initialize the function pointer and the user params
	m_pCallback = NULL;
	m_pParams = NULL;

	state = 0;
}


Driver::~Driver(void)
{
	//Cannot delete function pointer
	//delete m_pCallback;
	//m_pCallback = NULL;

	//Proper way to delete pointer
	//1. Free Memory
	delete m_pParams;
	//2. NULL the pointer
	m_pParams = NULL;
}

void Driver::RegisterCallBack(CBFunction *func, void *params)
{
	//Define the function pointer and user parameter
	this->m_pCallback = func;
	this->m_pParams	= params;
}

void Driver::CallHandler()
{
	if (this->m_pCallback != NULL)
	{
		//Calls the function in StateMachine class
		state = m_pCallback(state, m_pParams);	
	}

	switch(state)
	{
	case 1:
		StateOne();
		break;
	case 2:
		StateTwo();
		break;
	case 3:
		StateThree();
		break;
	default:
		break;

	}
}

void Driver::StateMachineCalledDriver()
{
}

void Driver::StateOne()
{
	std::cout << "StateOne: " << state << std::endl;
	this->CallHandler();
}

void Driver::StateTwo()
{
	std::cout << "StateTwo: " << state << std::endl;
	this->CallHandler();
}

void Driver::StateThree()
{
	std::cout << "StateThree: " << state << std::endl;
	this->CallHandler();
}
