#pragma once

//Define the call back function pointer
typedef int (CBFunction)(int x, void* param);

class Driver
{
public:
	Driver(void);
	virtual ~Driver(void);
	
	//This function is called from inside StateMachine to set it's callback function
	//which has to match the function pointer defined above.
	void RegisterCallBack(CBFunction *func, void *params);

	//This function allows Driver to call the StateMachine
	void CallHandler();
	
	//This function allows StateMachine to call the Driver
	void StateMachineCalledDriver();

	void StateOne();
	void StateTwo();
	void StateThree();

protected:
	//Function pointer
	CBFunction *m_pCallback;

	//User parameter
	void *m_pParams;
	
	
	int state;

};

