#include "StateMachine.h"


StateMachine::StateMachine(void)
{
	//1. Create Driver object
	m_driver = new Driver();

	//2. Set the callback function, which Driver obj will call
	m_driver->RegisterCallBack(DriverIsCallingThisFunction, this);

	//3. Call the driver's handler. This establishs one-way communication channel between Driver and StateMachine.
	m_driver->CallHandler();
}


StateMachine::~StateMachine(void)
{
}

//This function is indirectly called from Driver object.
int StateMachine::CommandFromDriver(int x)
{	
	switch (x)
	{
	case 0:
		stateCounter = 1;
		break;
	case 1:
		stateCounter = 2;
		break;
	case 2:			
		stateCounter = 3;
		break;
	default:
		stateCounter = 0;
		break;
	}

	return stateCounter;
}