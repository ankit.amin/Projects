#pragma once

//Include the driver class
#include "../Driver/Driver.h"
#include <iostream>

class StateMachine
{
public:
	StateMachine(void);
	virtual ~StateMachine(void);

	//Function that actually does the work in this class
	int CommandFromDriver (int x);

	//This function is called from "Driver" class
	static int DriverIsCallingThisFunction(int x, void *params)
	{		
		if ( (params != NULL) )
		{
			//Check to see if the params are of type StateMachine object
			StateMachine *sm = (StateMachine*)params;

			//Call the function that actually does the work
			return sm->CommandFromDriver(x);
		}
		else
			return -1;
	}

protected:
	//Instance of Driver class
	Driver *m_driver;

	int stateCounter;
};