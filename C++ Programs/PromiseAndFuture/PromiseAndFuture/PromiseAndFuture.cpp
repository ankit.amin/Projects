// PromiseAndFuture.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <thread>
#include <future>
#include <iostream>

using namespace std;

//&& is used because std::move syntax is used to move rvalue
void ThreadFunction(promise<string> * str) 
{
	string x = "Hello From Thread!";
	str->set_value(x);

}



int _tmain(int argc, _TCHAR* argv[])
{
	/*
	1. Create a Promise; this is INPUT channel of thread
	2. Extract a Future from the Promist;  this is OUTPUT channel of thread
	3. Promise has to set the value 1st before Future can retrieve it. 
	*/

	//Step 1
	promise<string> threadInput; 

	//Step 2
	future<string> threadOutput = threadInput.get_future();
	
	//Move the Promise to ThreadFunction
	//thread th(&ThreadFunction, std::move(threadInput));
	thread th(&ThreadFunction, &threadInput);

	std::cout << "Hello from Main Thread!" << endl;

	//This will block the Thread until secondary thread gets the output
	string OutputFromThread = threadOutput.get();

	cout << OutputFromThread << endl;

	//Must Have Following statement otherwise application may terminate without other thread from completing. 
	th.join();

	return 0;
}

