// CallbackFunctionExample.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Callee.h"
#include "Caller.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Caller *caller = new Caller();
	Callee *callee = new Callee();

	//Register Callee's Static_Callback_Function AND "Callee" object
	//This is passing in the ADDRESS of StaticCallBackFunction
	caller->RegisterCallbackFuntion(Callee::StaticCallBackFunction, &callee);

	//Call the callee's member callback function
	caller->Test();

	return 0;
}

