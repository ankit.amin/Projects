#pragma once

//Caller defines the type of static callback function Callee should have. 
typedef void(*CallbackFunctionPtr)(void *calleeObject, int x);

//Caller Class
class Caller
{
public:
	Caller(void);
	virtual ~Caller(void);

	//Regsiter the Static_Callback_Function for the Callee. 
	//Required information: Callee's Static_CallBack_Function and VOID * parameter
	//Callee's Static callback function should match the signature of "callbackFuntionPtr" 
	//from above
	void RegisterCallbackFuntion(CallbackFunctionPtr calleeStaticCBFunction, void* calleeObject);

	//test
	void Test();

private:
	//member variable to hold the pointer to callback function.
	CallbackFunctionPtr m_cbFuntion;

	//callee's object
	void* m_calleeObject;
};

