#pragma once

class Callee
{
public:
	Callee(void);
	virtual ~Callee(void);

	//Static_Callback_Function used to Register with Caller
	static void StaticCallBackFunction( void *calleeObject, int x)
	{
		if (calleeObject != NULL)
		{
			//Casting the void* to Callee object
			Callee *cObject = (Callee*)calleeObject;

			//Call the Callee object's member function
			cObject->MemberCallbackFunction(x);
		}
	}

	//Member_Callback_Function
	void MemberCallbackFunction(int x);
};

