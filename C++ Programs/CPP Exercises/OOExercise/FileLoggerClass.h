#pragma once
#include "ILoggerClass.h"

class FileLoggerClass : public ILoggerClass
{
public:
	FileLoggerClass(void);
	~FileLoggerClass(void);

	
	//ILoggerClass functions
	bool WriteLog(FILE *fp, const char *logMessage);
	bool ReadLog(const char *logMessage );
};

