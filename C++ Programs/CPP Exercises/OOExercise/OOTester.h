#pragma once
class OOTester
{
public:
	OOTester(void);
	~OOTester(void);

	void StartTests();

	void CastingExercise();
	
	void PrintDivider();

	void InterfaceExercise();

private:
	FILE *outputFile;
};

