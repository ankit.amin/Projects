#include "StdAfx.h"
#include "MyRectangles.h"
#include "Rectangles.h"
#include "Shapes.h"

#include <iostream>


MyRectangles::MyRectangles(void)
{
}


MyRectangles::~MyRectangles(void)
{
}

int MyRectangles::Volume()
{
	Rectangles *temp = (Rectangles*)(this);
	//Shapes s(temp); //Works

	//Pass Extended (MyRectangles object) with static_cast to Base class. 
	Shapes s( static_cast<Rectangles *>(this));
	
	//std::cout << s.RandomNumber();
	return s.RandomNumber();
}