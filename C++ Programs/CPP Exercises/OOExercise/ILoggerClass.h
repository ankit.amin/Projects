#pragma once

/*
 * -> Interfaces DOES NOT have..."Member Variables"...it only has "Member Functions"
 * -> Abstract Classes have "Member Variables" and "Member Functions"
 * -> Both Interfaces and Abstract Classes have "pure virtual functions" and CANNOT be instantiated
 */

class ILoggerClass
{
public:
	//ILoggerClass(){}
	//virtual ~ILoggerClass()	{}

	virtual bool WriteLog(FILE *fp, const char *logMessage) = 0;
	virtual bool ReadLog(const char *logMessage ) = 0;
};