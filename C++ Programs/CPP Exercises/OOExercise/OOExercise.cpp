// OOExercise.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MyRectangles.h"
#include <iostream>
#include "Pet.h"
#include "Dog.h"
#include "Cat.h"

int _tmain(int argc, _TCHAR* argv[])
{
	
	MyRectangles *temp = new MyRectangles();
	//temp->Volume();
	std::cout << std::endl << (temp->Volume()) << std::endl;

	//double myArray[1000];

	//for (int i = 0; i < 1000; i++)
	//{
	//	myArray[i] = i * 10.0;
	//}

    //double* pMyArray = &myArray[0];

	//Creating an object of the Wrapper Class
	//::CPPCPPComputingEngineWrapperClass wrapperObject( pMyArray, 1000);
	//
	////Calling the getSum() method from the CPPComputingEngineWrapperClass 
	//wrapperObject.CalculateSum();
	//
	////Getting the "sum" variable from the CPPComputingEngineWrapperClass
	//double sumOfArray = wrapperObject.sum;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//Casting exercise
	//
	//					   _______
	//					  |  Pet  |	
	//					   -------
	//					      ||
	//					     \||/
	//					      \/
	//				   ================
	//				 __|||__      __|||__ 
	//				|  Dog  |    |	Cat  |
	//				 -------      -------
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//A CAT pointer is Upcasted to a PET
	Pet* b = new Cat; // Upcast
			
	//A Downcast is attempted to a DOG pointer from PET
	Dog* d1 = dynamic_cast<Dog*>(b);
	
	//A Downcast is attempted to a CAT pointer from PET
	Cat* d2 = dynamic_cast<Cat*>(b);
	
	std::cout << std::endl << std::endl;
	std::cout << "Dog to Pet (where Pet is pointing to Cat Object) = " << (long)d1 << std::endl;  //Error
	std::cout << "Cat to Pet (where Pet is pointing to Cat Object) = " << (long)d2 << std::endl;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//A DOG pointer is Upcasted to a PET
	Pet* b1 = new Dog; // Upcast
		
	//A Downcast is attempted to a DOG pointer from PET
	Dog* d3 = dynamic_cast<Dog*>(b1);
	
	//A Downcast is attempted to a CAT pointer from PET
	Cat* d4 = dynamic_cast<Cat*>(b1);
	
	std::cout << std::endl << std::endl;
	std::cout << "Dog to Pet (where Pet is pointing to Dog Object) = " << (long)d3 << std::endl;  
	std::cout << "Cat to Pet (where Pet is pointing to Dog Object) = " << (long)d4 << std::endl; //Error
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//A pointer to PET object
	Pet* b2 = new Pet; 
		
	//A Downcast is attempted to a DOG pointer from PET
	Dog* d5 = dynamic_cast<Dog*>(b2);
	
	//A Downcast is attempted to a CAT pointer from PET
	Cat* d6 = dynamic_cast<Cat*>(b2);
	
	std::cout << std::endl << std::endl;
	std::cout << "Dog to Pet (where Pet is pointing to Pet Object) = " << (long)d5 << std::endl;  //Error
	std::cout << "Cat to Pet (where Pet is pointing to Pet Object) = " << (long)d6 << std::endl;  //Error

	std::cin.get();
	
	return 0;
}