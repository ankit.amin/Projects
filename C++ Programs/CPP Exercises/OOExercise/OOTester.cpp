#include "StdAfx.h"
#include "OOTester.h"
#include <stdlib.h>

#include "MyRectangles.h"
#include "Pet.h"
#include "Cat.h"
#include "Dog.h"

#include "TemplateExercises.h"

//Interface Exercise
#include "ILoggerClass.h"
#include "FileLoggerClass.h"

OOTester::OOTester(void)
{
	if ( (outputFile = fopen("OOExercise.txt", "w") ) == NULL )
	{
		printf("Cannot open OOExercise.txt.\n");
		exit(1);
	}
}


OOTester::~OOTester(void)
{
}

void OOTester::StartTests()
{
	MyRectangles *myRec = new MyRectangles();
	/*
	-> 1. "MyRectangle" is child of "Rectangle" class
	-> 2. "Shape" is separate class with EXPLICIT "Rectangle" object
	
	// myRec is of type "MyRectangles" which calls "MyRectangle::Volume()" which in turn calls "Shape::RandomNumber()" 

	// Therefore to call "Shape::RandomNumber()"...
	I.		"Shape" object is created using "static_cast"ing (upcasting) "MyRectangle" object into "Rectangle" object 
	II.		Passing that as the argument to "Shape::Shape(Rectangle)" constructor...and
	III.	Then calling "Shape::RandomNumber()" from "MyRectangle::Volume()"
	*/
	fprintf(outputFile, "See the desc. in the code:...Volume %d\n", myRec->Volume());
	
	this->PrintDivider();
	
	//Casting exercises
	this->CastingExercise();

	this->PrintDivider();
	/**********************************************************************************
	 * Testing Function Templates
	 **********************************************************************************/
	TemplateExercises tempEx = TemplateExercises();
	int i = tempEx.compare<int>(10, 11); //function template defined within a class
	fprintf(outputFile, "Template compare<int> %d\n", i);
	i = tempEx.compare<double>(1.5,0.5); //function template defined within a class
	fprintf(outputFile, "Template compare<double> %d\n", i);

	//Function Template outside TemplateExercise class.
	i = outSideCompare<int>(10,11);
	fprintf(outputFile, "outsideCompare<int> %d\n", i);
	i = outSideCompare<double>(2.5,2.5);
	fprintf(outputFile, "outsideCompare<double> %d\n", i);
	
	this->PrintDivider();
	/**********************************************************************************/
	
	this->InterfaceExercise();

	fclose(outputFile);
}

void OOTester::CastingExercise()
{
	fprintf(outputFile, "\t\tCasting Exercise\n");
	fprintf(outputFile, "\n");
	fprintf(outputFile, "\t\t      _______\n");
	fprintf(outputFile, "\t\t     |  Pet  |	\n");
	fprintf(outputFile, "\t\t      -------\n");
	fprintf(outputFile, "\t\t        ||\n");
	fprintf(outputFile, "\t\t       \\||/\n");
	fprintf(outputFile, "\t\t        \\/\n");
   	fprintf(outputFile, "\t\t  ================\n");
	fprintf(outputFile, "\t\t__|||__      __|||__ \n");
	fprintf(outputFile, "\t\t|  Dog  |    |	Cat  |\n");
	fprintf(outputFile, "\t\t-------      -------\n\n");
	
	/*************************************************************************************************************/
	//A CAT pointer is Upcasted to a PET
	Pet* b = new Cat; // Upcast
			
	//A Downcast is attempted to a DOG pointer from PET
	Dog* d1 = dynamic_cast<Dog*>(b);
	
	//A Downcast is attempted to a CAT pointer from PET
	Cat* d2 = dynamic_cast<Cat*>(b);
	
	fprintf(outputFile, "//A CAT pointer is Upcasted to a PET\n");
	fprintf(outputFile, "Pet* b = new Cat; // Upcast\n");
	fprintf(outputFile, "//A Downcast is attempted to a DOG pointer from PET\n");
	fprintf(outputFile, "Dog* d1 = dynamic_cast<Dog*>(b);\n");
	fprintf(outputFile, "//A Downcast is attempted to a CAT pointer from PET\n");
	fprintf(outputFile, "Cat* d2 = dynamic_cast<Cat*>(b);\n\n");
	fprintf(outputFile, "Dog to Pet (where Pet is pointing to Cat Object) = %ld //Error \n", (long)d1 );
	fprintf(outputFile, "Cat to Pet (where Pet is pointing to Cat Object) = %ld\n", (long)d2 );
	fprintf(outputFile, "*******************************************************************************\n\n");	
	/*************************************************************************************************************/

	//A DOG pointer is Upcasted to a PET
	Pet* b1 = new Dog; // Upcast
		
	//A Downcast is attempted to a DOG pointer from PET
	Dog* d3 = dynamic_cast<Dog*>(b1);
	
	//A Downcast is attempted to a CAT pointer from PET
	Cat* d4 = dynamic_cast<Cat*>(b1);
	
	fprintf(outputFile,	"//A DOG pointer is Upcasted to a PET\n");
	fprintf(outputFile,	"Pet* b1 = new Dog; // Upcast\n");
	fprintf(outputFile,	"//A Downcast is attempted to a DOG pointer from PET\n");
	fprintf(outputFile,	"Dog* d3 = dynamic_cast<Dog*>(b1);\n");
	fprintf(outputFile,	"//A Downcast is attempted to a CAT pointer from PET\n");
	fprintf(outputFile,	"Cat* d4 = dynamic_cast<Cat*>(b1);\n");
	fprintf(outputFile, "Dog to Pet (where Pet is pointing to Dog Object) = %ld \n", (long)d3 );  
	fprintf(outputFile, "Cat to Pet (where Pet is pointing to Dog Object) = %ld //Error\n", (long)d4 ); //Error
	fprintf(outputFile, "*******************************************************************************\n\n");
	/*************************************************************************************************************/

	//A pointer to PET object
	Pet* b2 = new Pet; 
		
	//A Downcast is attempted to a DOG pointer from PET
	Dog* d5 = dynamic_cast<Dog*>(b2);
	
	//A Downcast is attempted to a CAT pointer from PET
	Cat* d6 = dynamic_cast<Cat*>(b2);
	
	fprintf(outputFile, "//A pointer to PET object\n");
	fprintf(outputFile, "Pet* b2 = new Pet;\n");
	fprintf(outputFile, "//A Downcast is attempted to a DOG pointer from PET\n");
	fprintf(outputFile, "Dog* d5 = dynamic_cast<Dog*>(b2);\n");
	fprintf(outputFile, "//A Downcast is attempted to a CAT pointer from PET\n");
	fprintf(outputFile, "Cat* d6 = dynamic_cast<Cat*>(b2);\n");
	fprintf(outputFile, "Dog to Pet (where Pet is pointing to Pet Object) = %ld //Error\n", (long)d5 );  //Error
	fprintf(outputFile, "Cat to Pet (where Pet is pointing to Pet Object) = %ld //Error\n", (long)d6 );  //Error
	fprintf(outputFile, "*******************************************************************************\n\n");
}

void OOTester::PrintDivider()
{
	fprintf(outputFile, "\n /--------------------------------------------------------------------------------------------------------------------------\\ \n");
	fprintf(outputFile, "<||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||>\n");
	fprintf(outputFile, " \\--------------------------------------------------------------------------------------------------------------------------/ \n\n");
}

void OOTester::InterfaceExercise()
{

	ILoggerClass *fileLog = new FileLoggerClass(); //FYI...Upcasting FileLoggerClass to ILoggerClass

	fileLog->WriteLog(outputFile, "To write using interface following is done...\n\n");
	fileLog->WriteLog(outputFile, "\tILoggerClass *fileLog = new FileLoggerClass();\n");
	fileLog->WriteLog(outputFile, "\tfileLog->WriteLog(...)\n\n");
	fileLog->WriteLog(outputFile, "Writing through interface\n");

	delete fileLog;
}