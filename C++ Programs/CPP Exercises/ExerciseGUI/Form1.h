//#using<system.dll>

#pragma once

namespace ExerciseGUI 
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	using namespace System::IO;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  IntPointerEx_Btn;
	protected: 

	private: System::Windows::Forms::Button^  CharPointerEx_Btn;
	private: System::Windows::Forms::Button^  LinkedListEx_Btn;
	private: System::Windows::Forms::Button^  StructEx_Btn;
	public: System::Windows::Forms::TextBox^  OutputTextBox;
	private: System::Windows::Forms::Button^  ReferenceExe_Btn;
	private: System::Windows::Forms::Button^  OOExercise_Btn;
	protected: 


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->IntPointerEx_Btn = (gcnew System::Windows::Forms::Button());
			this->CharPointerEx_Btn = (gcnew System::Windows::Forms::Button());
			this->LinkedListEx_Btn = (gcnew System::Windows::Forms::Button());
			this->StructEx_Btn = (gcnew System::Windows::Forms::Button());
			this->OutputTextBox = (gcnew System::Windows::Forms::TextBox());
			this->ReferenceExe_Btn = (gcnew System::Windows::Forms::Button());
			this->OOExercise_Btn = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// IntPointerEx_Btn
			// 
			this->IntPointerEx_Btn->Location = System::Drawing::Point(12, 12);
			this->IntPointerEx_Btn->Name = L"IntPointerEx_Btn";
			this->IntPointerEx_Btn->Size = System::Drawing::Size(122, 23);
			this->IntPointerEx_Btn->TabIndex = 0;
			this->IntPointerEx_Btn->Text = L"Int Pointers Example";
			this->IntPointerEx_Btn->UseVisualStyleBackColor = true;
			this->IntPointerEx_Btn->Click += gcnew System::EventHandler(this, &Form1::IntPointerEx_Btn_Click);
			// 
			// CharPointerEx_Btn
			// 
			this->CharPointerEx_Btn->Location = System::Drawing::Point(140, 12);
			this->CharPointerEx_Btn->Name = L"CharPointerEx_Btn";
			this->CharPointerEx_Btn->Size = System::Drawing::Size(122, 23);
			this->CharPointerEx_Btn->TabIndex = 1;
			this->CharPointerEx_Btn->Text = L"Char Pointers Example";
			this->CharPointerEx_Btn->UseVisualStyleBackColor = true;
			this->CharPointerEx_Btn->Click += gcnew System::EventHandler(this, &Form1::CharPointerEx_Btn_Click);
			// 
			// LinkedListEx_Btn
			// 
			this->LinkedListEx_Btn->Location = System::Drawing::Point(268, 12);
			this->LinkedListEx_Btn->Name = L"LinkedListEx_Btn";
			this->LinkedListEx_Btn->Size = System::Drawing::Size(122, 23);
			this->LinkedListEx_Btn->TabIndex = 2;
			this->LinkedListEx_Btn->Text = L"Linked List Example";
			this->LinkedListEx_Btn->UseVisualStyleBackColor = true;
			this->LinkedListEx_Btn->Click += gcnew System::EventHandler(this, &Form1::LinkedListEx_Btn_Click);
			// 
			// StructEx_Btn
			// 
			this->StructEx_Btn->Location = System::Drawing::Point(396, 12);
			this->StructEx_Btn->Name = L"StructEx_Btn";
			this->StructEx_Btn->Size = System::Drawing::Size(122, 23);
			this->StructEx_Btn->TabIndex = 3;
			this->StructEx_Btn->Text = L"Struct Example";
			this->StructEx_Btn->UseVisualStyleBackColor = true;
			this->StructEx_Btn->Click += gcnew System::EventHandler(this, &Form1::StructEx_Btn_Click);
			// 
			// OutputTextBox
			// 
			this->OutputTextBox->Font = (gcnew System::Drawing::Font(L"Consolas", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->OutputTextBox->Location = System::Drawing::Point(12, 42);
			this->OutputTextBox->Multiline = true;
			this->OutputTextBox->Name = L"OutputTextBox";
			this->OutputTextBox->ReadOnly = true;
			this->OutputTextBox->ScrollBars = System::Windows::Forms::ScrollBars::Both;
			this->OutputTextBox->Size = System::Drawing::Size(1337, 683);
			this->OutputTextBox->TabIndex = 4;
			this->OutputTextBox->WordWrap = false;
			// 
			// ReferenceExe_Btn
			// 
			this->ReferenceExe_Btn->Location = System::Drawing::Point(524, 12);
			this->ReferenceExe_Btn->Name = L"ReferenceExe_Btn";
			this->ReferenceExe_Btn->Size = System::Drawing::Size(122, 23);
			this->ReferenceExe_Btn->TabIndex = 5;
			this->ReferenceExe_Btn->Text = L"References Example";
			this->ReferenceExe_Btn->UseVisualStyleBackColor = true;
			this->ReferenceExe_Btn->Click += gcnew System::EventHandler(this, &Form1::ReferenceExe_Btn_Click);
			// 
			// OOExercise_Btn
			// 
			this->OOExercise_Btn->Location = System::Drawing::Point(652, 13);
			this->OOExercise_Btn->Name = L"OOExercise_Btn";
			this->OOExercise_Btn->Size = System::Drawing::Size(122, 23);
			this->OOExercise_Btn->TabIndex = 6;
			this->OOExercise_Btn->Text = L"OO Example";
			this->OOExercise_Btn->UseVisualStyleBackColor = true;
			this->OOExercise_Btn->Click += gcnew System::EventHandler(this, &Form1::OOExercise_Btn_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoScroll = true;
			this->AutoSize = true;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(1361, 737);
			this->Controls->Add(this->OOExercise_Btn);
			this->Controls->Add(this->ReferenceExe_Btn);
			this->Controls->Add(this->OutputTextBox);
			this->Controls->Add(this->StructEx_Btn);
			this->Controls->Add(this->LinkedListEx_Btn);
			this->Controls->Add(this->CharPointerEx_Btn);
			this->Controls->Add(this->IntPointerEx_Btn);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
	private: 
		System::Void IntPointerEx_Btn_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			this->OutputTextBox->Text = "";
			
			String^ fp = "Integer Pointers Exercise.txt";
			try 
			{
				StreamReader^ din = File::OpenText(fp);

				String^ str;
				int count = 0;
				while ((str = din->ReadLine()) != nullptr) 
				{
					count++;
					this->OutputTextBox->AppendText(str + "\n");
				}
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
				{
					this->OutputTextBox->AppendText("File Not Found");
				}
				else
				{
					this->OutputTextBox->AppendText("Problem with reading file");
				}
			}
		}
	
		System::Void CharPointerEx_Btn_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			this->OutputTextBox->Text = "";

			String^ fp = "Character Pointers Exercise.txt";
			try 
			{
				StreamReader^ din = File::OpenText(fp);

				String^ str;
				int count = 0;
				while ((str = din->ReadLine()) != nullptr) 
				{
					count++;
					this->OutputTextBox->AppendText(str + "\n");
				}
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
				{
					this->OutputTextBox->AppendText("File Not Found");
				}
				else
				{
					this->OutputTextBox->AppendText("Problem with reading file");
				}
			}
		}
		
		System::Void LinkedListEx_Btn_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			this->OutputTextBox->Text = "";

			String^ fp = "Linked List Exercise.txt";
			try 
			{
				StreamReader^ din = File::OpenText(fp);

				String^ str;
				int count = 0;
				while ((str = din->ReadLine()) != nullptr) 
				{
					count++;
					this->OutputTextBox->AppendText(str + "\n");
				}
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
				{
					this->OutputTextBox->AppendText("File Not Found");
				}
				else
				{
					this->OutputTextBox->AppendText("Problem with reading file");
				}
			}
		}

		System::Void StructEx_Btn_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			this->OutputTextBox->Text = "";

			String^ fp = "Struct Exercise.txt";
			try 
			{
				StreamReader^ din = File::OpenText(fp);

				String^ str;
				int count = 0;
				while ((str = din->ReadLine()) != nullptr) 
				{
					count++;
					this->OutputTextBox->AppendText(str + "\n");
				}
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
				{
					this->OutputTextBox->AppendText("File Not Found");
				}
				else
				{
					this->OutputTextBox->AppendText("Problem with reading file");
				}
			}	
		}
	
		System::Void ReferenceExe_Btn_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			this->OutputTextBox->Text = "";

			String^ fp = "References Exercise.txt";
			try 
			{
				StreamReader^ din = File::OpenText(fp);

				String^ str;
				int count = 0;
				while ((str = din->ReadLine()) != nullptr) 
				{
					count++;
					this->OutputTextBox->AppendText(str + "\n");
				}
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
				{
					this->OutputTextBox->AppendText("File Not Found");
				}
				else
				{
					this->OutputTextBox->AppendText("Problem with reading file");
				}
			}	
		
		}
	private: 
		System::Void OOExercise_Btn_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			this->OutputTextBox->Text = "";

			String^ fp = "OOExercise.txt";
			try 
			{
				StreamReader^ din = File::OpenText(fp);

				String^ str;
				int count = 0;
				while ((str = din->ReadLine()) != nullptr) 
				{
					count++;
					this->OutputTextBox->AppendText(str + "\n");
				}
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
				{
					this->OutputTextBox->AppendText("File Not Found");
				}
				else
				{
					this->OutputTextBox->AppendText("Problem with reading file");
				}
			}
		}
};
}

