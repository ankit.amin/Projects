// ExerciseGUI.cpp : main project file.

#include "stdafx.h"
#include <iostream>
#include "Form1.h"
#include "..\MainExercises\LinkedListExercises.h"
#include "..\MainExercises\PointersExercise.h"
#include "..\MainExercises\StructExercises.h"
#include "..\MainExercises\ReferencesExercises.h"

#include "..\OOExercise\OOTester.h"

#import "..\COMExercise\bin\Debug\COMExercise.tlb"
using namespace COMExercise;

using namespace ExerciseGUI;

//TODO:
//Call C# class from C++
//Option 1:	Expose C# to COM
//Option 2:	Instantiate C# class in C++ using gcnew	
//using namespace CSharpControllingApp;

/*******************************************************************************************/
//Declaring the extern variable 
//This is defined in ReferenceExperience.cpp
extern int &externReference;
/*******************************************************************************************/

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	//PointersExercise pc = new PointersExercise(); //Invalid Statement: DOES NOT require initialization

	//Pointer *ptrExe requires initialization prior to use, 
	//therefore we HAVE to use "new PointersExercise()"
	//PointersExercise *ptrExe =  new PointerClass();  
	PointersExercise *ptrExe = new PointersExercise();
	ptrExe->StartPointerExercises();
	
	LinkedListExercises *llExe = new LinkedListExercises();
	llExe->StartLL();

	StructExercises *structEx = new StructExercises();
	structEx->StartStructExercise();
	
	//PointersExercise *ptrExe2 = new ReferencesExercises(); 
	//ptrExe2 can not call functions in ReferencesExercises class

	//ReferencesExercises &refExe2 = dynamic_cast<ReferencesExercises &> (*ptrExe2);
	//refExe2.IntegerReferenceExercise();

	//Following object creation and initialization is VALID and CORRECT
	//ReferencesExercises *refExe = new ReferencesExercises();
	//refExe->IntegerReferenceExercise();

	//Following object creation and initialization is another VALID and CORRECT
	//NOTE how the reference is created by dereferencing a pointer. 
	ReferencesExercises &refExe = (ReferencesExercises &) *(new ReferencesExercises());
	refExe.IntegerReferenceExercise();

	OOTester *ooTester = new OOTester();
	ooTester->StartTests();

	
	//COMExercise::COMClass ^comClass;

	//CoInitialize(NULL);
	//COMExercise::COMClass p(__uuidof(COMExercise::COMClass));
	//comClass = p;

	//comClass->ShowCOMDialog();


	/**************************************************************************************************************
	//******Getting the data from C# library into C++ Application
	double importOutput = 0.0;
	double wrapperOutput = 0.0;

	CSharpControllingApp::ImportExample ^importExample = gcnew CSharpControllingApp::ImportExample();
	//******IN ORDER FOR THESE FUNCTIONS TO GET CALLED...THE CSharpConrtollingApp SHOULD COMPILED AS 
	//LIBRARY AND NOT AN APPLICATION******
	importOutput = importExample->ImportFunctionExercise();

	CSharpControllingApp::WrapperExample ^wrapperExample = gcnew CSharpControllingApp::WrapperExample();
	wrapperOutput = wrapperExample->WrappingExercise();
	
	
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 
		
	// Create the main window and run it
	Form1 ^myForm = gcnew Form1();

	myForm->OutputTextBox->Text = "ImportFunction Output: " + importOutput + " Wrapper Output: "+ wrapperOutput + "\n";

	myForm->OutputTextBox->AppendText("ExternReference: " + externReference);
	/**************************************************************************************************************/

	Application::Run(gcnew Form1());
	//Application::Run(myForm);
	

	return 0;
}
