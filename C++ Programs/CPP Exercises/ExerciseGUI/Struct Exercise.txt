w = 0x4148 
*b++:00000048 	 *b:00000041 
This system is Little Endian

*****************************
 Shifting Example 
*****************************
dataToShift: 00000006 	 mask: 00000300
After shifting the data: = 00000600
mysteriousValue: = 00000073


*****************************
 Number of Set Bits Example 
*****************************
Number of bits set high in 12355 is 5.

*****************************
 Struct Serialization Example 
*****************************
Size of sA.a = 4 
Size of sA.b = 4 
Size of sA.c = 4 
Size of sA.d = 4 
Size of sA = 16 

Serialization of struct vertically
unsigned char *unsignedCharPtrToStruct[0] = 67 // 'g' 
unsigned char *unsignedCharPtrToStruct[1] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[2] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[3] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[4] = 68 // 'h' 
unsigned char *unsignedCharPtrToStruct[5] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[6] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[7] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[8] = 56 // 'V' 
unsigned char *unsignedCharPtrToStruct[9] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[10] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[11] = 00 // ' ' 
unsigned char *unsignedCharPtrToStruct[12] = 10 // '' 
unsigned char *unsignedCharPtrToStruct[13] = ED // '�' 
unsigned char *unsignedCharPtrToStruct[14] = 1F // '' 
unsigned char *unsignedCharPtrToStruct[15] = 00 // ' ' 


String sA.b is a actually a  two dimensional array, therefore
unsigned char * needs to travel horizotally to get the  bit representation of "Ankit" 
Unsigned char array: *unsignedCharPrtToStructString[0] = 00000041 // 'A'
Unsigned char array: *unsignedCharPrtToStructString[1] = 0000006E // 'n'
Unsigned char array: *unsignedCharPrtToStructString[2] = 0000006B // 'k'
Unsigned char array: *unsignedCharPrtToStructString[3] = 00000069 // 'i'
Unsigned char array: *unsignedCharPrtToStructString[4] = 00000074 // 't'


*****************************
 Converting Decimal to Binary Example 
*****************************
Coverting 352123 to decimal 00000000000001010101111101111011
