// CPPComputingEngineWrapper.h

#pragma once

//Location of the CPPComputingEngine
//#include "../CPPComputingEngine/ComputingEngine.h"
//#include "../CPPComputingEngine/ComputingEngine.cpp"

//Converted CPPComputingEngine to static lib. 
//Added location of following header file in Additional Include Directories
//Added the location of *lib file. 
#include "ComputingEngine.h"

using namespace System;

namespace CPPComputingEngineWrapper 
{
	public ref class CPPComputingEngineWrapperClass
	{
	public:
		CPPComputingEngineWrapperClass( double *pDbl, int arraySize);
		
		double sum; //This is public b/c we want this guy to be visible from C#.

		void CalculateSum();

	private:
		ComputingEngine *pComputingEngine;
	};
}
