// This is the main DLL file.

#include "stdafx.h"

#include "CPPComputingEngineWrapper.h"

//Location of the CPPComputingEngine
//#include "../CPPComputingEngine/ComputingEngine.h"
//#include "../CPPComputingEngine/ComputingEngine.cpp"


CPPComputingEngineWrapper::CPPComputingEngineWrapperClass::CPPComputingEngineWrapperClass( double *pDbl, int arraySize)
{
	//Init ComputingEngine
	pComputingEngine = new ComputingEngine( pDbl, arraySize );
}

void CPPComputingEngineWrapper::CPPComputingEngineWrapperClass::CalculateSum()
{
	//Calling ComputingEngine's SumArray function
	sum = pComputingEngine->SumArray();
}