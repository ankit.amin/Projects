#include "StdAfx.h"
#include "CBController.h"
#include <iostream>
using namespace std;

CBController::CBController(void)
{
	this->m_pCallBack = NULL;
	this->m_pUserData = NULL;
}

CBController::~CBController(void)
{
}

//Function to invoke the callee's callback function
int CBController::CallingCBFunction(int x, int y, double dx, double dy)
{
	std::cout << "In CBController::CallingCBFunction calling m_pCallBack: " << m_pCallBack << " with userdata: " << m_pUserData << endl;
	int retval = -1;

	if (m_pCallBack != NULL)
	{
		retval = (*m_pCallBack)(x, y, dx, dy, m_pUserData);
	}
	return retval;
}


//Function to register callback functions for Callee objects
bool CBController::SetCallBack(CBFunction *func, void* pUserData)
{
	std::cout << "In ClassWithCallback::SetCallBack with func and userdata: " << func << "  " << (int*)pUserData << endl;
	this->m_pCallBack = func;
	this->m_pUserData = pUserData;
	return true;
}

bool CBController::CallingCBFunctionFromClassWithCallBack()
{
	return true;
}
