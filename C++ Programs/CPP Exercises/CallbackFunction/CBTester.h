#pragma once

#include "CBController.h"
#include "../ControllerClass/MainController.h"
//#include "../ControllerClass/MainController.cpp"
#include <iostream>
using namespace std;


//Callee Class
class CBTester
{
public:
	CBTester(void);
	virtual ~CBTester(void);
	
	int LocalCallBackFunction(int x, int y, double dx, double dy);
	static int CBFunctionInCBTester( int x, int y, double dx, double dy, void *pUserData)
	{
		std::cout << "In Controller::CBFunctionInCBTester" << endl;
		if (pUserData != NULL)
		{
			CBTester *cb = (CBTester *)pUserData;
			return cb->LocalCallBackFunction(x, y, dx, dy);
		}
		else
		{
			return -1;
		}
	}

protected:
	CBController *client;
	MainController *m_client;

};

