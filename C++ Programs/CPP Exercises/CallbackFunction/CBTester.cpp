#include "StdAfx.h"
#include "CBTester.h"

#include <iostream>
using namespace std;

CBTester::CBTester(void)
{
	//Create the Controller object
	this->client = new CBController();
	this->m_client = new MainController();

	//Assign the Callback function (CBFunctionInCBTester declared AND defined in header file) to client
	//this->client->SetCallBack(CBTester::CBFunctionInCBTester, this);
	this->m_client->SetCallBack(CBTester::CBFunctionInCBTester, this);

	//Technically this function should not call the CallingCBFunction().
	//Controller call should invoke the callback function to this class.
	this->client->CallingCBFunction(4, 5, 6.0, 7.0);
}


CBTester::~CBTester(void)
{
}


int CBTester::LocalCallBackFunction(int x, int y, double dx, double dy)
{
	std::cout << "In Controller::LocalCallBackFunction()...this " << (int*) this << endl;
	std::cout << "In Controller::LocalCallBackFunction()" << endl << endl;
	std::cout << "  x +  y = " << x+y << " where x = " << x << " and y = " << y << endl;
	std::cout << " dx + dy = " << dx+dy << " where dx = " << dx << " and dy = " << dy << endl;
	return 0;
}