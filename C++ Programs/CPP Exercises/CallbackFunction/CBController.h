#pragma once

typedef int (CBFunction)( int x, int y, double dx, double dy, void *pUserData);


//Caller Class
class CBController
{
public:
	CBController(void);
	virtual ~CBController(void);

	//Function to invoke the callee's callback function
	int CallingCBFunction(int x, int y, double dx, double dy);
	
	//Function to register callback functions for Callee objects
	bool SetCallBack(CBFunction *func, void* pUserData);
	
	bool CallingCBFunctionFromClassWithCallBack();

protected:
	CBFunction *m_pCallBack;
	void *m_pUserData;

};

