#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

#include "PointersExercise.h"

#define TEST_A 0x29 //')'
#define TEST_B 0x3E //'>'

/*******************************************************************************************/
//Declaring the extern variable 
//This is defined in ReferenceExperience.cpp
extern int &externReference;
/*******************************************************************************************/

PointersExercise::PointersExercise(void)
{
}


PointersExercise::~PointersExercise(void)
{
}

void PointersExercise::StartPointerExercises()
{
	/***************************************************************************************************/
	//Integer pointer Exercise
	FILE *fp;
	if ( (fp = fopen("Integer Pointers Exercise.txt", "w") ) == NULL )
	{
		printf("Cannot open Pointers Exercise.txt.\n");
		exit(1);
	}

	this->SimpleIntegerPointerExercise(fp);
	this->PrintDivider(fp);
	
	this->OneDIntegerPointerExercise(fp);
	this->PrintDivider(fp);
	
	this->TwoDIntegerPointerExercise(fp);
	this->PrintDivider(fp);


	/***************************************************************************************************/
	//Character pointer Exercise

	fclose(fp);  //Close the integer pointer file

	if ( (fp = fopen("Character Pointers Exercise.txt", "w") ) == NULL )
	{
		printf("Cannot open Pointers Exercise.txt.\n");
		exit(1);
	}

	this->CharacterPointerExercise(fp);
	this->PrintDivider(fp);

	//char *nChar = Alloc_AA(10, fp);
	//fprintf(fp, "%s \n",nChar);
	//
	//nChar = Alloc_AA (4, fp);
	//fprintf(fp, "%s \n",nChar);

	//nChar = Alloc_AA (3, fp);
	//fprintf(fp, "%s \n",nChar);

	//nChar = Alloc_AA(2, fp);
	//fprintf(fp, "%s \n",nChar);

	//nChar = Alloc_AA(1, fp);
	//fprintf(fp, "%s \n",nChar);

	//nChar = Alloc_AA(1, fp);
	//fprintf(fp, "%s \n",nChar);

	//Passing #define macro as char pointer
	char buff[] = "HelloThere!";
	PassingCharPtrToCharBufferArray((char *)&buff[0], fp);
	this->PrintDivider(fp);

	std::vector<int> arr(5);
	arr[0] = 1;
	arr[1] = 2;
	arr[2] = 3;
	arr[3] = 2;
	arr[4] = 1;
	
	PatternFoundAfter( arr, fp);
	this->PrintDivider(fp);

	this->NumberOfUniqueDigits(-222, fp);
	this->PrintDivider(fp);
	
	this->DoublePointerExercise(fp);
	this->PrintDivider(fp);
	
	fclose(fp); //Close the char pointer file
}

void PointersExercise::SimpleIntegerPointerExercise(FILE *fp)
{	
	fprintf(fp, " Printing exterReference from ReferencesExercises.cpp = %d\n", externReference); 
	
	fprintf(fp, " ****************************\n * Simple Pointer           *\n ****************************\n"); 
	fprintf(fp, " |*********||***************|\n"); 
	fprintf(fp, " | Address || Value         |\n");
	fprintf(fp, " |*********||***************|\n"); 
	fprintf(fp, " | a       || 90            |\n");
	fprintf(fp, " |---------||---------------|\n"); 
	fprintf(fp, " | p1      || Address of a  |\n"); 
	fprintf(fp, " |---------||---------------|\n"); 
	fprintf(fp, " | p2      || Address of p1 |\n"); 
	fprintf(fp, " |---------||---------------|\n"); 
	fprintf(fp, " |*********||***************|\n\n"); 

	int a = 90, *p1, **p2; 
	p1 = &a;  //p1 = Address of a. Equivalent to p1 = a
	p2 = &p1; //p2 = Address of p1. Equivalent to p2 = p1 

	fprintf(fp, "a-->Address of (&a):  %08X \t Value of (a):  %d\n", &a, a);
	fprintf(fp, "p1->Address of (&p1): %08X \t Value of (p1): %08X \t (*p1): %d\n", &p1,p1,*p1); //p1 holds the value of address of a
	fprintf(fp, "p2->Address of (&p2): %08X \t Value of (p2): %08X \t (*p2): %08X \t (**p2): %d\n", &p2, p2, *p2, **p2); //for DOUBLE pointer
}

void PointersExercise::OneDIntegerPointerExercise(FILE *fp)
{
	fprintf( fp, "****************************\n* Pointers to 1D Int Array *\n****************************\n\n");

	int iArray[4] = {11,12,13,14};
	int *pArray;
	pArray = iArray; //Equivalent to pArray = &intArray[0];

	fprintf(fp, "int *pArray = iArray; Equivalent to pArray = &intArray[0];\n\n");

	fprintf(fp, "&iArray[0]: %08X \t\t\t\t iArray[0]:  %d \n", &iArray[0], iArray[0]);
	fprintf(fp, "&pArray+0:  %08X \t pArray+0: %08X ", &pArray+0, pArray+0);	
	fprintf(fp,	"\t*(pArray+0): %d \t\t*pArray+0: %d\n\n", *(pArray+0), *pArray+0);//*(pArray+0) = *pArray+0

	fprintf(fp, "&iArray[1]: %08X \t\t\t\t iArray[1]:  %d \n", &iArray[1], iArray[1]);
	fprintf(fp, "&pArray+1:  %08X \t pArray+1: %08X ", &pArray+1, pArray+1);	
	fprintf(fp,	"\t*(pArray+1): %d \t\t*pArray+1: %d\n\n", *(pArray+1), *pArray+1);//*(pArray+1) = *pArray+1

	fprintf(fp, "&iArray[2]: %08X \t\t\t\t iArray[2]:  %d \n", &iArray[2], iArray[2]);
	fprintf(fp, "&pArray+2:  %08X \t pArray+2: %08X ", &pArray+2, pArray+2);	
	fprintf(fp,	"\t*(pArray+2): %d \t\t*pArray+2: %d\n\n", *(pArray+2), *pArray+2);//*(pArray+2) = *pArray+2

	fprintf(fp, "&iArray[3]: %08X \t\t\t\t iArray[3]:  %d \n", &iArray[3], iArray[3]);
	fprintf(fp, "&pArray+3:  %08X \t pArray+3: %08X ", &pArray+3, pArray+3);	
	fprintf(fp,	"\t*(pArray+3): %d \t\t*pArray+3: %d\n\n", *(pArray+3), *pArray+3);//*(pArray+3) = *pArray+3	

	fprintf(fp, "NOTE:  *(pArray+i) = *pArray+i \n");

	fprintf(fp, "\n-------Example of *integers[3]------- ARRAY OF 3 POINTERS TO INTEGERS\n");

	int *integers[3];
	int aa = 121, bb = 1234, cc = 4522;
	integers[0] = &aa;
	integers[1] = &bb;
	integers[2] = &cc;

	fprintf(fp, "integers[0] = &aa (where aa is int) \t integers[0]:: %08X \t *integers[0]:: %d \n" , integers[0], *integers[0]);
	fprintf(fp, "integers[1] = &bb (where bb is int) \t integers[1]:: %08X \t *integers[1]:: %d \n" , integers[1], *integers[1]);
	fprintf(fp, "integers[2] = &cc (where cc is int) \t integers[2]:: %08X \t *integers[2]:: %d \n" , integers[2], *integers[2]);

	fprintf(fp, "\n-------Example of (*integers2)[3]------- TO AN ARRAY OF 3 INTEGERS\n");

	int (*integers2)[3];
	int intArray[3] = { 12873, 90128, 81092};
	integers2 = &intArray;

	fprintf(fp, "integers2 = &intArray (where intArray is array of 3 integers)\n\n");

	fprintf(fp, "&intArray:: %08X \t integers2:: %08X \t *integers2:: %08X\n", &intArray, integers2, *integers2);
	fprintf(fp, "(*integers2)[0]:: %d \t*integers2[0]:: %d\n", (*integers2)[0], *integers2[0]); // Outputs 12873 and 12873
	fprintf(fp, "(*integers2)[1]:: %d \t*integers2[1]:: %d\n", (*integers2)[1], *integers2[1]); // Outputs 90128 and some garbage
	fprintf(fp, "(*integers2)[2]:: %d \t*integers2[2]:: %d\n", (*integers2)[2], *integers2[2]); // Outputs 90128 and some garbag
}

void PointersExercise::TwoDIntegerPointerExercise(FILE *fp)
{
	fprintf( fp, "****************************\n* Pointers to 2D Int Array *\n****************************\n\n");

	int a[3][4];
	int (*pa)[4] = a; // Pointer Initialization pa = a;

	//Array Initialization
	for (int i=0; i<3; i++) 
	{
		fprintf(fp,"||| ");

		for (int j=0; j<4; j++) 
		{
			a[i][j] = i+j+10;
			fprintf(fp, "a[%d][%d]->%d :: %08X ||| ", i, j, a[i][j], &a[i][j]);
		}
		fprintf (fp, "\n");
	}

	//Print the address of a[0][0] and pa. They should be equal
	fprintf(fp, "\n&a[0][0]: %08X \n", &a);
	fprintf(fp, "&pa: %08X | pa: %08X \n\n", &pa, pa);

	//Address decoding
	for (int i=0; i<3; i++) 
	{
		fprintf(fp, "&pa+%d->%08X ||| pa+%d->%08X ||| *(pa+%d)->%08X ||| *(*(pa+%d))->%d\n", i, &pa+i, i, pa+i, i, *(pa+i), i, *(*(pa+i)) );
	}

	fprintf (fp, "\n\n");

	for (int i=0; i<3; i++) 
	{
		//Printing the Row vectors pa[i][0]
		//a[i][j] = pa[i]= pa+i
		fprintf(fp, "(pa+%d):%08X = &a[%d][0]--> \n",  i,(pa+i), i);

		for (int j=0; j<4; j++) 
		{
			//Printing the address and contents of each pa element.
			fprintf(fp, "\t\t\t\t(*(pa+%d)+%d)->%08X ||| *(*(pa+%d)+%d)->%d\n", 
				i,  j, (*(pa+i)+j),     i,  j, *(*(pa+i)+j) );
		}
		fprintf (fp, "\n");
	}
	//Multi-dimensional array and pointer formula
	//a[i][j] = *(*(pa+i) +j)

}

void PointersExercise::CharacterPointerExercise(FILE *fp)
{
	fprintf(fp, "*******************\n* Character Array *\n*******************\n\n");

	char cArray[] = "Hello", *cPtr;
	cPtr = cArray; // cPtr = "Hello"

	fprintf(fp, "cArray: %s \t\t &cArray: %08X \n",cArray, &cArray); //prints Hello

	fprintf(fp, "cPtr: %s \t\t &cPtr: %08X \t *(cPtr): %c \n", cPtr, &cPtr, *cPtr); //Hello and H

	fprintf(fp, "*(cPtr+0): %c\n", *(cPtr+0) );  // "H"
	fprintf(fp, "*(cPtr+1): %c\n", *(cPtr+1) );  // "e"
	fprintf(fp, "*(cPtr+2): %c\n", *(cPtr+2) );  // "l"
	fprintf(fp, "*(cPtr+3): %c\n", *(cPtr+3) );  // "l"
	fprintf(fp, "*(cPtr+4): %c\n", *(cPtr+4) );  // "o"
	fprintf(fp, "*(cPtr+5): %c -->Character Array termination character \'\\0\'\n", *(cPtr+5) );// \0 character
	fprintf(fp, "*(cPtr+6): %c -->This is just Array Index out of bounds \n", *(cPtr+6) );// Index out of bounds
	fprintf(fp, "*cPtr++: %c \n", *cPtr++); // "H" then increment the pointer to point to next character which is 'e'
	fprintf(fp, "*cPtr: %c \n", *cPtr );	// "e"
	fprintf(fp, "cPtr: %s \n\n", cPtr);		//"ello"	

	fprintf(fp, "STRCOPY EXAMPLE \n\n");

	char a[] = "Ankit",  b[] = "Nujra";
	StringCopy(&a[0], &b[0], fp);
	//char *a = "Ankit",  *b = "Nujra";
	//strcopy(a, b);

	fprintf (fp, "After the STRCOPY function\n");
	fprintf (fp, "a: %s \t*a: %c\n", a, *a);
	fprintf (fp, "b: %s \t*b: %c\n\n", b, *b);


	char *lines[3];
	char d[] = "Ankit Amin";
	char m[] = "Tina Amin";
	char s[] = "Arjun Ankit Amin";
	lines[0] = d;
	lines[1] = m;
	lines[2] = s;

	fprintf (fp, "lines[0] = d (is char array) \t lines[0]:: %s \t *lines[0]:: %c\n", lines[0], *lines[0] );  
	fprintf (fp, "lines[0] = m (is char array) \t lines[0]:: %s \t *lines[1]:: %c\n", lines[1], *lines[1] );  
	fprintf (fp, "lines[0] = s (is char array) \t lines[0]:: %s \t *lines[2]:: %c\n", lines[2], *lines[2] );  

	fprintf(fp, "\n****************************\n* Unsigned Character Array *\n****************************\n\n");
	unsigned char myIntArray[] =  {255,11,12,13,14,15,16,8,9,10,12,45}; 
	fprintf(fp, " \"unsigned char myIntArray[] =  {255,11,12,13,14,15,16,8,9,10,12,45};\" \n");
	fprintf (fp, "Unsigned char array size = %d\n", sizeof(myIntArray) );  

	for (int i = 0; i < sizeof(myIntArray); i++)
	{
		fprintf (fp, "Unsigned char array: myIntArray[%d] = %08X\n", i, myIntArray[i] );  
	}

	unsigned char myString[] = "This is unsigned char array myString";
	unsigned char *uPtrString = &myString[0]; //Points to "T" in the myString

	fprintf (fp, "\n \"unsigned char myString[] = \"This is unsigned char array myString\"; \n \"unsigned char *uPtrString = &myString[0]; //Points to T in the myString\n");

	for(int i =0; i < sizeof(myString); i++)
	{
		//This will print the ASCII represenation of the char at the location its pointing at.
		fprintf (fp, "Unsigned char array: *uPtrString[%d] = %08X // '%c' \n",i, *uPtrString, (char)(*uPtrString) ); 

		//Increment the pointer to the next element in the myString array
		uPtrString++; 
	}

	//unsigned char c = 'A';
	//unsigned char *uPtrChar = &c;
	char c = 'A';
	unsigned char *uPtrChar = (unsigned char *)&c;
	fprintf (fp, "\n \"unsigned char c = 'A'; \t unsigned char *uPtrChar = &c;\" \n\t\tOR");
	fprintf (fp, "\n \"char c = 'A'; \t unsigned char *uPtrChar = (unsigned char * )&c;\"");
	fprintf (fp, "\n Unsigned char: *uPtrChar = %08X\n", *uPtrChar );  //Prints out the ASCII value of A in Hex which is 41

	int x = 128;
	unsigned char *u = (unsigned char *)&x;
	fprintf(fp, "\n \"int x = %d; \t unsigned char *u = (unsigned char *)&x;\"\n ",x);
	fprintf (fp, "Unsigned char * of Int: *u = %08X\n", *u ); 

	*u = *u - 12;
	fprintf(fp, "\n \"*u = *u - 12;\"\n ");
	fprintf (fp, "After subtracting 128-12: unsigned char * of Int: *u = %08X\n", *u );
}

void PointersExercise::StringCopy ( char *s , char *t, FILE *fp)
{
	fprintf(fp, "In STRCOPY function --> Before the COPY: \n");
	fprintf(fp, "s: %s \t*s: %c \n", s, *s);
	fprintf(fp, "t: %s \t*t: %c \n", t, *t);
	fprintf(fp, "*(s+0): %c \t *(t+0):  %c \n", *(s+0), *(t+0) );
	fprintf(fp, "*(s+1): %c \t *(t+1):  %c \n", *(s+1), *(t+1) );
	fprintf(fp, "*(s+2): %c \t *(t+2):  %c \n", *(s+2), *(t+2) );
	fprintf(fp, "*(s+3): %c \t *(t+3):  %c \n", *(s+3), *(t+3) );
	fprintf(fp, "*(s+4): %c \t *(t+4):  %c \n\n", *(s+4), *(t+4) );
	
	while ( (*s++ = *t++) )
	{
		//cout << *s << endl;
		//cout << *t << endl;
		//s = t;
		;
	}
	
	//All of the following output will be NULL because we already have 
	//gone through char array that was passed in.
	fprintf(fp, "*(s+0): %c \t *(t+0):  %c \n", *(s+0), *(t+0) );
	fprintf(fp, "*(s+1): %c \t *(t+1):  %c \n", *(s+1), *(t+1) );
	fprintf(fp, "*(s+2): %c \t *(t+2):  %c \n", *(s+2), *(t+2) );
	fprintf(fp, "*(s+3): %c \t *(t+3):  %c \n", *(s+3), *(t+3) );
	fprintf(fp, "*(s+4): %c \t *(t+4):  %c \n\n", *(s+4), *(t+4) );
}

void PointersExercise::PrintDivider(FILE *fp)
{
	fprintf(fp, "\n /--------------------------------------------------------------------------------------------------------------------------\\ \n");
	fprintf(fp, "<||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||>\n");
	fprintf(fp, " \\--------------------------------------------------------------------------------------------------------------------------/ \n\n");
}

void PointersExercise::PassingCharPtrToCharBufferArray(const char *pChar, FILE *fp)
{
	char Buffer[2];
	Buffer[0] = (char) *pChar;
	Buffer[1] = 0x50; //'P' //Turns the Hex representation to ASCII char value of 'P"



	fprintf(fp,"Buffer: %c\n", Buffer[0]);	
	fprintf(fp,"Buffer: %c\n", Buffer[1]);	

	//char *s = const_cast<char*>(pChar);
//	fprintf(fp,"S: %s\n", *s);	


}

//Complexity of N
void PointersExercise::PatternFoundAfter( const std::vector<int> &myArray, FILE *fp)
{
	int OldLocation, CurrentLocation, NewLocation;
	int counter = 0;
	
	//Complexity of N

	//For the start of array
	OldLocation		= 0;
	CurrentLocation = 0;
	NewLocation		= myArray[CurrentLocation];
	
	//
	while (NewLocation < myArray.size() && counter <= myArray.size() )
	{
		OldLocation		= CurrentLocation;
		CurrentLocation = NewLocation;
		NewLocation		= myArray[CurrentLocation];
		if (NewLocation == OldLocation)
		{
			break;
			//return counter;
		}
		else
		{
			counter++;
		}
	}
	fprintf(fp,"Counter: %d\n", counter);
}

//Overall Complexity of N
void PointersExercise::NumberOfUniqueDigits(int N, FILE *fp)
{
	//Complaxity of 1
	fprintf(fp, "Number: %d\n", N);
	
	//Complaxity of 1
	std::stringstream NasString;
	NasString << N;

	//Complaxity of 1
	std::string output = NasString.str();
	
	//Complaxity of 1
	std::vector<char> data(output.begin(), output.end());;
	//Complaxity of 1
	std::vector<char> UniqueData;

	//Complaxity of N
	//Traverse the vector of char
	for (std::vector<char>::const_iterator it = data.begin(); it != data.end(); it++)
	{
		//Complaxity of N-1
		fprintf(fp, "Char %c\n", *it);

		//Complaxity of N-1
		//Add only unique chars to UniqueData vector
		if (std::find(UniqueData.begin(), UniqueData.end(), *it) == UniqueData.end()  && *it != '-' )
		{
			UniqueData.push_back(*it);
		}
	}
	//Complaxity of 1
	fprintf(fp, "Number of Unique digits: %d in %d", UniqueData.size(), N);
}


void PointersExercise::DoublePointerExercise(FILE *fp)
{
	fprintf(fp, "***************************\n* Double Pointer Exercise *\n***************************\n\n");
	
	char *charBufferPointer = new char[100];
	fprintf(fp, "char *charBufferPointer = new char[100]\n");
	fprintf(fp, "    charBufferPointer --> Address|Value\t[%08X] %08X\n\n", &charBufferPointer, charBufferPointer);
	
							// | XXXX: Address	| Value |	
	char temp = 'A';		// | 1001: temp		| 'A'   |
	char *pChar = &temp;	// | 1002: pChar	| 1001	|
	char **ppChar = &pChar; // | 1003: ppChar	| 1002	|
	char temp2 = 'B';		// | 1004: temp2	| 'B'   |
	char *pChar2 = &temp2;  // | 1005: pChar2	| 1004	|
	
	fprintf(fp, "                        // | XXXX: Address | Value |\n");	
	fprintf(fp, "    char temp = 'A';    // | 1001: temp    | 'A'   |\n");
	fprintf(fp, "  char *pChar = &temp;  // | 1002: pChar   | 1001  |\n");
	fprintf(fp, "char **ppChar = &pChar; // | 1003: ppChar  | 1002  |\n");
	fprintf(fp, "   char temp2 = 'B';    // | 1004: temp2   | 'B'   |\n");
	fprintf(fp, " char *pChar2 = &temp2; // | 1005: pChar2  | 1004  |\n\n");
	
	fprintf(fp, "          char temp--> Address|Value\t[%08X] %c\n", &temp, temp);
	fprintf(fp, "             *pChar--> Address|Value\t[%08X] %08X\n", &pChar, pChar);
	fprintf(fp, "         char temp2--> Address|Value\t[%08X] %c\n", &temp2, temp2);
	fprintf(fp, "            *pChar2--> Address|Value\t[%08X] %08X\n", &pChar2, pChar2);
	fprintf(fp, "           **ppChar--> Address|Value\t[%08X] %08X\n", &ppChar, ppChar);
	fprintf(fp, "          (*ppChar)--> %08X //Value of pChar\n", *ppChar);
	fprintf(fp, "     (char*)&ppChar--> %08X //Address of ppChar\n", (char*)&ppChar);
	fprintf(fp, "     (char&)*ppChar--> %08X //Value of pChar...Notice: it only printed 1 byte b/c char is size of 1 byte\n",(char&)*ppChar);
	fprintf(fp, "(long long&)*ppChar--> %08X //Value of pChar\n\n",(long long&)*ppChar);
	//fprintf(fp, " &(*ppChar)--> Address|Value\t[%08X] %08X\n", (char&)(*ppChar), *ppChar);
	
	*ppChar = pChar2;
	fprintf(fp, "After *ppChar = pChar2 //*ppChar (which is talking about \"pChar\") Value of pChar2. \n\n");
	
	fprintf(fp, "          char temp--> Address|Value\t[%08X] %c\n", &temp, temp);
	fprintf(fp, "             *pChar--> Address|Value\t[%08X] %08X\n", &pChar, pChar);
	fprintf(fp, "         char temp2--> Address|Value\t[%08X] %c\n", &temp2, temp2);
	fprintf(fp, "            *pChar2--> Address|Value\t[%08X] %08X\n", &pChar2, pChar2);
	fprintf(fp, "           **ppChar--> Address|Value\t[%08X] %08X\n", &ppChar, ppChar);
	fprintf(fp, "          (*ppChar)--> %08X //Value of pChar\n", *ppChar);
	fprintf(fp, "     (char*)&ppChar--> %08X //Address of ppChar\n", (char*)&ppChar);
	fprintf(fp, "     (char&)*ppChar--> %08X //Value of pChar...Notice: it only printed 1 byte b/c char is size of 1 byte\n",(char&)*ppChar);
	fprintf(fp, "(long long&)*ppChar--> %08X //Value of pChar\n\n",(long long&)*ppChar);

	char *pToAddress = *ppChar;
	fprintf(fp, "char *pToAddress = *ppChar; // *ppChar returns a single pointer\n");
	fprintf(fp, "    *pToAddress--> Address|Value\t[%08X] %08X\t*pToAddress: %c\n\n", &pToAddress, pToAddress, *pToAddress);

	
	char *pppChar; 
	fprintf(fp, "pppChar--> Address|Value\t[%08X] NULL\n", &pppChar);
	this->DoublePointerTest(fp,&pppChar); //passing Pointer to the Address of pppChar
	fprintf(fp, "pppChar--> Address|Value\t[%08X] %08X  and deref the *pppChar: %c\n", &pppChar, pppChar, *pppChar);	
}

void PointersExercise::DoublePointerTest(FILE *fp, char **pChar)
{
	*pChar = new char[10];
	*pChar = "AnkitAmin0";
	//*(pChar[1]) = 'f';
	fprintf(fp, "  pChar--> Address|Value\t[%08X] %08X\n", &pChar, pChar);
	fprintf(fp, " *pChar--> %08X\n", *pChar);
	fprintf(fp, "**pChar--> %c\n", **pChar);
}

//char PointersExercise::*Alloc_AA(int n, FILE *fp)
//{
//	if (allocbuf + ALLOCSIZE - allocp >= n) 
//	{
//		fprintf(fp, "allocbuf: %s \t ALLOCSIZE:  %d \t allocp: %s \t%d \t", allocbuf, ALLOCSIZE, allocp, allocbuf + ALLOCSIZE - allocp);
//
//		allocp += n;
//		return allocp - n;
//	}
//	else
//	{
//		return 0;
//	}
//}
//
//void PointersExercise::Afree_AA (char *p)
//{
//	if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
//	{
//		allocp = p;
//	}
//}
