#include "stdafx.h"

#pragma once

typedef struct Node
{
	int data;
	Node* next;
} Node;


class LinkedListExercises
{
public:
	LinkedListExercises(void);
	~LinkedListExercises(void);
	
	int StartLL();
	
	Node* CreateLL(int sizeOfLL);
	
	Node* CreateNode(int data);

	void AddToEnd( Node* head, Node* node);
	
	Node* AddToFront( Node* head, Node* newnode);
	
	void DelFromEnd( Node* node);
	
	Node* DelFromFront( Node* node);
	
	void PrintLL(char* str, Node* node, FILE *out);
	
	Node* ReverseLL( Node* node);
	
	int LLSize( Node* list);
	
	Node* HighestNode( Node* list);
	
	Node* MergeSort (Node* list);
	
	void InsertAfterNode(Node* head, Node* nodeToInsert, int data);
};

