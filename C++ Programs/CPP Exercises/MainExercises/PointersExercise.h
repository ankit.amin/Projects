#include "stdafx.h"
#include <vector>

#pragma once

#define ALLOCSIZE 21

class PointersExercise
{
public:
	PointersExercise(void);
	~PointersExercise(void);

	//static char allocbuf[ALLOCSIZE] = "Hello_My_Name_Is_AMA";
	//static char *allocp = allocbuf;
	//char *Alloc_AA(int n, FILE *fp);
	//void Afree_AA (char *p); 
	
	void StartPointerExercises();
	
	void SimpleIntegerPointerExercise(FILE *fp);
	void OneDIntegerPointerExercise(FILE *fp);
	void TwoDIntegerPointerExercise(FILE *fp);
	
	void CharacterPointerExercise(FILE *fp);
	
	void StringCopy ( char *s , char *t, FILE *fp);

	void PrintDivider(FILE *fp);
	void PassingCharPtrToCharBufferArray(const char *pChar, FILE *fp);

	void PatternFoundAfter( const std::vector<int> &myArray, FILE *fp);
	void NumberOfUniqueDigits(int N, FILE *fp);

	void DoublePointerExercise(FILE *fp);
	void DoublePointerTest(FILE *fp,char **pChar);

	//virtual void dummy();
};

