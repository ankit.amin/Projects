#include "stdafx.h"

#pragma once

#define BIG_ENDIAN 0
#define LITTLE_ENDIAN 1

typedef struct StructA
{
	unsigned int a; //1st in the memory
	unsigned int c; //2nd in the memory
	unsigned int d; //3rd in the memory
	unsigned char *b; //4th in the memory
	//unsigned long e; //5th in the memory
} StructA;

class StructExercises
{
public:
	StructExercises(void);
	~StructExercises(void);
	
	void StartStructExercise();
	void ShiftExample (void * p, FILE *fp);
	void StructSerialization(FILE *fp);
	int TestByteOrder(FILE *fp);
	int FindNumberOfSetBitsIn(int number);
	char* Decimal2Binary(int n);

};

