#pragma once
#include "PointersExercise.h"

class ReferencesExercises : public PointersExercise
{
public:
	
	void IntegerReferenceExercise();

	ReferencesExercises(void);
	~ReferencesExercises(void);

	void PassingArgsByRef(int& var1);
	void PassingArgsByPointers(int *var1);
	int counter;
};

