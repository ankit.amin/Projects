#include "StdAfx.h"
#include "ReferencesExercises.h"
#include <stdio.h>
#include <stdlib.h>

#include <iostream>

using namespace std;
/*******************************************************************************************/
//Example of Extern Reference 
//We will print the value in PointersExercise.cpp/SimpleIntegerPointerExercise()
//and ExerciseGUI.cpp
int xys = 55;
int &externReference = xys;
/*******************************************************************************************/

ReferencesExercises::ReferencesExercises(void)
{
}


ReferencesExercises::~ReferencesExercises(void)
{
}

void ReferencesExercises::IntegerReferenceExercise()
{
	FILE *fp;
	if ( (fp = fopen("References Exercise.txt", "w") ) == NULL )
	{
		printf("Cannot open Pointers Exercise.txt.\n");
		exit(1);
	}
	fprintf(fp, "References Example\n************************\n");	
	
	int a = 5;
	int *pointerToA = &a; //Valid
	int &referenceToA = a; //Valid
	//int &referenceToA = &pointerToA; //Invalid (makes this a double pointer. you could dereference it in correct way but it will be tooo messy.
	//int &referenceToA = *pointerToA; //Valid

	//*pointerToA = 4; //Valid
	//referenceToA = 4; //Valid

	int b = 165;
	int *pointerToB = &b;

	// Following is "Valid but INCORRCT". 
	// referenceToB = to the "address of B" and not "the value of B"
	// AKA. referenceToB is referencing pointerToB not B.
	int &referenceToB = (int &) pointerToB;	
	fprintf(fp, "int &referenceToB = (int &) pointerToB; //Valid but INCORRECT result \n and the output is ... %d (%08X)\n \n", referenceToB, referenceToB);	
	
	// Following is "Valid and CORRECT". 
	// referenceToB is not referecing to B. 
	// referenceToB = to "value of B" (in this case it is 145).
	referenceToB = (int &) *pointerToB;	
	fprintf(fp, "referenceToB = (int &) *pointerToB; //Valid and CORRECT result \n and the output is ... %d (%08X)\n\n", referenceToB, referenceToB);	
	
	this->PrintDivider(fp); //Call the PrintDivider from PointersExercises class.

	//fprintf(fp, "File: %s\n", __FILE__);
	//fprintf(fp, "Function: %s\n", __FUNCTION__);

	/**********************************************************************************************************/
	//How-To pass REFERENCEs arguments
	fprintf(fp, "Following exercise shows how-to pass REFERENCE arguments in to a function\n");
	fprintf(fp, "For our exercise following function is used...\n\tvoid PassingArgsByRef( int& var1);\n");
	fprintf(fp, "Using following...\nint i = 5;\nint *pI = &i;\nint &refI = i;\n\n");  
	
	int i = 5;
	int *pI = &i;
	int &refI = i;  
	
	fprintf(fp,"Value of i: %d before passing into function()\n", i);
	
	counter = 1;
	this->PassingArgsByRef(i);			//Value after function execution = 6
	fprintf(fp, "-->Passing (int i = 5) to\tthis->PassingArgsByRef( i );\t\t//Values after function call %d\n", i);	
	
	counter = 2;
	//this->PassingArgsByRef((int&)pI);		// Legal but incorrect...passing the address of pI not the value..Will Cause RUNTIME ERROR
	fprintf(fp, "-->Passing (int *pI = &i) to\tthis->PassingArgsByRef( (int&)pI );\t//Runtime error\n");	
	this->PassingArgsByRef(*pI);	//Value after fuction execution = 7;
	fprintf(fp, "-->Passing (int *pI = &i) to\tthis->PassingArgsByRef( *pI );\t\t//Values after function call %d\n", i);	
	this->PassingArgsByRef((int&)*pI);	//Value after fuction execution = 7;
	fprintf(fp, "-->Passing (int *pI = &i) to\tthis->PassingArgsByRef( (int&)*pI );\t//Values after function call %d. Valid but casting ( (int&)*pI ) to reference not needed\n", i);	

	counter = 3;
	this->PassingArgsByRef(refI);		//Value after fuction execution = 8;
	fprintf(fp, "-->Passing (int &refI = i) to\tthis->PassingArgsByRef( refI );\t\t//Values after function call %d\n\n", i);	

	fprintf(fp, "LESSON LEARN: Functions that take REFERENCE as arguments are expecting contain the VALUE of the argument and NOT ADDRESS!!!!\n");
	this->PrintDivider(fp);
		
	/*************************************************************************************************************/
	//How-To pass POINTERs arguments
	fprintf(fp, "Following exercise shows how-to pass POINTERs arguments in to a function\n");
	fprintf(fp, "For our exercise following function is used...\n\tvoid PassingArgsByPointers(int *var1);\n");
	fprintf(fp, "Using following...\nint ii = 5; \nint *pIInt = &ii;\nint &refII = ii;\nint &refPointer = *pIInt;\nint *pointerToRef;\npointerToRef = &refPointer;\n\n");

	int ii = 5;
	int *pIInt = &ii;
	int &refII = ii;
	int &refPointer = *pIInt;	
	int *pointerToRef;
	pointerToRef = &refPointer;
	
	fprintf(fp, "\n");
	fprintf(fp, "ii's->\t\t\tValue [%08X]\tAddress (%08X)\n", ii, &ii);
	fprintf(fp, "pIInt's->\t\tValue [%08X]\tAddress (%08X)\n", pIInt, &pIInt);
	fprintf(fp, "refII's->\t\tValue [%08X]\tAddress (%08X)\n", refII, &refII);
	fprintf(fp, "refPointer's->\t\tValue [%08X]\tAddress (%08X)\n", refPointer, &refPointer);	
	fprintf(fp, "pointerToRef's->\tValue [%08X]\tAddress (%08X)\n\n",pointerToRef, &pointerToRef);
	
	counter = 1;
	this->PassingArgsByPointers( &ii );
	fprintf(fp, "-->Passing (int ii = 5) to\t\tthis->PassingArgsByPointers( &ii );\t\t//Values after function call %d\n", ii);	

	counter = 2;
	this->PassingArgsByPointers(pIInt);
	fprintf(fp, "-->Passing (int *pIInt = &ii) to\tthis->PassingArgsByPointers( pIInt );\t\t//Values after function call %d\n", ii);	

	counter = 3;
	this->PassingArgsByPointers(&refII);
	//this->PassingArgsByPointers((int*)&refII); //Valid but casting to pointer not needed
	fprintf(fp, "-->Passing (int &refII = ii) to\t\tthis->PassingArgsByPointers( &refII );\t\t//Values after function call %d\n", ii);	
	fprintf(fp, "-->Passing (int &refII = ii) to\t\tthis->PassingArgsByPointers( (int*)&refII );\t//Values after function call %d. Valid but casting ( (int*)&refII ) to pointer not needed\n", ii);	
	fprintf(fp, "-->Passing (int &refII = ii) to\t\tthis->PassingArgsByPointers( (int*)refII );\t//Runtime Error\n", ii);	
	fprintf(fp, "Something to Note on the previous call...\n--> refII = value of that it is referencing and\n--> &refII = address of its location\n\n" );	
	fprintf(fp, "LESSON LEARN: Functions that take POINTERS as arguments are expecting contain the ADDRESS of the argument and NOT VALUE!!!!\n");
	this->PrintDivider(fp);
	fclose(fp);
}

void ReferencesExercises::PassingArgsByRef(int& var1)
{
	switch(counter)
	{
	case 1:
		var1 = 6;
		break;
	case 2:
		var1 = 7;
		break;
	case 3:
		var1 = 8;
		break;
	default:
		var1 = 5;
		break;
	}
}

void ReferencesExercises::PassingArgsByPointers(int *var1)
{
	switch(counter)
	{
	case 1:
		*var1 = 6;
		break;
	case 2:
		*var1 = 7;
		break;
	case 3:
		*var1 = 8;
		break;
	default:
		*var1 = 5;
		break;
	}
}