// MainExercises.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "PointersExercise.h"
#include "LinkedListExercises.h"
#include "StructExercises.h"
#include "ReferencesExercises.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//PointersExercise pc = new PointersExercise(); //Invalid Statement: DOES NOT require initialization
	
	//Pointer *ptrExe requires initialization prior to use, 
	//therefore we HAVE to use "new PointersExercise()"
	//PointersExercise *ptrExe =  new PointerClass();  
	
	PointersExercise *ptrExe = new PointersExercise();
	ptrExe->StartPointerExercises();
	
	LinkedListExercises *llExe = new LinkedListExercises();
	llExe->StartLL();

	StructExercises *structEx = new StructExercises();
	structEx->StartStructExercise();

	//PointersExercise *ptrExe2 = new ReferencesExercises(); 
	//ptrExe2 can not call functions in ReferencesExercises class

	//ReferencesExercises &refExe2 = dynamic_cast<ReferencesExercises &> (*ptrExe2);
	//refExe2.testCall1();

	ReferencesExercises *refExe = new ReferencesExercises();
	refExe->IntegerReferenceExercise();


	return 0;
}

