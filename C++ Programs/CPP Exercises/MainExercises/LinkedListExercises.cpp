#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>

#include "LinkedListExercises.h"

LinkedListExercises::LinkedListExercises(void)
{
}


LinkedListExercises::~LinkedListExercises(void)
{
}

int LinkedListExercises::StartLL()
{
	Node *head, *ptr;
	
	FILE *out;
	if ( (out = fopen("Linked List Exercise.txt", "w") ) == NULL )
	{
		printf("Cannot open Linked List Exercise.txt.\n");
		exit(1);
	}
	
	//Create the Linked List
	head = this->CreateLL(5); 
	//Print the List
	this->PrintLL("Initial List: " , head, out);	
	
	//Create new node
	//Add new node to end of the list and Print the list
	ptr = this->CreateNode(97);
	this->AddToEnd(head, ptr);
	this->PrintLL("Added a new node: ", head, out);

	//Create new node
	//Add new node to end of the list and Print the list
	this->AddToEnd(head, this->CreateNode(96));
	this->PrintLL("Added another new node: ", head, out);

	//Delete from the End
	this->DelFromEnd(head);
	this->PrintLL("Deleted last node: ", head, out);

	//Add to the front of the list
	head = this->AddToFront(head, this->CreateNode(50));
	this->PrintLL("Added node to front of the list: ", head, out);

	//Add to the front of the list
	head = this->AddToFront(head, this->CreateNode(654));
	this->PrintLL("Added another node to front of the list: ", head, out);
	
	//Delete a node from the front
	head = this->DelFromFront(head);
	this->PrintLL("Deleted a node from the front: ", head, out);

	//Reverse the list
	head = this->ReverseLL(head);
	this->PrintLL("Reversed List: ", head, out);

	this->InsertAfterNode(head, this->CreateNode(72311), ptr->data);
	this->PrintLL("After Inserted Node List: ", head, out);

	ptr = this->HighestNode(head);
	fprintf(out, "Highest Node: \n%d \n", ptr->data);

	ptr = this->MergeSort(head);
	this->PrintLL("Left: ", ptr, out);
	
	fclose(out);
	
	return 0;
}

Node *LinkedListExercises::CreateLL(int sizeOfLL)
{
	Node *head, *ptr;
	
	head = (Node*)malloc( sizeof( Node ) );
	head->data = 100*sizeOfLL;
	head->next = this->CreateNode(111*sizeOfLL);
	
	for (int i = 0; i < sizeOfLL; i++)
	{
		//Create new node
		//Add new node to end of the list and Print the list
		ptr = this->CreateNode((i+1)*sizeOfLL);
		this->AddToEnd(head, ptr);
	}
	return head;
}

//Node* LinkedListExercises::CreateLL( int data, int sizeOfLL)
//{
//	return NULL;
//}

Node* LinkedListExercises::CreateNode(int data)
{
	struct Node* newNode;

	if( !(newNode = (struct Node*)malloc( sizeof( struct Node ) )))
	{
		return NULL;
	}
	newNode->data = data;
	newNode->next = NULL;
	return newNode;
}

void LinkedListExercises::AddToEnd( Node* head, Node* node)
{
	//Verify the head or node is not NULL
	if(head != NULL && node != NULL)
	{
		//Traverse the list until last node (head->next == NULL)
		while(head->next != NULL)
		{
			head = head->next;
		}
		//Assign last node's next(head->next) to new node.
		head->next = node;
	}
}

Node* LinkedListExercises::AddToFront( Node* head, Node* newnode)
{
	//Set the newnode's next to head
	newnode->next = head;
	//Set head = newnode
	head = newnode;
	//Return the new head of the list
	return head;
}

void LinkedListExercises::DelFromEnd( Node* node)
{
	if (node != NULL)
	{
		//Step 1. Loop until 2nd to the last node 
		while (node->next->next != NULL)
		{
			node = node->next;
		}
		
		//if the node is 2nd to the last node then 
		//Step 2. free the last node and 
		//Step 3. set the node->next = NULL 
		free(node->next);
		node->next = NULL;	

		/* Alternative method 
		 *
		while (node->next != NULL)
		{
			//Step 1. Check for 2nd to the last node
			if (node->next->next == NULL)
			{
				//if the node is 2nd to the last node then 
				//Step 2. free the last node and 
				//Step 3. set the node->next = NULL 
				free(node->next);
				node->next = NULL;	
			}
			else //set the node to node->next
				node = node->next;
		}
		*/
	}

}

Node* LinkedListExercises::DelFromFront( Node* node)
{
	//Allocate the space for temp node
	Node* temp = (Node*)malloc( sizeof( Node ) );
	//Set the temp node to head
	temp = node;
	//Set head (node) to head->next
	//A.K.A setting second element in the list as the new head of the list.
	node = node->next;
	//Free up the old list
	free(temp);
	
	//Return the new head of the list.
	return node;
}

void LinkedListExercises::PrintLL(char* str, Node* node, FILE *out)
{
	fprintf(out, "%s\n", str);

	while(node != NULL)
	{
		fprintf( out, "%d, ", node->data );
		node = node->next;
	}
	fprintf(out, "\n");
}

Node* LinkedListExercises::ReverseLL( Node* node)
{
	Node* previous = NULL; 
	Node* current = node;
	Node* forward;

	while(current != NULL)
	{
		//Keep next node since we trash the next pointer.
		forward = current->next;
		//Switch the next pointer to point backwards.
		current->next = previous;

		//Move both pointers to the next element. 
		previous = current;
		current = forward;
	}
	return previous;
}

int LinkedListExercises::LLSize( Node* list)
{
	int count = 0;

	while (list != NULL)
	{
		count++;
		list= list->next;
	}
	return count;
}

Node* LinkedListExercises::HighestNode( Node* list)
{
	Node* temp = (Node*)malloc( sizeof(Node) );
	temp->data = 0;
	temp->next = NULL;
	int counter = LLSize(list);

	for (int i = 0; i< counter ; i++)
	{
		if(temp->data <= list->data)
			temp->data = list->data;
		list = list->next;
	}
	return temp;
}

Node* LinkedListExercises::MergeSort (Node* list)
{
	int counter = LLSize(list);
	
	if (counter <= 1)
	{
		return list;
	}

	Node* left; 

	int dividedLength = counter/2;

	for (int i = 0; i< dividedLength; i++)
	{
		if (i == 0)
		{
			left = this->CreateNode(list->data);
		}
		Node* previous = left;
		previous->next = list->next;
		list = list->next;		
	}

	//cout << endl << "Left: " << endl;
	//printLL("Left: ", left);
	
	return left;
}

void LinkedListExercises::InsertAfterNode(Node* head, Node* nodeToInsert, int data)
{
	while (head != NULL)
	{
		if (head->data == data)
		{
			//create a temp node and assign head->next 
			//This way we can reassign  head->next to point to new node
			//and new node's next (nodeToInsert->next) point to temp(which is pointing to original head->next)
			struct Node* temp = head->next;
			head->next = nodeToInsert;
			nodeToInsert->next = temp;
		}
		//Move the head to next node.
		head = head->next;
	}
}
