#include "StdAfx.h"
#include <iostream>
#include "StructExercises.h"

using namespace std;

StructExercises::StructExercises(void)
{
}


StructExercises::~StructExercises(void)
{
}


void StructExercises::StartStructExercise()
{
	FILE *fp;
	if ( (fp = fopen("Struct Exercise.txt", "w") ) == NULL )
	{
		printf("Cannot open Pointers Exercise.txt.\n");
		exit(1);
	}	
	
	if (this->TestByteOrder(fp)) 
	{
		fprintf(fp, "This system is Little Endian");
	}
	else
	{
		fprintf(fp, "This system is Big Endian");
	}

	fprintf(fp, "\n\n*****************************\n Shifting Example \n*****************************\n");
	//Integer to Shift.
	int x = 6;
	this->ShiftExample(&x, fp);


	fprintf(fp, "\n\n*****************************\n Number of Set Bits Example \n*****************************\n");
	int num = 12355;
	fprintf(fp, "Number of bits set high in %d is %d.", num, this->FindNumberOfSetBitsIn(num));


	fprintf(fp, "\n\n*****************************\n Struct Serialization Example \n*****************************\n");
	this->StructSerialization(fp);	

	fprintf(fp, "\n\n*****************************\n Converting Decimal to Binary Example \n*****************************\n");
	fprintf(fp, "Coverting %d to decimal %s\n",352123,this->Decimal2Binary(352123));

	/*fprintf(fp, "Output: %d\n", 3 & 1);
	fprintf(fp, "Output: %d\n", (3 >> 2) & 1);*/

	fclose(fp);
}

void StructExercises::ShiftExample (void *p, FILE *fp)
{	
	unsigned int dataToShift = *(unsigned int*)p;
	unsigned int mask = ( ( *(unsigned int*)(p) ) << 7 );
	
	fprintf(fp, "dataToShift: %08X \t mask: %08X\n", dataToShift, mask);
	
	//dataToShift = dataToShift | mask;
	dataToShift = (dataToShift >> 8) | (dataToShift << 8); //short
	//dataToShift =  (dataToShift>>24) | ( (dataToShift<<8) & 0x00FF0000 ) | ( (dataToShift>>8) & 0x0000FF00 ) | (dataToShift<<24);
	
	fprintf(fp, "After shifting the data: = %08X\n", dataToShift);


	int mysteriousValue = 115;
	int mask_ = 0xFF;
	fprintf(fp, "mysteriousValue: = %08X\n", (mysteriousValue & mask_));
}

void StructExercises::StructSerialization(FILE *fp) 
{
	StructA sA;
		
	unsigned char temp[] = "Ankit";
	
	sA.a = 103;  // = 0x67 hex = g
	sA.c = 104;  // = 0x68 hex = h
	sA.d = 86;   // = 0x56 hex = V
 	sA.b = (unsigned char *)&temp;
	//sA.e = 33; //= 21 Hex

	fprintf (fp, "Size of sA.a = %d \n", sizeof(sA.a) );
	fprintf (fp, "Size of sA.b = %d \n", sizeof(sA.b) );
	fprintf (fp, "Size of sA.c = %d \n", sizeof(sA.c) );
	fprintf (fp, "Size of sA.d = %d \n", sizeof(sA.d) );
	//fprintf (fp, "Size of sA.e = %d \n", sizeof(sA.e) );
	fprintf (fp, "Size of sA = %d \n", sizeof(sA));

	unsigned char* unsignedCharPtrToStruct =  (unsigned char *) &sA;
	
	fprintf(fp, "\nSerialization of struct vertically\n");
	for (int i = 0;  i < sizeof(sA); i++)
	{
		fprintf(fp, "unsigned char *unsignedCharPtrToStruct[%d] = %02X // '%c' \n", i, *unsignedCharPtrToStruct, (char)(*unsignedCharPtrToStruct) );
		unsignedCharPtrToStruct++;
	}
	
	fprintf(fp, "\n\nString sA.b is a actually a  two dimensional array, therefore\n");
	fprintf(fp, "unsigned char * needs to travel horizotally to get the  bit representation of \"Ankit\" \n");
	//String sA.b is a actually a  two dimensional array. Therefore unsigned char * needs to travel horizotally to get the 
	//bit representation of "Ankit"
	unsigned char* unsignedCharPrtToStructString = (unsigned char *)&sA.b[0];
	
	for (int i = 0;  i <= sizeof(sA.b); i++)
	{
		fprintf (fp, "Unsigned char array: *unsignedCharPrtToStructString[%d] = %08X // '%c'\n",i, *unsignedCharPrtToStructString, (char)(*unsignedCharPrtToStructString) ); 
		unsignedCharPrtToStructString++;
	}

	
}

int StructExercises::TestByteOrder(FILE *fp) 
{
	int w = 0x4148; //41 = A;  48 = H
 	unsigned char *b = (unsigned char*) &w;

	fprintf(fp, "w = 0x4148 \n*b++:%08X \t *b:%08X \n", *b++, *b );

	return (b[0] ? LITTLE_ENDIAN : BIG_ENDIAN);
}


int StructExercises::FindNumberOfSetBitsIn( int number)
{
	int counter = 0;
	
	while(number)
	{
		//Bitwise and with x and x-1
		number = number & (number-1);
		counter++;
	}
	return counter;
}


char* StructExercises::Decimal2Binary(int n)
{
	int c, d, count;
	char *pointer;
	
	count = 0;
	pointer = (char*)malloc(32+1);
	
	if ( pointer == NULL )
		exit(EXIT_FAILURE);

	for ( c = 31 ; c >= 0 ; c-- )
	{
		d = n >> c;

		if ( d & 1 )
		{
			*(pointer+count) = 1 + '0';
		}
		else
		{
			*(pointer+count) = 0 + '0';
		}
		
		count++;
	}
	*(pointer+count) = '\0';

	return pointer;
}
