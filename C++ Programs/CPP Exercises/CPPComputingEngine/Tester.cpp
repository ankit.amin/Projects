#include "ComputingEngine.h"
#include <iostream>


void main()
{
	int noElements = 5;
	double *myTestArray =  new double[noElements];

	for (int i=0; i< noElements; i++)
	{
		myTestArray[i] = 10.0 + i;
	}
	
	ComputingEngine ce = ComputingEngine();
	std::cout<< "SumOfX_Y: " << ce.SumOfX_Y(40.0, 40.0) << std::endl;

	ComputingEngine cE(myTestArray, noElements);

	std::cout << "Sum: " << cE.SumArray();
	std::cin.get();
	
}