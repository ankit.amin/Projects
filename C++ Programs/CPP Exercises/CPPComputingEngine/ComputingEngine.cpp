#pragma once
#include "ComputingEngine.h"
#include <vector>

/*************************************************************************
Following code is for demonstartion for How-To Export C++ functions to C#
*************************************************************************/
ComputingEngine::ComputingEngine()
{
	x = 0.0;
	y = 0.0;
}

double ComputingEngine::SumOfX_Y(double x_, double y_)
{
	return (x_+y_);
}

ComputingEngine::ComputingEngine(double x_, double y_)
{
	x = x_;
	y = y_;
}

double ComputingEngine::SumOfX_Y()
{
	return (x+y);
}

/*************************************************************************
Following code is for demonstartion for How-To wrap a C++ class	for C#
*************************************************************************/
ComputingEngine::ComputingEngine(double *pDouble, int arraySize)
{
	for( int i = 0; i < arraySize; i++)
	{
		vec.push_back( pDouble[i] );
	}
}

double ComputingEngine::SumArray()
{
	double sum = 0.0;
	for ( int i = 0; i < vec.size(); i++)
	{
		sum = sum + vec[i];
	}
	
	return sum; 

}

ComputingEngine::~ComputingEngine(void)
{
}