#pragma once
#include <vector>

class ComputingEngine
{
public:
	/*************************************************************************
	Following code is for demonstartion for How-To Export C++ functions to C#
	*************************************************************************/
	ComputingEngine();
	double SumOfX_Y(double x_, double y_);
	
	ComputingEngine(double x_, double y_);
	double SumOfX_Y();
		
	/*************************************************************************
	Following code is for demonstartion for How-To wrap a C++ class	for C#
	*************************************************************************/
	ComputingEngine(double *pDouble, int arraySize);
	double SumArray();

	~ComputingEngine(void);

private:

	std::vector<double> vec;
	double x, y;
};

