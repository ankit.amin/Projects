﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Allows us to use DLLImport
using System.Runtime.InteropServices;


namespace CSharpControllingApp
{

    #region Region_ImportingExample_Class
    
    public class ImportExample
    {
        //SumOfTwoNumber() must match the function NAME and SIGNATURE from ExportExample.cpp
        //
        // * This is expecting to find this .dll in the its runtime envrionment otherwise the call to imported function will fail
        //
        [DllImport("CPPClassDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double SumOfTwoNumber(double x_, double y_);

        public ImportExample()
        {
        }

        ~ImportExample() 
        {
        }

        public double ImportFunctionExercise()
        {
            double result = SumOfTwoNumber(50.1, 50.1);
            Console.WriteLine( "ImportExmaple::ImportFunctionExercise\t" + 
                               "FunOutput of Imported SumOfTwoNumber from CPPClassDLL (Dynamic Link Library):  {0}\n", result);
            return result;
        }
 
    }

    #endregion Region_ImportingExample_Class


    #region Region_WrapperExample_Class
   
    public class WrapperExample
    {
        public WrapperExample()
        {
        }

        ~WrapperExample()
        {
        }

        public double WrappingExercise()
        {
            //Wrapper Example
            int numElements = 5;
            double[] myArray = new double[numElements];

            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = i + 1.3;
            }

            /*
             * ********************************************************************************************************
             * HOW-TO WRAP C++ APPLICATION TO C#
             * 
             * 1. C# main controlling engine (Add Reference to C++ Wrapper Class Library generated in 2.) 			
             *      -- Program.cs (Console App .exe generated)
             * 
             *  2. C++ Wrappering Class ( "CLR Class Library" type (generates dll which is referenced in C# project.) ) 	
             *      -- CPPComputingEngineWrapperClass.cpp (CLR Class Library)
             * 
             * 3. C++ Main Computing Class. (This is the functional class ).  							
             *      -- ComputingEngine.cpp (Win32 Console Application) * 
             * ********************************************************************************************************
             */
            double sumOfArray = 0.0;
            unsafe
            {
                fixed (double* pMyArray = &myArray[0])
                {
                    //Creating an object of the Wrapper Class
                    CPPComputingEngineWrapper.CPPComputingEngineWrapperClass wrapperObject = new CPPComputingEngineWrapper.CPPComputingEngineWrapperClass(pMyArray, numElements);

                    //Calling the getSum() method from the CPPComputingEngineWrapperClass 
                    wrapperObject.CalculateSum();

                    //Getting the "sum" variable from the CPPComputingEngineWrapperClass
                    sumOfArray = wrapperObject.sum;
                }
            }
            Console.WriteLine("WrapperExample::WrappingExercise\t" + 
                               "SumOfArray computed using" +
                               " \"CPPComputingEngineWrapper::CPPComputingEngineWrapperClass\" to \"ComputingEngine.cpp\": {0:f}",
                               sumOfArray);

            Console.ReadLine();

            return sumOfArray;
        }
    }
    #endregion Region_WrapperExample_Class

    class Program
    {
        //For Testing purpose only
        static void Main(string[] args)
        {
            ImportExample importExample = new ImportExample();
            importExample.ImportFunctionExercise();


            WrapperExample wrapperExample = new WrapperExample();
            wrapperExample.WrappingExercise();

            Console.ReadLine();

        }
    }
}
