#include "MainController.h"
#include <iostream>

MainController::MainController(void)
{
	this->m_pCallBack = NULL;
	this->m_pUserData = NULL;
}


MainController::~MainController(void)
{
}

int MainController::SetCallBack(MainCTLCallbackFunction *func, void* pUserData)
{
	this->m_pCallBack = func;
	this->m_pUserData = pUserData;

	std::cout << "In MainController::Setback calling m_pCallBack: " << m_pCallBack << " with userdata: " << m_pUserData << std::endl;
	int retval = -1;

	if (m_pCallBack != NULL)
	{
		retval = m_pCallBack(11,12,13.0, 14.0, m_pUserData);
	}
	return retval;	
}