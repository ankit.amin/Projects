#pragma once

typedef int MainCTLCallbackFunction( int x, int y, double dx, double dy, void *pUserData);

class MainController
{
public:
	MainController(void);
	virtual ~MainController(void);

	//int CallingCBFunction(int x, int y, double dx, double dy);
	int SetCallBack(MainCTLCallbackFunction *func, void* pUserData);
	//bool CallingCBFunctionFromClassWithCallBack();

protected:
	MainCTLCallbackFunction *m_pCallBack;
	void *m_pUserData;

};

