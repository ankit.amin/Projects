#pragma once

#include <Windows.h>
#include <tchar.h>
#include <strsafe.h>

#define MAX_THREADS 3
#define BUF_SIZE 255

typedef struct MyData
{
	int x;
	int y;
} *PMYDATA;

DWORD __stdcall ThreadFunction(void *params);

class ThreadLauncher
{
public:
	ThreadLauncher(void);
	virtual ~ThreadLauncher(void);
	void DoWork();


};

