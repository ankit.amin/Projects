#include "../CPPComputingEngine/ComputingEngine.h"
#include "../CPPComputingEngine/ComputingEngine.cpp"

//exporting SumOfTwoNumber() which calls ComputingEngine's SumOfX_Y()
extern "C" __declspec(dllexport) double SumOfTwoNumber(double x_, double y_)
{
	ComputingEngine cE = ComputingEngine(); //Legal notice: lack of "new" keyword....do not need it.
	return cE.SumOfX_Y(x_, y_);
	//ComputingEngine *cE = new ComputingEngine();
	
	//ComputingEngine cE(x_, y_);
	//return cE.SumOfX_Y(); 
}