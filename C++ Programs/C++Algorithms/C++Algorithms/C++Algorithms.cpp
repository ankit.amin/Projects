// C++Algorithms.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include "BFSExample.h"
#include "Graph.h"
using namespace std;

/*
void Merge(int arr[], int low, int high, int mid)
{

	int i, j, k, c[50];
	i = low; //index to left most element in array
	k = low; //index set to left most element in array
	j = mid + 1; //index to middle point of array

	//if i is less than middle pointer AND j is less than MAX of array size
	while (i <= mid && j <= high)
	{
		//if
		if (arr[i] < arr[j])
		{
			c[k] = arr[i];
			k++;
			i++;
		}
		else
		{
			c[k] = arr[j];
			k++;
			j++;
		}
	}

	//Copy rest of the elements from first half the list
	while (i <= mid)
	{
		c[k] = arr[i];
		k++;
		i++;
	}

	//Copy rest of the elements from second half the list
	while (j <= high)
	{
		c[k] = arr[j];
		k++;
		j++;
	}


	//Copy c to original array arr
	for (i = low; i < k; i++)
	{
		arr[i] = c[i];
	}
}*/
void Merge(int a[], int Firstindex, int m, int Lastindex);


//Divide and Conquer 
void MergeSort(int arr[], int leftIndex, int rightIndex)
{
	int middleIndex;
	if (leftIndex < rightIndex)
	{
		middleIndex = leftIndex + (rightIndex - leftIndex) / 2;
		MergeSort(arr, leftIndex, middleIndex);
		MergeSort(arr, middleIndex + 1, rightIndex);
		Merge(arr, leftIndex, middleIndex, rightIndex);
		//merge(a, Firstindex, m, Lastindex);
	}
}

void Merge(int a[], int Firstindex, int m, int Lastindex)
{
	int x;
	int y;
	int z;
	int sub1 = m - Firstindex + 1;
	int sub2 = Lastindex - m;


	//int First[sub1];  //temp array
	//int Second[sub2];

	int First[10];  //temp array
	int Second[10];

	for (x = 0; x < sub1; x++) // copying data to temp arrays
		First[x] = a[Firstindex + x];
	for (y = 0; y < sub2; y++)
		Second[y] = a[m + 1 + y];


	x = 0;
	y = 0;
	z = Firstindex;
	while (x < sub1 && y < sub2)
	{
		if (First[x] <= Second[y])
		{
			a[z] = First[x];
			x++;
		}
		else
		{
			a[z] = Second[y];
			y++;
		}
		z++;
	}
	while (x < sub1)
	{
		a[z] = First[x];
		x++;
		z++;
	}
	while (y < sub2)
	{
		a[z] = Second[y];
		y++;
		z++;
	}
}



int main()
{
	/*
	int myarray[30], num;
	cout << "Enter number of elements to be sorted:";
	cin >> num;
	cout << "Enter " << num << " elements to be sorted:";
	for (int i = 0; i < num; i++) {
		cin >> myarray[i];
	}

	for (auto i : myarray)
	{
		cout << i << " ";
	}
	cout << "\n";
	MergeSort(myarray, 0, num - 1);
	cout << "Sorted array\n";
	for (int i = 0; i < num; i++)
	{
		cout << myarray[i] << "\t";
	}
	*/
	////////////////////////////////////////////////////


	//const int size = 10;
	//
	///*cout << "Enter the size of the Array that is to be sorted: "; 
	//cin >> size;*/
	//
	//int Hello[size], i;
	//
	//cout << "Enter the elements of the array one by one:n";
	//
	//for (i = 0; i < size; i++)
	//{
	//	cin >> Hello[i];
	//}

	//MergeSort(Hello, 0, size - 1);
	//cout << "The Sorted List isn";
	//for (i = 0; i < size; i++)
	//{
	//	cout << Hello[i] << " ";
	//}

	////////////////////////////////////////////////////

	BFSExample* bfs = new BFSExample();
	//bfs->Driver();

	Graph g(4);
	g.addEdge(0, 1);
	g.addEdge(0, 2);
	g.addEdge(1, 2);
	g.addEdge(2, 0);
	g.addEdge(2, 3);
	g.addEdge(3, 3);
	g.BFS(2);
	
	std::cout << "Hello World!\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
