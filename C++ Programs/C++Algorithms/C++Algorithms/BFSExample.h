#pragma once
#include <iostream>
#include <vector>
#include <queue>

using namespace std;
class BFSExample
{

public:
	BFSExample();
	virtual ~BFSExample() {}

	void Edge(int a, int b);
	void BFS(int u);
	void Driver();

	vector <bool> visited;
	vector <vector<int>> g;
};

