#include "BFSExample.h"
//#include <bits/stdc++.h> 

//#define pb push_back 
using namespace std;

BFSExample::BFSExample()
{

}

void BFSExample::Edge(int a, int b)
{
    g[a].push_back(b);

    // for undirected graph add this line 
    // g[b].pb(a); 
}

void BFSExample::BFS(int u)
{
    queue<int> q;

    q.push(u);
    visited[u] = true;

    while (!q.empty()) 
    {
        int f = q.front();
        q.pop();

        cout << f << " ";

        // Enqueue all adjacent of f and mark them visited  
        for (auto i = g[f].begin(); i != g[f].end(); i++) 
        {
            if (!visited[*i])
            {
                q.push(*i);
                visited[*i] = true;
            }
        }
    }
}

// Driver code 
void BFSExample::Driver()
{
    int n, e;
    cin >> n >> e;

    visited.assign(n, false);
    g.assign(n, vector<int>());

    int a, b;
    for (int i = 0; i < e; i++) 
    {
        cin >> a >> b;
        Edge(a, b);
    }

    for (int i = 0; i < n; i++) 
    {
        if (!visited[i])
        {
            BFS(i);
        }
    }
}