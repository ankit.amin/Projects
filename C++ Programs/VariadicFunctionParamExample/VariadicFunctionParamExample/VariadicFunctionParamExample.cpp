// Temp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdarg.h>
#include <stdio.h>

using namespace std; 

/***********************************************************************************/
/* 
 * C++ Interface 
 */
class XYZ
{
public:
    /* Title: How to write a defaults for function parameter
     * Rules: Default parameters go at the end of the function param list.
     */
    virtual void DefaultFunctionParam(int x, int y, const char* c = "name") = 0;
    virtual void VariadicFunctionParam(int numberOfPairs, ...) = 0;
};

/***********************************************************************************/
/*
 * Implementing C++ Interface
 */
class ABC : public XYZ
{
public:
    /* Title: How to write a defaults for function parameter
     * Rules: Default parameters go at the end of the function param list.
     *        We MUST HAVE default parameter listed otherwise function override is not valid
     */
    virtual void DefaultFunctionParam(int x, int y, const char* c = "ANKIT");

    /*
     *
     */
    virtual void VariadicFunctionParam(int numberOfPairs, ...);
};

//CPP Impl.

// Function Implementation 
void ABC::DefaultFunctionParam(int x, int y, const char* c)
{
    cout << c << endl;
}

// Function Implementation 
void ABC::VariadicFunctionParam(int numberOfPairs, ...)
{
    //Get the list of variadic arguments
    va_list ap; 
    
    //Temp variable holding output of arguments
    char* arg; 

    //Start traversing the va_list 
    va_start(ap, numberOfPairs); 

    //Print of the va_arg
    for (int i = 0; i < numberOfPairs; i++)
    {
        arg = va_arg(ap, char*);
        cout << arg << endl;
    }

    // End va_end list
    va_end(ap);
}

/***********************************************************************************/
double average(int count, ...) {
    va_list ap;

    double sum = 0;

    va_start(ap, count); /* Requires the last fixed parameter (to get the address) */
    
    for (int j = 0; j < count; j++) 
    {
        sum += va_arg(ap, int); /* Increments ap to the next argument. */
    }
    va_end(ap);

    return sum / count;
}


/***********************************************************************************/
int main()
{
    ABC* abc = new ABC();
    
    abc->DefaultFunctionParam(1, 2);

    cout << "Avg: " << average(4, 1, 2, 3,4) << endl; 

    abc->VariadicFunctionParam(6, "Raju", "Amin", "Hello" , "Ankit" , "Arjun" , "Aarav");
    
    std::cout << "Hello World!\n";
}