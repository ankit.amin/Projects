// CStringCatchingException.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <afx.h>
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	
	//string s;
	int i;
	//cout << "Type Something: ";
	//getline(cin, s);
	cin >> i; 

	try
	{
		//printf("Hello %s", s.c_str());
		printf("Hello", i);
		//throw;
	}
	catch (CException &e)
	{
		e.ReportError();
	}

	std::getchar();
	return 0;
}

