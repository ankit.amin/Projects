/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hellojni;

/**
 *
 * @author ankit
 */
public class HelloJNIApp 
{
    static 
    {
        System.out.println(System.getProperty("sun.arch.data.model"));
        System.loadLibrary("ExportingFromDLLExample");
    }
    
    private native void sayHello();
    
    public static void main (String[] args)
    {
        new HelloJNIApp().sayHello();
    }
}
