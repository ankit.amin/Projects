using System;
using System.Threading;
using System.Threading.Tasks;
using HubServiceInterfaces;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Clients.ConsoleTwo
{
    public class ClockHubClient : IHostedService //, IClock
    {
        private readonly ILogger<ClockHubClient> _logger;
        private HubConnection _connection;

        public ClockHubClient(ILogger<ClockHubClient> logger)
        {
            _logger = logger;
            var serverURI = Strings.HubUrl;
            serverURI = "https://localhost:5001/hubs/clock";
            Console.WriteLine("SignalR Server Name: {0}", serverURI);
            _connection = new HubConnectionBuilder()
                .WithUrl(serverURI)
                .Build();

            /*
             * This function maps the "ShowTime" and "ReceiveMessage" functions, from 
             * Hub<IClock> interface, to their respective function handlers. 
             * */
            //_connection.On<DateTime>(Strings.Events.TimeSent, ShowTime);
            _connection.On<DateTime>(Strings.Events.TimeSent, ShowTimeHandler);
            _connection.On<string>(Strings.Events.MessageSent, ReceiveMessageHandler);
            //_connection.On<string>(Strings.Events.MessageSent, ReceiveMessage);

        }
        /*
        public async Task ReceiveMessage(string message)
        {
            _logger.LogInformation("Received: {0}", message);
            await _connection.InvokeAsync("SendMessage", "hi ankit here");
        }
        */
        public async Task ReceiveMessageHandler(string message)
        {
            _logger.LogInformation("Received Message from Server: {0}", message);

            /* 
             * This function invokes "SendMessage" function on Server. 
             * Please refer to SendMessage function in ClockHub.cs file 
             * */
            //await _connection.InvokeAsync("SendMessage", "Hello from Client #2");
            await _connection.InvokeAsync("SendMessageWithId", "Hello from Client #2", _connection.ConnectionId);
        }
        /*
        public Task ShowTime(DateTime currentTime)
        {
            _logger.LogInformation("{CurrentTime}", currentTime.ToShortTimeString());

            return Task.CompletedTask;
        }
        */
        public Task ShowTimeHandler(DateTime currentTime)
        {
            _logger.LogInformation("{CurrentTime}", currentTime.ToShortTimeString());

            return Task.CompletedTask;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // Loop is here to wait until the server is running
            while (true)
            {
                try
                {
                    await _connection.StartAsync(cancellationToken);

                    break;
                }
                catch
                {
                    await Task.Delay(1000);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return _connection.DisposeAsync();
        }
    }
}