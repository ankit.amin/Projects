using System;
using HubServiceInterfaces;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Server
{
    public class ClockHub : Hub<IClock>
    {
        //This method gets executed when Client calls them. 
        public async Task SendTimeToClients(DateTime dateTime)
        {
            await Clients.All.ShowTime(dateTime);
        }

        //This method gets executed when Clients calls them. 
        public async Task SendMessage (string message)
        {
            Console.WriteLine("Server::SendMessage:: client== {0} message== {1}", Context.ConnectionId, message);
            await Clients.Caller.ReceiveMessage("Server calling all Client's ReceiveMessage function");
        }

        //This method gets executed when Clients calls them. 
        public async Task SendMessageWithId(string message, string connectionID)
        {
            Console.WriteLine("Server::SendMessageWithId:: message== {0} from Client== {1}", message, connectionID);
            await Clients.Client(connectionID).ReceiveMessage("Hello from Server!!");
        }
    }
}