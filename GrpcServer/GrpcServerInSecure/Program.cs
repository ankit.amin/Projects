﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Helloworld;
using Rpc;

namespace GrpcServerInSecure
{

    class MessageInterventionImpl : EdgeService.EdgeServiceBase
    {
        public override Task<EdgeResponse> Send(EdgeRequest request, ServerCallContext context)
        {
            return null;
        }
        
    }

    class Program
    {
        const int Port = 50051;

        public static void Main(string[] args)
        {
            Server server = new Server
            {
                Services = { Greeter.BindService(new GreeterImpl()) },
                Ports = { new ServerPort("192.168.50.32", Port, ServerCredentials.Insecure) }
            };
            server.Start();

            Console.WriteLine("Greeter server listening on port " + Port);
            Console.WriteLine("Press any key to stop the server...");
            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
}
