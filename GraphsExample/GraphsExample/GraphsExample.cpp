// GraphsExample.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

using namespace std; 

//vector< vector<int> > grid(4, vector<int> (5));
vector< vector<int> > grid{ {1,1,1,1}, 
                            {0,0,1,0},
                            {0,1,0,1},
                            {1,0,1,1} 
                          };

int recurs( vector<vector<int>>bOrig , int i, int j)
{
    auto b = bOrig;
    if (i < 0 || i >= b.size() ||
        j < 0 || j >= b[0].size() ||
        b[i][j] == 0)
    {
        return 0;
    }
    b[i][j] = 0;
    int count = 1; 

    count += recurs(b, i + 1, j);
    count += recurs(b, i - 1, j);
    count += recurs(b, i, j + 1);
    count += recurs(b, i, j - 1);
    
    return count;
}

int numOfIslands(vector<vector<int>> a)
{
    int count = 0;
    for (int i = 0; i < a.size(); i++)
    {
        for (int j = 0; j < a[i].size(); j++)
        {
            if (a[i][j] == 1)
            {
                count = std::max( count, recurs(a, i, j));
            }
        }
    }
    return count;
}


int main()
{
    cout << numOfIslands(grid) << endl;

    std::cout << "Hello World!\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
