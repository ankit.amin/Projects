// BitManipulationExample.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
#include <list>
#include <map>
#include <iomanip>
//#include <bits/stdc++.h> 
//
using namespace std;

int CounterNumberOfOnes(int x)
{
    int counter = 0;

    while (x > 0)
    {
        counter += x & 1; // adding the last significant bit to counter
        x >>= 1;
    }
    return counter;
}

void SetExample()
{
    set<int, greater<int> > s;
    s.insert({45,132,45, 456,897,189,46,9784,89456,465,97});
    
    for (int i : s)
    {
        cout << i << " "; 
    }
    cout << endl;
}

void MapExample()
{
    typedef map<int, int> StringStringMap;

    StringStringMap stocks;

    stocks[987465] = 132;
    stocks[123] = 3542;
    stocks[8793] = 156;

    StringStringMap::iterator position; 
    
    cout << "Left: " << left << endl;
    for (position = stocks.begin(); position != stocks.end(); position++)
    {
        //cout << "First Name: " << setw(12) << position->first << " Last Name: " << position->second << endl;
        cout << "First Name: " << position->first << " Last Name: " << position->second << endl;

    }

}


using namespace std;

// Function to return gcd of a and b 
int gcd(int a, int b)

{
    if (a == 0)
        return b;
    return gcd(b % a, a);
}

// Function to find gcd of array of 
// numbers 
//int findGCD(int arr[], int n)
int findGCD(int *arr, int n)
{
    int result = arr[0];
    for (int i = 1; i < n; i++)
    {
        result = gcd(arr[i], result);

        if (result == 1)
        {
            return 1;
        }
    }
    return result;
}

//// Driver code 
//int main()
//{
//    int arr[] = { 2, 4, 6, 8, 16 };
//    int n = sizeof(arr) / sizeof(arr[0]);
//    cout << findGCD(arr, n) << endl;
//    return 0;
//}

int main()
{
    int x = 1;

    while (x > 0)
    {
        std::cout << "Enter Number: ";
        cin >> x;
        cout << "Number of 1s in " << x << " : " << CounterNumberOfOnes(x) << endl;

        SetExample();
        MapExample();
        
    }

    list<int> first(4, 100);
    list<int> second(first.begin(), first.end());
    list<int> third;

    for (size_t i = 0; i < 10; i++)
    {
        //third.push_back(i); //will add in following order: 0,1,2,3,...9
        third.push_front(i);  //will add in following order: 9,8,7,6,...0
    }


    for (std::list<int>::iterator it = second.begin(); it != second.end(); it++)
    {
        cout << *it << endl;
    }

    for (std::list<int>::iterator it = third.begin(); it != third.end(); it++)
    {
        cout << *it << endl;
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
