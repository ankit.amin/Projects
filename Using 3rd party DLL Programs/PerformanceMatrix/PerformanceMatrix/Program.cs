﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PerformanceMatrix
{
    class Program
    {
        
        static void Main(string[] args)
        {
            ReadSearchListFile rSearchFile = new ReadSearchListFile();
            rSearchFile.PopulateSearchListArray();
            rSearchFile.PrintSearchListArray();                  
        }
    }

    public class ReadSearchListFile
    {
        string[] readLists;
        public string[] SearchList 
        { 
            get
            {
                return readLists;
            }        
        }
        
        string filePath = @"C:\Users\aa185224\Desktop\TraceScripts\traceList.txt";

        public ReadSearchListFile()
        {
            if (!File.Exists(filePath))
            {
                throw new Exception();
            }
        }

        public void PopulateSearchListArray()
        {
            readLists = File.ReadAllLines(filePath);
        }

        public void PrintSearchListArray()
        {
            foreach (string s in readLists)
            {
                Console.WriteLine(s);
            }
        }

    }


}
