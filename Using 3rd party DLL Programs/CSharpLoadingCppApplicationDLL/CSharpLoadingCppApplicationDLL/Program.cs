﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CppCLRApplicationDLL;

// Adding CppCLRApplicationDLL in "Reference"
// Configuration should be x86

namespace CSharpLoadingCppApplicationDLL
{
    class Program
    {
        static void Main(string[] args)
        {
            CallCppObject c = new CallCppObject();
            c.callCppFunction();

            Console.ReadLine();
        }
    }
    
    public class CallCppObject
    {
        public CallCppObject()
        {

        }

        ~CallCppObject()
        {

        }

        public void callCppFunction()
        {
            unsafe
            {
                //C++/CLI Wrapper object
                CppCLRApplicationDLLClass mCppCLRObject = new CppCLRApplicationDLLClass();

                mCppCLRObject.CallCppApplicationFuntion();
               
            }
        }
    }
}
