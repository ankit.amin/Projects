// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CPPAPPLICATIONDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CPPAPPLICATIONDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#pragma once

#ifdef CPPAPPLICATIONDLL_EXPORTS_IMPORTS
#define CPPAPPLICATIONDLL_API __declspec(dllexport)
#else
#define CPPAPPLICATIONDLL_API __declspec(dllimport)
#endif

namespace MyProgram
{
	namespace CppApp
	{
		// This class is exported from the CppApplicationDLL.dll
		class CPPAPPLICATIONDLL_API CCppApplicationDLL
			//class CCppApplicationDLL
		{
		public:
			//CCppApplicationDLL(void);
			//virtual ~CCppApplicationDLL();
			
			void CppFunction1();
			void CppFunction2();
		};
	}
}
////Global functions
//CCppApplicationDLL *gp_CppApplicationDLLObject = nullptr;
//
//extern "C"
//{
//	CPPAPPLICATIONDLL_API CCppApplicationDLL* fnGetCppApplicationDLLObject();
//	CPPAPPLICATIONDLL_API void fnDisposeCppApplicationDLLObject();
//	CPPAPPLICATIONDLL_API void fnGetCppApplicationDLLFunction1();
//	CPPAPPLICATIONDLL_API void fnGetCppApplicationDLLFunction2();
//}