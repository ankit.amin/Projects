// CppApplicationDLL.cpp : Defines the exported functions for the DLL application.
//
#pragma once 
#include "stdafx.h"
#include "CppApplicationDLL.h"
#include <iostream>
using namespace std;
using namespace MyProgram::CppApp;

///****************************************************
//*****************************************************/
//CPPAPPLICATIONDLL_API CCppApplicationDLL* fnGetCppApplicationDLLObject()
//{
//	if (gp_CppApplicationDLLObject == nullptr)
//	{
//		gp_CppApplicationDLLObject = new CCppApplicationDLL();
//	}
//	return gp_CppApplicationDLLObject;
//}
//
///****************************************************
//*****************************************************/
//CPPAPPLICATIONDLL_API void fnDisposeCppApplicationDLLObject()
//{
//	if (gp_CppApplicationDLLObject != NULL)
//	{
//		delete gp_CppApplicationDLLObject;
//		gp_CppApplicationDLLObject = nullptr;
//	}
//}
//
///****************************************************
//*****************************************************/
//CPPAPPLICATIONDLL_API void fnGetCppApplicationDLLFunction1()
//{
//	if (gp_CppApplicationDLLObject != NULL)
//	{
//		gp_CppApplicationDLLObject->CppFunction1();
//	}
//}
//
///****************************************************
//*****************************************************/
//CPPAPPLICATIONDLL_API void fnGetCppApplicationDLLFunction2()
//{
//	if (gp_CppApplicationDLLObject != NULL)
//	{
//		gp_CppApplicationDLLObject->CppFunction2();
//	}
//}

///****************************************************
//// This is the constructor of a class that has been exported.
//// see CppApplicationDLL.h for the class definition
//*****************************************************/
//CCppApplicationDLL::CCppApplicationDLL()
//{
//	//CppFunction1();
//	//CppFunction2();
//	return;
//}
//
///****************************************************
//*****************************************************/
//CCppApplicationDLL::~CCppApplicationDLL()
//{
//
//}

/****************************************************
*****************************************************/
void CCppApplicationDLL::CppFunction1()
{
	std::cout << "Inside: CppFunction1" << endl;
}

/****************************************************
*****************************************************/
void CCppApplicationDLL::CppFunction2()
{
	std::cout << "Inside: CppFunction2" << endl;
}


