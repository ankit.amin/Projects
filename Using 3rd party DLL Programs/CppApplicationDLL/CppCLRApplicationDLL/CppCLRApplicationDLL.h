// CppCLRApplicationDLL.h
#pragma once

//#include "../CppApplicationDLL/CppApplicationDLL.cpp"
//#include "CppApplicationDLL.cpp"

using namespace System;

//This is C++/CLI DLL
namespace MyProgram
{
	namespace CppApp
	{
		class CCppApplicationDLL;

		namespace CppCLRApplicationDLL
		{
			public ref class CppCLRApplicationDLLClass
			{
				// TODO: Add your methods for this class here.
			public:
				CppCLRApplicationDLLClass();
				virtual ~CppCLRApplicationDLLClass();
				!CppCLRApplicationDLLClass();
				
				void CallCppApplicationFuntion();

				void Destroy();

			private:
				CppApp::CCppApplicationDLL* m_cppAppObject;

			};
		}
	}
}
