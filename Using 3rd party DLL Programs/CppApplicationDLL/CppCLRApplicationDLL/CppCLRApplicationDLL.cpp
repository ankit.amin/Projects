// This is the main DLL file.
#include "stdafx.h"
#include "CppApplicationDLL.h"
#include "CppCLRApplicationDLL.h"

using namespace MyProgram::CppApp::CppCLRApplicationDLL;

CppCLRApplicationDLLClass::CppCLRApplicationDLLClass()
{
	m_cppAppObject = new CppApp::CCppApplicationDLL();
}

CppCLRApplicationDLLClass::~CppCLRApplicationDLLClass()
{
	Destroy();
}

CppCLRApplicationDLLClass::!CppCLRApplicationDLLClass()
{
	Destroy();
}

void CppCLRApplicationDLLClass::CallCppApplicationFuntion()
{
	m_cppAppObject->CppFunction1();
}

void CppCLRApplicationDLLClass::CppCLRApplicationDLLClass::Destroy()
{
	if (m_cppAppObject != nullptr)
	{
		delete m_cppAppObject;
		m_cppAppObject = nullptr;
	}
}


