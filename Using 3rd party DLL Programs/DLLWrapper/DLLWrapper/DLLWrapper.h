// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DLLWRAPPER_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DLLWRAPPER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#pragma once
#include "ExportingFromDLLExample.h"
#include <Windows.h>

#ifdef DLLWRAPPER_EXPORTS
#define DLLWRAPPER_API __declspec(dllexport)
#else
#define DLLWRAPPER_API __declspec(dllimport)
#endif

/***********************************************************************
	1. Need to have HANDLE member variable for DLLs
	2. Define a function pointer which are exported in DLL 
	3. Define an instance of function pointer
	4. Instance of DLL's internal object.
	5. Proxy functions to Exported functions
************************************************************************/

/* This class is used to load either Dummy DLL or Actual DLL*/
class CDLLWrapper : public IMyInterface
{
private: 
	
	// 1. Get the Handle for DLL you want to load
	HMODULE m_hDLL;

	// 2. Define a function pointers which are exported in DLL
	typedef IMyInterface* (*GetDLLFunction)(int x);
	
	// 3. An instance of the function pointers from above
	GetDLLFunction m_pfDLLFunction;

	// 4. Instance of internal DLL Object
	IMyInterface* m_DLLObject;

public:
	CDLLWrapper(int x);
	virtual ~CDLLWrapper();

	//
	IMyInterface* GetDLLObject();

	// 5. Proxy functions to Exported Functions.
	int Addition(int x) override;
	int Subtraction(int y) override;
	void Release() override;
};

/*	
	I tried to see if I can call into CDLLWrapper class functions
	directly
*/
//class CDLLWrapper 
//{
//private:
//
//	// 1. Get the Handle for DLL you want to load
//	HMODULE m_hDLL;
//
//	// 2. Define a function pointers which are exported in DLL
//	typedef IMyInterface* (*GetDLLFunction)(int x);
//
//	// 3. An instance of the function pointers from above
//	GetDLLFunction m_pfDLLFunction;
//
//	// 4. Instance of internal DLL Object
//	IMyInterface* m_DLLObject;
//
//public:
//	CDLLWrapper();
//	virtual ~CDLLWrapper();
//
//	//
//	IMyInterface* GetDLLObject();
//
//	// 5. Proxy functions to Exported Functions.
//	int Addition(int x) ;
//	int Subtraction(int y) ;
//	void Release() ;
//};


