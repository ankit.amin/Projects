// DLLWrapper.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "DLLWrapper.h"
#include <cstring>
#include <iostream>
using namespace std;


/***********************************************************************
// This is the constructor of a class that has been exported.
// see DLLWrapper.h for the class definition
***********************************************************************/
CDLLWrapper::CDLLWrapper(int x)
{
	//Following code is used to load the desired library. For this example we are loading dummy application library. 
	//Load the desired library
	//m_hDLL = LoadLibrary(_T("DummyApplicationOne.dll"));
	m_hDLL = LoadLibrary(_T("ExportingFromDLLExample.dll"));
	
	//Get the DLL's function
	m_pfDLLFunction = reinterpret_cast<GetDLLFunction>(GetProcAddress(m_hDLL, "GetMyObject"));
	if (!m_pfDLLFunction)
	{
		return;
	}
	//Call the DLL's function (GetMyObject)
	//Returns a concrate object
	m_DLLObject = m_pfDLLFunction(x);
		
	return;
}


/***********************************************************************

***********************************************************************/
CDLLWrapper::~CDLLWrapper()
{
	delete m_DLLObject;
	m_DLLObject = NULL;

}

/***********************************************************************
//This function acts as a PROXY.
//It forwards the call to actual DLL's object.
***********************************************************************/
int CDLLWrapper::Addition(int x) 
{
	cout << "CDLLWrapper::Addition()" << endl;
	return m_DLLObject->Addition(x);
}


/***********************************************************************
//This function acts as a PROXY.
//It forwards the call to actual DLL's object.
***********************************************************************/
int CDLLWrapper::Subtraction(int y) 
{
	cout << "CDLLWrapper::Subtraction()" << endl;
	return m_DLLObject->Subtraction(y);
}


/***********************************************************************
//This function acts as a PROXY.
//It forwards the call to actual DLL's object.
***********************************************************************/
void CDLLWrapper::Release() 
{
	cout << "CDLLWrapper::Release()" << endl;
	m_DLLObject->Release();
}

/***********************************************************************

***********************************************************************/
IMyInterface* CDLLWrapper::GetDLLObject()
{
	return m_DLLObject;
}


/***********************************************************************/
//Client Application calls the following functions. 
//Global WrapperObject
CDLLWrapper *g_DLLWrapper = nullptr;

/***********************************************************************
//Global get Wrapper Object
//NOTICE: WINAPI is not present in this call.
//VERIFIED WITH DEPENDS.exe
//    -> following function name will not have decorated name i.e. it will show only show "GetWrapperObject"
***********************************************************************/
extern "C" DLLWRAPPER_API IMyInterface* GetWrapperObject(int x)
//extern "C" DLLWRAPPER_API CDLLWrapper* GetWrapperObject()
{
	if (g_DLLWrapper == nullptr)
	{
		g_DLLWrapper = new CDLLWrapper(x);
	}
	return g_DLLWrapper;
}

/***********************************************************************
//Relase Global Wrapper Object
***********************************************************************/
extern "C" DLLWRAPPER_API void ReleaseWrapperObject()
{
	delete g_DLLWrapper;
	g_DLLWrapper = nullptr;
}
