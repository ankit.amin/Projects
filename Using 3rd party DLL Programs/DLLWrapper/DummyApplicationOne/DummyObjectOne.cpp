#include "stdafx.h"
#include "DummyObjectOne.h"
#include <iostream>
using namespace std;

DummyObjectOne::DummyObjectOne()
{
	//m_Object = ::GetMyObject(1);
}


DummyObjectOne::~DummyObjectOne()
{
	//delete m_Object;
	//m_Object = nullptr;
}

int DummyObjectOne::Addition(int x)
{
	cout << "DummyObjectOne::Addition" << endl;
	return x+x;
}

int DummyObjectOne::Subtraction(int y)
{
	cout << "DummyObjectOne::Subtraction" << endl;
	return y*y - y;
}

void DummyObjectOne::Release()
{
	cout << "DummyObjectOne::Release" << endl;
	delete this;
}



/*
 we override the exported GetMyObject function from DLL because this is a DUMMY application
 */
DummyObjectOne* gp_DummyObject = nullptr;

extern "C" APIExpose IMyInterface* GetMyObject(int x)
{
	if (gp_DummyObject == nullptr)
	{
		gp_DummyObject = new DummyObjectOne();
	}
	return gp_DummyObject;

}

