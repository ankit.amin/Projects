#pragma once
#include "ExportingFromDLLExample.h"


/*
This class must also implement IMyInterface::GetMyObject function as well
to return a pointer to DummyObjectOne's object. 
*/
class DummyObjectOne : public IMyInterface
{
public:
	DummyObjectOne();
	virtual ~DummyObjectOne();

	int Addition(int x) override; 
	int Subtraction(int y) override; 
	void Release() override;

};

