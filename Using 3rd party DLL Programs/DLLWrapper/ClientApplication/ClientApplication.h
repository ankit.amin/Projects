#pragma once
#include <windows.h>
#include <tchar.h>
#include <iostream>

//Used for CDLLWrapper* below
//#include "../DLLWrapper/DLLWrapper.h" 
#include "ExportingFromDLLExample.h"

// Get the Handle for DLL for Wrapper DLL
HMODULE hDll;

//Define function pointers to Get Wrapper Object
typedef IMyInterface* (*pFGetWrapperObject)(int x);
//typedef CDLLWrapper* (*pFGetWrapperObject)();

//Define function pointers to Release Wrapper Object
typedef void (*pFReleaseWrapperObject)();

//Instance of GetWrapperObject function pointer
pFGetWrapperObject m_fpWrapperObject;

//Instance of Release Wrapper Object function pointer
pFReleaseWrapperObject m_fpReleaseWrapperObject;

//Get Wrapper Object
IMyInterface* myWrapperObj;
//CDLLWrapper* myWrapperObj;
