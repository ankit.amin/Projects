// ClientApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ClientApplication.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	//Loads the Wrapper DLL
	hDll = LoadLibrary(_T("DLLWrapper.dll"));
	
	if (hDll != NULL)
	{
		//Get the Wrapper Object. 
		m_fpWrapperObject = reinterpret_cast<pFGetWrapperObject>(GetProcAddress(hDll, "GetWrapperObject"));
		if (!m_fpWrapperObject)
		{
			return 0;
		}

		m_fpReleaseWrapperObject = reinterpret_cast<pFReleaseWrapperObject>(GetProcAddress(hDll, "ReleaseWrapperObject"));
		if (!m_fpReleaseWrapperObject)
		{
			return 0;
		}
	}

	int userInput = 0; 
	cin >> userInput;
	//Call the DLL's function (GetWrapperObject)
	myWrapperObj = m_fpWrapperObject(userInput);

	for (size_t i = 0; i < 50; i++)
	{
		//Call DLL's internal object's functions. 
		cout << myWrapperObj->Addition(5) << endl;
		cout << myWrapperObj->Subtraction(50) << endl;
	}
	
	m_fpReleaseWrapperObject();
	
	FreeLibrary(hDll);

	cin.get();
	return 0;
}

