// ClientApplicationUsingDLL.cpp : Defines the entry point for the console application.
//

// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION
// THIS IS JUST TEST APPLICATION

#include "stdafx.h"
#include "ExportingFromDLLExample.h"
#include <iostream>

using namespace std;


/*
Following must be done in order to use ExportingFromDLLExample.dll
1. Add "Reference" to this project
2. Make sure the "Project Depenedencies" configuration are as expected
3. Add "Additional Include Directories" under "C/C++ Properties". This will allow 
   us to #include ExportingFromDLLExample.h file.

*/

typedef void (*gFunc) (int x);
int _tmain(int argc, _TCHAR* argv[])
{
	int x = 0;
	cin >> x;
	while (x >= 1)
	{
		IMyInterface *myObject = GetMyObject(x);
		
		cout << myObject->Addition(x) << endl;
		cout << myObject->Subtraction(x) << endl;
		myObject->Release();
		cout << endl;
		cin >> x;
	}

	return 0;
}