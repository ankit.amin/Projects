#include "stdafx.h"
#include "InternalObjectOne.h"
#include <iostream>


InternalObjectOne::InternalObjectOne(void)
{
}


InternalObjectOne::~InternalObjectOne(void)
{
}

int InternalObjectOne::Addition(int x)
{
	std::cout << "+InternalObjectOne::Addition()" << std::endl;
	return x+x;
}

int InternalObjectOne::Subtraction(int y)
{
	std::cout << "+InternalObjectOne::Subtraction()" << std::endl;
	return y-y-2;
}
	
void InternalObjectOne::Release()
{
	std::cout << "+InternalObjectOne::Release()" << std::endl;
	delete this;
}

