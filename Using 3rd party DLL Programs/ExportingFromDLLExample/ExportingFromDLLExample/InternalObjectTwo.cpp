#include "stdafx.h"
#include "InternalObjectTwo.h"
#include <iostream>

InternalObjectTwo::InternalObjectTwo()
{
}


InternalObjectTwo::~InternalObjectTwo()
{
}

int InternalObjectTwo::Addition(int x)
{
	std::cout << "+InternalObjectTwo::Addition()" << std::endl;
	return x + x;
}

int InternalObjectTwo::Subtraction(int y)
{
	std::cout << "+InternalObjectTwo::Subtraction()" << std::endl;
	return y - y - 2;
}

void InternalObjectTwo::Release()
{
	std::cout << "+InternalObjectTwo::Release()" << std::endl;
	delete this;
}