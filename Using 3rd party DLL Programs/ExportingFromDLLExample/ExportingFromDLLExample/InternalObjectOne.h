#pragma once
#include "ExportingFromDLLExample.h"

class InternalObjectOne: public IMyInterface
{
public:
	InternalObjectOne(void);
	virtual ~InternalObjectOne(void);

	int Addition(int x);
	int Subtraction(int y);
	void Release();

};
