#pragma once
#include "ExportingFromDLLExample.h"

class InternalObjectTwo : public IMyInterface
{
public:
	InternalObjectTwo(void);
	virtual ~InternalObjectTwo(void);

	int Addition(int x);
	int Subtraction(int y);
	void Release();

};
