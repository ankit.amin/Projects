#include "stdafx.h"
#include "InternalMyObject.h"


InternalMyObject::InternalMyObject(void)
{
}


InternalMyObject::~InternalMyObject(void)
{
}

int InternalMyObject::Function1(int x)
{
	return x+x;
}

int InternalMyObject::Function2 (int y)
{
	return y-y-2;
}
	
void InternalMyObject::Release()
{
	delete this;
}

//Factory function returns a concrete object of IMyInterface
//This is the only function that is EXPORTed
//This is needed here. 
APIExpose IMyInterface* WINAPI GetMyObject(VOID)
{
	return new InternalMyObject();
}
