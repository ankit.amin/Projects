#include "stdafx.h"
#include "FactoryMethod.h"

#include "ExportingFromDLLExample.h"
#include "InternalObjectOne.h"
#include "InternalObjectTwo.h"

#include "jni.h"
#include "com_mycompany_hellojni_HelloJNIApp.h"
#include <iostream>

JNIEXPORT void JNICALL Java_com_mycompany_hellojni_HelloJNIApp_sayHello(JNIEnv* env, jobject thisObj)
{
	std::cout << "Hello World from C++" << std::endl;
	return;
	
}


FactoryMethod::FactoryMethod()
{
}


FactoryMethod::~FactoryMethod()
{
}

//Factory function returns a concrete object of IMyInterface
//This is the only function that is EXPORTed
//This is needed here. 
APIExpose IMyInterface*  GetMyObject(int x)
{
	if (x == 1)
		return new InternalObjectOne();
	else
		return new InternalObjectTwo();
}

APIExpose int ExportedFunction1(int x)
{
	return x*x;
}

APIExpose int ExportedFunction2(int x)
{
	return x + x;
}

APIExpose int ExportedFunction3(int x)
{
	return (x + 100) - x;
}

APIExpose int ExportedFunction4(int x)
{
	return (x + 1) / x;
}

APIExpose double ExportedFunction5(double x)
{
	return x / (x + 1);
}