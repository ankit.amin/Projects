

#pragma once
#include "ExportingFromDLLExample.h"

class InternalMyObject: public IMyInterface
{
public:
	InternalMyObject(void);
	virtual ~InternalMyObject(void);

	int Function1(int x);
	int Function2 (int y);
	void Release();

};
