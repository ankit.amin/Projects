#include "pch.h"

#include "MyProjectCLR.h"

using namespace MyProjectCLR;

MyCLRClass::MyCLRClass()
{
	myCLRObject = GetMyObject(1);
}


int MyProjectCLR::MyCLRClass::Addition(int x)
{
	return x;
}

int MyProjectCLR::MyCLRClass::Subtraction(int y)
{
	return y;
}

void MyProjectCLR::MyCLRClass::Release()
{
	Destroy();
}

MyProjectCLR::MyCLRClass::~MyCLRClass()
{
	Destroy();
}

MyProjectCLR::MyCLRClass::!MyCLRClass()
{
	Destroy();
}

void MyProjectCLR::MyCLRClass::Destroy()
{
	if (myCLRObject)
	{
		delete myCLRObject;
		myCLRObject = nullptr;
	}
}