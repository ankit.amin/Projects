#pragma once

using namespace System;
#include "ExportingFromDLLExample.h"

namespace MyProjectCLR
{
	//class IMyInterface;
	public ref class MyCLRClass : public IMyInterface
	{
	public: 
		MyCLRClass();
		virtual ~MyCLRClass();
		!MyCLRClass();

		void Destroy();

		void CallNativeFunction();

		IMyInterface* myCLRObject;

		// Inherited via IMyInterface
		virtual int Addition(int x) override;

		virtual int Subtraction(int y) override;

		virtual void Release() override;

		// TODO: Add your methods for this class here.
	};
}
