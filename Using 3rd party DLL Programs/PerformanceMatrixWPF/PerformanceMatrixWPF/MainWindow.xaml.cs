﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace PerformanceMatrixWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ReadSearchListFile readSearchListFile;

        public long m_currentTime = 0;
        public long m_lastTime = 0;
        public long m_timeDiff = 0;

        //Start Timer
        public long m_startTime = 0;

        //End Timer
        public long m_endTime = 0;

        private List<TabItem> m_tabItemsList;

        public MainWindow()
        {
            InitializeComponent();

            //readSearchListFile = new ReadSearchListFile(@"C:\Users\aa185224\Desktop\TraceScripts\traceList.txt");
            m_tabItemsList = new List<TabItem>();

            //Add initial Tab;
            ICustomTabItem cti = new CustomTabItem();
            m_tabItemsList.Add(cti.MyTabItem);
            this.DynamicTabControl.Items.Add(cti.AddTabItem("new"));
        }
        
        /*
         * Read File Button Click event
         */
        private void ReadFileButton_Click(object sender, RoutedEventArgs e)
        {
            //Get the file from User
            this.FileLocationTextBox.Text = @"C:\Users\aa185224\Desktop\p\Traces.log";                                              
            readSearchListFile = new ReadSearchListFile(this.FileLocationTextBox.Text);
            
            //Populate the SearchList array
            readSearchListFile.PopulateSearchListArray();

            //Get the Search list array;
            string[] tempString = readSearchListFile.SearchList;

            //UpdateRichTextBox
            UpdateRichTextBox(tempString);

            //UpdateTabControl
            //UpdateTabControl();

        }

        //class member variable
        ICustomTabItem m_cti;

        private void UpdateTabControl(ICustomTabItem iCti, Paragraph p)
        {
            //ICustomTabItem cti = new CustomTabItem();
            m_tabItemsList.Add(iCti.MyTabItem);
            this.DynamicTabControl.Items.Add(iCti.AddTabItem(p));
        }

        private void UpdateTabControl()
        {
            ICustomTabItem cti = new CustomTabItem();
            m_tabItemsList.Add(cti.MyTabItem);
            this.DynamicTabControl.Items.Add(cti.AddTabItem("new"));
        }

        /*
         * This method updates RichTextBox
         */
        private void UpdateRichTextBox(string[] stringArray)
        {            
            int numberOfTransactions = 0;

            //Document for RichTextBox
            FlowDocument fDoc = new FlowDocument();

            //Paragraph for the Document
            Paragraph pObject = new Paragraph();
            
            //StringParser
            StringParser sParser;

            long startTime = 0;
            long endTime = 0;
                        
            foreach (string strToParse in stringArray)
            {
                //Parse the string to get the Time
                sParser = new StringParser(strToParse);

                //Lower the case
                string line = strToParse.ToLower();

                if (line.Contains("evt 0, dev 15") ||
                    line.Contains("+dmdispensechange") ||
                    line.Contains("-dmdispensechange") ||
                    line.Contains("changing state to smthankyou,")
                    )
                {
                    //Get New Formatted String with Timediff in front
                    //line = GetNewFormattedString(sParser);

                    if (line.Contains("evt 0, dev 15"))
                    {
                        //Get the starting Time
                        startTime = sParser.GetTime();
                        //Console.WriteLine("StartTime: " + startTime);
                    }

                    if (line.Contains("to smthankyou,"))
                    {
                        numberOfTransactions++;

                        //Format the line and add it to Paragraph object
                        FormatLineInParagraph(pObject, line, false);
                        
                        //Get the End Time
                        endTime = sParser.GetTime();
                        //Console.WriteLine("EndTime: " + endTime);
                        if (startTime > 0)
                        {
                            AddToParagraph( 
                                pObject, 
                                "Total time Device accepting the Cash to Completing Transaction:\t" + (endTime - startTime) + "\n", 
                                true);
                            
                        }

                        startTime = 0;

                        //Send the pObject to m_cti object
                        m_cti = new CustomTabItem();
                        UpdateTabControl(m_cti, pObject);
                                                
                    }//if
                    else
                    {
                        //Add lines to Paragraph 
                        AddToParagraph(pObject, "" + line, false);
                    }//else

                    //Add new line
                    AddToParagraph(pObject, "\n", false);

                    //Add the Paragraph to Document block
                    fDoc.Blocks.Add(pObject);

                }//if
            }//for loop

            //Add the Document to RichTextBox
            this.SearchStringsListRichTextBox.Document = fDoc;
            this.numberOfTranxLabel.Content = numberOfTransactions.ToString();
        }

        /*
         * This method is used to format the line in RichTextBox
         */
        private void FormatLineInParagraph(Paragraph parObject, string strToFormat, bool invertColors)
        {
            //Get the string to Run
            Run f_Run = new Run(strToFormat);

            //Bold the string
            Bold f_Bold;
            
            if (invertColors)
            {
                //Format the bold string
                f_Bold = new Bold(f_Run) { Foreground = Brushes.Black, Background = Brushes.Yellow };
            }
            else
            {
                //Format the bold string
                f_Bold = new Bold(f_Run) { Foreground = Brushes.Yellow, Background = Brushes.Black };
            }
            
            //Add the formatted string to Paragraph Object
            parObject.Inlines.Add(f_Bold);
            
            //Add new line
            parObject.Inlines.Add(new Run("\n"));
        }


        /*
         * This method is used to reformat the strings to add the time differences from previous state or line
         */
        private string GetNewFormattedString(StringParser strParser)
        {
            //GetTime from the String
            m_currentTime = strParser.GetTime();

            //Get the timeDiff (this is an amount of time we are in this state)
            m_timeDiff = m_currentTime - m_lastTime;

            //Concat time and StringToParse
            string newFormattedStr = m_timeDiff.ToString() + "\t" + strParser.StringToParse.ToLower();

            //set the lastTime to currentTime;
            m_lastTime = m_currentTime;
            
            //return the new string with TIME in front
            return newFormattedStr;

        }

        private void AddToParagraph(Paragraph p, string s, bool withStyle)
        {
            if (withStyle)
            {
                p.Inlines.Add( new Run(s) 
                    { Foreground = Brushes.Blue, Background = Brushes.Yellow, FontSize = 16 }
                    );
            }
            else
            {
                p.Inlines.Add(new Run(s));
            }
        }
    }

}
