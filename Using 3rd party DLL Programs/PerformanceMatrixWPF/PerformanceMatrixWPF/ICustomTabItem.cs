﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;


namespace PerformanceMatrixWPF
{
    interface ICustomTabItem
    {
        RichTextBox MyRichTextBox 
        { 
            get; set;
        }

        FlowDocument MyFlowDocument 
        { 
            get; set;
        }

        Paragraph MyParagraph 
        { 
            get; set;
        }

        TabItem MyTabItem
        {
            get; set;
        }

        TabItem AddTabItem(string str);

        TabItem AddTabItem(Paragraph p);
     

    }
}
