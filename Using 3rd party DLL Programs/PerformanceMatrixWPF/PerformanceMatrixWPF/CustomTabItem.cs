﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerformanceMatrixWPF
{
    public class CustomTabItem: ICustomTabItem      
    {
        /*
        public RichTextBox rtb;
        public RichTextBox MyRichTextBox 
        { 
            get
            {
                return this.rtb;
            }

            set
            {
                this.rtb = value; 
            }
        }

        public FlowDocument fd;
        public FlowDocument MyFlowDocument 
        { 
            get
            {
                return this.fd;
            }
            set
            {
                this.fd = value;
            }
        }

        public Paragraph par;
        public Paragraph MyParagraph 
        { 
            get
            {
                return this.par;
            }
            set
            {
                this.par = value;
            }
        }
         
        public CustomTabItem()
        {
            this.rtb = new RichTextBox();
            this.fd = new FlowDocument();
            this.par = new Paragraph();

        }
        
        public CustomTabItem AddTabItem(string str)
        {
            /*
         * Old STUFF for Concept understanding
             * TabItem newTab = new TabItem();
               newTab.Header = "NEW TAB";
               //newTab.HeaderTemplate = this.DynamicTabControl.FindResource("TabHeader") as DataTemplate;

            RichTextBox rtb = new RichTextBox();
            FlowDocument fd = new FlowDocument();
            Paragraph par = new Paragraph();

            par.Inlines.Add(new Run("TEST"));

            fd.Blocks.Add(par);
            rtb.Document = fd;

            newTab.Content = rtb;
             
            */
        /*
            this.Header = "New CustomTabItem";
            this.par.Inlines.Add(new Run(str));
            
            this.fd.Blocks.Add(MyParagraph);
            this.rtb.Document = this.MyFlowDocument;

            this.Content = this.MyRichTextBox;
            
            return this;
        }
         * */

        public RichTextBox rtb;
        public RichTextBox MyRichTextBox
        {
            get
            {
                return this.rtb;
            }

            set
            {
                this.rtb = value;
            }
        }

        public FlowDocument fd;
        public FlowDocument MyFlowDocument
        {
            get
            {
                return this.fd;
            }
            set
            {
                this.fd = value;
            }
        }

        public Paragraph par;
        public Paragraph MyParagraph
        {
            get
            {
                return this.par;
            }
            set
            {
                this.par = value;
            }
        }

        public TabItem ti;
        public TabItem MyTabItem
        {
            get
            {
                return this.ti;
            }
            set
            {
                this.ti = value;
            }
        }

        public CustomTabItem()
        {
            this.ti = new TabItem();
            this.rtb = new RichTextBox();
            this.fd = new FlowDocument();
            this.par = new Paragraph();

        }

        public TabItem AddTabItem(string str)
        {
            this.ti.Header = "New CustomTabItem";
            this.par.Inlines.Add(new Run(str));

            this.fd.Blocks.Add(MyParagraph);
            this.rtb.Document = this.fd;

            this.ti.Content = this.rtb;

            return this.ti;            
        }

        public TabItem AddTabItem(Paragraph p)
        {
            this.ti.Header = "Adding paragraph";
            this.par = p;

            this.fd.Blocks.Add(MyParagraph);
            this.rtb.Document = this.MyFlowDocument;

            this.ti.Content = this.MyRichTextBox;

            return this.ti;
            
        }
    }


}
