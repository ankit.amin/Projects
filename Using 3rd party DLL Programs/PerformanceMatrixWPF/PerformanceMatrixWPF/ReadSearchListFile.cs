﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PerformanceMatrixWPF
{
    public class ReadSearchListFile
    {
        string[] readLists;
        public string[] SearchList
        {
            get
            {
                return readLists;
            }
        }

        //string filePath = @"C:\Users\aa185224\Desktop\TraceScripts\traceList.txt";
        string filePath;

        public ReadSearchListFile(string fPath)
        {
            this.filePath = fPath;
            if (!File.Exists(filePath))
            {
                throw new Exception();
            }
        }

        public void PopulateSearchListArray()
        {
            readLists = File.ReadAllLines(filePath);
        }

        public void PrintSearchListArray()
        {
            foreach (string s in readLists)
            {
                string temp = s.ToLower();

                Console.WriteLine(temp);
            }
        }

    }
}
