﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PerformanceMatrixWPF
{
    public class StringParser
    {
        private long currentTimeStamp;
        public long CurrentTimeStamp 
        {
            get 
            { 
                return currentTimeStamp; 
            }
            set 
            { 
                currentTimeStamp = value; 
            }
        }

        //String to Parse. 
        //This is assigned when 
        private string stringToParse ;
        public string StringToParse 
        {
            get 
            { 
                return stringToParse; 
            }
            set 
            {
                stringToParse = value; 
            }
        }

        //The deliminators
        private char[] deliminators = { ' ', ';'};

        //Constructor
        public StringParser(string parseString)
        {
            this.stringToParse = parseString;
        }

        //Get the time 
        public long GetTime()
        {
            string[] temp = this.StringToParse.Split(deliminators);

            try
            {
                Int64.TryParse(temp[3], out this.currentTimeStamp);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return this.CurrentTimeStamp;
        }
    }
}
