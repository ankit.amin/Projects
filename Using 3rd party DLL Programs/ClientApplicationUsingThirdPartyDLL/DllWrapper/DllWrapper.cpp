	sv4// DllWrapper.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
#include <atlstr.h>
#include <iostream>
#include "DllWrapper.h"

/*******************************************************************************************************************
//#include "ExportingFromDLLExample.h" //Include this header ONLY when you are calling the imported functions directly. 
//#include <Windows.h>
*******************************************************************************************************************/

using namespace std;


//Function Pointers. 
//note that WINAPIV (__cdecl) is used instead of WINAPI
typedef int(WINAPIV *pFn1)(int x);
typedef int(WINAPIV *pFn2)(int x);
typedef int(WINAPIV *pFn3)(int x);
typedef int(WINAPIV *pFn4)(int x);
typedef double (WINAPIV *pFn5)(double x);

//Declare FunctionX variables of type FunctionPointers
static pFn1 Function1;
static pFn2 Function2;
static pFn3 Function3;
static pFn4 Function4;
static pFn5 Function5;

typedef struct _stFnPtrMap
{
	LPCSTR szName;
	FARPROC *lppProc;
} FUNCTIONPOINTERMAP;

FUNCTIONPOINTERMAP s_FnPtrMap[] =
{
	{ "ExportedFunction1", (FARPROC*)&Function1 },
	{ "ExportedFunction2", (FARPROC*)&Function2 },
	{ "ExportedFunction3", (FARPROC*)&Function3 },
	{ "ExportedFunction4", (FARPROC*)&Function4 },
	{ "ExportedFunction5", (FARPROC*)&Function5 }
};

//Global WrapperObject
DllWrapper *g_DLLWrapper = nullptr;


/*******************************************************************************
//Global get Wrapper Object
//NOTICE: WINAPI is not present in this call.
//VERIFIED WITH DEPENDS.exe
//    -> following function name will not have decorated name i.e. it will show only show "GetWrapperObject"
********************************************************************************/
extern "C" __declspec(dllexport) DllWrapper* GetWrappedObject()
{
	if (g_DLLWrapper == nullptr)
	{
		g_DLLWrapper = new DllWrapper();
	}
	return g_DLLWrapper;
}

/*******************************************************************************
//Relase Global Wrapper Object
********************************************************************************/
extern "C" __declspec(dllexport) void ReleaseWrappedObject()
{
	delete g_DLLWrapper;
	g_DLLWrapper = nullptr;
}

/*******************************************************************************
//Wrapper for LoadDLL() function
********************************************************************************/
extern "C" __declspec(dllexport) HMODULE GetLoadDllHandle()
{
	if (g_DLLWrapper != nullptr)
	{
		return g_DLLWrapper->LoadDll();
	}
	return NULL;
}
/*******************************************************************************
//Wrapper for MapDllFunction();
********************************************************************************/
extern "C" __declspec(dllexport) void CallMappedDllFunctions(HMODULE hModule)
{
	if (g_DLLWrapper != nullptr)
	{
		g_DLLWrapper->MapDllFunctions(hModule);
	}
}
/*******************************************************************************
DllWrapper()
********************************************************************************/
DllWrapper::DllWrapper()
{
	//Call the exporting functions directly 
	CallDllExportedFunctionsDirectly();

	//get the handle of the DLL
	//HMODULE hModule = LoadDll();
	
	//MapDllFunctions(m_hDLL);

}


/*******************************************************************************
~DllWrapper()
********************************************************************************/
DllWrapper::~DllWrapper()
{

}


/*******************************************************************************
LoadDll()
********************************************************************************/
HMODULE DllWrapper::LoadDll()
{
	this->m_csDLLName = _T("ExportingFromDLLExample.dll");
	
	//this->m_hDLL = GetModuleHandle(this->m_csDLLName);
	this->m_hDLL = LoadLibrary(this->m_csDLLName);

	if (this->m_hDLL == NULL)
	{
		cout << "Failed to load the DLL" << endl;
		return NULL;
	}
	
	return this->m_hDLL;
}


/*******************************************************************************

Load-time Dynamic Linking
1. Must include LIBs
2. Must include #include *.h file.

********************************************************************************/
void DllWrapper::CallDllExportedFunctionsDirectly()
{
	/****************************************************************
	to call the Functions from 3rd party DLL DIRECTLY, you will need to do the following:
	
	1. Add ExportingFromDLLExample.lib in Linker->Input->Additional Dependencies
	
	2. Add ..\sdk\libs in Linker->General->Addition Library Directories 
	
	Microsoft calls this "Load-time dynamic linking" https://msdn.microsoft.com/en-us/library/ms810279.aspx 
	This is exmaple of "Load-time dynamic linking". For this reason the DLLWrapper project properties was updated for 
	Linker. ...
		"In load-time dynamic linking, a module makes explicit calls to exported DLL functions. 
		This requires you to link the module with the import library for the DLL. 
		An import library supplies the system with the information needed to load the DLL and 
		locate the exported DLL functions when the application is loaded"

	****************************************************************/

	//Calling the exported functions directlly
	//Option 1:
	// you MUST include "ExportingFromDLLExample.h" in #include "ExportingFromDLLExample.h"
	/*
	std::cout << "\nCalling Exported Functions Directly \n";
	std::cout << "\t::ExportedFunction 1 ->\t" << ::ExportedFunction1(144) << std::endl;
	std::cout << "\t::ExportedFunction 2 ->\t" << ::ExportedFunction2(12) << std::endl;
	std::cout << "\t::ExportedFunction 3 ->\t" << ::ExportedFunction3(13) << std::endl;
	std::cout << "\t::ExportedFunction 4 ->\t" << ::ExportedFunction4(14) << std::endl;
	std::cout << "\t::ExportedFunction 5 ->\t" << ::ExportedFunction5(15) << std::endl;
	*/
}


/*******************************************************************************

1. You DONOT need to put *.lib in Linker. But you DO need to copy ONLY DLL in the same directory as application Executable. 

********************************************************************************/
void DllWrapper::MapDllFunctions(HMODULE hModule)
{
	/*
		This is exmaple of Run-Time Dynamic linking. You dont need to Add anything in Linker properties.
		This eliminates the need for an Import Library.

		Microsoft call this "Run-time Dynamic Linking
		https://msdn.microsoft.com/en-us/library/ms810279.aspx
	*/

	if (hModule)
	{
		for (int iIndex = 0; s_FnPtrMap[iIndex].szName; iIndex++)
		{
			*(s_FnPtrMap[iIndex].lppProc) = GetProcAddress(hModule, s_FnPtrMap[iIndex].szName);

			if (!*((s_FnPtrMap[iIndex]).lppProc))
			{
				LPVOID lpMsgBuf;
				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					GetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR)&lpMsgBuf,
					0,
					NULL
					);

				std::cout << s_FnPtrMap[iIndex].szName << " --> ";
				std::wcout << ((LPTSTR)lpMsgBuf) << std::endl;

				// Free the message buffer.
				LocalFree(lpMsgBuf);
				std::cout << "Failed!!" << std::endl;
				break;
			}
		}
	}

	//Call the exported functions. 
	std::cout << "\nCalling Exported Functions BY using FUNCTION POINTER MAPs" << std::endl;
	std::cout << "\tExportedFunction 1 ->\t" << (*Function1)(100) << std::endl;
	std::cout << "\tExportedFunction 2 ->\t" << (*Function2)(2) << std::endl;
	std::cout << "\tExportedFunction 3 ->\t" << (*Function3)(3) << std::endl;
	std::cout << "\tExportedFunction 4 ->\t" << (*Function4)(4) << std::endl;
	std::cout << "\tExportedFunction 5 ->\t" << (*Function5)(5) << std::endl;

	std::cin.get();

}