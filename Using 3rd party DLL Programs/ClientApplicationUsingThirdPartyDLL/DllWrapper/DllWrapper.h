#pragma once
#include <atlstr.h>


#ifdef API_EXPORT_IMPORT
#define APIExpose __declspec(dllexport)
#else
#define APIExpose __declspec(dllimport)
#endif


class DllWrapper 
{

public:
	DllWrapper();
	virtual ~DllWrapper();

	HMODULE LoadDll();
	void MapDllFunctions(HMODULE hModule);
	void CallDllExportedFunctionsDirectly();
	
	HMODULE m_hDLL;
	CString m_csDLLName;
};








