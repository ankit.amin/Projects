// ClientApplicationUsingThirdPartyDLL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "..\DllWrapper\DllWrapper.h"
#include <iostream>
using namespace std;

/*
#include "ExportingFromDLLExample.h"
#include <iostream>
#include <atlstr.h>
#include <iostream>
using namespace std;

/*************************************************************
1. Add #include <windows.h> 

2. Add the location of the header files in
   "C\C++ Properties"->"Additional Include Directories"

3. Add the location of libraries (*.lib, *.dll, *.exp, *.ilk, *.pdb) in 
   "Linker Properties"->"Additional Library Directories"

4. Add "ExportingFromDLLExample.lib" in
   "Linker Properties"->"Input"->"Additional Dependencies"

5. Add xcopy command to copy libraries in OUTPUT folder (in this case its a Debug folder)
   "Build Envents"->"Post-Build Event"->"Command Line"
   example of the command is: 
   xcopy "$(SolutionDir)sdk\libs\*.*" "$(SolutionDir)Debug\" /Y
/*************************************************************
#ifdef API_EXPORT_IMPORT
#define APIExpose __declspec(dllexport)
#else
#define APIExpose __declspec(dllimport)
#endif


typedef struct _stFnPtrMap
{
	LPCSTR szName;
	FARPROC *lppProc;
} FUNCTIONPOINTERMAP;

//Function Pointers. 
//note that WINAPIV (__cdecl) is used instead of WINAPI
typedef int(WINAPIV *pFn1)(int x);
typedef int(WINAPIV *pFn2)(int x);
typedef int(WINAPIV *pFn3)(int x);
typedef int(WINAPIV *pFn4)(int x);
typedef double (WINAPIV *pFn5)(double x);

//Declare FunctionX variables of type FunctionPointers
static pFn1 Function1; 
static pFn2 Function2;
static pFn3 Function3;
static pFn4 Function4;
static pFn5 Function5;

FUNCTIONPOINTERMAP s_FnPtrMap[] =
{
	{ "ExportedFunction1", (FARPROC*)&Function1, },
	{ "ExportedFunction2", (FARPROC*)&Function2, },
	{ "ExportedFunction3", (FARPROC*)&Function3, },
	{ "ExportedFunction4", (FARPROC*)&Function4, },
	{ "ExportedFunction5", (FARPROC*)&Function5, },
};


//typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
*/

int _tmain(int argc, _TCHAR* argv[])
{
	HMODULE hDll = LoadLibrary(_T("DllWrapper.dll"));

	typedef DllWrapper* (*pFGetWrappedObject)();
	typedef void(*pFReleaseWrappedObject)();
	typedef HMODULE(*pFGetLoadDllHandle)();
	typedef void(*pFCallMappedDllFunctions)(HMODULE);
	
	pFGetWrappedObject pFunction1;
	pFReleaseWrappedObject pFunction2;
	pFGetLoadDllHandle pFunction3;
	pFCallMappedDllFunctions pFunction4; 


	pFunction1 = reinterpret_cast<pFGetWrappedObject>(GetProcAddress(hDll, "GetWrappedObject"));
	if (!pFunction1)
	{
		return 0;
	}

	pFunction2 = reinterpret_cast<pFReleaseWrappedObject>(GetProcAddress(hDll, "ReleaseWrappedObject"));
	if (!pFunction2)
	{
		return 0;
	}
	
	pFunction3 = reinterpret_cast<pFGetLoadDllHandle>(GetProcAddress(hDll, "GetLoadDllHandle"));
	if (!pFunction1)
	{
		return 0;
	}

	pFunction4 = reinterpret_cast<pFCallMappedDllFunctions>(GetProcAddress(hDll, "CallMappedDllFunctions"));
	if (!pFunction2)
	{
		return 0;
	}



	DllWrapper *myObj = pFunction1();
	
	/*
	* Lesson Learned: Cannot call the LoadDLL function directly. 
	* 
	* Need to create export functions in order to work.
	* This is the reason I needed to create pFunction3 and pFunction4. 
	*/
	//HMODULE loadDLL = myObj->LoadDll();
	//myObj->MapDllFunctions(loadDLL);
	
	//Please review the comments above. 
	HMODULE loadDll = pFunction3();
	pFunction4(loadDll);

	std::cin.get();

	return 0;

	/*
	//This is calling GetMyObject function from DLL 
	IMyInterface *myObject = ::GetMyObject(5);
	cout << myObject->Addition(15) << endl;
	cout << myObject->Subtraction(10) << endl;
	myObject->Release();

	//Calling the exported functions directlly
	//Option 1:
	std::cout << "\nCalling Exported Functions Directly \n";
	std::cout << "\t::ExportedFunction 1 ->\t" << ::ExportedFunction1(144) << std::endl;
	std::cout << "\t::ExportedFunction 2 ->\t" << ::ExportedFunction2(12) << std::endl;
	std::cout << "\t::ExportedFunction 3 ->\t" << ::ExportedFunction3(13) << std::endl;
	std::cout << "\t::ExportedFunction 4 ->\t" << ::ExportedFunction4(14) << std::endl;
	std::cout << "\t::ExportedFunction 5 ->\t" << ::ExportedFunction5(15) << std::endl;


	//Loading the DLL and then mapping the Exported functions to FunctionMap from above.
	//Option 2:
	HMODULE hModule;
	CString csModuleName = _T("ExportingFromDLLExample.dll");

	hModule = GetModuleHandle(csModuleName);

	if (hModule)
	{
		for (int iIndex = 0; s_FnPtrMap[iIndex].szName; iIndex++)
		{
			*(s_FnPtrMap[iIndex].lppProc) = GetProcAddress(hModule, s_FnPtrMap[iIndex].szName);

			if (!*((s_FnPtrMap[iIndex]).lppProc))
			{
				LPVOID lpMsgBuf;
				FormatMessage(	FORMAT_MESSAGE_ALLOCATE_BUFFER | 
								FORMAT_MESSAGE_FROM_SYSTEM |
								FORMAT_MESSAGE_IGNORE_INSERTS,
								NULL,
								GetLastError(),
								MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
								(LPTSTR)&lpMsgBuf,
								0,
								NULL
							);

				std::cout << s_FnPtrMap[iIndex].szName << " --> ";
				std::wcout << ((LPTSTR)lpMsgBuf) << std::endl;

				// Free the message buffer.
				LocalFree(lpMsgBuf);
				std::cout << "Failed!!" << std::endl;
				break;
			}
		}
	}
	else
	{
		std::cout << "Failed to get module's handle" << std::endl;
		return 0;
	}
	//Call the exported functions. 
	std::cout << "\nCalling Exported Functions BY using FUNCTION POINTER MAPs" << std::endl;
	std::cout << "\tExportedFunction 1 ->\t" << (*Function1)(144) << std::endl;
	std::cout << "\tExportedFunction 2 ->\t" << (*Function2)(12) << std::endl;
	std::cout << "\tExportedFunction 3 ->\t" << (*Function3)(13) << std::endl;
	std::cout << "\tExportedFunction 4 ->\t" << (*Function4)(14) << std::endl;
	std::cout << "\tExportedFunction 5 ->\t" << (*Function5)(15) << std::endl;
	*/

}