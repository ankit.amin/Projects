// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the API_EXPORT_IMPORT
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// API_EXPORT_IMPORT functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

// **** MUST HAVE the following macro.
#pragma once

#ifdef API_EXPORT_IMPORT
#define APIExpose __declspec(dllexport)
#else
#define APIExpose __declspec(dllimport)
#endif

class IMyInterface 
{
public:
	//must have these in order to clean up properly. 
	IMyInterface() {}
	virtual ~IMyInterface() {} 


	virtual int Addition(int x) = 0;
	virtual int Subtraction(int y) = 0;
	virtual void Release() = 0;
};
//Factory function returns a concrete object of IMyInterface
//This is the only function that is EXPORTed
// This must be declared here in HEADER File otherwise following function will not be exported. 
extern "C" APIExpose IMyInterface*  GetMyObject(int x);

/********************************************************************************************/


/*********************************************************
* ANOTHER WAY OF EXPORTING FUNCTIONS
* -> 2 ways to export functions:
* -> These functions are defined in FactoryMethod.cpp however, we can define these methods anywhere in the DLL.
*
* Option 1: Use Undecorated names. 
*			With this option export must be implemented in following manner
*			I. For DLL, which is exporting functions: 
*				1.	Must USE "extern "C"" to ensure the C++ name mangling is NOT used. i.e., The GetProcAddress function is expecting
*					undecorated function names. 
*				2.	Do NOT add "APIENTRY" OR "WINAPI" in front of Function Name
*					Example:
*							extern "C"
*							{
*								APIExpose int ExportedFunction1(int x);
*								APIExpose int ExportedFunction2(int x);
*								APIExpose int ExportedFunction3(int x);
*								APIExpose int ExportedFunction4(int x);
*							}						
*
*
*			II. For Client Application, which is using the DLL from (I)
*				1.	Function pointers must use "WINAPIV (__cdecl)" before function pointer name
*					Example:
*							//note that WINAPIV (__cdecl) is used instead of WINAPI
*							typedef int(WINAPIV *pFn1)(int x);
*
*
*
* Option 2: Use Decorated names.
*			With this option export must be implemented in following manner
*			I. For DLL, which is exporting functions:
*				1.	Must USE "extern "C"" 
*				2.	You could use "APIENTRY" OR "WINAPI" in front of Function Name if and only if following pragma is used
*					#pragma comment(linker, "/export:functionName=_functionName@number_of_byte_for_arguments")
*					Example: 
*							APIExpose double WINAPI ExportedFunction5(double x);
*							
*							#pragma comment(linker, "/export:ExportedFunction5=_ExportedFunction5@8")
*			
*			II. For Client Application, which is using the DLL from (I)
*				1.	Function pointers must use "WINAPI (__stdcall)" before function pointer name
*					Example:
*							//Function Pointer. Please note the WINAPI (__stdcall) 
*							typedef double(WINAPI *pFn5)(double x);
*/

//Option 1
extern "C"
{

	APIExpose int ExportedFunction1(int x);
	APIExpose int ExportedFunction2(int x);
	APIExpose int ExportedFunction3(int x);
	APIExpose int ExportedFunction4(int x);
}

//Option 2
/*
#if !defined(_WIN64)
#pragma comment(linker, "/export:ExportedFunction5=_ExportedFunction5@8")
#endif  // _WIN64
*/

// The WINAPI
extern "C" APIExpose double ExportedFunction5(double x);
//APIExpose double WINAPI ExportedFunction5(double x);


/* Similar issue encountered by another dev. 

I made a DLL with a function named "render()" and I want to load it dynamically 
to my application, but GetProcAddress cannot find it.Here's the DLL .h:

**********************************************************
// In Header Files 
#ifdef D3D_API_EXPORTS
#define D3D_API_API __declspec(dllexport)
#else
#define D3D_API_API __declspec(dllimport)
#endif

D3D_API_API void render();

**********************************************************
// In CPP Files And here's DLL .cpp:
#include "stdafx.h"
#include "D3D_API.h"
#include <iostream>

D3D_API_API void render()
{
	std::cout << "method called." << std::endl;
}

**********************************************************
// Client Application
Here's the application that tries to use that function:

#include "stdafx.h"
#include <windows.h>
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	HINSTANCE myDLL = LoadLibrary(L"D3D_API.dll");

	if (myDLL == NULL) {
		std::cerr << "Loading of D3D_API.dll failed!" << std::endl;
	}

	typedef void (WINAPI *render_t)();

	render_t render = (render_t)GetProcAddress(myDLL, "render");

	if (render == NULL) {
		std::cerr << "render() not found in .dll!" << std::endl;
	}
	return 0;
}
**********************************************************
// RESOLUTION 
The function you export is treated as a C++ function(because of *.cpp file extension) 
and so C++ name mangling is used to decorate the exported function name.If you use the 
Dependency Walker tool from Microsoft to inspect your created DLL you will see the functions full name.

You can either use that decorated name in your import code or force the compiler to 
export your function in C style, that is, in its undecorated form that your current 
import code expects.

You can tell the compiler to do so by adding extern "C" to your functions signature. 
Something like this:

		extern "C" D3D_API_API void render();

Now your import code should work as expexted.

**********************************************************/

