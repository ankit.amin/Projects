#ifdef EXPORTINGTOCSHARP_EXPORTS
#define APIExpose __declspec (dllexport)
#else 
#define APIExpose __declspec (dllimport)
#endif


class IExportingToCSharp
{
public:
	IExportingToCSharp(void) {}
	virtual ~IExportingToCSharp() {}

	virtual void ExportingFunctionOne(LPCTSTR str) = 0;
};

class ExportingToCSharpClient : public IExportingToCSharp
{
public:
	ExportingToCSharpClient();
	virtual ~ExportingToCSharpClient();

	// Inherited via IExportingToCSharp
	virtual void ExportingFunctionOne(LPCTSTR str) override;
};

extern "C"
{
	APIExpose void function1(int x);
	APIExpose IExportingToCSharp* fnGetExportingClient(void);
	APIExpose void fnReleaseExportingClient(void);
}