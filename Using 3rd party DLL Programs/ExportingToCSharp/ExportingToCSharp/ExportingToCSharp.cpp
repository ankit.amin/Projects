// ExportingToCSharp.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ExportingToCSharp.h"

#include <iostream>
using namespace std;


ExportingToCSharpClient* g_Obj = nullptr;


APIExpose void function1(int x)
{
	cout << "In function 1: " << x << endl;
}

APIExpose IExportingToCSharp* fnGetExportingClient(void)
{
	if (g_Obj == nullptr)
	{
		g_Obj = new ExportingToCSharpClient();
	}
	return g_Obj;
}

APIExpose void fnReleaseExportingClient(void)
{
	if (g_Obj)
	{
		delete g_Obj;
		g_Obj = nullptr;
	}
}

void ExportingToCSharpClient::ExportingFunctionOne(LPCTSTR str)
{
	cout << "In ExportingToCSharpClient: " << str;
}

ExportingToCSharpClient::ExportingToCSharpClient()
{

}

ExportingToCSharpClient::~ExportingToCSharpClient()
{

}