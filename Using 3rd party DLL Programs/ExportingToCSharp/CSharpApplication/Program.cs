﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Allows us to use DLLIMPORT
using System.Runtime.InteropServices;

namespace CSharpApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            ImportExample x = new ImportExample();
            x.CallImportedFunction();
        }
    }

    public class ImportExample
    {
        public ImportExample()
        {
            //TODO:
        }

        ~ImportExample()
        {

        }

        public void CallImportedFunction()
        {
            function1(7);

            IntPtr y = fnGetExportingClient();

            Console.WriteLine("Pointer:  {0}",y.ToString());
        }

        [DllImport ("ExportingToCSharp.dll", CallingConvention= CallingConvention.Cdecl)]
        public static extern void function1(int x);
        
        [DllImport("ExportingToCSharp.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr fnGetExportingClient();

        [DllImport("ExportingToCSharp.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void fnReleaseExportingClient();
        
    }
}
